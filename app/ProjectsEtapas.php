<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsEtapas extends Model
{
    protected $table = 'projects_etapas';
    public  $timestamps = false;
}

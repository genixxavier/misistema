<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorasExtras extends Model
{
    protected $table = 'horas_extras';
    public $timestamps = false;
    
}

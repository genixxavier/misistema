<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encargado extends Model
{
   	protected $table = 'cliente_user';
    protected $fillable = ['user_id','cliente_id'];

}

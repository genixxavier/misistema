<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idreunion extends Model
{
    //
    protected $table = 'idreunion';

    protected $primary = 'id';
    
    protected $fillable = [
    	'nombre','estado','descripcion','fecha_inicio','horas','cliente_id','user_id','user_create'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function user_create()
    {
        return $this->belongsTo('App\User');
    }

     public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }
    public static function get_temas($id)
    {
        $temas = Tema::where('id_contact_report','=',$id)
                                ->get();
        return $temas;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorasTotales extends Model
{
    protected $table = 'horas_totales';
    public  $timestamps = false;
}

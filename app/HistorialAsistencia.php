<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistorialAsistencia extends Model
{
    protected $table  = 'historial_asistencia';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dias extends Model
{
    //
    protected $table = 'dias';

    protected $primary = 'id';
    
    protected $fillable = [
    	'descripcion'
    ];
}

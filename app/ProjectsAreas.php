<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsAreas extends Model
{
    protected $table = 'projects_areas';
    public  $timestamps = false;
}

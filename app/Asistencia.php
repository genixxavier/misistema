<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asistencia extends Model
{
    //
    protected $table = 'asistencia';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'fecha','hora_inicio','hora_fin','user_id','estado','descripcion','extras','deuda'
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public static function verificar_asistencia($fecha,$id)
    {
        // $user = \Auth::user()->id;
        $user =$id;
        $ff = date('Y-m-d');
        if($fecha > $ff){
            return "";
        }
        $falto = Asistencia::where('user_id','=',$user)->where('fecha',$fecha)->first();
        // if(count($falto) == 0){
        if(empty($falto)){
            return "disable_table alert_falto";
        }
        $asistencia = Asistencia::where('user_id','=',$user)
            ->where('fecha',$fecha)
            ->whereIn('estado', ['', 'J', 'V'])
            ->first();
        // if(count($asistencia)>0){
        if(!empty($asistencia)){
            if($asistencia->estado == "V"){
                return "disable_table alert_vaciones";
            }
            else{
                return "disable_table alert_justificacion";
            }
        }
        else{
            return "";
        }
    }
}

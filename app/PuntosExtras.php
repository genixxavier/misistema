<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PuntosExtras extends Model
{
    //
    protected $table = 'puntos';

    protected $primary = 'id';
    
    protected $fillable = [
    	'puntaje','motivo','area_id','mes','anio'
    ];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CotizacionStaff extends Model
{
  //
  protected $table = 'cotizacion_staff';
  public $timestamps = false;
}

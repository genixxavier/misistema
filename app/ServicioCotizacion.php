<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicioCotizacion extends Model
{
    protected $table = 'servicios_cotizacion';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'title','description','nota','precio','descuento','total','id_cotizacion','estado'
    ];
}

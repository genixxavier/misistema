<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    //
    protected $table = 'reuniones';

    protected $primary = 'id';
    
    protected $fillable = [
    	'nombre','estado','descripcion','fecha_inicio','horas','cliente_id','user_id','user_create','id_reunion'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function user_create()
    {
        return $this->belongsTo('App\User');
    }

    public function idreunion()
    {
        return $this->belongsTo('App\Idreunion');
    }

     public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentFiles extends Model
{
    protected $table = 'payment_files';
    public  $timestamps = false;
}

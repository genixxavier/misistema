<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizador extends Model
{
  //
  protected $table = 'cotizador';
  public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'area_id','codigo','estado','tribu_id','fecha_ingreso','img'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    function socialProviders()
    {
        return $this->hasMany(SocialProvider::class);
    }

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function clientes()
    {
        return $this->belongsToMany('App\Cliente');
    }

    public function user_id()
    {
        return $this->belongsToMany('App\Asistencia');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentImg extends Model
{
    protected $table = 'payment_img';
    public  $timestamps = false;
}

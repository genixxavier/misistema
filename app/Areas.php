<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
    //
    protected $table = 'areas';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'name'
    ];

    public static function datos($id,$mes){
    	return PuntosExtras::where('area_id',$id)->where('mes',$mes)->get();
    }

}

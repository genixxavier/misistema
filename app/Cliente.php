<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
   	protected $table = 'clientes';

    protected $primary = 'id';

    protected $fillable = [
    	'nombre','prioridad','color'
    ];

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Colaborador extends Model
{
   	protected $table = 'colaboradores';

    protected $primary = 'id';
    
    protected $fillable = [
    	'nombre','apellido','estado','monto'
    ];

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }
}

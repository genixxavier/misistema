<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskBack extends Model
{
  protected $table = 'tasks_back';

  protected $fillable = [
      'title', 'description','start', 'end', 'id_cliente', 'cliente', 'duration', 'state', 'color','user_id','area_id'
  ];
}

<?php

namespace App\Http\Controllers;

use App\Tema;
use function GuzzleHttp\Psr7\try_fopen;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCotizacionesRequest;
use App\Idreunion;
use App\Cliente;
use App\Cotizacion;
use App\ContactReport;
use App\Reunion;
use App\User;
use App\ServicioCotizacion;
use App\{
    ClienteUser, ObsCotizacion, StatusCotizacion, CotizacionStaff, Cotizador
};
class CotizacionesController extends Controller
{

   
    public function index(Request $request)
    {
        //$list_cotizacion = Cotizacion::orderby('id','desc')->paginate(8);
        if (\Auth::user()->id == 10) {
            return redirect()->route('status_cotizacion');
        }
        $clientes = Cotizacion::join('clientes','clientes.id','=','cotizaciones.id_cliente')
          ->select('clientes.nombre','clientes.id')
          ->distinct()
          ->orderby('clientes.nombre','asc')
          ->get();

        $cliente = $request->query('cliente');
        $nombre = $request->query('nombre');
        if ($request->query('cliente') && !$request->query('nombre')) {
          $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
            ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
            ->join('clientes','clientes.id','=','cotizaciones.id_cliente')
            ->where('status_cotizacion.status','!=',6)
            ->where('clientes.nombre','LIKE','%'.$cliente.'%')
            ->orderby('id','desc')->paginate(40);


          return view ('frontend.cotizaciones',compact('cotizacion','clientes'));

        } else if ($request->query('nombre') && $request->query('cliente') == '0') {
          $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
            ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
            ->where('status_cotizacion.status','!=',6)
            ->where('c_nombre_cotizacion','LIKE','%'.$nombre.'%')
            ->orderby('id','desc')->paginate(40);
          return view ('frontend.cotizaciones',compact('cotizacion','clientes'));

        } else if ($request->query('nombre') && !$request->query('cliente')) {
          $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
            ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
            ->where('status_cotizacion.status','!=',6)
            ->where('c_nombre_cotizacion','LIKE','%'.$nombre.'%')
            ->orderby('id','desc')->paginate(40);
          return view ('frontend.cotizaciones',compact('cotizacion','clientes'));

        } else if ($request->query('nombre') && $request->query('cliente')) {

          $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
            ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
            ->where('status_cotizacion.status','!=',6)
            ->join('clientes','clientes.id','=','cotizaciones.id_cliente')
            ->where('clientes.nombre','LIKE','%'.$cliente.'%')
            ->where('c_nombre_cotizacion','LIKE','%'.$nombre.'%')
            ->orderby('id','desc')->paginate(40);
          return view ('frontend.cotizaciones',compact('cotizacion','clientes'));

        }



         $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
        ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
        ->where('status_cotizacion.status','!=',6)

        ->orderby('id','desc')->paginate(20);

      return view ('frontend.cotizaciones',compact('cotizacion','clientes'));



    }

   
    public function create()
    {
        
        $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->select('clientes.id as id_cliente','contact_reports.id as id','contact_reports.cr_tema as tema','clientes.nombre as cliente','contact_reports.cr_fecha_insert as fecha')->orderby('contact_reports.id','desc')->get();

        $last_cotizacion =  Cotizacion::orderby('id','DESC')->take(1)->get();
        $last_cotizacion = collect($last_cotizacion);
        $cotizacion_num = $last_cotizacion->first();
        $num = Cotizacion::count() ;
        if ($num == 0 || null ) {
            $num = 466;
        }
        else {
        $num = Cotizacion::where('id','>',0)->max('c_num');
        $num = $num + 1;    
        }
        $staff = Cotizador::all();
        // $num = $cotizacion_num == null ? 1 : $cotizacion_num->c_num + 1 ;
        return view ('frontend.crear_cotizacion' ,compact('num','contact','staff'));
    }


    public function create_2 () {
        $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->select('contact_reports.id as id','contact_reports.cr_tema as tema','clientes.nombre as cliente','contact_reports.cr_fecha_insert as fecha')->orderby('contact_reports.id','desc')->get();
        //$clients =  Cliente::where('activo','=',1)->orderby("type","desc")->orderby("nombre")->get();
        $last_cotizacion =  Cotizacion::orderby('id','DESC')->take(1)->get();
        $last_cotizacion = collect($last_cotizacion);
        $cotizacion_num = $last_cotizacion->first();
        $num = Cotizacion::count() ;
        if ($num == 0 || null ) {
            $num = 466;
        }
        else {
        $num = Cotizacion::where('id','>=',0)->max('c_num');
        $num = $num + 1;    
        }

        /*if(\Auth::user()->tipo == 'jefe'){

            $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
        }
        else if(\Auth::user()->tipo == 'gerente'){
*/
            $clientes = Cliente::select('clientes.id','clientes.nombre','clientes.type','clientes.id as cliente_id','clientes.ruc','clientes.razon_social')->where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();

        //}
      /*  else{
            $id = \Auth::id();
            $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
            ->select('clientes.id','clientes.nombre','clientes.type','cliente_user.cliente_id','clientes.ruc','clientes.razon_social')
                ->where('cliente_user.user_id','=',$id)
                ->where('clientes.activo','=',1)
                ->orderby("clientes.nombre")
                ->get();

        }*/



        // $num = $cotizacion_num == null ? 1 : $cotizacion_num->c_num + 1 ;
        $staff = Cotizador::all();
        return view ('frontend.crear_cotizacion_c' ,compact('num','contact','clientes','staff'));
    }
  
    public function store(Request $request)
    {
       
        try {

            $coMax = Cotizacion::max('c_num');
            DB::beginTransaction();
                $cotizacion = new Cotizacion;
                $fecha = explode('-',$request->fecha);
                $fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
                $cotizacion->c_fecha =$fecha;
                $cotizacion->c_tipo_moneda = $request->moneda;
                $cotizacion->contact_reports_id = $request->id_cr;
                $cotizacion->c_contacto = $request->nombre ;
                $cotizacion->c_extras = 0;
                $cotizacion->c_nombre_cotizacion = $request->nombre_cotizacion;
                $cotizacion->id_cliente = $request->id_cliente;
                $cotizacion->c_num = $coMax + 1;
                $cotizacion->c_boolean_inversion = isset($request->inversion_total) ? $request->inversion_total : 0;
                $cotizacion->boolean_igv = isset($request->igv) ? $request->igv : 0;
                //$cotizacion->c_ruc = $request->ruc;
                $cotizacion->c_tipo_comprobante = $request->tipo_combrobante;
                //$cotizacion->c_razon_social = $request->razon_social;
                $cotizacion->id_cliente = $request->id_cliente;
                $cotizacion->c_consideraciones = $request->consideraciones_servicios_html;
                $cotizacion->c_acciones = $request->acciones_no_incluidas_html;
                $cotizacion->c_type_file = $request->adjunto;
                $cotizacion->c_user_create = \Auth::user()->id;
                $cotizacion->c_id_version = $cotizacion->id;
                //$cotizacion->c_marca = $request->input('cliente');
                if ($request->adjunto == 1) {
                    $file = $request->file('me_files');
                    $filename = 'mi_'.date('Y-m-d').'_'.rand().'.'.$file->getClientOriginalExtension();
                    $destination = public_path().'/files/'.$filename;
                    $flag = move_uploaded_file($file,$destination);
                    $cotizacion->c_file = $filename;
                }
                else if ($request->adjunto == 2 ) {
                    $cotizacion->c_file = $request->link_google;
                }
                else {
                    $cotizacion->c_file = null;
                }



                $cotizacion->c_version = 1;
                $cotizacion->save();
                $cotizacion->c_id_version = $cotizacion->id;
                $cotizacion->update();
                foreach ($request->id_servicios as $id) {
                    $servicio = ServicioCotizacion::find($id);
                    $servicio->id_cotizacion = $cotizacion->id;
                    $servicio->update();
                }

                $status = new StatusCotizacion;
                $status->id_cotizacion = $cotizacion->id;
                $status->status = 5;
                if(isset($request->listStaffCotizador) && !empty($request->listStaffCotizador)) {

                  for ($i = 0; $i < count($request->listStaffCotizador);$i++) {

                    $staffCotizador = CotizacionStaff::find($request->listStaffCotizador[$i]);
                    $staffCotizador->cotizacion_id = $cotizacion->id;

                    $staffCotizador->save();
                  }
                }
                $status->save();
                $message = [
                                'status' => 200,
                                'id' => $cotizacion->id,
                                'message' => 'Se registro correctamente'
                            ];
            
            DB::commit();
            return $message;
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'message' => 'No se pudo registrar', 
                'message_error' => $e->getMessage(), 
                'line' => $e->getLine(),
                'code'  => $e->getCode()
            ];
        }
            
    }

    public function store_2(Request $request)
    {
        try {

            $coMax = Cotizacion::max('c_num');
            DB::beginTransaction();
                $cotizacion = new Cotizacion;
                $fecha = explode('-',$request->fecha);
                $fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
                $cotizacion->c_fecha =$fecha;
                $cotizacion->c_extras = 0;
                $cotizacion->c_contacto = $request->nombre ;
                $cotizacion->c_nombre_cotizacion = $request->nombre_cotizacion;
                $cotizacion->c_num = $coMax  + 1;
                $cotizacion->c_boolean_inversion = isset($request->inversion_total) ? $request->inversion_total : 0;
                $cotizacion->boolean_igv = isset($request->igv) ? $request->igv : 0;
                $cotizacion->c_tipo_moneda = $request->moneda;
                $cotizacion->c_tipo_comprobante = $request->tipo_combrobante;
                if ($request->cliente == 'otros') {
                    $cn = new Cliente ;
                    $cn->nombre = $request->nombre_comercial;
                    $cn->razon_social = $request->razon_social;
                    $cn->ruc = $request->ruc;
                    $cn->type = $request->tipo_cliente;
                    $cn->activo = 1;
                    $cn->save();
                    $cotizacion->id_cliente = $cn->id;
                    $cliente_user = new ClienteUser();
                    $cliente_user->cliente_id = $cn->id;
                    $cliente_user->user_id = \Auth::user()->id;
                    $cliente_user->save();
                }
                else {
                    $cotizacion->id_cliente = $request->cliente;
                }
                //$cotizacion->c_ruc = $request->ruc;
                //$cotizacion->c_razon_social = $request->razon_social;
                $cotizacion->c_consideraciones = $request->consideraciones_servicios_html;
                $cotizacion->c_acciones = $request->acciones_no_incluidas_html;
                $cotizacion->c_type_file = $request->adjunto;
                $cotizacion->c_user_create = \Auth::user()->id;
                //$cotizacion->c_marca = $request->razon_social;
                $cotizacion->c_id_version = $cotizacion->id;
                if ($request->adjunto == 1) {
                    $file = $request->file('me_files');
                    $filename = 'mi_'.date('Y-m-d').'_'.rand().'.'.$file->getClientOriginalExtension();
                    $destination = public_path().'/files/'.$filename;
                    $flag = move_uploaded_file($file,$destination);
                    $cotizacion->c_file = $filename;
                }
                else if ($request->adjunto == 2 ) {
                    $cotizacion->c_file = $request->link_google;
                }
                else {
                    $cotizacion->c_file = null;
                }
                
                $cotizacion->c_version = 1;
                $cotizacion->save();
                $cotizacion->c_id_version = $cotizacion->id;
                $cotizacion->update();
                foreach ($request->id_servicios as $id) {
                    $servicio = ServicioCotizacion::find($id);
                    $servicio->id_cotizacion = $cotizacion->id;
                    $servicio->update();
                }

                $status = new StatusCotizacion;
                $status->id_cotizacion = $cotizacion->id;
                $status->status = 5;
                if(isset($request->listStaffCotizador) && !empty($request->listStaffCotizador)) {
                 for ($i = 0; $i < count($request->listStaffCotizador);$i++) {
                  $staffCotizador = CotizacionStaff::find($request->listStaffCotizador[$i]);
                  $staffCotizador->cotizacion_id = $cotizacion->id;
                  $staffCotizador->save();
                 }
                }
                $status->save();
                $message = [
                                'status' => 200,
                                'id' => $cotizacion->id,
                                'message' => 'Se registro correctamente'
                            ];
            DB::commit();
            return $message;
          
            
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'message' => 'No se pudo registrar', 
                'message_error' => $e->getMessage(), 
                'line' => $e->getLine(),
                'code'  => $e->getCode()
            ];
        }
        
    }

    public function show($id)
    {

    }

    public function edit( $id)
    {
        $st = StatusCotizacion::where('id_cotizacion', $id)->get();
        $flag = $st[0]->status == 4 ? 1 : 0 ;

       $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->select('clientes.id as id_cliente','contact_reports.id as id','contact_reports.cr_tema as tema','clientes.nombre as cliente','contact_reports.cr_fecha_insert as fecha')->orderby('contact_reports.id','desc')->get();

       $cotizacion = Cotizacion::find($id);
       $service_cotizacion = ServicioCotizacion::where('id_cotizacion' , '=' , $id)->get();
       $title_tema = '';
       $contact_report = '';
       if ($cotizacion->contact_reports_id != 0 || $cotizacion->contact_reports_id != null) {
        $contact_report = ContactReport::find($cotizacion->contact_reports_id);
        $temas = Tema::where('id_contact_report' , '=' , $contact_report->id)->get();
         $title_tema = '';
         foreach ($temas as $tema ) {
             $title_tema .= $tema->title.',';
         }
         $title_tema = substr($title_tema,0,-1);
       }
        $cliente = Cliente::find($cotizacion->id_cliente);
        //return view ('frontend.crear_cotizacion' ,compact('num','contact'));
        $fecha = $cotizacion->c_fecha;
        $fecha = explode('-',$fecha);
        $fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];

        $clientes = Cliente::select('clientes.id','clientes.nombre','clientes.type','clientes.id as cliente_id','clientes.ruc','clientes.razon_social')->where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();


        /*        if(\Auth::user()->tipo == 'jefe'){

                    $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
                }
                else if(\Auth::user()->tipo == 'gerente'){

                    $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
                }
                else{
                    $id = \Auth::id();
                    $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
                    ->select('clientes.id','clientes.nombre','clientes.type','cliente_user.cliente_id','clientes.ruc','clientes.razon_social')
                        ->where('cliente_user.user_id','=',$id)
                        ->where('clientes.activo','=',1)
                        ->orderby("clientes.nombre")
                        ->get();

                }
        */
        $staffCotizacion =  CotizacionStaff::join('cotizador','cotizador.id','=','cotizacion_staff.cotizador_id')->select('cotizacion_staff.id','cotizacion_staff.cotizacion_id',
        'cotizacion_staff.cotizador_id','cotizacion_staff.horas','cotizacion_staff.total','cotizador.persona')
          ->where('cotizacion_id',$cotizacion->id)->get();
        $sumTotalStaffCotizacion = DB::table('cotizacion_staff')->where('cotizacion_id',$cotizacion->id)->sum('total');

      $staff = Cotizador::all();
        return view ('frontend.edit_cotizacion', compact('flag','clientes','contact','cotizacion','service_cotizacion','contact_report' , 'cliente' ,'title_tema','fecha','staffCotizacion','staff','sumTotalStaffCotizacion'));

    }
    public function edit_2( $id)
    {
    
        try {
            $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->select('clientes.id as id_cliente','contact_reports.id as id','contact_reports.cr_tema as tema','clientes.nombre as cliente','contact_reports.cr_fecha_insert as fecha')->orderby('contact_reports.id','desc')->get();

       $co = Cotizacion::find($id);
    
       try {
        DB::beginTransaction();
                $sc = ServicioCotizacion::where('id_cotizacion' , '=' , $id)->get();
                $cotizacion = new Cotizacion;
                $cotizacion->c_fecha = $co->c_fecha;
                $cotizacion->c_contacto = $co->c_contacto;
                $cotizacion->contact_reports_id = $co->contact_reports_id;
                $cotizacion->c_extras = $co->c_extras;
                //$cotizacion->c_ruc = $co->c_ruc;
                //$cotizacion->c_razon_social = $co->c_razon_social;
                $cotizacion->c_file = $co->c_file;
                $cotizacion->id_cliente = $co->id_cliente;
                $cotizacion->c_num = $co->c_num;
                $cotizacion->c_nombre_cotizacion = $co->c_nombre_cotizacion;
                $cotizacion->c_tipo_moneda = $co->c_tipo_moneda;
                $cotizacion->c_version = $co->c_version + 1;
                $cotizacion->c_boolean_inversion = $co->c_boolean_inversion;
                $cotizacion->boolean_igv = $co->boolean_igv;
                $cotizacion->c_consideraciones = $co->c_consideraciones;
                $cotizacion->c_acciones = $co->c_acciones;
                $cotizacion->c_tipo_comprobante = $co->c_tipo_comprobante;
                $cotizacion->c_type_file = $co->c_type_file;
                $cotizacion->c_user_create = $co->c_user_create;
                $cotizacion->c_id_version = $co->c_id_version;
                $cotizacion->c_marca = $co->c_marca;
                
                $cotizacion->save();

                if (count($sc) > 0) {
                    foreach ($sc as  $value) {
                        $new_sc = new ServicioCotizacion;
                        $new_sc->title = $value->title;
                        $new_sc->description = $value->description;
                        $new_sc->nota = $value->nota;
                        $new_sc->precio = $value->precio;
                        $new_sc->descuento = $value->descuento;
                        $new_sc->total = $value->total;
                        $new_sc->id_cotizacion = $cotizacion->id;
                        $new_sc->estado = 5 ;
                        $new_sc->save();
                    }
                }
                else {
                    $new_sc = new ServicioCotizacion;
                    $new_sc->title = $sc->title;
                    $new_sc->description = $sc->description;
                    $new_sc->nota = $sc->nota;
                    $new_sc->precio = $sc->precio;
                    $new_sc->descuento = $sc->descuento;
                    $new_sc->total = $sc->total;
                    $new_sc->id_cotizacion = $cotizacion->id;
                    $new_sc->estado = 5;
                    $new_sc->save();
                }


                $status = new StatusCotizacion;
                $status->id_cotizacion = $cotizacion->id;
                $status->status = 5;
                $status->save();
                $st = StatusCotizacion::where('id_cotizacion','=',$co->id)->get();
                $s= StatusCotizacion::find($st[0]->id);
                $s->status = 6;
                $s->save();

               $insertStaffCotizacion = CotizacionStaff::where('cotizacion_id','=',$co->id)->get();
               if (!empty($insertStaffCotizacion)) {

                 foreach ($insertStaffCotizacion as $insert ) {
                   $newStaff = new CotizacionStaff();
                   $newStaff->cotizacion_id = $cotizacion->id;
                   $newStaff->cotizador_id = $insert->cotizador_id;
                   $newStaff->horas = $insert->horas;
                   $newStaff->total = $insert->total;
                   $newStaff->save();
                 }

               }

        DB::commit();

       } catch (\Exception $e) {
           DB::rollBack();
           return [
            'message' => $e->getMessage(),
            'code' => $e->getCode(), 
            'line' => $e->getLine()
        ];
       }
       $title_tema = '';
      
        $service_cotizacion = ServicioCotizacion::where('id_cotizacion' , '=' , $cotizacion->id)->get();
        $contact_report = '';
       if ($cotizacion->contact_reports_id  != 0 ) {
        $contact_report = ContactReport::find($cotizacion->contact_reports_id);
        $temas = Tema::where('id_contact_report' , '=' , $contact_report->id)->get();
        
         foreach ($temas as $tema ) {
             $title_tema .= $tema->title.',';
         }
         $title_tema = substr($title_tema,0,-1);
 


       }
        $cliente = Cliente::find($cotizacion->id_cliente);
        $fecha = $cotizacion->c_fecha;
        $fecha = explode('-',$fecha);
        $fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];


         /*   if(\Auth::user()->tipo == 'jefe'){

                $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
            }
            else if(\Auth::user()->tipo == 'gerente'){

                $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
            }
            else{
                $id = \Auth::id();
                $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
                ->select('clientes.id','clientes.nombre','clientes.type','cliente_user.cliente_id','clientes.ruc','clientes.razon_social')
                    ->where('cliente_user.user_id','=',$id)
                    ->where('clientes.activo','=',1)
                    ->orderby("clientes.nombre")
                    ->get();

            }*/
            $clientes = Cliente::select('clientes.id','clientes.nombre','clientes.type','clientes.id as cliente_id','clientes.ruc','clientes.razon_social')->where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();

          $staffCotizacion =  CotizacionStaff::join('cotizador','cotizador.id','=','cotizacion_staff.cotizador_id')->select('cotizacion_staff.id','cotizacion_staff.cotizacion_id',
            'cotizacion_staff.cotizador_id','cotizacion_staff.horas','cotizacion_staff.total','cotizador.persona')
            ->where('cotizacion_id',$cotizacion->id)->get();
          $sumTotalStaffCotizacion = DB::table('cotizacion_staff')->where('cotizacion_id',$cotizacion->id)->sum('total');

          $staff = Cotizador::all();

            return view ('frontend.edit_cotizacion_2', compact('clientes','contact','cotizacion','service_cotizacion','contact_report' , 'cliente' ,'title_tema','fecha','co','staffCotizacion','staff','sumTotalStaffCotizacion'));
        } catch (\Exception $e) {
           return [
               'message' => $e->getMessage(),
               'code' => $e->getCode(), 
               'line' => $e->getLine()
           ];
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        function arrayToText ($data) {
            $text = '';
            foreach ($data as $item) {
                $text = $text .$item .'.';
            };

            return $text;
        }

        $cotizacion_id_report = Cotizacion::find($id);

        $cotizacion = new Cotizacion;
        $nameServices = arrayToText($request->nameService);

        $cotizacion->c_services_name = $nameServices ;

        $subTotal = arrayToText($request->subTotal);
        $cotizacion->c_sub_total = $subTotal ;

        $descuento = arrayToText($request->descuento);
        $cotizacion->c_descuento = $descuento ;

        $descripcion = arrayToText($request->description);
        $cotizacion->c_descripcion = $descripcion ;
        $cotizacion->c_nombre = $request->nombre;
        $cotizacion->contact_reports_id = $cotizacion_id_report->contact_reports_id;
        $date_today = date('Y-m-d G-i-s');

        $cotizacion->c_version = $cotizacion_id_report->c_version + 1;
        $cotizacion->c_fecha = $date_today;
        // $cotizacion->c_estado = $request->estado;
        $total = Cotizacion::all();
        $cotizacion->c_num = count($total) + 1 ;
        // Calcular el total
        $sumTotal = 0;
        for ($i = 0 ; $i < count($request->subTotal); $i++)  {
            $sumTotal = $sumTotal + ($request->subTotal[$i] - $request->descuento[$i]);

        }
        $igv  = round (($sumTotal * 0.18),2);
        $total_neto = $sumTotal + $igv;

        $cotizacion->c_total = $total_neto;

        $cotizacion->c_serie = $request->serie;
        $cotizacion->c_numero = $request->numero;
        $fecha = date('Y-m-d');
        $date_vencimiento = strtotime ( '+1 month' , strtotime ( $fecha ) ) ;
        $date_vencimiento = date ( 'Y-m-d' , $date_vencimiento );
        $cotizacion->c_fecha_vencimiento = $date_vencimiento;

        $cotizacion->save();

        return redirect()->route('cotizaciones_index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function viewContactReports ()
    {
        $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->select('contact_reports.id','idreunion.nombre','clientes.nombre as cliente','contact_reports.cr_fecha_insert')->orderby('contact_reports.id','desc')->get();
        return $contact;
    }

    public function cotizar ($id) {
        $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')
            ->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')
            ->select('contact_reports.id as id','contact_reports.cr_tema as tema','clientes.nombre as                              cliente','contact_reports.cr_fecha_insert as fecha')
            ->orderby('contact_reports.id','desc')->get();
        $creport = ContactReport::find($id);
        // $id = $creport->id ;
        //  $tema = $creport->cr_tema;
        //  $flag = 'flag';
        // $true = 'as';
        //    dd($tema);

        //Find Contact Report
        $contact_report = ContactReport::find($id);
        //Find que reunion le pertenece el contact report
        $id_reunion = Idreunion::find($contact_report->reuniones_id);
        //Find Cliente
        $cliente = Cliente::find($id_reunion->cliente_id);
        // Find los temas del contact_report
        $temas = Tema::where('id_contact_report','=',$id)->get();
        $title_tema = '';
        foreach ($temas as $tema ) {
            $title_tema .= $tema->title.',';
        }
        $title_tema = substr($title_tema,0,-1);

        $last_cotizacion =  Cotizacion::orderby('id','DESC')->take(1)->get();
        $last_cotizacion = collect($last_cotizacion);
        $cotizacion_num = $last_cotizacion->first();

        $num = $cotizacion_num == null ? 1 : $cotizacion_num->c_num + 1 ;
        //Flag para mostrar estas variables en el frontend
        $flag = 1 ;
        return view ('frontend.crear_cotizacion' ,
            compact('contact_report','cliente','flag' , 'contact' ,'title_tema',
                'num'));
    }


    public function ver_pdf ($id) {

        $cotizacion = Cotizacion::find($id);
        $cliente = '';
        if ($cotizacion->contact_reports_id != 0 || $cotizacion->contact_reports_id != null) {
            $contact_report = ContactReport::find($cotizacion->contact_reports_id);

            $reunion = Idreunion::find($contact_report->reuniones_id);
            $cliente  = Cliente::find($reunion->cliente_id);
        }
        /*TODO hacer el reporte de cotizaciones */
        $servicios = ServicioCotizacion::where('id_cotizacion','=',$cotizacion->id)->get();
        $services_name = '';
        $precio_total = 0;
        if (count($servicios) > 0 ) {
            foreach ($servicios as $servicio) {
                $precio_total += $servicio->total;
                $services_name.=' '.$servicio->title;
            }
        }
        else {
            $precio_total = $servicios->total;
        }
        $precio_total= number_format($precio_total,2,'.',',');
        $fecha = $cotizacion->c_fecha;
        $fecha = explode(' ',$fecha);
        $fecha_new = explode('-' , $fecha[0]);

        $year = $fecha_new[0];
        $mes = '';
        $day = $fecha_new[2];
        switch ($fecha_new[1]) {
            case '01':
                $mes = 'Enero';
                break;
            case '02':
                $mes = 'Febrero';
                break;
            case '03':
                $mes = 'Marzo';
                break;
            case '04':
                $mes = 'Abril';
                break;
            case '05':
                $mes = 'Mayo';
                break;
            case '06':
                $mes = 'Junio';
                break;
            case '07':
                $mes = 'Julio';
                break;
            case '08':
                $mes = 'Agosto';
                break;
            case '09':
                $mes = 'Septiembre';
                break;
            case '10':
                $mes = 'Octubre';
                break;
            case '11':
                $mes = 'Noviembre';
                break;
            case '12':
                $mes = 'Diciembre';
                break;
        }
      
        $cliente_cotizacion = Cliente::find($cotizacion->id_cliente);


        $name_file = 'Cotización N0000'.$cotizacion->c_num.' - '.$cotizacion->c_nombre_cotizacion.'.pdf';


        //$name_file = 'MEDIAIMPACT-'.strtoupper($cliente_cotizacion->nombre).'-'.'N0000'.$cotizacion->c_num.'.pdf';
        $pdf = \PDF::loadView('pdf.cotizaciones_new',[
                'cliente_cotizacion' => $cliente_cotizacion,
                'cotizacion' => $cotizacion,
                'servicios' => $servicios,
                'day' => $day,
                'month' => $mes,
                'year' => $year,
                // 'cliente' => $cliente->nombre,
                'cliente' => $cliente,
                'total' => $precio_total
            ]
        )->setOptions([ 'defaultFont' => 'arial']);
        return $pdf->stream($name_file);

    }


    public function add_cotizacion(Request $request){
        //dd($request->all());
        if ($request->id_servicio != '' || $request->id_servicio != null) {
            $add = ServicioCotizacion::find($request->id_servicio);
        }
        else {
            $add = new ServicioCotizacion;
        }

        $add->title = $request->title;
        $add->description = $request->description;
        $add->nota = $request->nota;
        $add->precio = $request->price;
        $add->descuento = $request->discount;
        $add->total = $request->total;
        // $add->id_cotizacion = $request->id;
        $add->estado = 1;
        $add->save();
        $data = $request;
        // $servicio = ServicioCotizacion::find($add->id);

        return view('frontend.view_servicio', compact('data' ,'add'));

        // $data = TemaAcuerdo::find($request->id);
        // return $data;


        //return view('frontend.view_servicio');

    }
    public function delete_service (Request $request) {
        try  {
            $service = ServicioCotizacion::find($request->id);
            $service->delete();
            $data = [
                'status' => 200,
                'message' => 'Servicio eliminado'
            ];
            return $data;
        }
        catch (\Exception $e ) {
            $data = [
                'status' => 400,
                'message' => 'No se pudo eliminar el servicio'
            ];
            return $data;

        }
    }
    public function show_service_cotizacion (Request $request) {
        $service = ServicioCotizacion::find($request->id);
        return $service;
    }
    public function update_service_cotizacion (Request $request) {
       try {
           $service_cotizacion = ServicioCotizacion::find($request->id);
           $service_cotizacion->title = $request->service;
           $service_cotizacion->description = $request->description;
        //    $service_cotizacion->nota = $request->nota;
           $service_cotizacion->precio = $request->precio;
           $service_cotizacion->descuento = $request->descuento;
           $service_cotizacion->total = $request->total;

           $service_cotizacion->update();
           $data = [
               'status' => 200,
               'message' => 'Se actualizo correctamente'
           ];
           return view('frontend.show_servicio' , compact('service_cotizacion'));
       }
       catch (\Exception $e ) {

       }
    }
    public function save_cotizacion_in_edit(Request $request) {

        try {
            $service_cotizacion = new ServicioCotizacion;
            $service_cotizacion->title = $request->service;
            $service_cotizacion->description = $request->description;
            // $service_cotizacion->nota = $request->nota;
            $service_cotizacion->precio = $request->precio;
            $service_cotizacion->descuento = $request->descuento;
            $service_cotizacion->total = $request->total;
            $service_cotizacion->id_cotizacion = $request->id;
            $service_cotizacion->estado = 1;
            $service_cotizacion->save();
            $data = [
                'status' => 200,
                'message' => 'Se actualizo correctamente'
            ];
            return view('frontend.show_servicio' , compact('service_cotizacion'));
        }
        catch (\Exception $e ) {

        }
    }
    public function update_cotizacion (Request $request) {

        $cotizacion = Cotizacion::find($request->id_cotizacion);

        $cotizacion->contact_reports_id = $request->id_cr;
        $cotizacion->c_contacto = $request->nombre;
        $cotizacion->c_tipo_moneda = $request->moneda;
        $cotizacion->c_tipo_comprobante = $request->tipo_combrobante;
        $fecha = explode('-',$request->fecha);
        $cotizacion->id_cliente = $request->id_cliente;
        $cotizacion->c_nombre_cotizacion = $request->nombre_cotizacion;
        $fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
        $cotizacion->c_fecha = $fecha;
        $cotizacion->c_type_file = $request->adjunto;
        $cotizacion->c_boolean_inversion = isset($request->inversion_total) ? $request->inversion_total : 0;
        $cotizacion->boolean_igv = isset($request->igv) ? $request->igv : 0;
        if ($cotizacion->c_consideraciones !== $request->consideraciones_servicios_html ) {
            $cotizacion->c_consideraciones = $request->consideraciones_servicios_html;
        }
        if ($cotizacion->c_acciones !== $request->acciones_no_incluidas_html){
            $cotizacion->c_acciones = $request->acciones_no_incluidas_html;
        }
        
        if ($request->adjunto == 1) {
            if ($request->file('me_files')) {
            $file = $request->file('me_files');
            $filename = 'mi_'.date('Y-m-d').'_'.rand().'.'.$file->getClientOriginalExtension();
            $destination = public_path().'/files/'.$filename;
            $flag = move_uploaded_file($file,$destination);
            $cotizacion->c_file = $filename;
            }
        }
        else if ($request->adjunto == 2) {
            $cotizacion->c_file = $request->link_google;
        }
        else {
            $cotizacion->c_file = null;
        }
        $cotizacion->update();
        if(isset($request->listStaffCotizador) && !empty($request->listStaffCotizador)) {

          for ($i = 0; $i < count($request->listStaffCotizador);$i++) {

            $staffCotizador = CotizacionStaff::find($request->listStaffCotizador[$i]);
            $staffCotizador->cotizacion_id = $cotizacion->id;

            $staffCotizador->save();
          }
        }
        $data = [
            'status' => 200,
            'message' => 'Se actualizo correctamente',
           
        ];

        return $data;

    }

    public function delete_cotizacion(Request $request) {
       try {
           $cotizacion = Cotizacion::find($request->id);
           $cotizacion->delete();
           $service = ServicioCotizacion::where('id_cotizacion','=',$request->id)->delete();
           $data = [
               'message' => 'Se elimino correctamente',
               'status' => 200
           ];
           return $data;
       }
       catch (\Exception $e) {

       }
    } 
    public function status_cotizacion (Request $request) {

        if (\Auth::user()->id != 10) {
            return redirect()->route('cotizaciones_index');
        }

        /*
        $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
        ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
        ->where('status_cotizacion.status','!=',6)
        ->orderby('id','desc')->paginate(20);
        */

        $clientes = Cotizacion::join('clientes','clientes.id','=','cotizaciones.id_cliente')
          ->select('clientes.nombre','clientes.id')
          ->distinct()
          ->orderby('clientes.nombre','asc')
          ->get();

        $cliente = $request->query('cliente');
        $nombre = $request->query('nombre');
        if ($request->query('cliente') && !$request->query('nombre')) {
          $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
            ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
            ->join('clientes','clientes.id','=','cotizaciones.id_cliente')
            ->where('status_cotizacion.status','!=',6)
            ->where('clientes.nombre','LIKE','%'.$cliente.'%')
            ->orderby('id','desc')->paginate(40);


          return view ('frontend.status_cotizacion',compact('cotizacion','clientes'));

        } else if ($request->query('nombre') && $request->query('cliente') == '0') {
          $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
            ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
            ->where('status_cotizacion.status','!=',6)
            ->where('c_nombre_cotizacion','LIKE','%'.$nombre.'%')
            ->orderby('id','desc')->paginate(40);
          return view ('frontend.status_cotizacion',compact('cotizacion','clientes'));

        } else if ($request->query('nombre') && !$request->query('cliente')) {
          $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
            ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
            ->where('status_cotizacion.status','!=',6)
            ->where('c_nombre_cotizacion','LIKE','%'.$nombre.'%')
            ->orderby('id','desc')->paginate(40);
          return view ('frontend.status_cotizacion',compact('cotizacion','clientes'));

        } else if ($request->query('nombre') && $request->query('cliente')) {

          $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
            ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
            ->where('status_cotizacion.status','!=',6)
            ->join('clientes','clientes.id','=','cotizaciones.id_cliente')
            ->where('clientes.nombre','LIKE','%'.$cliente.'%')
            ->where('c_nombre_cotizacion','LIKE','%'.$nombre.'%')
            ->orderby('id','desc')->paginate(40);
          return view ('frontend.status_cotizacion',compact('cotizacion','clientes'));

        }



        $cotizacion = DB::table('cotizaciones')->select(DB::raw('(SELECT us.name FROM users as us WHERE us.id = cotizaciones.c_user_create ) as ejecutivo,cotizaciones.id,cotizaciones.c_marca,id_cliente,c_id_version,cotizaciones.c_tipo_moneda,c_contacto,c_type_file,c_version,c_file,c_fecha,c_num ,(SELECT SUM(sc.total) FROM servicios_cotizacion as sc WHERE sc.id_cotizacion = cotizaciones.id ) as total,status_cotizacion.id as id_status , status_cotizacion.status,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as marca ,(SELECT title FROM servicios_cotizacion WHERE servicios_cotizacion.id_cotizacion = cotizaciones.id ORDER BY servicios_cotizacion.id ASC LIMIT 1) as title_servicio'))
          ->join('status_cotizacion','status_cotizacion.id_cotizacion','=','cotizaciones.id')
          ->where('status_cotizacion.status','!=',6)

          ->orderby('id','desc')->paginate(20);



      return view ('frontend.status_cotizacion',compact('cotizacion','clientes'));
    }
    public function update_status_cotizacion(Request $request) {
        $id = $request->id;
        $status = $request->status;
        $obs = nl2br($request->obs); 
        
        try {
            DB::beginTransaction();
                $status_cotizacion =StatusCotizacion::find($id);
                $status_cotizacion->status = $status;
                $status_cotizacion->update();
                $cotizacion = Cotizacion::find($status_cotizacion->id_cotizacion);
                if ($status == 4 ) {
                    $observacion = new  ObsCotizacion(); 
                    $observacion->id_status_cotizacion = $id;
                    $observacion->descripcion = $obs;
                    $observacion->save();
                }
                $observaciones = '';
                $user = \Auth::user()->id;
                $user_name = User::find($user);
               $id_user = $cotizacion->c_user_create;

                $mensaje = '';
                $st = '';
                if ($status == 1) {
                    $mensaje = 'El usuario (a): '.$user_name->name .' ha <strong>aprobado</strong> la cotización   <strong>#'.$cotizacion->c_num.'</strong>.';
                    $st = 'Aprobado';
                }
                else if ($status == 2) {
                    $mensaje = 'El usuario (a): '.$user_name->name .' ha <strong>desaprobado</strong> la cotización   <strong>#'.$cotizacion->c_num.'</strong>.';
                    $st = 'Desaprobado';
                }
                else if ($status == 3) {
                    $mensaje = 'El usuario (a): '.$user_name->name .' actualizó la cotización #'.$cotizacion->c_num.' que está  <strong>pendiente</strong> a revisión.' ;
                   $id_user = 10;

                    $st = 'Pendiente';
                }
                else if ($status == 4) {
                    $mensaje = 'El usuario (a): '.$user_name->name .' añadió algunas <strong>observaciones</strong> a la cotización <strong>#'.$cotizacion->c_num.'</strong>.' ;
                   $observaciones = $obs;
                    $st = 'Observaciones';
                  
                }

                $data = [
                    'usuario' => $user_name->name, 
                    'mensaje' => $mensaje, 
                    'observaciones' => $observaciones,
                    'contacto' => $cotizacion->c_contacto,
                    'razon_social' => $cotizacion->c_razon_social,
                    'id_cotizacion' => $cotizacion->id,
                    'estado' => $st
                ];
                if ($observaciones == '') {
                  unset($data['observaciones']);
                }
              $this->enviarEmail($data,$id_user);
            DB::commit();
            $data = [
                'message' => 'Se actualizó correctamente',
                'status' => 200
            ];
            return $data;
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'error' => $e->getMessage(),
                'line' => $e->getLine(),
                'El código de excepción es ' =>  $e->getCode()

            ];
        }
        
       
    }
    public function observacion (Request $request) {
        $observacion = ObsCotizacion::where('id_status_cotizacion' , '=' , $request->id)->orderBy('id','desc')->get(); 
        // dd($observacion);
        return view('frontend.obs_cotizacion',compact('observacion'));
    }

    public function download_file ($file) {
        $path = public_path().'/files/'.$file;
        return response()->download($path);
    }

    public function enviarEmail ($datos,$id) {
        $us = User::find($id);

      Mail::send('emails.cotizacion', ($datos), function($body) use ($datos,$us) {
            $body->to($us->email,$us->name);
            $body->subject($datos['estado']);
            $body->from('russ3ll.513@gmail.com','Cotizaciones MI');

        } );
    }

    public function cotizacion_cliente(Request $request) {
        $cliente = Cliente::find($request->id);
        return $cliente;
    }


    //Template


    // Cotizador

    public function addStaffToCotizacion(Request $staff) {

       try {
         $cotizador = Cotizador::find($staff->cotizador_id);
         $staffCotizador = new CotizacionStaff();
         //$staffCotizador->cotizacion_id = $staff->cotizacion_id;
         $staffCotizador->cotizador_id = $staff->cotizador_id;
         $total = $cotizador->costo_hora * $staff->horas;
         $notFormatMoney = $total;
         $total = number_format($total,2,'.',',');
         $staffCotizador->horas = $staff->horas;
         $staffCotizador->total = $total;
         $staffCotizador->save();

        return view('frontend.templateCotizador', compact('staffCotizador','cotizador','notFormatMoney'));
       }
       catch (\Exception $e) {
          return [
            'message' => $e->getMessage(),
            'status' => 400,
            'success' => false
          ];
       }
    }

    public function deleteStaffCotizador ($id) {
      try {
        $staffCotizador = CotizacionStaff::find($id);
        $staffCotizador->delete();
        return [
          'status' => 200,
          'message' => 'Se elimino correctamente',
          'success' => true
        ];
      }
      catch (\Exception $e) {
        return [
          'status' => 401,
          'message' =>'No se pudo eliminar  error: ' . $e->getMessage(),
          'success' => false
        ];
      }
    }

    public function getStaffs () {

      return Cotizador::orderBy('id', 'ASC')->get();

    }

}

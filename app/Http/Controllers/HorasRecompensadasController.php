<?php

namespace App\Http\Controllers;

use App\{User, HorasTotales, HorasExtras};
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\HorasRecompensadas;
use Illuminate\Support\Facades\DB;

class HorasRecompensadasController extends Controller
{
    public function index()
    {
        $horas = HorasRecompensadas::where('estado',1)->paginate(20);
       return view('frontend.horas_recompensadas', compact('horas'));
    }

    public  function  data() {
        $horas = HorasRecompensadas::join('users','users.id','=','horas_recompensadas.user_id')

            ->select(DB::raw("users.name,horas_recompensadas.id,horas_recompensadas.user_id,horas_recompensadas.horas,
                DATE_FORMAT(horas_recompensadas.fecha,'%d-%m-%Y') as fecha"))
            ->where('horas_recompensadas.estado',1)->orderby('horas_recompensadas.id','DESC')->get();
        $users = User::where('users.estado',1)->orderby('name','ASC')->get();
        return [
            'horas' => $horas,
            'users' => $users
        ];
    }

    public function store(Request $request)
    {

        try {

            $flag = 'no';
            $message = 'Guardado correctamente';
            $horas_recompasada =  new  HorasRecompensadas;
            if ( $request->id != 'undefined') {

                $flag = 'ok';
                $message = 'Actualizado correctamente';
                $horas_recompasada =  HorasRecompensadas::find($request->id);
            }
            $horas_recompasada->user_id = $request->user_id;
            $horas_recompasada->horas = $request->horas;
            $horas_recompasada->fecha = $request->fecha;
            $horas_recompasada->created_at = Carbon::now();
            $fecha = explode('-',$horas_recompasada->fecha);
            $fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0];
            $horas_recompasada->estado = 1;
            $user = User::find($request->user_id);
            $horas_recompasada->save();
            return [
                'success' => 'ok',
                'message' => $message,
                'flag' => $flag,
                'data' => [
                    'id' => $horas_recompasada->id,
                    'horas' => $horas_recompasada->horas,
                    'fecha' => $fecha,
                    'name' => $user->name,
                    'user_id' => $horas_recompasada->user_id
                ],
            ];
        }
        catch (\Exception $e ) {
            return [
                'success' => false,
                'message' => 'No se pudo realizar la operacion',
                'message_error' => $e->getMessage(),
            ];
        }
    }


    public function destroy($id)
    {
      try{
          $horas_recompasada = HorasRecompensadas::find($id);
          $horas_recompasada->estado = 0;
          $horas_recompasada->save();
          return [
              'success' =>'ok',
              'message' => 'Eliminado correctamente',
              'status' => 200
          ];
      }
      catch (\Exception $e) {
          return [
              'success' => false,
              'status' => 401,
              'message' => 'No se pudo eliminar',
              'message_error' => $e->getMessage(),
          ];
      }

    }
    public function resumen () {
      $myData = [];
      // Obtener las horas extras de cada usuario
      $horas_mes = HorasExtras::rightJoin('users','users.id','=','horas_extras.user_id')
        ->select(DB::raw('users.name,SUM(horas_extras.horas_extras) as horas_extras, users.id as user_id'))
        ->where('users.estado',1)
        ->groupby('users.id')
        ->orderby('users.name','ASC')
        ->get();
      // recorremos las horas recompensadas de cada usuario
      foreach ($horas_mes as $hora ) {
        $recompensado = HorasRecompensadas::select(DB::raw('SUM(horas) as horas_recompensadas'))
          ->groupby('user_id')
          ->where('user_id','=',$hora->user_id)
          ->get();
       if (count($recompensado) > 0 ) {
         $horas_recompensadas = $recompensado[0]->horas_recompensadas;
       }
       else {
         $horas_recompensadas = 0;
       }
        // Horas deuda = horas_extras - horas_recompensadas
        $horas_deuda = $hora->horas_extras - $horas_recompensadas;
        $data   = [
          'usuario'  => $hora->name,
          'horas_extras' => $hora->horas_extras,
          'horas_recompensadas' => $horas_recompensadas,
          'horas_deuda' => $horas_deuda
        ];
        array_push($myData, $data);
      }
    return $myData;
    }

    public function balance_horas ()
    {
      $query = DB::table('asistencia')
        ->join('users','users.id','=','asistencia.user_id')
        ->select(DB::raw('
        users.name,
          IF (users.horas_totales = 187,
           CASE
            WHEN asistencia.hora_fin IS NULL
                THEN   "no marcó salida"
           WHEN asistencia.estado = "M"
               THEN "Llego tarde"
           WHEN asistencia.estado = "TJ" AND (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8)),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_inicio,1,8))) ) >=  570
                THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_inicio,1,8)),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) ) -  570
        
            WHEN (SUBSTRING(asistencia.hora_inicio,1,8) >= "09:01:00"  AND SUBSTRING(asistencia.hora_inicio,1,8) < "09:30:00") AND SUBSTRING(asistencia.hora_fin,1,8) > "19:00:00"
                THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 19:00:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        
            WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "09:00:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:31:00")  AND SUBSTRING(asistencia.hora_fin,1,8) >"18:30:00"
                THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 18:30:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        
            WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "08:30:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:01:00") AND SUBSTRING(asistencia.hora_fin,1,8) > "18:00:00"
                   THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 18:00:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        
            WHEN SUBSTRING(asistencia.hora_inicio,1,8) < "08:00:00"  AND  SUBSTRING(asistencia.hora_fin,1,8) > "17:30:00"
                THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 17:30:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        

        END
        ,
        CASE
         WHEN asistencia.hora_fin IS NULL
             THEN   "no marcó salida"
         WHEN SUBSTRING(asistencia.hora_inicio,1,8)  <= ADDTIME(CONCAT((SELECT entrada FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"),"00:30:00") AND
             (SUBSTRING(asistencia.hora_fin,1,8) >= CONCAT((SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"))
            THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",(SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00" ),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) )
        END
       )
       AS "extras",
       IF (users.horas_totales = 187,
            CASE
                WHEN asistencia.hora_fin IS NULL
                    THEN   "no marcó salida"


            WHEN (asistencia.estado = "TJ" OR asistencia.estado = "G") AND TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_inicio,1,8)),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) <  570

                THEN 570 - TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_inicio,1,8)),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8)))

                WHEN (SUBSTRING(asistencia.hora_inicio,1,8) >= "09:01:00"  AND SUBSTRING(asistencia.hora_inicio,1,8) < "09:30:00") AND SUBSTRING(asistencia.hora_fin,1,8) <= "19:00:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 19:00:00" )) )

                WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "09:00:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:31:00")  AND SUBSTRING(asistencia.hora_fin,1,8) <= "18:30:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 18:30:00" )) )

                WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "08:30:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:01:00") AND SUBSTRING(asistencia.hora_fin,1,8) <= "18:00:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 18:00:00" )) )

                WHEN SUBSTRING(asistencia.hora_inicio,1,8) < "08:00:00"  AND  SUBSTRING(asistencia.hora_fin,1,8) <= "17:30:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 17:30:00" )) )

                WHEN  SUBSTRING(asistencia.hora_inicio,1,8) > "09:30:00" AND SUBSTRING(asistencia.hora_fin,1,8) <= "19:00:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 19:00:00" )) )  + 120

                WHEN  SUBSTRING(asistencia.hora_inicio,1,8) > "09:30:00" AND SUBSTRING(asistencia.hora_fin,1,8) >= "19:00:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 19:00:00" ) ,  CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8)) ) )   - 120

            END
            ,
            CASE
                WHEN asistencia.hora_fin IS NULL
                    THEN   "no marcó salida"
                WHEN SUBSTRING(asistencia.hora_inicio,1,8) >  ADDTIME(CONCAT((SELECT entrada FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"),"00:30:00") AND
                    (SUBSTRING(asistencia.hora_fin,1,8) < CONCAT((SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"))
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",(SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00" ),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) ) - 120
            END
            )
            AS "deuda",
            asistencia.fecha, asistencia.hora_inicio, asistencia.hora_fin,asistencia.estado ,WEEKDAY(asistencia.fecha) + 1 AS "Dia"
        '))->where('asistencia.fecha','>=','2019-01-14')->where('asistencia.user_id',71)->get();


        return $query;





//     $query = DB::table('asistencia')
//     ->join('users','users.id','=','asistencia.user_id')
//     ->select(DB::raw('
//     users.name,users.id,
//       IF (users.horas_totales = 187,
      
//      ROUND(SUM(  CASE
//         WHEN asistencia.hora_fin IS NULL
//             THEN   "no marcó salida"
//         WHEN asistencia.estado = "M"
//             THEN "Llego tarde"
//         WHEN asistencia.estado = "TJ" AND (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8)),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_inicio,1,8))) ) >=  570
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_inicio,1,8)),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) ) -  570
        
//             WHEN (SUBSTRING(asistencia.hora_inicio,1,8) >= "09:01:00"  AND SUBSTRING(asistencia.hora_inicio,1,8) < "09:30:00") AND SUBSTRING(asistencia.hora_fin,1,8) > "19:00:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 19:00:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        
//             WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "09:00:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:31:00")  AND SUBSTRING(asistencia.hora_fin,1,8) >"18:30:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 18:30:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        
//             WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "08:30:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:01:00") AND SUBSTRING(asistencia.hora_fin,1,8) > "18:00:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 18:00:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        
//             WHEN SUBSTRING(asistencia.hora_inicio,1,8) < "08:00:00"  AND  SUBSTRING(asistencia.hora_fin,1,8) > "17:30:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 17:30:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
//         END
//     )/60),
//     ROUND(SUM(    
//     CASE
//             WHEN asistencia.hora_fin IS NULL
//                 THEN   "no marcó salida"
//             WHEN SUBSTRING(asistencia.hora_inicio,1,8)  <= ADDTIME(CONCAT((SELECT entrada FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"),"00:30:00") AND
//                 (SUBSTRING(asistencia.hora_fin,1,8) >= CONCAT((SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"))
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",(SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00" ),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) )
//     END
//     )/60    
//     ))
//    AS "extras",
//    IF (users.horas_totales = 187,
//         ROUND(SUM(
//         CASE
//             WHEN asistencia.hora_fin IS NULL
//                 THEN   "no marcó salida"


//             WHEN (asistencia.estado = "TJ" OR asistencia.estado = "G") AND TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_inicio,1,8)),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) <  570

//             THEN 570 - TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_inicio,1,8)),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8)))

//             WHEN (SUBSTRING(asistencia.hora_inicio,1,8) >= "09:01:00"  AND SUBSTRING(asistencia.hora_inicio,1,8) < "09:30:00") AND SUBSTRING(asistencia.hora_fin,1,8) <= "19:00:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 19:00:00" )) )

//             WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "09:00:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:31:00")  AND SUBSTRING(asistencia.hora_fin,1,8) <= "18:30:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 18:30:00" )) )

//             WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "08:30:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:01:00") AND SUBSTRING(asistencia.hora_fin,1,8) <= "18:00:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 18:00:00" )) )

//             WHEN SUBSTRING(asistencia.hora_inicio,1,8) < "08:00:00"  AND  SUBSTRING(asistencia.hora_fin,1,8) <= "17:30:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 17:30:00" )) )

//             WHEN  SUBSTRING(asistencia.hora_inicio,1,8) > "09:30:00" AND SUBSTRING(asistencia.hora_fin,1,8) <= "19:00:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 19:00:00" )) )  + 120

//             WHEN  SUBSTRING(asistencia.hora_inicio,1,8) > "09:30:00" AND SUBSTRING(asistencia.hora_fin,1,8) >= "19:00:00"
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 19:00:00" ) ,  CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8)) ) )   - 120

//         END
//        )/60) ,
//        ROUND(SUM(
//         CASE
//             WHEN asistencia.hora_fin IS NULL
//                 THEN   "no marcó salida"
//             WHEN SUBSTRING(asistencia.hora_inicio,1,8) >  ADDTIME(CONCAT((SELECT entrada FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"),"00:30:00") AND
//                 (SUBSTRING(asistencia.hora_fin,1,8) < CONCAT((SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"))
//                 THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",(SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00" ),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) ) - 120
//         END
//         )/60))
//         AS "deuda"
//     '))->where('asistencia.fecha','>=','2019-01-14')->where('users.estado',1)->groupBy('asistencia.user_id')->orderBy('users.name','ASC')->get();
//         return $query;
    }
}

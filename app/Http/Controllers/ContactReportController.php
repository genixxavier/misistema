<?php

namespace App\Http\Controllers;
use App\Http\Requests\CreateContactReportRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Idreunion;
use App\Cliente;
use App\Cotizacion;
use App\ContactReport;
use App\Reunion;
use App\User;
use App\Tema;
use App\TemaAcuerdo;
use Session;
use App\ServicioCotizacion;

class ContactReportController extends Controller
{
    public function contact_report_index () {
        $idreu_list = Idreunion::all();
        $clientes = Cliente::join('idreunion', 'idreunion.cliente_id','=','clientes.id')->select('clientes.nombre')->get();

        $list_contact_report = Idreunion::join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->join('clientes','clientes.id','=','idreunion.cliente_id')->select('clientes.nombre' , 'contact_reports.cr_fecha_reunion', 'contact_reports.cr_fecha_insert','contact_reports.id','contact_reports.cr_total_percent_m','contact_reports.cr_total_percent_c')->orderby('contact_reports.id','desc')->paginate(10);
        // $list_contact_report = DB::table('idreunion')->select(DB::raw('clientes.nombre,contact_reports.cr_fecha_reunion ,contact_reports.cr_fecha_insert, contact_reports.id,contact_reports.cr_total_percent_m, contact_reports.cr_total_percent_c'))->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->join('clientes','clientes.id','=','idreunion.cliente_id')->orderby('contact_reports.id','desc')->paginate(10);
        $cotizacion = Cotizacion::all();
        $coti_realizadas = [];

        foreach ($cotizacion as $coti) {
            array_push($coti_realizadas,$coti->contact_reports_id);
        }
       
        return view ('frontend.contact_report' , compact('idreu_list' , 'clientes' , 'list_contact_report','coti_realizadas'));
      
    }

    
    public function crear_contact_report () {
      
       $lis_idreunion =Cliente::join('idreunion', 'idreunion.cliente_id','=','clientes.id')->select('idreunion.id','idreunion.nombre as nombre_reunion','idreunion.fecha_inicio','clientes.nombre')->orderby('idreunion.id','desc')->get();
        $id_reunion  = Idreunion::all();
        // dd($id_reunion[0]->id);
        

        return view ('frontend.crear_contact_report' , compact('lis_idreunion'));


    }

    // Function para mostrar las reuniones con sus clientes 
    public function clientes_reu_show ( Request $request) {
        $id = $request->id;
        $cliente_reu = Idreunion::select('cliente_id')->where('idreunion.id','=',$id)->get();
       $name_cliente = Cliente::select('nombre')->where('id','=',$cliente_reu[0]->cliente_id)->get();
        return json_encode($name_cliente);
    } 

    public function proyectos_index () {
        return view('frontend.proyectos');
    }

    // public function save_contact_report (CreateContactReportRequest $request) {
    public function save_contact_report (Request $request) {
        $integrantesc = "";
        foreach($request->integrantes_c as $valor){
            $integrantesc .= $valor.",";
        }
        // dd($lista);
        // function data ($data) {
        //     $text = '';
        //     foreach ($data as $item) {
        //             $text = $text .$item .',';
        //         };
        // return $text;
        // }

        $total = ContactReport::all();
        $reports = new ContactReport;
        $date_reu = Idreunion::find($request->id);
        $reports->reuniones_id = $request->id;
        $reports->cr_fecha_reunion = $date_reu->fecha_inicio;
        $date_today = date('Y-m-d G-i-s');
        $reports->cr_fecha_insert = $date_today;
        // $acuerdosc = data($request->acuerdosC);
        // $acuerdosm = data($request->acuerdosM);
        // $integrantesc = data( $request->integrantes_c);
        // $reports->cr_acuerdosC = $acuerdosc;
        // $reports->cr_acuerdosM = $acuerdosm;
        $reports->cr_integrantesC = $integrantesc;
        $reports->cr_total_percent_m = 0;
        $reports->cr_total_percent_c = 0; 
    //    $reports->cr_tema = $request->tema;
        $reports->cr_num = (count($total) + 1);
        $reports->cr_user_create = \Auth::user()->id;
        $reports->save();

       if ($reports->save()) {
            $ic = $reports->id;
            foreach($request->temas as $valor){
                $up = Tema::find($valor);
                $up->id_contact_report = $ic;
                $up->estado = 1;
                $up->save();
            }
            // return redirect()->route('contact_report_index'); 
            Session::flash('status', 'exito');
            return 'ok';
       }
       else {
            Session::flash('status', 'error');
            return 'No se pudo registrar';
       }
    }
    public function contact_report_edit ($id) {
        
        $lis_idreunion =Cliente::join('idreunion', 'idreunion.cliente_id','=','clientes.id')->select('idreunion.id','idreunion.nombre as nombre_reunion','idreunion.fecha_inicio','clientes.nombre')->orderby('idreunion.id' ,'desc')->paginate(10);

        $edit_report = ContactReport::find($id);
        function data ($data) {
            $arr = explode(',',$data);
            array_pop($arr);
        return $arr;
        }
        $checkElementsM = data($edit_report->checkElementsM);
        $checkElementsC = data($edit_report->checkElementsC);
    
        $acuerdosc = data($edit_report->cr_acuerdosC);     
        $acuerdosm = data($edit_report->cr_acuerdosM);       
        $integrantesc = data($edit_report->cr_integrantesC);    
        $cliente_reu = Idreunion::select('cliente_id', 'nombre')->where('idreunion.id','=',$edit_report['reuniones_id'])->get();
        
        $id_cliente = $cliente_reu[0]->cliente_id; 
        $name_cliente = Cliente::select('nombre')->where('id','=',$id_cliente)->get();
        
        // Name usuarios
        $user_id = Reunion::select('user_id')->where('id_reunion', '=' , $edit_report['reuniones_id'])->get();
    
        $name = [];
        for ($i = 0 ; $i < count($user_id); $i ++) {
            $user = User::select('name')->where('id','=',$user_id[$i]->user_id)->get(); 
            array_push($name, $user);
        }

        $id_reu = $id;

        $id_reunion = Idreunion::find($edit_report->reuniones_id);
        $user_create = $id_reunion->user_create;
        $find_user  = User::find($user_create);
        $name_user = $find_user->name;

        //temas
        $temas = Tema::where('id_contact_report',$id)->get();
        return view('frontend.edit_contact_report', compact('checkElementsC','checkElementsM','lis_idreunion','integrantesc','acuerdosc','acuerdosm' ,'name_cliente','cliente_reu', 'name' , 'id_reu','edit_report','name_user'))
                ->with('temas',$temas);
    
    }


    // viewReunion

    public function viewReunion (Request $request) {
        $id_reunion = Idreunion::find($request->id);
        $user_create = $id_reunion->user_create;
        $find_user  = User::find($user_create);
        $name_user = $find_user->name;
        $user_id = Reunion::select('user_id')->where('id_reunion', '=' , $request->id)->get();
        $name = [];
        for ($i = 0 ; $i < count($user_id); $i ++) {
            $user = User::select('name','id')->where('id','=',$user_id[$i]->user_id)->get(); 
           if ($user[0]->id != $find_user->id) {
            array_push($name, $user);
           }
        };
       
        array_push($name,$name_user);
        return $name;
    }

    public function put_contact_report (Request $request ) {
        $integrantesc = "";
        foreach($request->integrantes_c as $valor){
            $integrantesc .= $valor.",";
        }
        $contact_report = ContactReport::find($request->id);
        $contact_report->cr_integrantesC = $integrantesc;
        $contact_report->update();
        
        if ($contact_report->update()) {
            $ic = $contact_report->id;
            $ctami = 0;
            $ctacl = 0;
            $ctamiac = 0;
            $ctaclac = 0;
            foreach($request->temas as $valor){
                $up = Tema::find($valor);
                $up->id_contact_report = $ic;
                $up->estado = 1;
                $up->save();

                //procedemos a actualizar los % de acuerdos
                $tami = TemaAcuerdo::where('id_tema',$valor)->where('type','=',1)->get();
                $ctami = $ctami + count($tami);
                $tacl = TemaAcuerdo::where('id_tema',$valor)->where('type','=',2)->get();
                $ctacl = $ctacl + count($tacl);
                //contacmos los activos
                $tamiac = TemaAcuerdo::where('id_tema',$valor)->where('type','=',1)->where('estado','=',1)->get();
                $ctamiac = $ctamiac + count($tamiac);
                $taclac = TemaAcuerdo::where('id_tema',$valor)->where('type','=',2)->where('estado','=',1)->get();
                $ctaclac = $ctaclac + count($taclac);
            }

            $x = ($ctamiac*100) / $ctami;
            $x = round($x);

            $y = ($ctaclac*100) / $ctacl;
            $y = round($y);

            $contact_report = ContactReport::find($request->id);
            $contact_report->checkElementsM = $x;
            $contact_report->checkElementsC = $y;
            $contact_report->cr_total_percent_m = $x;
            $contact_report->cr_total_percent_c = $y;
            $contact_report->save();
            
            Session::flash('status', 'exito');
            return 'ok';
        }
        else {
            Session::flash('status', 'error');
            return 'error';
        }
       
    }

    public function ver_pdf ($id) {

        $contact = ContactReport::find($id);
        $idcliente = Idreunion::select('cliente_id')->where('id','=',$contact->reuniones_id)->get();
        $nombre_cliente = Cliente::select('nombre')->where('id','=',$idcliente[0]->cliente_id)->get();

        function data ($data) {
            $arr = explode(',',$data);
            array_pop($arr);
        return $arr;
        }
        $checkElementsM = data($contact->checkElementsM);
        $checkElementsC = data($contact->checkElementsC);
    
        $acuerdosc = data($contact->cr_acuerdosC);     
        $acuerdosm = data($contact->cr_acuerdosM);
        $integrantesc = data($contact->cr_integrantesC);  
    
        // TRAER A LOS USERS 
        $user_id = Reunion::select('user_id')->where('id_reunion', '=' ,$contact->reuniones_id)->get();
   
        $name = [];
        for ($i = 0 ; $i < count($user_id); $i ++) {
            $user = User::select('name','id')->where('id','=',$user_id[$i]->user_id)->get(); 
            array_push($name, $user);
        }

        $fecha = $contact->cr_fecha_insert;
        $fecha = explode(' ',$fecha);
        $fecha_new = explode('-' , $fecha[0]);
            
        $year = $fecha_new[0];
        $mes = '';
        $day = $fecha_new[2];
        switch ($fecha_new[1]) {
            case '01':
                 $mes = 'Enero';
                break;
            case '02':
                 $mes = 'Febrero';
                break;
            case '03':
                 $mes = 'Marzo';
                break;
            case '04':
                 $mes = 'Abril';
                break;
            case '05':
                 $mes = 'Mayo';
                break;
            case '06':
                $mes = 'Junio';
                break;
           case '07':
                 $mes = 'Julio';
                break;
            case '08':
                 $mes = 'Agosto';
                 break;
            case '09':
                 $mes = 'Septiembre';
                 break;
            case '10':
                 $mes = 'Octubre';
                 break;
            case '11':
                 $mes = 'Noviembre';
                 break;
            case '12':
                 $mes = 'Diciembre';
                 break;    
        } 

        $id_reunion = Idreunion::find($contact->reuniones_id);
        $user_create = $id_reunion->user_create;
        $dataUserCreate  = User::find($user_create);
        // $name_user = $find_user->name; 

        $temas = Tema::where('id_contact_report',$contact->id)->get();
       
        $pdf = \PDF::loadView('pdf.contact_report',[
                'contact' => $contact,
                'nombre_cliente' => $nombre_cliente[0]->nombre,
                'acuerdosc' => $acuerdosc,
                'acuerdosm' => $acuerdosm,
                'integrantesc' => $integrantesc,
                'integrantesm' => $name,
                'dia' => $day,
                'mes' => $mes,
                'year' => $year,
                'dataUserCreate' => $dataUserCreate,
                'temas' => $temas,

            ]
        )->setOptions([ 'defaultFont' => 'cursive']);;
        return $pdf->stream('contact.pdf'); 
    }


    public function contact_report_eliminar ( $id ) {
        $contact = ContactReport::find($id);
        $contact->delete();
        return redirect()->route('contact_report_index'); 
    }

    // 
    public function add_tema(Request $request){

        $add = new Tema;
        $add->title = $request->tema;
        $add->estado = 10;
        $add->save();

        $tema = Tema::find($add->id);
        $tipo = $request->type;
        return view('frontend.view_acuerdo')->with('tema',$tema)->with('tipo',$tipo);
    }

    public function add_tema_acuerdo(Request $request){

        $add = new TemaAcuerdo;
        $add->description = $request->data_description;
        $add->category = $request->data_category;
        $add->type = $request->data_type;
        $add->estado = 0;
        $add->id_tema = $request->data_tema_id;
        $add->save();

        $acuerdo = TemaAcuerdo::find($add->id);
        return $acuerdo;
    }

    public function delete_acuerdo_item(Request $request){

        $del = TemaAcuerdo::find($request->id);
        $del->delete();

        return 1;
    }
    
    public function eliminar_tema(Request $request){
        //podemos tambien eliminar los acuerdos del tema a eliminar
        $del = Tema::find($request->id);
        $del->delete();
    
        return 1;
    }

    public function update_item_acuerdo(Request $request){
        //actualizacion de estado
        $up = TemaAcuerdo::find($request->id);
        $up->estado = $request->estado;
        $up->save();
        
        $data = TemaAcuerdo::find($request->id);
        return $data;
    }
    public function editar_tema (Request $request) {
        $tema = Tema::find($request->id);
        $tema->title = $request->text;
        $tema->update();
        return [
            'status' => 200,
            'message' => 'Se actualizó correctamente'
         ];

    }
    public function editar_acuerdo (Request $request) {
        $tema = TemaAcuerdo::find($request->id);
        $tema->description = $request->text;
        $tema->update();
        return [
            'status' => 200,
            'message' => 'Se actualizó correctamente'
        ];

    }




}
 
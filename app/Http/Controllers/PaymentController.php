<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;
use File;
use Illuminate\Support\Facades\Mail;
use App\{
    Areas,Tribu, PaymentFiles, PaymentImg, ProjectsAreas, Etapas, ProjectsEtapas, Cotizacion, StatusCotizacion, Projects
};
use Carbon\Carbon;
class PaymentController extends Controller
{

    public function index()
    {

        $filterTribu = 0;
      

        $payment = DB::table('payment_files')->join('projects','projects.id','=','payment_files.id_project')
            ->select(DB::raw('projects.name,
            (SELECT tribus.nombre FROM tribus WHERE tribus.id = (SELECT users.tribu_id FROM users
             WHERE users.id = (SELECT cotizaciones.c_user_create FROM cotizaciones WHERE cotizaciones.id = projects.id_cotizacion)))
             as tribu
            , (SELECT tribus.id FROM tribus WHERE tribus.id = (SELECT users.tribu_id FROM users
            WHERE users.id = (SELECT cotizaciones.c_user_create FROM cotizaciones WHERE cotizaciones.id = projects.id_cotizacion)))
            as tribu_id
           ,
             (CASE WHEN (type = 1) 
                     THEN "Pago Total"
                     ELSE "Servicio Adicional" 
              END) as pagos,
              (CASE 
                    WHEN (voucher = 1)  THEN "Factura" 
                    WHEN (voucher = 2)  THEN "Boleta"
                    ELSE "Otros"
                END    
              ) as tipo_comprobante,
              (CASE 
                WHEN (payment_files.oc IS NULL OR payment_files.oc = "") THEN "No"
                ELSE "Si"
                END
         
              )as oc,
               (CASE 
                WHEN (payment_files.factura IS NULL OR payment_files.factura = "") THEN "No"
                ELSE "Si"
                END
         
              )as factura,
               (CASE 
                WHEN (payment_files.codigo_entrada IS NULL OR payment_files.codigo_entrada = "") THEN "No"
                ELSE "Si"
                END
         
              )as codigo,
              payment_files.id ,
              payment_files.total as precio'))->orderBy('payment_files.id','DESC')->paginate(10);
        if (isset($_GET['tribu'])){
            $filterTribu = $_GET['tribu'];
        }
       
        $tribus = Tribu::all();
        return view('frontend.payment.index',compact('payment','filterTribu','tribus'));
    }


    public function create()
    {
        $projects = DB::table('projects')->join('cotizaciones','projects.id_cotizacion','=','cotizaciones.id')->select(DB::raw('projects.id,projects.name,estado,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as cliente,cotizaciones.id as id_cotizacion'))->orderBy('projects.id','DESC')->get();

        return view('frontend.payment.create',compact('projects')) ;
    }


    public function store(Request $request)
    {


        try {
            DB::beginTransaction();
            $pj = Projects::find($request->id_proyecto);

            $payment  = new PaymentFiles;
            $payment->id_project = $request->id_proyecto;
            $payment->type = $request->type;
            $payment->voucher = $request->voucher;
            $payment->total = $request->monto;
            $payment->date = date('Y-m-d');
            if ($request->type == 2) {
                $coti = Cotizacion::find($pj->id_cotizacion);
                $coti->c_extras += $request->monto;
                $coti->save();
            }
            if ($request->type_payment !== null && $request->type !== 1) {
                if (count($request->type_payment) > 0) {
                    $des = '';
                    foreach ($request->type_payment as $pay) {
                        $des .= $pay.',';
                    }
                    $payment->type_payment = $des;
                }
                else {
                    $payment->type_payment = $request->type_payment[0] + ',';
                }
                $payment->description = $request->descripcion;
            }

            $payment->oc = !empty($request->file('oc')) ? $this->move_file($request->file('oc')): '';
            $payment->factura = !empty($request->file('factura')) ? $this->move_file($request->file('factura')): '';
            $payment->codigo_entrada = !empty($request->file('codigo_entrada')) ? $this->move_file($request->file('codigo_entrada')): '';

             $payment->save();

            if (!empty($request->file('oc')) && $request->type != 2) {
                $data = [
                    'mensaje' => 'Se registró la OC del Proyecto <strong>'.$pj->name .'</strong>',
                    'uri' => 'http://localhost:8000/uploads/'.$payment->oc,
                    'message_button' => 'Ver OC'
                ];
                $this->enviarEmail($data);
            }
            if (!empty($request->file('codigo_entrada')) && $request->type != 2) {
                $data = [
                    'mensaje' => 'Se registró el codigo de entrada  del Proyecto <strong>'.$pj->name .'</strong>',
                    'uri' => 'http://localhost:8000/uploads/'.$payment->codigo_entrada,
                    'message_button' => 'Ver codigo de entrada'
                ];
                $this->enviarEmail($data);
            }


             DB::commit();
            return [
                'message' => 'Registrado correctamente',
                'status' => 200

            ];

        }
        catch (\Exception $e) {
            DB::rollBack();
            return [
                'message' => 'No se puede registrar',
              'error' => $e->getMessage(),
                'code' => $e->getCode(),
                'line' => $e->getLine()
            ];
        }

    }


    public function show($id)
    {

    }


    public function edit($id)
    {

        $payment = PaymentFiles::find($id);

        $project = Projects::find($payment->id_project);

        $oc = $this->show_extension($payment->oc);
        $ce = $this->show_extension($payment->codigo_entrada);
        $f = $this->show_extension($payment->factura);

        $array_options = [];
        if ($payment->type == 2) {
            $txt = substr($payment->type_payment, 0, -1);
            $array_options = explode(',',$txt);
        }
        $projects = DB::table('projects')->join('cotizaciones','projects.id_cotizacion','=','cotizaciones.id')->select(DB::raw('projects.id,projects.name,estado,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as cliente,cotizaciones.id as id_cotizacion'))->orderBy('projects.id','DESC')->get();

        return view('frontend.payment.edit',compact('oc','ce','f','projects','payment','id' ,'project','array_options')) ;
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $pj = Projects::find($request->id_proyecto);
            $payment  = PaymentFiles::find($request->id_payment);
            $payment->id_project = $request->id_proyecto;
            $payment->type = $request->type;
            $payment->voucher = $request->voucher;
            $payment->total = $request->monto;
            $payment->date = date('Y-m-d');

            if ($request->type != 1) {
                if ($request->type_payment !== null && count($request->type_payment) > 0) {
                    $des = '';
                    foreach ($request->type_payment as $pay) {
                        $des .= $pay.',';
                    }
                    $payment->type_payment = $des;
                }
                else {
                    $payment->type_payment = $request->type_payment[0] + ',';;
                }
                $payment->description = $request->descripcion;
            }
            else {
                $payment->type_payment = '';
                $payment->description = '';
            }

            $payment->oc = !empty($request->file('oc')) ? $this->move_file($request->file('oc')): $payment->oc;
            $payment->factura = !empty($request->file('factura')) ? $this->move_file($request->file('factura')): $payment->factura;
            $payment->codigo_entrada = !empty($request->file('codigo_entrada')) ? $this->move_file($request->file('codigo_entrada')): $payment->codigo_entrada;

            $payment->save();

            if (!empty($request->file('codigo_entrada')) && $request->type != 2) {
                $data = [
                    'mensaje' => 'Se registró el codigo de entrada  del Proyecto <strong>'.$pj->name .'</strong>',
                    'uri' => 'http://localhost:8000/uploads/'.$payment->codigo_entrada,
                    'message_button' => 'Ver codigo de entrada'
                ];
                $this->enviarEmail($data);
            }

            if ($request->type == 2) {
                $coti = Cotizacion::find($pj->id_cotizacion);
                $sum = DB::table('payment_files')->select(DB::raw('SUM(total) as precio_total'))
                       ->where('payment_files.id_project','=',$pj->id)->where('payment_files.type','=',2)->get();
                //$sum = PaymentFiles::where('id_proyecto','=',$pj->id)->sum();

                $coti->c_extras = $sum[0]->precio_total;
                $coti->save();
            }


            DB::commit();
            return [
                'message' => 'Actualizado correctamente',
                'status' => 200

            ];

        }
        catch (\Exception $e) {
            DB::rollBack();
            return [
                'message' => 'No se pudo actualizar',
                'message_error' => $e->getMessage(),
                'code' => $e->getCode(),
                'line' => $e->getLine(),
                'all' => $request->all()

            ];
        }
    }


    public function destroy($id)
    {

    }

    public function add_payment ($id) {

        $addProject = Projects::find($id);
        $projects = DB::table('projects')->join('cotizaciones','projects.id_cotizacion','=','cotizaciones.id')->select(DB::raw('projects.id,projects.name,estado,(SELECT nombre FROM clientes WHERE clientes.id = cotizaciones.id_cliente) as cliente,cotizaciones.id as id_cotizacion'))->orderBy('projects.id','DESC')->get();
        return view('frontend.payment.create', compact('addProject','projects'));
    }

    private function  move_file ($file) {

        $extension = $file->getClientOriginalExtension();
        $fname = $file->getFilename().'.'.$extension;
        Storage::disk('public_two')->put($fname,File::get($file));
        return $fname;
    }
    private function show_extension ($data) {
        if ($data == null || $data == '') {
            return '';
        }
        else {
            $f = explode('.',$data);
            $ext = $f[2];
            if ($ext == 'jpg' || $ext == 'png' || $ext == 'jpeg') {
                return 1;
            }
            else {
                return 0;
            }

        }
    }
    private function enviarEmail ($datos) {


        Mail::send('emails.factura', $datos, function($body) use ($datos) {

            $body->to('luischungar7@gmail.com','Luis');
            //$body->cc('e@mediaimpact.pe','Erick Saldaña');
            //$body->cc('luischungar7@gmail.com','Luis CHUNGA');
            $body->subject('Facturación');
            $body->from('russ3ll.513@gmail.com','Cotizaciones MI');

        } );
    }
}

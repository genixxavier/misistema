<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\SocialProvider;
use App\User;
use Sesion;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
         try
         {
            $socialUser = Socialite::driver($provider)->user(); 
         }
         catch(\Exception $e)
         {
             dd($e);
             return redirect('/');
         }
          

         $socialProvider = SocialProvider::where('provider_id',$socialUser->getId())->first();
         if(!$socialProvider)
         {

            //COMENTAR ESTO PARA REGISTRO
            // return redirect('/')->with('status', 'Usuario no registrado');
            //


            if($provider == 'facebook'){
                $user = User::firstOrCreate(
                     ['email' => $socialUser->getEmail()],
                     ['name' => $socialUser->user['first_name']],
                     ['lastname' => $socialUser->user['last_name']],
                     ['photo' => $socialUser->getAvatar()]
                );

                $user->lastname = $socialUser->user['last_name'];
                $user->photo = $socialUser->getAvatar();
                $user->save();
            }else{

                if(User::where('email',$socialUser->getEmail())->first()){
                    $user = User::where('email',$socialUser->getEmail())->first();
                }else{
                    $user = new User();
                    $user->email = $socialUser->getEmail();
                    $user->name = $socialUser->getName();
                    $user->area_id =1;
                    $user->estado =1;
                    $user->save();
                }

                
            }
            $user->socialProviders()->create(
                 ['provider_id' => $socialUser->getId(), 'provider' => $provider]
            );
         }
         else
            $user = $socialProvider->user;
            auth()->login($user);

        return redirect('historial');
    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cliente;
use App\Ticket;
use App\Areas;
use App\TiketHistorial;
use App\TaskBack;
use DateTime;
use Session;
use Carbon\Carbon;

class CalendarController extends Controller
{
  public function index(){
    $id = \Auth::id();
    if(\Auth::user()->tipo == 'jefe'){
       $clientes = Cliente::where('activo','=',1)->orderby("nombre")->get();
       $idarea = \Auth::user()->area_id;
       $areas = Areas::where('id','=',$idarea)->get();
    }
    else if(\Auth::user()->tipo == 'gerente'){
        $clientes = Cliente::where('activo','=',1)->orderby("nombre")->get();
        $areas = Areas::all();
    }
    else{
        // $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
        //     ->where('cliente_user.user_id','=',$id)
        //     ->where('clientes.activo','=',1)
        //     ->orderby("clientes.nombre")
        //     ->get();
        // $areas = Areas::all();

       $clientes = Cliente::where('activo','=',1)->orderby("nombre")->get();
       $idarea = \Auth::user()->area_id;
       $areas = Areas::where('id','=',$idarea)->get();
      

    }
    
    $id_trello = \Auth::user()->id_trello;
    $id_key = \Auth::user()->key_trello;
    $id_token = \Auth::user()->token_trello;
    $listTaskBack = TaskBack::where('user_id','=',$id )->get();
    $list_card = Ticket::whereNotNull('id_card')->select('id_card')->get();
    

    return view('frontend.ticket_calendario', compact('id_trello', 'id_key','id_token', 'list_card'))->with('listTaskBack',$listTaskBack)->with('clientes',$clientes);
  }

  // public function list_task(){
  public function list_task(){
    $start = date('Y-m-d H:i:s', strtotime($_GET['start']));
    $end = date('Y-m-d H:i:s', strtotime($_GET['end']));
    // return $start." - ".$end;
    $tasks = Ticket::join('clientes','clientes.id','=','tickets.cliente_id')
              ->where('tickets.user_id','=',\Auth::user()->id)
              ->whereBetween('tickets.fecha_inicio',[$start,$end])
              ->orderBy('tickets.fecha_inicio','ASC')
              ->select('tickets.id','tickets.descripcion as description','tickets.nombre as title','tickets.user_id','tickets.fecha_inicio as start','tickets.fecha_fin as end','tickets.cliente_id as id_cliente','clientes.color','clientes.nombre as cliente')
              ->get();
    // dd($tasks);
    return $tasks;
    // return view('frontend.ticket_historial')->with('tasks',$tasks);
  }

  public function list_clients(){

    if(\Auth::user()->tipo == 'jefe'){
       $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
    }
    else if(\Auth::user()->tipo == 'gerente'){
        $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
    }
    else{
        $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
            ->where('clientes.activo','=',1)
            ->where('cliente_user.user_id','=',\Auth::id() )
            ->orderby("clientes.nombre")
            ->get();
    }

    return $clientes;
  }

  public function add_task_back(Request $request){
    
      $cliente = Cliente::find($request->task_cliente);
      if( $cliente){
        $color = $cliente->color;
      }
      else{
        $color = '#2196F3';
      }


      $add_task = New TaskBack();
      $add_task->title = $request->name_task;
      $add_task->description = $request->description_task;
      $add_task->id_cliente = $request->task_cliente;
      $add_task->duration = $request->task_duration;
      $add_task->cliente = $cliente->nombre;
      $add_task->state = 1;
      $add_task->color = $color;
      $add_task->user_id = \Auth::user()->id;
      $add_task->area_id = \Auth::user()->area_id;
     
      
      if($request->isChangeTicket){
          $add_task->correccion = 1;
       }
      if( $add_task->save() ){
          return $add_task;
      }
    }

    public function list_task_back(){
      $listTaskBack = TaskBack::get();
      return $listTaskBack;
    }

    public function add_task(Request $request){
      // buscamos la tarea en el backlist
      $checkkk = $request->checkin;
      
      $glist = null;
      if ($request->isTrello == 'ok') {
        $glist = 'trello';
      } else {
        $glist = TaskBack::find($request->task_id) ;
       
      }

    

      
      
      if($glist){
        // comparamos si la hora es diferente de las 13horas
        $uno = explode('T',$request->task_start);

        $dos = explode('T',$request->task_end);
        if ((substr($uno[1],0,-5) > '13:00:00' &&  substr($uno[1],0,-5) < '13:59:00' )
          || ((substr($dos[1],0,-5) > '13:00:00' && substr($dos[1],0,-5) < '13:59:00' ))) {
            return "No se puede comenzar o terminar una actividad dentro de la hora de almuerzo";
        }

          if( $uno[1] != '13:00:00' ){

          // $fecha1 = new DateTime($request->task_start);//fecha inicial
          // // calcular tiempo:
          // $fecha2 = new DateTime($request->task_end);//fecha de cierre
          // $intervalo = $fecha1->diff($fecha2);
          // $duration = (int)$intervalo->format('%H');

          // dd($intervalo);

          $add_task = New Ticket();
          if ($glist == 'trello' ) {
          $add_task->descripcion = 'trello';
          }else{
          $add_task->descripcion = $glist->description;
          }
          if ($glist == 'trello'){
          $add_task->nombre = $request->description;
          }else{
            $add_task->nombre = $glist->title;
          }
          if ($glist == 'trello'){
          $add_task->cliente_id = 5;
          }else{
          $add_task->cliente_id = $glist->id_cliente;
          }
          $add_task->correccion_2 = $glist->correccion;
          
          $add_task->fecha_inicio = $request->task_start;
          $add_task->fecha_fin = $request->task_end;
          $add_task->horas_pedido = 1;
          $add_task->estado = 2;
          if($glist=='trello'){
          $add_task->area_id = \Auth::user()->area_id;
          }else{  
            $add_task->area_id = $glist->area_id;
          }
          if($glist=='trello'){
            if($checkkk == 'false' ) {
              $add_task->id_card =$request->task_id;  
          }
          
          }
          if($glist =='trello'){
          $add_task->user_id =  \Auth::user()->id;;
          }else{
            $add_task->user_id = $glist->user_id;
          }
          $add_task->save();

          // eliminamos
          if($glist != 'trello') {
            
            if($checkkk == 'false'){
              
              
              $glist = TaskBack::find($request->task_id);
              $glist->delete();
            }
          }


          return "exito";
        }else{
          return "almuerzo";
        }

      } 

      return "error";
    }

    public function update_task(Request $request){
      // $start = date('Y-m-d H:i:s', strtotime( $request->task_start ));
      // $end = date('Y-m-d H:i:s', strtotime( $request->task_end ));

      // comparamos si la hora es diferente de las 13horas

      $uno = explode('T',$request->task_start);
      if( $uno[1] == '13:00:00.000Z' ){
        return "almuerzo";
      }
      $dos = explode('T',$request->task_end);
      if( $dos[1] == '14:00:00.000Z' ){
        return "finalmuerzo";
      }

      if ((substr($uno[1],0,-5) > '13:00:00' &&  substr($uno[1],0,-5) < '14:00:00' )
        || ((substr($dos[1],0,-5) > '13:00:00' && substr($dos[1],0,-5) < '14:00:00' ))) {
        return "No se puede comenzar o terminar una actividad dentro de la hora de almuerzo";
      }

      $fecha1 = new DateTime($request->task_start);//fecha inicial
      $fecha2 = new DateTime($request->task_end);//fecha de cierre
      $intervalo = $fecha1->diff($fecha2);
      $duration = (int)$intervalo->format('%H');

      if( $uno[1] < '13:00:00.000Z' ){
        if( $dos[1] > '14:00:00.000Z' ){
          $duration = $duration -1;
        }
      }

      // consultamos el id
      $update_task = Ticket::find($request->task_id);
      if($update_task){
        $update_task->fecha_inicio = $request->task_start;
        $update_task->fecha_fin = $request->task_end;
        $update_task->horas_pedido = $duration;
        $update_task->save();
        return "exito";
      }
      else{
        return "error";
      }

    }

    public function update_edit_task(Request $request){
      // consultamos el id
      $update_task = Ticket::find($request->edit_id_task);
      if($update_task){
        $update_task->cliente_id = $request->task_cliente;
        $update_task->nombre = $request->edit_name_task;
        $update_task->descripcion = $request->edit_description_task;
        
        if($request->isChangeTicket){
          $update_task->correccion_2 = 1;
       } else {
        $update_task->correccion_2 = 0;
       }
        $update_task->save();
        return $update_task;
      }
      else{
        return "error";
      }

    }

    public function task_id($task_id){
      $task = Ticket::where('id','=',$task_id)
                      ->select('tickets.id','tickets.descripcion as description','tickets.nombre as title','tickets.user_id','tickets.fecha_inicio as start','tickets.fecha_fin as end','tickets.cliente_id as id_cliente','tickets.correccion_2 as correccion')
                      ->first();
      if($task){
        return $task;
      }
      else{
        return "error";
      }
    }

    public function delete_task_id($task_id){
      $task = Ticket::find($task_id);
      if ( $task->delete() ){
        return "exito";
      }
      else{
        return "error";
      }
    }
}

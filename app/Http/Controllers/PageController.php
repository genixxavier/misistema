<?php

    namespace App\Http\Controllers;

    use App\Exports\ReportsExport;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use App\Ticket;
    use App\TiketHistorial;
    use App\Asistencia;
    use App\Cliente;
    use App\ClienteUser;
    use App\Colaborador;
    use App\User;
    use App\Areas;
    use App\Reunion;
    use App\Idreunion;
    use App\Dias;
    use App\HorarioUser;
    use App\PuntosExtras;
    use App\HorasExtras;
    use DateTime;
    use Session;
    use Carbon\Carbon;
    use Illuminate\Support\Facades\Mail;
    use App\Mail\TiketAlerta;
    use App\Tribu;
    use App\Client;
    use App\HistorialAsistencia;
    use App\HorasRecompensadas;
    use App\Resumen;
    use Maatwebsite\Excel\Facades\Excel;

    class PageController extends Controller
    {
        //

        public function asistencia_diaria($fecha = null){

            if(is_null($fecha)){

                $hoy = setlocale(LC_TIME, "C");
                $fecha = date("d-m-Y",strtotime($hoy));

            }

            $resultado = Asistencia::join('users','asistencia.user_id','=','users.id')
                            ->where('users.tribu_id',\Auth::user()->tribu_id)
                            ->where('asistencia.fecha',$fecha)
                            ->select('asistencia.fecha','asistencia.hora_inicio as entrada','users.name as nombre')
                            ->orderby('asistencia.hora_inicio')
                            ->get();

            $tribus = Tribu::find(\Auth::user()->tribu_id);

            $fecha2 = date("Y-m-d",strtotime($fecha));
            // dd(date("Y-m-d"));
            // dd($fecha2);
            return view('frontend.asistencia_diaria')->with('resultado',$resultado)->with('tribus',$tribus)->with('fecha2',$fecha2);

            // foreach ($resultado as $key => $result) {
            //     echo $result->nombre.' - '.$result->entrada.'<br>';
            // }
        }
        public function bravo(){

            $cadena = \Auth::user()->email;
            $key = 'T0y0t4P3ru2015';

            $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));


        return redirect('https://rockstar.equipowik.com/token?dato='.$encrypted);
    }




        public function puntajes($mes){

            $n_mes = $mes;

            // $mes =8;
            $anio = 2019;
            $areas = Tribu::all();
            $areas2 = Tribu::all();
            $areas3 = Tribu::all();

            $usuarios = User::where('estado',1)->where('horas_totales','!=',null)->orderby('area_id','asc')->get();

            $asistencias = Asistencia::join('users','users.id','=','asistencia.user_id')->where('asistencia.fecha','>=',$anio.'-'.$mes.'-01')->where('asistencia.fecha','<=',$anio.'-'.$mes.'-31')->select('users.area_id','asistencia.*')->get();

            $puntos = PuntosExtras::where('anio',$anio)->where('mes','<=',$mes)->get();
            // dd($usuarios);

            // foreach ($usuarios as $key => $user) {
            //     $suma_de_horas = 0;
            //     $horas = Ticket::where('user_id',$user->id)->where('fecha_inicio','>=',$anio.'-'.$mes.'-01 00:00:00')->where('fecha_inicio','<=',$anio.'-'.$mes.'-31 24:00:00')->where('estado',2)->get();

            //     foreach ($horas as $key => $hora) {
            //         $suma_de_horas = $suma_de_horas + $hora->horas_pedido;
            //     }

            //     $asistencia = Asistencia::where('user_id',$user->id)->where('estado','V')->where('created_at','>=',$anio.'-'.$mes.'-01 00:00:00')->where('created_at','<=',$anio.'-'.$mes.'-31 24:00:00')->count();
            //     $asistencia2 = Asistencia::where('user_id',$user->id)->where('estado','J')->where('created_at','>=',$anio.'-'.$mes.'-01 00:00:00')->where('created_at','<=',$anio.'-'.$mes.'-31 24:00:00')->count();

            //     $asistencia = $asistencia + $asistencia2;

            //     $porcentaje_trello = ($suma_de_horas/(($user->horas_totales)-($asistencia*9)))*100;
            //     $user->trello = round($porcentaje_trello);
            // }

            // foreach ($areas as $key => $area) {
            //     $cont = 0;
            //     $total = 0;
            //     foreach ($asistencias as $key => $asistencia) {
            //         if($asistencia->area_id == $area->id){
            //             $total++;
            //             if($asistencia->estado == 'M' || $asistencia->estado == 'T'){
            //                 $cont++;
            //             }
            //         }
            //     }
            //     $area->tardanza = $cont;
            //     $area->total    = $total;
            // }

            // foreach ($areas as $key => $area) {
            //     $cont = 0;
            //     $trello = 0;
            //     foreach ($usuarios as $key => $user) {
            //         if($user->area_id == $area->id){
            //             $trello = $user->trello + $trello;
            //             $cont++;
            //         }
            //     }

            //     $area->trello = round($trello/$cont);
            //     $area->trello = 0;
            //     $area->personas = $cont;
            // }

            foreach ($areas as $key => $area) {

                $puntos_extras = PuntosExtras::where('area_id',$area->id)->where('anio',$anio)->where('mes',$mes)->get();

                $puntaje_extra = 0;
                foreach ($puntos_extras as $key => $punto_extra) {
                    $puntaje_extra = $puntaje_extra+$punto_extra->puntaje;
                }
                $area->puntaje_extra = $puntaje_extra;

                $total_tardanzas_permitidas = $area->personas * 5;

                $area->puntaje_asistencia = 0;
                if($area->tardanza < $total_tardanzas_permitidas*0.95){
                    $area->puntaje_asistencia = 1;
                }if($area->tardanza < $total_tardanzas_permitidas*0.90){
                    $area->puntaje_asistencia = 2;
                }if($area->tardanza < $total_tardanzas_permitidas*0.85){
                    $area->puntaje_asistencia = 3;
                }if($area->tardanza < $total_tardanzas_permitidas*0.80){
                    $area->puntaje_asistencia = 4;
                }if($area->tardanza <  $total_tardanzas_permitidas){
                    $area->puntaje_asistencia = 5;
                }


                $area->puntaje_trello = 0;

                if($area->trello > 80){
                    $area->puntaje_trello = 1;
                }if($area->tardanza > 90){
                    $area->puntaje_trello = 2;
                }if($area->tardanza > 100){
                    $area->puntaje_trello = 3;
                }if($area->tardanza > 110){
                    $area->puntaje_trello = 4;
                }if($area->tardanza > 120){
                    $area->puntaje_trello = 5;
                }

                // $area->puntaje_total = $area->puntaje_trello + $area->puntaje_extra + $area->puntaje_asistencia;
                $area->puntaje_total =  $area->puntaje_extra ;
            }


            $areas = $areas->sortByDesc('puntaje_total');

            // $areas->values()->all();

            foreach ($areas2 as $key => $area2) {

                $puntos_extras = PuntosExtras::where('area_id',$area2->id)->where('anio',$anio)->where('mes',$mes)->get();

                $puntaje_extra = 0;
                foreach ($puntos_extras as $key => $punto_extra) {
                    $puntaje_extra = $puntaje_extra+$punto_extra->puntaje;
                }
                $area2->puntaje_extra = $puntaje_extra;

                $area2->puntaje_total = $area2->puntaje_extra;
            }
            $areas2 = $areas->sortByDesc('puntaje_total');

            setlocale(LC_TIME, 'es_ES');

            $fecha = DateTime::createFromFormat('!m', $mes);
            $mes = strftime("%B", $fecha->getTimestamp());

            foreach ($areas3 as $key => $area_suma) {
                $area_suma->suma_total = 0;
                foreach ($puntos as $key => $punto) {
                    if($area_suma->id == $punto->area_id){
                        $area_suma->suma_total = $area_suma->suma_total + $punto->puntaje;
                    }
                }
            }

            $areas3 = $areas3->sortByDesc('suma_total');

            $ganadores = array();
            foreach ($areas3 as $key => $area) {
                array_push($ganadores, array('name'=>$area->nombre,'puntaje'=>$area->suma_total));
            }
            // dd($ganadores);

            return view('frontend.podio')
                    ->with('areas',$areas)
                    ->with('areas2',$areas2)
                    ->with('mes',$mes)
                    ->with('n_mes',$n_mes)
                    ->with('ganadores',$ganadores)
                    ->with('usuarios',$usuarios);
        }


        public function puntajes_individuales($mes){

            $n_mes = $mes;

            // $mes =8;
            $anio = 2018;
            $areas = Areas::all();
            $areas2 = Areas::all();
            $areas3 = Areas::all();

            $usuarios = User::where('estado',1)->where('horas_totales','!=',null)->orderby('area_id','asc')->get();

            $asistencias = Asistencia::join('users','users.id','=','asistencia.user_id')->where('asistencia.fecha','>=',$anio.'-'.$mes.'-01')->where('asistencia.fecha','<=',$anio.'-'.$mes.'-31')->select('users.area_id','asistencia.*')->get();

            $puntos = PuntosExtras::where('anio',$anio)->where('mes','<=',$mes)->get();
            // dd($usuarios);

            foreach ($usuarios as $key => $user) {
                if($mes == 12){
                    $suma_de_horas = $user->horas_totales/20;
                }else{
                    $suma_de_horas = 0;
                }
                $horas = Ticket::where('user_id',$user->id)->where('fecha_inicio','>=',$anio.'-'.$mes.'-01 00:00:00')->where('fecha_inicio','<=',$anio.'-'.$mes.'-31 24:00:00')->where('estado',2)->get();

                foreach ($horas as $key => $hora) {
                    $suma_de_horas = $suma_de_horas + $hora->horas_pedido;
                }

                $asistencia = Asistencia::where('user_id',$user->id)->where('estado','V')->where('created_at','>=',$anio.'-'.$mes.'-01 00:00:00')->where('created_at','<=',$anio.'-'.$mes.'-31 24:00:00')->count();
                $asistencia2 = Asistencia::where('user_id',$user->id)->where('estado','J')->where('created_at','>=',$anio.'-'.$mes.'-01 00:00:00')->where('created_at','<=',$anio.'-'.$mes.'-31 24:00:00')->count();

                $asistencia = $asistencia + $asistencia2;

                $porcentaje_trello = ($suma_de_horas/(($user->horas_totales)-($asistencia*($user->horas_totales/20))))*100;
                $user->asistencia = $asistencia;
                $user->suma_de_horas = $suma_de_horas;
                $user->trello = round($porcentaje_trello);
            }

            foreach ($areas as $key => $area) {
                $cont = 0;
                $total = 0;
                foreach ($asistencias as $key => $asistencia) {
                    if($asistencia->area_id == $area->id){
                        $total++;
                        if($asistencia->estado == 'M' || $asistencia->estado == 'T'){
                            $cont++;
                        }
                    }
                }
                $area->tardanza = $cont;
                $area->total    = $total;
            }

            foreach ($areas as $key => $area) {
                $cont = 0;
                $trello = 0;
                foreach ($usuarios as $key => $user) {
                    if($user->area_id == $area->id){
                        $trello = $user->trello + $trello;
                        $cont++;
                    }
                }
                $area->trello = round($trello/$cont);
                $area->personas = $cont;
            }

            foreach ($areas as $key => $area) {

                $puntos_extras = PuntosExtras::where('area_id',$area->id)->where('anio',$anio)->where('mes',$mes)->get();

                $puntaje_extra = 0;
                foreach ($puntos_extras as $key => $punto_extra) {
                    $puntaje_extra = $puntaje_extra+$punto_extra->puntaje;
                }
                $area->puntaje_extra = $puntaje_extra;

                $total_tardanzas_permitidas = $area->personas * 5;

                $area->puntaje_asistencia = 0;
                if($area->tardanza < $total_tardanzas_permitidas*0.95){
                    $area->puntaje_asistencia = 1;
                }if($area->tardanza < $total_tardanzas_permitidas*0.90){
                    $area->puntaje_asistencia = 2;
                }if($area->tardanza < $total_tardanzas_permitidas*0.85){
                    $area->puntaje_asistencia = 3;
                }if($area->tardanza < $total_tardanzas_permitidas*0.80){
                    $area->puntaje_asistencia = 4;
                }if($area->tardanza <  $total_tardanzas_permitidas){
                    $area->puntaje_asistencia = 5;
                }


                $area->puntaje_trello = 0;

                if($area->trello > 80){
                    $area->puntaje_trello = 1;
                }if($area->tardanza > 90){
                    $area->puntaje_trello = 2;
                }if($area->tardanza > 100){
                    $area->puntaje_trello = 3;
                }if($area->tardanza > 110){
                    $area->puntaje_trello = 4;
                }if($area->tardanza > 120){
                    $area->puntaje_trello = 5;
                }

                // $area->puntaje_total = $area->puntaje_trello + $area->puntaje_extra + $area->puntaje_asistencia;
                $area->puntaje_total =  $area->puntaje_extra ;
            }


            $areas = $areas->sortByDesc('puntaje_total');

            // $areas->values()->all();

            foreach ($areas2 as $key => $area2) {

                $puntos_extras = PuntosExtras::where('area_id',$area2->id)->where('anio',$anio)->where('mes',$mes)->get();

                $puntaje_extra = 0;
                foreach ($puntos_extras as $key => $punto_extra) {
                    $puntaje_extra = $puntaje_extra+$punto_extra->puntaje;
                }
                $area2->puntaje_extra = $puntaje_extra;

                $area2->puntaje_total = $area2->puntaje_extra;
            }
            $areas2 = $areas->sortByDesc('puntaje_total');

            setlocale(LC_TIME, 'es_ES');

            $fecha = DateTime::createFromFormat('!m', $mes);
            $mes = strftime("%B", $fecha->getTimestamp());

            foreach ($areas3 as $key => $area_suma) {
                $area_suma->suma_total = 0;
                foreach ($puntos as $key => $punto) {
                    if($area_suma->id == $punto->area_id){
                        $area_suma->suma_total = $area_suma->suma_total + $punto->puntaje;
                    }
                }
            }

            $areas3 = $areas3->sortByDesc('suma_total');

            $ganadores = array();
            foreach ($areas3 as $key => $area) {
                array_push($ganadores, array('name'=>$area->name,'puntaje'=>$area->suma_total));
            }
            // dd($ganadores);

            return view('frontend.puntajes')
                    ->with('areas',$areas)
                    ->with('areas2',$areas2)
                    ->with('mes',$mes)
                    ->with('n_mes',$n_mes)
                    ->with('ganadores',$ganadores)
                    ->with('usuarios',$usuarios);
        }
        public function loginfake($id){
            $usuario = User::find($id);
             \Auth::login($usuario);

             return redirect('/historial');
        }
        public function home(){
            //dd("ssss");
            return redirect('/');
        }
        public function inicio(){
            return view('frontend.login');
        }

        public function asistencia(){
            // $codigo = \Auth::user()->codigo;
            $id = \Auth::user()->id;
            $fecha = date('Y-m-d');
            $fec = date('Y-m').'%';

            $asistencia = Asistencia::where('fecha','=',$fecha)
                ->where('user_id','=',$id)
                ->first();

            // $getasis = DB::table('asistencia')
            //     ->select(DB::raw('Count(estado) as cant,estado'))
            //     ->where('fecha','like',$fec)
            //     ->where('user_id','=',$id)
            //     ->groupBy('estado')->get();
                
  
                
              
                        
                        Session::flash('status', 'G');
                   
                
            //dd($getasis);
            return view('frontend.asistencia')
                ->with('asistencia',$asistencia);
                // ->with('getasis',$getasis);
        }

            public function historial(){
            $reg_campo = collect();
            $naprobado = 0;
            $natrazadp = 0;
            if(\Auth::user()->tipo == 'jefe'){
                $idarea = \Auth::user()->area_id;

                $ticketwhite = Ticket::where('area_id','=',$idarea)
                        ->where('estado','=',10)
                        ->get();
                //dd($ticketwhite);

                $tickets = Ticket::where('area_id','=',$idarea)
                ->where('estado','!=',6)
                ->where('estado','!=',10)
                ->where('estado','!=',9)
                ->orderby('id','desc');
                $alltk = $tickets->get();
                $tickets = $tickets->paginate(10);

                foreach($alltk as $tk){
                    if($tk->estado == 3){
                        $natrazadp = $natrazadp + 1;
                    }
                    if($tk->estado == 4){
                        $naprobado = $naprobado + 1;
                    }
                }
                $reg_campo[] = array('natrazadp' => $natrazadp,'naprobado'=> $naprobado );
                $users = User::all();

                $getcolaboradores = User::where('area_id',$idarea)->get();
                $getclientes = ClienteUser::join('clientes','clientes.id','=','cliente_user.cliente_id')
                                ->where('clientes.activo','=',1)->groupBy('clientes.id')->get();

                return view('frontend.historial')
                ->with('getcolaboradores',$getcolaboradores)
                ->with('tickets',$tickets)
                ->with('ticketwhite',$ticketwhite)
                ->with('users',$users)
                ->with('getclientes',$getclientes)
                ->with('reg_campo',$reg_campo);
            }
            else if(\Auth::user()->tipo == 'gerente'){
                $id = \Auth::user()->id;
                $tickets = Ticket::where('estado','!=',6)
                    ->where('estado','!=',10);
                $alltk = Ticket::all();
                $tickets =  $tickets->orderBy('id','desc')->paginate(8);

                foreach($alltk as $tk){
                    if($tk->estado == 3){
                        $natrazadp = $natrazadp + 1;
                    }
                    if($tk->estado == 4){
                        $naprobado = $naprobado + 1;
                    }
                }
                $reg_campo[] = array('natrazadp' => $natrazadp,'naprobado'=> $naprobado );
                $colaboradores = User::where('tipo','colaborador')->get();

                $users = User::all();
                // $getcolaboradores = User::where('tipo','colaborador')->get();
                $getcolaboradores = User::all();
                $getclientes = Cliente::where('clientes.activo','=',1)->get();
                return view('frontend.historial')
                ->with('tickets',$tickets)
                ->with('colaboradores',$colaboradores)
                ->with('getcolaboradores',$getcolaboradores)
                ->with('users',$users)
                ->with('getclientes',$getclientes)
                ->with('reg_campo',$reg_campo);
            }
            else{
                //consultas los id de tus clientes
                $id = \Auth::user()->id;
                $tickets = ClienteUser::join('clientes as cl','cl.id','=','cliente_user.cliente_id')
                                        ->join('tickets as ti','ti.cliente_id','=','cl.id')
                                        ->where('cliente_user.user_id', $id)
                                        ->where('ti.estado','!=',6)
                                        ->where('ti.estado','!=',10)
                                        ->select('ti.id','ti.estado','ti.fecha_inicio','ti.fecha_fin','ti.nombre','ti.user_id','ti.area_id','cl.nombre as cliente','ti.user_create')
                                        ->orderBy('ti.id','desc');
                $alltk = $tickets->get();
                $tickets = $tickets->paginate(8);
                // dd($tickets);
                foreach($alltk as $tk){
                    if($tk->estado == 3){
                        $natrazadp = $natrazadp + 1;
                    }
                    if($tk->estado == 4){
                        $naprobado = $naprobado + 1;
                    }
                }
                $reg_campo[] = array('natrazadp' => $natrazadp,'naprobado'=> $naprobado );
                $colaboradores = User::where('tipo','colaborador')->get();

                $getcolaboradores = User::where('tipo','colaborador')->get();

                $getclientes = ClienteUser::join('clientes','clientes.id','=','cliente_user.cliente_id')
                                            ->where('clientes.activo','=',1)->groupBy('clientes.id')->get();
                $users = User::all();
                $areas = Areas::all();
                //dd($tickets);
                return view('frontend.historial')
                ->with('tickets',$tickets)
                ->with('colaboradores',$colaboradores)
                ->with('getcolaboradores',$getcolaboradores)
                ->with('areas',$areas)
                ->with('users',$users)
                ->with('getclientes',$getclientes)
                ->with('reg_campo',$reg_campo);

                //$tickets = Ticket::where('cliente_id',$id)->orderby('id','desc')->paginate(8);
            }
            //dd($tickets->cliente->nombre);
        }
          
        public function reuniones(){
            $idarea = \Auth::user()->area_id;
            $reuniones = Idreunion::orderby('id','desc')->paginate(20);
            $users = User::all();

            return view('frontend.reuniones')
            ->with('reuniones',$reuniones)
            ->with('users',$users);

        }

        public function postcolaborador(Request $request){
            $ticket = Ticket::find($request->get_asignar);
            $ticket->user_id = $request->iduserc;
            $ticket->save();

            Session::flash('status', 'asignado');
            return back();

        }

        public function informes(){

            return view('frontend.informes');

        }

        public function documentos(){

            // $idarea = \Auth::user()->area_id;
            $documentos = Asistencia::where('estado','=','V')
                ->orWhere('estado','=','J')
                ->orderby('id','desc')->paginate(20);
            $users = User::all();

            return view('frontend.documentos')
            ->with('documentos',$documentos)
            ->with('users',$users);

        }

        public function ordenar($id){
            $ide = $id;
            $reg_campo = collect();
            $naprobado = 0;
            $natrazadp = 0;
            if(\Auth::user()->tipo == 'jefe'){
                $idarea = \Auth::user()->area_id;
                $tickets = Ticket::where('area_id','=',$idarea);
                $alltk = $tickets->get();
                $tickets = $tickets->where('estado','=',$ide)
                ->orderby('id','desc')
                ->paginate(8);

                foreach($alltk as $tk){
                    if($tk->estado == 3){
                        $natrazadp = $natrazadp + 1;
                    }
                    if($tk->estado == 4){
                        $naprobado = $naprobado + 1;
                    }
                }
                $reg_campo[] = array('natrazadp' => $natrazadp,'naprobado'=> $naprobado );
                $users = User::all();

                $getcolaboradores = User::where('area_id','=',$idarea)->get();
                $getclientes = ClienteUser::join('clientes','clientes.id','=','cliente_user.cliente_id')
                                ->where('clientes.activo','=',1)->groupBy('clientes.id')->get();

                return view('frontend.historial')
                ->with('tickets',$tickets)
                ->with('getcolaboradores',$getcolaboradores)
                ->with('users',$users)
                ->with('getclientes',$getclientes)
                ->with('reg_campo',$reg_campo);
            }
            else if(\Auth::user()->tipo == 'gerente'){
                $id = \Auth::user()->id;
                $tickets = Ticket::where('estado','=',$ide)
                ->orderBy('id','desc')->paginate(8);
                $colaboradores = User::where('tipo','colaborador')->get();
                $alltk = Ticket::all();
                // $getcolaboradores = User::where('tipo','colaborador')->get();
                foreach($alltk as $tk){
                    if($tk->estado == 3){
                        $natrazadp = $natrazadp + 1;
                    }
                    if($tk->estado == 4){
                        $naprobado = $naprobado + 1;
                    }
                }
                $reg_campo[] = array('natrazadp' => $natrazadp,'naprobado'=> $naprobado );
                $users = User::all();

                $getcolaboradores = User::all();
                $getclientes = Cliente::where('activo','=',1)->get();

                return view('frontend.historial')
                ->with('tickets',$tickets)
                ->with('colaboradores',$colaboradores)
                ->with('getcolaboradores',$getcolaboradores)
                ->with('users',$users)
                ->with('getclientes',$getclientes)
                ->with('reg_campo',$reg_campo);
            }
            else{
                //consultas los id de tus clientes
                $id = \Auth::user()->id;
                $tickets = ClienteUser::join('clientes as cl','cl.id','=','cliente_user.cliente_id')
                    ->join('tickets as ti','ti.cliente_id','=','cl.id')
                    ->where('cliente_user.user_id', $id);

                $alltk = $tickets->get();

                $tickets = $tickets->where('estado','=',$ide)
                            ->select('ti.id','ti.estado','ti.fecha_inicio','ti.fecha_fin','ti.nombre','ti.user_id','cl.nombre as cliente','ti.user_create')
                            ->orderBy('ti.id','desc')
                            ->paginate(8);

                foreach($alltk as $tk){
                    if($tk->estado == 3){
                        $natrazadp = $natrazadp + 1;
                    }
                    if($tk->estado == 4){
                        $naprobado = $naprobado + 1;
                    }
                }
                $reg_campo[] = array('natrazadp' => $natrazadp,'naprobado'=> $naprobado );

                $colaboradores = User::where('tipo','colaborador')->get();
                $getcolaboradores = User::where('tipo','colaborador')->get();
                $getclientes = ClienteUser::join('clientes','clientes.id','=','cliente_user.cliente_id')
                                            ->where('clientes.activo','=',1)->groupBy('clientes.id')->get();

                $users = User::all();
                $areas = Areas::all();
                // dd($tickets);
                return view('frontend.historial')
                ->with('tickets',$tickets)
                ->with('colaboradores',$colaboradores)
                ->with('getcolaboradores',$getcolaboradores)
                ->with('areas',$areas)
                ->with('users',$users)
                ->with('getclientes',$getclientes)
                ->with('reg_campo',$reg_campo);

                //$tickets = Ticket::where('cliente_id',$id)->orderby('id','desc')->paginate(8);
            }
            //dd($tickets->cliente->nombre);

        }
        public function filtro_c(Request $request, $colaborador, $filtro, $orden=null){
            // dd($filtro);
            $idc = $filtro;
            $orden =  $orden;
            $mensaje_filtro = "";
            $reg_campo = collect();
            $naprobado = 0;
            $natrazadp = 0;
            if(\Auth::user()->tipo == 'jefe'){
                $idarea = \Auth::user()->area_id;
                $tickets = Ticket::where('area_id','=',$idarea);
                if($colaborador == 'colaborador'){
                    if(isset($idc) && $idc!=""){
                        $userr = User::find($idc);
                        $mensaje_filtro = 'Colaborador: '.$userr->name;
                        $tickets = $tickets->where('user_id','=',$idc);
                        $alltk = $tickets->get();
                    }
                }
                if($colaborador == 'cliente'){
                    if(isset($idc) && $idc!=""){
                        $client = Cliente::find($idc);
                        $mensaje_filtro ='Cliente: '.$client->nombre;
                        $tickets = $tickets->where('cliente_id','=',$idc);
                        $alltk = $tickets->get();
                    }
                }
                if($colaborador == 'fecha'){
                    if(isset($idc) && $idc!=""){
                        $mensaje_filtro = 'Fecha: '.$idc;
                        $fecha = explode(' ',$idc);
                        if($fecha[0] == $fecha[2]){
                            $tickets = $tickets->whereDate('fecha_inicio','=',$fecha[0]);
                        }
                        else{
                            $tickets = $tickets->whereBetween('fecha_inicio', [$fecha[0], $fecha[2]]);
                        }
                        $alltk = $tickets->get();
                    }
                }
                if(isset($orden) && $orden!=""){
                    $tickets = $tickets->where('estado','=',$orden);
                }
                $tickets = $tickets->orderby('id','desc')
                            ->paginate(8);

                foreach($alltk as $tk){
                    if($tk->estado == 3){
                        $natrazadp = $natrazadp + 1;
                    }
                    if($tk->estado == 4){
                        $naprobado = $naprobado + 1;
                    }
                }
                $reg_campo[] = array('natrazadp' => $natrazadp,'naprobado'=> $naprobado );
                $users = User::all();

                $getcolaboradores = User::where('area_id','=',$idarea)->get();
                $getclientes = ClienteUser::join('clientes','clientes.id','=','cliente_user.cliente_id')
                                ->where('clientes.activo','=',1)->groupBy('clientes.id')->get();

                return view('frontend.historial')
                ->with('tickets',$tickets)
                ->with('getcolaboradores',$getcolaboradores)
                ->with('users',$users)
                ->with('getclientes',$getclientes)
                ->with('mensaje_filtro',$mensaje_filtro)
                ->with('activeco',$colaborador)
                ->with('reg_campo',$reg_campo);
            }
            else if(\Auth::user()->tipo == 'gerente'){
                if($colaborador == 'colaborador'){
                    if(isset($idc) && $idc!=""){
                        $userr = User::find($idc);
                        $mensaje_filtro = 'Colaborador: '.$userr->name;
                        $tickets = Ticket::where('user_id','=',$idc);
                        $alltk = $tickets->get();
                    }
                }
                if($colaborador == 'cliente'){
                    if(isset($idc) && $idc!=""){
                        $client = Cliente::find($idc);
                        $mensaje_filtro ='Cliente: '.$client->nombre;
                        $tickets = Ticket::where('cliente_id','=',$idc);
                        $alltk = $tickets->get();
                    }
                }
                if($colaborador == 'fecha'){
                    if(isset($idc) && $idc!=""){
                        $mensaje_filtro = 'Fecha: '.$idc;
                        $fecha = explode(' ',$idc);
                        if($fecha[0] == $fecha[2]){
                            $tickets = Ticket::whereDate('fecha_inicio','=',$fecha[0]);
                        }
                        else{
                            $tickets = Ticket::whereBetween('fecha_inicio', [$fecha[0], $fecha[2]]);
                        }
                        $alltk = $tickets->get();
                    }
                }
                if(isset($orden) && $orden!=""){
                    $tickets = $tickets->where('estado','=',$orden);
                }
                $tickets = $tickets->orderBy('id','desc')->paginate(8);
                $colaboradores = User::where('tipo','colaborador')->get();
                // $getcolaboradores = User::where('tipo','colaborador')->get();

                $getcolaboradores = User::all();
                $getclientes = Cliente::where('activo','=',1)->get();
                //dd($tickets);
                foreach($alltk as $tk){
                    if($tk->estado == 3){
                        $natrazadp = $natrazadp + 1;
                    }
                    if($tk->estado == 4){
                        $naprobado = $naprobado + 1;
                    }
                }
                $reg_campo[] = array('natrazadp' => $natrazadp,'naprobado'=> $naprobado );
                $users = User::all();

                return view('frontend.historial')
                ->with('tickets',$tickets)
                ->with('colaboradores',$colaboradores)
                ->with('getcolaboradores',$getcolaboradores)
                ->with('users',$users)
                ->with('mensaje_filtro',$mensaje_filtro)
                ->with('getclientes',$getclientes)
                ->with('activeco',$colaborador)
                ->with('reg_campo',$reg_campo);
            }
            else{
                //consultas los id de tus clientes
                $id = \Auth::user()->id;
                $tickets = ClienteUser::join('clientes as cl','cl.id','=','cliente_user.cliente_id')
                                        ->join('tickets as ti','ti.cliente_id','=','cl.id')
                                        ->where('cliente_user.user_id', $id);

                if($colaborador == 'colaborador'){
                    if(isset($idc) && $idc!=""){
                        $userr = User::find($idc);
                        $mensaje_filtro = 'Colaborador: '.$userr->name;
                        $tickets = $tickets->where('ti.user_id','=',$idc);
                        $alltk = $tickets->get();
                    }
                }
                if($colaborador == 'cliente'){
                    if(isset($idc) && $idc!=""){
                        $client = Cliente::find($idc);
                        $mensaje_filtro ='Cliente: '.$client->nombre;
                        $tickets = $tickets->where('ti.cliente_id','=',$idc);
                        $alltk = $tickets->get();
                    }
                }
                if($colaborador == 'fecha'){
                    if(isset($idc) && $idc!=""){
                        $mensaje_filtro = 'Fecha: '.$idc;
                        $fecha = explode(' ',$idc);
                        if($fecha[0] == $fecha[2]){
                            $tickets = $tickets->whereDate('ti.fecha_inicio','=',$fecha[0]);
                        }
                        else{
                            $tickets = $tickets->whereBetween('ti.fecha_inicio', [$fecha[0], $fecha[2]]);
                        }
                        $alltk = $tickets->get();
                    }
                }
                if(isset($orden) && $orden!=""){
                    $tickets = $tickets->where('estado','=',$orden);
                }
                $tickets = $tickets->select('ti.id','ti.estado','ti.fecha_inicio','ti.fecha_fin','ti.nombre','ti.user_id','cl.nombre as cliente','ti.user_create')
                                    ->orderBy('ti.id','desc')
                                    ->paginate(8);
                foreach($alltk as $tk){
                    if($tk->estado == 3){
                        $natrazadp = $natrazadp + 1;
                    }
                    if($tk->estado == 4){
                        $naprobado = $naprobado + 1;
                    }
                }
                $reg_campo[] = array('natrazadp' => $natrazadp,'naprobado'=> $naprobado );

                $colaboradores = User::where('tipo','colaborador')->get();
                $getcolaboradores = User::where('tipo','colaborador')->get();
                $getclientes = ClienteUser::join('clientes','clientes.id','=','cliente_user.cliente_id')
                                            ->where('clientes.activo','=',1)->groupBy('clientes.id')->get();

                $users = User::all();
                $areas = Areas::all();
                // dd($tickets);
                return view('frontend.historial')
                ->with('tickets',$tickets)
                ->with('colaboradores',$colaboradores)
                ->with('getcolaboradores',$getcolaboradores)
                ->with('areas',$areas)
                ->with('users',$users)
                ->with('mensaje_filtro',$mensaje_filtro)
                ->with('getclientes',$getclientes)
                ->with('activeco',$colaborador)
                ->with('reg_campo',$reg_campo);
            }
            //dd($tickets->cliente->nombre);
        }

        public function detalle_tiket($id){

            $ticket = Ticket::find($id);
            return view('frontend.detalle_tiket')
                    ->with('ticket',$ticket);
        }

        public function detalle_reunion($id){

            $reunion = Idreunion::find($id);

            $colaboradores = Reunion::join('users','reuniones.user_id','=','users.id')
                    ->where('reuniones.id_reunion','=',$reunion->id)
                    ->select('users.name','reuniones.id','reuniones.id_reunion')
                    ->get();
            $users = User::all();

            //dd($colaboradores);
            return view('frontend.detalle_reunion')
                    ->with('colaboradores',$colaboradores)
                    ->with('users',$users)
                    ->with('reunion',$reunion);

        }

        public function detalle_documento($id){

            $documento = Asistencia::find($id);
            $users = User::all();

            //dd($colaboradores);
            return view('frontend.detalle_documento')
                    ->with('users',$users)
                    ->with('documento',$documento);

        }

        public function pausar_tk($id){
            $ticket = Ticket::find($id);
            $ticket->estado = 10;
            $ticket->save();
            //al historial
            $ticket_hist = TiketHistorial::create(['id_tiket' => $ticket->id, 'nombre'=>$ticket->nombre,'descripcion'=>$ticket->descripcion, 'cliente_id'=>$ticket->cliente_id,'user_id'=>$ticket->user_id,'area_id'=>$ticket->area_id, 'user_create' => $ticket->user_create]);
            $ticket_hist->estado = 10;
            $ticket_hist->observacion = "Pausado";
            $ticket_hist->horas_pedido = $ticket->horas_pedido;
            $ticket_hist->horas_supervision = $ticket->horas_supervision;
            $fechaf = date('Y-m-d H:i:s');
            //$fecha = date('Y-m-d');
            $ticket_hist->fecha_inicio = $ticket->fecha_inicio;
            $ticket_hist->fecha_fin = $fechaf;
            $ticket_hist->save();

            Session::flash('status', 'pausa');
            return back();
        }

        public function play_tk($id){
            $ticket = Ticket::find($id);
            //obtenemos el resultado max
            $horafin = TiketHistorial::where('id_tiket','=',$id)
                        ->orderby('id', 'desc')
                        ->first();

            $horagg = date('Y-m-d H:i:s');
            $hora = date("H", strtotime($horagg));
            //dd($hora);
            $nfechainicio = date ( 'Y-m-d');
            $fecha_inicio = $nfechainicio." ".$hora.":00:00";

            $fecha1 = new DateTime($horafin->fecha_inicio);//fecha inicial
            $fecha2 = new DateTime($horafin->fecha_fin);//fecha de cierre

            $intervalo = $fecha1->diff($fecha2);
            $menosh = $intervalo->format('%H');

            $hdpf = ($horafin->horas_pedido) - $menosh;

            $fechas_p_s = ($horafin->horas_pedido+$horafin->horas_supervision) - $menosh;
            //$fechas_p_s = $hdpf;


            //calculamos el nueva fecha fin
            if(\Auth::user()->tipo == 'jefe'){

                $fechas_p_s = $fechas_p_s;
                //obtenemos dia y hora
                $ghora = $fechas_p_s % 9;
                $gdias = intval($fechas_p_s / 9);

                $canthoras = $fechas_p_s;
                $finicio = $fecha_inicio;
                $fecha = strtotime($fecha_inicio);
                $horainicio = (int)date('H', $fecha);
                $fecha_int_d = (int)date('d', $fecha);
                $total = $horainicio+$fechas_p_s;

                $countdia = 0;
                 //preguntamos el dia
                switch (date('w', $fecha)){
                    case 0: $dia = 0; break;
                    case 1: $dia = 1; break;
                    case 2: $dia = 2; break;
                    case 3: $dia = 3; break;
                    case 4: $dia = 4; break;
                    case 5: $dia = 5; break;
                    case 6: $dia = 6; break;
                }
                if($total > 19){
                    if($horainicio < 13){
                    $desc = 1;
                    }
                    else{
                        $desc = 0;
                    }

                    $hfalta = 19 - $horainicio -$desc;

                    $canthorasfaltan = $canthoras - $hfalta;

                    if($dia == 5){
                        $dia = 0;
                        $countdia++;
                        $countdia++;
                    }
                    $dia = $dia +1;
                    $countdia++;

                    //dd($dia);
                    while($canthorasfaltan >= 10){
                        if($dia == 5){
                            $dia = 0;
                            $countdia++;
                            $countdia++;
                        }
                        $dia = $dia +1;
                        $countdia++;
                        $canthorasfaltan = $canthorasfaltan-9;
                    }
                    $horafinal = 9 + $canthorasfaltan;
                    //dd($horafinal);
                    if($horafinal > 13){
                        $horafinal = $horafinal + 1;
                    }
                }
                else{
                    if($horainicio < 13 & $total > 13){
                        $desc = 1;
                    }
                    else{
                        $desc = 0;
                    }

                    $horafinal = $total + $desc;
                }

                $nuevafecha = strtotime ( '+'.$countdia.' day' , strtotime ($fecha_inicio ) ) ;
                $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                $nfds = $nuevafecha." ".$horafinal.":00:00";
            }
            //dd($nfds);
            //dd($fechas_p_s);
            $ticket->estado = 1;
            $ticket->fecha_fin = $nfds;
            $ticket->save();
            //creamos un nuevo historial
            $ticket_hist = TiketHistorial::create(['id_tiket' => $ticket->id, 'nombre'=>$ticket->nombre,'descripcion'=>$ticket->descripcion, 'cliente_id'=>$ticket->cliente_id,'user_id'=>$ticket->user_id,'area_id'=>$ticket->area_id, 'user_create' => $ticket->user_create]);
            $ticket_hist->estado = 1;
            $ticket_hist->observacion = "Play";

            $ticket_hist->fecha_inicio = $ticket->fecha_inicio;
            $ticket_hist->fecha_fin = $nfds;
            $ticket_hist->horas_pedido = $hdpf;
            $ticket_hist->save();
            //dd($nfds);

            Session::flash('status', 'play');
            return back();
        }

        public function eliminar($id){
            $ticket = Ticket::find($id);
            // $ticket->estado = 6;
            // $ticket->save();

            //al historial
            $ticket_hist = TiketHistorial::create(['id_tiket' => $ticket->id, 'nombre'=>$ticket->nombre,'descripcion'=>$ticket->descripcion, 'cliente_id'=>$ticket->cliente_id,'user_id'=>$ticket->user_id,'area_id'=>$ticket->area_id, 'user_create' => $ticket->user_create]);
            $ticket_hist->estado = 6;
            $ticket_hist->observacion = "Eliminado";
            $ticket_hist->save();

            $ticket->delete();
            Session::flash('status', 'eliminado');
            return back();
        }

        public function delete_documento($id){
            $documento = Asistencia::find($id);
            //$documento->delete();
            if($documento->delete()){
                Session::flash('status', 'exito');
            }
            else{
                Session::flash('status', 'error');
            }
            return back();
        }

        public function create_asistencia(Request $request){

            // $codigo = \Auth::user()->codigo;
            $id = \Auth::user()->id;
        
            $getip = '209.45.57.2';
            $mostrar_combo = 1;
            $asistencia = '';


            $tipo = 1;
            
            // if($request->codigo == $codigo){
                $hora = date('H:i:s a');
                $fecha = date('Y-m-d');
                        
                $fff = strtotime($fecha);
                $dd = date('w', $fff);

                $fec = date('Y-m').'%';
                    
                //crear asistencia
                $asistencia = Asistencia::create();
                $asistencia->estado = 'G';
                $asistencia->hora_inicio = $hora;
                $asistencia->hora_inicio_real ='09:00:00 am';
                $asistencia->user_id = $id;
                $asistencia->fecha = $fecha;
                $asistencia->tipo = $tipo;
                $asistencia->save();

                $getasis = DB::table('asistencia')
                ->select(DB::raw('Count(estado) as cant,estado'))
                ->where('fecha','like',$fec)
                ->where('user_id','=',$id)
                ->groupBy('estado')->get();
                Session::flash('status', 'G');
                    
            // }
            // else{
                //Session::flash('status', 'X');
                //return back();
            // }
            

           return  view('frontend.asistencia',compact('asistencia','getasis'));
        }
    
        public function create_ticket(Request $request){
            //dd("olas");
            if(\Auth::user()->tipo == 'jefe'){
                $fechas_p_s = $request->horas_pedido+$request->horas_supervision;
                //obtenemos dia y hora
                $ghora = $fechas_p_s % 9;
                $gdias = intval($fechas_p_s / 9);
                $canthoras = $fechas_p_s;
                $finicio = $request->fecha_inicio;
                $fecha = strtotime($request->fecha_inicio);
                $horainicio = (int)date('H', $fecha);
                $fecha_int_d = (int)date('d', $fecha);
                $total = $horainicio+$fechas_p_s;
                //dd($total );
                $countdia = 0;  
                 //preguntamos el dia
                switch (date('w', $fecha)){
                    case 0: $dia = 0; break;
                    case 1: $dia = 1; break;
                    case 2: $dia = 2; break;
                    case 3: $dia = 3; break;
                    case 4: $dia = 4; break;
                    case 5: $dia = 5; break;
                    case 6: $dia = 6; break;
                }
                if($total >= 19){
                    if($horainicio < 13){
                    $desc = 1;
                    }
                    else{
                        $desc = 0;
                    }

                    $hfalta = 19 - $horainicio -$desc;

                    $canthorasfaltan = $canthoras - $hfalta;

                    if($dia == 5){
                        $dia = 0;
                        $countdia++;
                        $countdia++;
                    }
                    $dia = $dia +1;
                    $countdia++;

                    //dd($dia);
                    while($canthorasfaltan >= 10){
                        if($dia == 5){
                            $dia = 0;
                            $countdia++;
                            $countdia++;
                        }
                        $dia = $dia +1;
                        $countdia++;
                        $canthorasfaltan = $canthorasfaltan-9;
                    }
                    $horafinal = 9 + $canthorasfaltan;
                    if($horafinal > 13){
                        $horafinal = $horafinal + 1;
                    }
                }
                else{
                    if($horainicio < 13 & $total > 13){
                        $desc = 1;
                    }
                    else{
                        $desc = 0;
                    }

                    $horafinal = $total + $desc;
                }

                $nuevafecha = strtotime ( '+'.$countdia.' day' , strtotime ($request->fecha_inicio ) ) ;
                $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                $nfds = $nuevafecha." ".$horafinal.":00:00";
            }
            if($request->idtk){
                //$ticket = Ticket::find($request->idtk);
                $ticket = Ticket::find($request->idtk);
                $ticket->fill($request->all());
                if(\Auth::user()->tipo == 'jefe'){
                    $fechapp = date('Y-m-d H:i:s');
                    if(strtotime($fechapp) >= strtotime($request->fecha_inicio) ){
                        $hact = 1;
                    }
                    else{
                        $hact = 5;
                    }
                    $idarea = \Auth::user()->area_id;
                    $ticket->estado = $hact;
                    $ticket->fecha_fin = $nfds;
                    $ticket->area_id = $idarea;
                }
                $ticket->save();
                Session::flash('status', 'Ticket actualizado');
            }
            else{
                //preguntamos si hay ticket existente en ese rango de horas
                    $antfecha = strtotime ( '-1 day' , strtotime ($request->fecha_inicio ) ) ;
                    $dia = date("Y-m-d",strtotime($request->fecha_inicio));
                    $si = 0;
                    $getticketfecha = Ticket::where('fecha_inicio','like',$dia.'%')
                                    ->where('user_id','=',$request->user_id)
                                    ->whereIn('estado',[1,5])
                                    ->get();

                    foreach($getticketfecha as $gettiketf){
                        $gfi = strtotime($gettiketf->fecha_inicio);
                        $gff = strtotime($gettiketf->fecha_fin);
                        $fhoy = strtotime($request->fecha_inicio);
                        if($gfi <= $fhoy and $fhoy < $gff){
                            $si = 1;
                            break;
                        }
                    }

                    if($si == 1){
                        Session::flash('status', 'existe');
                        return back();
                    }
                //fin de si existe
                $ticket = Ticket::create($request->all());
                $ticket_hist = TiketHistorial::create($request->all());
                if(\Auth::user()->tipo == 'jefe'){
                    $fechapp = date('Y-m-d H:i:s');
                    if(strtotime($fechapp) >= strtotime($request->fecha_inicio) ){
                        $hact = 1;
                    }
                    else{
                        $hact = 5;
                    }
                    $idarea = \Auth::user()->area_id;
                    $ticket->estado = $hact;
                    $ticket->fecha_fin = $nfds;
                    $ticket->area_id = $idarea;
                    $ticket->save();


                    $ticket_hist->id_tiket = $ticket->id;
                    $ticket_hist->estado = $hact;
                    $ticket_hist->fecha_fin = $nfds;
                    $ticket_hist->area_id = $idarea;
                    $ticket_hist->save();
                }
                else{
                    $ticket_hist->id_tiket = $ticket->id;
                    $ticket_hist->save();
                }
                Session::flash('status', 'Creado exitosamente');
            }

            return back();
        }

        public function create_ticket2(Request $request){
            //dd("olas");
            if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente'){

                $fechas_p_s = $request->horas_pedido+$request->horas_supervision;
                //obtenemos dia y hora
                $ghora = $fechas_p_s % 9;
                $gdias = intval($fechas_p_s / 9);

                $canthoras = $fechas_p_s;
                $finicio = $request->fecha_inicio;
                $fecha = strtotime($request->fecha_inicio);
                $horainicio = (int)date('H', $fecha);
                $fecha_int_d = (int)date('d', $fecha);
                $total = $horainicio+$fechas_p_s;

                $countdia = 0;
                 //preguntamos el dia
                switch (date('w', $fecha)){
                    case 0: $dia = 0; break;
                    case 1: $dia = 1; break;
                    case 2: $dia = 2; break;
                    case 3: $dia = 3; break;
                    case 4: $dia = 4; break;
                    case 5: $dia = 5; break;
                    case 6: $dia = 6; break;
                }
                if($total > 19){
                    if($horainicio < 13){
                    $desc = 1;
                    }
                    else{
                        $desc = 0;
                    }

                    $hfalta = 19 - $horainicio -$desc;

                    $canthorasfaltan = $canthoras - $hfalta;

                    if($dia == 5){
                        $dia = 0;
                        $countdia++;
                        $countdia++;
                    }
                    $dia = $dia +1;
                    $countdia++;

                    //dd($dia);
                    while($canthorasfaltan >= 10){
                        if($dia == 5){
                            $dia = 0;
                            $countdia++;
                            $countdia++;
                        }
                        $dia = $dia +1;
                        $countdia++;
                        $canthorasfaltan = $canthorasfaltan-9;
                    }
                    $horafinal = 9 + $canthorasfaltan;
                    //dd($horafinal);
                    if($horafinal > 13){
                        $horafinal = $horafinal + 1;
                    }
                }
                else{
                    if($horainicio < 13 & $total > 13){
                        $desc = 1;
                    }
                    else{
                        $desc = 0;
                    }

                    $horafinal = $total + $desc;
                }

                $nuevafecha = strtotime ( '+'.$countdia.' day' , strtotime ($request->fecha_inicio ) ) ;
                $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
                $nfds = $nuevafecha." ".$horafinal.":00:00";
            }

            if($request->idtk){
                //$ticket = Ticket::find($request->idtk);
                $ticket = Ticket::find($request->idtk);
                $ticket->fill($request->all());
                if(\Auth::user()->tipo == 'jefe'){
                    $fechapp = date('Y-m-d H:i:s');

                    $idarea = \Auth::user()->area_id;
                    $ticket->estado = 2;
                    $ticket->fecha_fin = $nfds;
                    $ticket->area_id = $idarea;
                }
                $ticket->save();
                Session::flash('status', 'Tiket actualizado');
            }
            else{
                $dia = date("Y-m-d",strtotime($request->fecha_inicio));//5
                $hora = date("H", strtotime($request->fecha_inicio));
                $horaf = date("H", strtotime($nfds));
                $dd = $horaf-$hora;
                $si = 0;
                // aqui preguntamos si son horas extras
                    if($hora >= 19){
                        // comparamos que no pase de las 24horas
                        $dd = $hora + $request->horas_pedido;

                        if($dd <= 24){
                            //preguntamos si el ticket existe
                            for ($i = $hora; $i <= ($dd-1) ; $i++) {
                                $ff = $dia." ".$i.":00:00";
                                $existe = Ticket::where('fecha_inicio','=',$ff)
                                                ->where('user_id','=',$request->user_id)
                                                ->get();
                                if(count($existe) > 0){
                                    $si = 1;
                                    break;
                                }
                            }
                            if($si == 1){
                                Session::flash('status', 'existe_extra');
                                return back();
                            }
                            $ticket = Ticket::create($request->all());
                            $ticket_hist = TiketHistorial::create($request->all());
                            if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente'){
                                $fechapp = date('Y-m-d H:i:s');
                                if($dd == 24){
                                    $dd = 00;
                                }
                                $ffinextra = date('Y-m-d' , strtotime($request->fecha_inicio));
                                $ffinextra = $ffinextra." ".$dd.":00:00";

                                $hact = 2;
                                $idarea = \Auth::user()->area_id;
                                $ticket->estado = $hact;
                                $ticket->fecha_fin = $ffinextra;
                                $ticket->observacion = $request->descripcion;
                                $ticket->area_id = $idarea;
                                $ticket->save();

                                $ticket_hist->id_tiket = $ticket->id;
                                $ticket_hist->estado = $hact;
                                $ticket_hist->fecha_fin = $ffinextra;
                                $ticket_hist->observacion = $request->descripcion;
                                $ticket_hist->area_id = $idarea;
                                $ticket_hist->save();
                            }
                            else{
                                $ticket_hist->id_tiket = $ticket->id;
                                $ticket_hist->save();
                            }
                            Session::flash('status', 'add_extra');
                            return back();
                        }
                        else{
                            Session::flash('status', 'excedio_extra');
                            return back();
                        }
                    }
                //comprobamos que no pase de las 18 horas
                $ffin = $hora + ($request->horas_pedido -1);
                if($hora < 13 and ($hora+$dd) > 13){
                    $ffin = $ffin + 1;
                }
                if($ffin > 18){
                    Session::flash('status', 'excedio');
                    return back();
                }
                //preguntamos si el ticket existe
                for ($i = $hora; $i <= ($horaf-1) ; $i++) {
                    $ff = $dia." ".$i.":00:00";
                    $existe = Ticket::where('fecha_inicio','=',$ff)
                                    ->where('user_id','=',$request->user_id)
                                    ->get();
                    if(count($existe) > 0){
                        $si = 1;
                        break;
                    }
                }
                if($si == 1){
                    Session::flash('status', 'existe');
                    return back();
                }
                //dd("creartiket");
                $ticket = Ticket::create($request->all());
                $ticket_hist = TiketHistorial::create($request->all());
                if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente'){
                    $fechapp = date('Y-m-d H:i:s');
                    $idarea = \Auth::user()->area_id;
                    $ticket->estado = 2;
                    $ticket->fecha_fin = $nfds;
                    $ticket->area_id = $idarea;
                    $ticket->save();

                    $ticket_hist->id_tiket = $ticket->id;
                    $ticket_hist->estado = 2;
                    $ticket_hist->fecha_fin = $nfds;
                    $ticket_hist->area_id = $idarea;
                    $ticket_hist->save();
                }
                else{
                    $ticket_hist->id_tiket = $ticket->id;
                    $ticket_hist->save();
                }
                Session::flash('status', 'Creado exitosamente');
            }

            return back();
        }

        public function crear_documento(){
            //$colaboradores = User::all();
            $colaboradores = User::where('estado','=',1)
                            ->get();

            return view('frontend.documento')
                ->with('colaboradores',$colaboradores);
        }

        public function create_documento(Request $request){

            $tipo = $request->tipo;
            $idu = $request->user_id;
            $fi = $request->fecha_inicio;
            $ff = $request->fecha_fin;
            $text = $request->descripcion;

            $f = "";
            $d = "";
            $n = 0;
            for($i=$fi;$i<=$ff;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                $asistencia = Asistencia::where('fecha','=',$i)
                    ->where('user_id','=',$idu)
                    ->first();
                $fecha = strtotime($i);
                switch (date('w', $fecha)){
                    case 0: $dia = 0; break;
                    case 1: $dia = 1; break;
                    case 2: $dia = 2; break;
                    case 3: $dia = 3; break;
                    case 4: $dia = 4; break;
                    case 5: $dia = 5; break;
                    case 6: $dia = 6; break;
                }
                if($dia != 6){
                    if($dia != 0){
                        if($asistencia){
                            if($asistencia->estado){
                                $f = $asistencia->fecha;
                                $n = 1;
                                break;
                            }
                            else{
                                $f .= $i."vacioi<br>";
                            }
                        }
                        else{
                            $f .= $i."vacio<br>";
                        }
                    }
                }
            }

            if($n == 0){
                //imprimir
                for($i=$fi;$i<=$ff;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                    $fecha = strtotime($i);
                    switch (date('w', $fecha)){
                        case 0: $dia = 0; break;
                        case 1: $dia = 1; break;
                        case 2: $dia = 2; break;
                        case 3: $dia = 3; break;
                        case 4: $dia = 4; break;
                        case 5: $dia = 5; break;
                        case 6: $dia = 6; break;
                    }
                    if($dia != 6){
                        if($dia != 0){
                            //crear asistencia
                            $asistencia = Asistencia::create();
                            $asistencia->estado = $tipo;
                            $asistencia->user_id = $idu;
                            $asistencia->fecha = $i;
                            $asistencia->descripcion = $text;
                            $asistencia->save();
                        }
                    }
                }
                Session::flash('status', 'exito');
            }
            else{
                //alerta
                Session::flash('status', 'error');
                Session::flash('dia', $f);
            }
            return back();
        }

        public function update_documento(Request $request){

            $tipo = $request->tipo;
            $ida = $request->iddoc;

            $documento = Asistencia::find($ida);
            //$ticket->fill($request->all());
            $documento->estado = $tipo;
            $documento->save();
            Session::flash('status', 'exito');
            return back();
        }

        public function crear_ticket(){
            if(\Auth::user()->tipo == 'jefe'){
               $clientes = Cliente::where('activo','=',1)->orderby("nombre")->get();
               $idarea = \Auth::user()->area_id;
               $areas = Areas::where('id','=',$idarea)->get();
            }
            else if(\Auth::user()->tipo == 'gerente'){
                $clientes = Cliente::where('activo','=',1)->orderby("nombre")->get();
                $areas = Areas::all();
            }
            else{
                $id = \Auth::id();
                $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
                    ->where('cliente_user.user_id','=',$id)
                    ->where('clientes.activo','=',1)
                    ->orderby("clientes.nombre")
                    ->get();
                $areas = Areas::all();
            }
            $hh = \Auth::user()->area_id;
            $colaboradores = User::where('area_id',$hh)
                            ->where('estado','=',1)
                            ->get();

            return view('frontend.tiket')
                ->with('clientes',$clientes)
                ->with('areas',$areas)
                ->with('colaboradores',$colaboradores);
        }

        public function crear_reunion(){
            if(\Auth::user()->tipo == 'jefe'){
            //    $clientes = Cliente::where('activo','=',1)->orderby("nombre")->get();
            $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
               $idarea = \Auth::user()->area_id;
               $areas = Areas::where('id','=',$idarea)->get();
            }
            else if(\Auth::user()->tipo == 'gerente'){
                // $clientes = Cliente::where('activo','=',1)->orderby("nombre")->get();
                $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
                $areas = Areas::all();
            }
            else{
                $id = \Auth::id();
                $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
                    ->where('cliente_user.user_id','=',$id)
                    ->where('clientes.activo','=',1)
                    ->orderby("clientes.nombre")
                    ->get();
                $areas = Areas::all();
            }
            // $hh = \Auth::user()->area_id;
            // $colaboradores = User::where('area_id',$hh)
            $colaboradores = User::where('estado','=',1)->orderBy('name','ASC')
                        ->get();

            return view('frontend.reunion')
                ->with('clientes',$clientes)
                ->with('areas',$areas)
                ->with('colaboradores',$colaboradores);
        }

        public function create_reunion(Request $request){
            //$cc = count($request->datajson);
            //dd($request);
            $reu = Idreunion::create($request->all());

            $reu->fecha_inicio = Date('Y').'-'.$request->fecha_inicio.':00';
            $reu->save();
            $idr = $reu->id;
            $data = json_decode($request->datajson);
            for ($i=0; $i < count($data); $i++) {
                $reunion = Reunion::create($request->all());
                $reunion->fecha_inicio = Date('Y').'-'.$request->fecha_inicio.':00';
                $reunion->user_id = $data[$i]->ii;
                $reunion->id_reunion = $idr;
                $reunion->save();
            }
            Session::flash('status', 'Creado exitosamente');
            return back();
        }
        public function edit_ticket($id){
            $ticket = Ticket::find($id);
            //dd($ticket);
            if(\Auth::user()->tipo == 'jefe'){
               $clientes = Cliente::all();
               $idarea = \Auth::user()->area_id;
               $areas = Areas::where('id','=',$idarea)->get();
            }
            else if(\Auth::user()->tipo == 'gerente'){
                $clientes = Cliente::all();
                $areas = Areas::all();
            }
            else{
                $id = \Auth::id();
                $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
                    ->where('cliente_user.user_id','=',$id)
                    ->get();
                $areas = Areas::all();
            }
            $hh = \Auth::user()->area_id;
            $colaboradores = User::where('area_id',$hh)->get();

            return view('frontend.edit_tiket')
                ->with('ticket',$ticket)
                ->with('clientes',$clientes)
                ->with('areas',$areas)
                ->with('colaboradores',$colaboradores);
        }

        public function edit_documento($id){
            $documento = Asistencia::find($id);
            $colaboradores = User::all();

            return view('frontend.edit_documento')
                ->with('documento',$documento)
                ->with('colaboradores',$colaboradores);
        }
        public function tiket_completo(){
            return view('frontend.tiket_completo');
        }

        public function asignar($id){
            $ticket = Ticket::find($id);
            $hh = \Auth::user()->area_id;
            $colaboradores = User::where('area_id',$hh)
                            ->where('estado','=',1)
                            ->get();

            return view('frontend.asignar')
                ->with('ticket',$ticket)
                ->with('colaboradores',$colaboradores);
        }

        public function culminar($id){
            $ticket = Ticket::find($id);
            $ticket->estado =2;
            $ticket->save();

            //al historial
            $ticket_hist = TiketHistorial::create(['id_tiket' => $id, 'nombre'=>$ticket->nombre,'descripcion'=>$ticket->descripcion, 'cliente_id'=>$ticket->cliente_id,'user_id'=>$ticket->user_id,'area_id'=>$ticket->area_id, 'user_create' => $ticket->user_create]);
            $ticket_hist->estado =2;
            $ticket_hist->fecha_inicio = $ticket->fecha_inicio;
            $ticket_hist->fecha_fin =  $ticket->fecha_fin;
            $ticket_hist->fecha_limite = $ticket->fecha_limite;
            $ticket_hist->horas_pedido = $ticket->horas_pedido;
            $ticket_hist->horas_supervision = $ticket->horas_supervision;
            $ticket_hist->save();

            $area = Areas::find($ticket->area_id);
            $user = User::find($ticket->user_id);
            $jefe = User::find($ticket->user_create);
            $cliente = Cliente::find($ticket->cliente_id);
            $fecha = date('Y-m-d H:i:s');
            $url = url('/');
            $title = $ticket->nombre;
            $clien = $cliente->nombre;
            if($jefe->tipo == 'gerente'){
                $too = $jefe->email;

                Mail::send('emails.tiketalerta', ['cliente'=>$cliente->nombre,'ticket'=>$ticket->nombre,'ticket'=>$ticket->nombre,'area'=>$area->name,'colaborador'=>$user->name,'jefe'=>$jefe->name,'fecha'=>$fecha,'url'=>$url], function ($message) use ($too,$title,$clien)
                {
                    $message->from('digital.mi@mediaimpact.pe', 'Ticket - MI');
                    $message->to($too);
                    //$message->bcc('genixxavier@gmail.com');
                    $message->subject($clien.' - '.$title);

                });
            }
            else{
                $too = ClienteUser::join('users', 'cliente_user.user_id', '=', 'users.id')
                    ->where('cliente_user.cliente_id','=',$ticket->cliente_id)
                    ->select('cliente_user.*', 'users.email')
                    ->get();

                foreach ($too as $tod) {
                    $too = $tod->email;
                   Mail::send('emails.tiketalerta', ['cliente'=>$cliente->nombre,'ticket'=>$ticket->nombre,'ticket'=>$ticket->nombre,'area'=>$area->name,'colaborador'=>$user->name,'jefe'=>$jefe->name,'fecha'=>$fecha,'url'=>$url], function ($message) use ($too,$title,$clien)
                    {
                        $message->from('digital.mi@mediaimpact.pe', 'Ticket - MI');
                        $message->to($too);
                        //$message->bcc('genixxavier@gmail.com');
                        $message->subject($clien.' - '.$title);

                    });
                }
            }



            return back();
        }

        public function culminarticket(Request $request){
            //dd($request);
            $id =  $request->ticketuser;
            $ticket = Ticket::find($id);
            $ticket->estado =2;
            $ticket->observacion = $request->observacion;
            $ticket->save();

            //al historial
            $ticket_hist = TiketHistorial::create(['id_tiket' => $id, 'nombre'=>$ticket->nombre,'descripcion'=>$ticket->descripcion, 'cliente_id'=>$ticket->cliente_id,'user_id'=>$ticket->user_id,'area_id'=>$ticket->area_id, 'user_create' => $ticket->user_create]);
            $ticket_hist->estado =2;
            $ticket_hist->observacion = $ticket->observacion;
            $ticket_hist->fecha_inicio = $ticket->fecha_inicio;
            $ticket_hist->fecha_fin =  $ticket->fecha_fin;
            $ticket_hist->fecha_limite = $ticket->fecha_limite;
            $ticket_hist->horas_pedido = $ticket->horas_pedido;
            $ticket_hist->horas_supervision = $ticket->horas_supervision;
            $ticket_hist->save();

            $area = Areas::find($ticket->area_id);
            $user = User::find($ticket->user_id);
            $jefe = User::find($ticket->user_create);
            $cliente = Cliente::find($ticket->cliente_id);
            $fecha = date('Y-m-d H:i:s');
            $url = url('/');
            $title = $ticket->nombre;
            $clien = $cliente->nombre;
            if($jefe->tipo == 'gerente'){
                $too = $jefe->email;

                Mail::send('emails.tiketalerta', ['cliente'=>$cliente->nombre,'ticket'=>$ticket->nombre,'ticket'=>$ticket->nombre,'area'=>$area->name,'colaborador'=>$user->name,'jefe'=>$jefe->name,'fecha'=>$fecha,'url'=>$url], function ($message) use ($too,$title,$clien)
                {
                    $message->from('digital.mi@mediaimpact.pe', 'Ticket - MI');
                    $message->to($too);
                    //$message->bcc('genixxavier@gmail.com');
                    $message->subject($clien.' - '.$title);

                });
            }
            else{
                $too = ClienteUser::join('users', 'cliente_user.user_id', '=', 'users.id')
                    ->where('cliente_user.cliente_id','=',$ticket->cliente_id)
                    ->select('cliente_user.*', 'users.email')
                    ->get();

                foreach ($too as $tod) {
                    $too = $tod->email;
                   Mail::send('emails.tiketalerta', ['cliente'=>$cliente->nombre,'ticket'=>$ticket->nombre,'ticket'=>$ticket->nombre,'area'=>$area->name,'colaborador'=>$user->name,'jefe'=>$jefe->name,'fecha'=>$fecha,'url'=>$url], function ($message) use ($too,$title,$clien)
                    {
                        $message->from('digital.mi@mediaimpact.pe', 'Ticket - MI');
                        $message->to($too);
                        //$message->bcc('genixxavier@gmail.com');
                        $message->subject($clien.' - '.$title);

                    });
                }
            }

            Session::flash('status', 'culminado');
            return back();
        }

        public function asignar_post(Request $request, $id){

            $fechas_p_s = $request->horas_pedido+$request->horas_supervision;
            //obtenemos dia y hora
            $ghora = $fechas_p_s % 9;
            $gdias = intval($fechas_p_s / 9);

            $canthoras = $fechas_p_s;
            $finicio = $request->fecha_inicio;
            $fecha = strtotime($request->fecha_inicio);
            $horainicio = (int)date('H', $fecha);
            $fecha_int_d = (int)date('d', $fecha);
            $total = $horainicio+$fechas_p_s;


            $countdia = 0;
             //preguntamos el dia
            switch (date('w', $fecha)){
                case 0: $dia = 0; break;
                case 1: $dia = 1; break;
                case 2: $dia = 2; break;
                case 3: $dia = 3; break;
                case 4: $dia = 4; break;
                case 5: $dia = 5; break;
                case 6: $dia = 6; break;
            }
            if($total > 19){
                if($horainicio < 13){
                $desc = 1;
                }
                else{
                    $desc = 0;
                }

                $hfalta = 19 - $horainicio -$desc;

                $canthorasfaltan = $canthoras - $hfalta;

                if($dia == 5){
                    $dia = 0;
                    $countdia++;
                    $countdia++;
                }
                $dia = $dia +1;
                $countdia++;

                //dd($dia);
                while($canthorasfaltan >= 10){
                    if($dia == 5){
                        $dia = 0;
                        $countdia++;
                        $countdia++;
                    }
                    $dia = $dia +1;
                    $countdia++;
                    $canthorasfaltan = $canthorasfaltan-9;
                }
                $horafinal = 9 + $canthorasfaltan;
                //dd($horafinal);
                if($horafinal > 13){
                    $horafinal = $horafinal + 1;
                }
            }
            else{
                if($horainicio < 13  & $total > 13){
                    $desc = 1;
                }
                else{
                    $desc = 0;
                }

                $horafinal = $total + $desc;
            }

            $nuevafecha = strtotime ( '+'.$countdia.' day' , strtotime ($request->fecha_inicio ) ) ;
            $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
            $nfds = $nuevafecha." ".$horafinal.":00:00";
            //dd("dia: ".$dia." / horas:". $horafinal." / ".$nfds. "/ ".$countdia);

            //dd($nfds);
            //calculamos el estado
            $fechapp = date('Y-m-d H:i:s');
            if(strtotime($fechapp) > strtotime($nfds) ){
                $hact = 3;
            }
            else{
                $hact = 5;
            }

            $ticket = Ticket::find($id);
            $ticket = $ticket->fill($request->all());
            $ticket->estado = $hact;
            $ticket->fecha_fin =  $nfds;
            $ticket->save();

            //al historial
            $ticket_hist = TiketHistorial::create(['id_tiket' => $id, 'nombre'=>$ticket->nombre,'descripcion'=>$ticket->descripcion, 'cliente_id'=>$ticket->cliente_id,'user_id'=>$ticket->user_id,'area_id'=>$ticket->area_id, 'user_create' => $ticket->user_create]);
            $ticket_hist->estado = $hact;
            $ticket_hist->fecha_inicio = $ticket->fecha_inicio;
            $ticket_hist->fecha_fin =  $nfds;
            $ticket_hist->fecha_limite = $ticket->fecha_limite;
            $ticket_hist->horas_pedido = $ticket->horas_pedido;
            $ticket_hist->horas_supervision = $ticket->horas_supervision;
            $ticket_hist->save();

            Session::flash('status', 'Asignado exitosamente');
            return back();
        }

        public function alerta(){
            $tickets = Ticket::where('estado','!=',0)
                    ->where('estado','!=',4)
                    ->where('estado','!=',2)
                    ->where('area_id','=',1)
                    ->where('estado','!=',6)
                    ->orderby('fecha_inicio')
                    ->get();
            $fecha = date('Y-m-d H:i:s');
            for($i=0;$i < count($tickets) ; $i++){
                if($tickets[$i]->estado == 5){
                    if(strtotime($tickets[$i]->fecha_inicio) <= strtotime($fecha)){
                        $tickets[$i]->estado = 1;
                        $tickets[$i]->save();
                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 1;
                        $ticket_hist->save();
                    }
                    if(strtotime($tickets[$i]->fecha_fin) <= strtotime($fecha)){
                        $tickets[$i]->estado = 3;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 3;
                        $ticket_hist->save();
                    }
                }
                if($tickets[$i]->estado == 1){
                    if(strtotime($fecha) > strtotime($tickets[$i]->fecha_fin)){
                        $tickets[$i]->estado = 3;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 3;
                        $ticket_hist->save();
                    }
                }
            }
            return view('frontend.alertas')
                ->with('tickets',$tickets);
        }

        public function validar(Request $request){
            $id = $request->idvalidar;
            $ticket = Ticket::find($id);
            $ticket->estado = 4;
            $ticket->save();

            //al historial
            $ticket_hist = TiketHistorial::create(['id_tiket' => $id, 'nombre'=>$ticket->nombre,'descripcion'=>$ticket->descripcion, 'cliente_id'=>$ticket->cliente_id,'user_id'=>$ticket->user_id,'area_id'=>$ticket->area_id, 'user_create' => $ticket->user_create]);
            $ticket_hist->estado = 4;
            $ticket_hist->fecha_inicio = $ticket->fecha_inicio;
            $ticket_hist->fecha_fin =  $ticket->fecha_fin;
            $ticket_hist->fecha_limite = $ticket->fecha_limite;
            $ticket_hist->horas_pedido = $ticket->horas_pedido;
            $ticket_hist->horas_supervision = $ticket->horas_supervision;
            $ticket_hist->save();

            return back();
        }

        public function alertacm(){

            $tickets = Ticket::where('estado','!=',0)
                    ->where('estado','!=',4)
                    ->where('estado','!=',2)
                    ->where('area_id','=',3)
                    ->where('estado','!=',6)
                    ->orderby('fecha_inicio')
                    ->get();

            $fecha = date('Y-m-d H:i:s');


            for($i=0;$i < count($tickets) ; $i++){

                if($tickets[$i]->estado == 5){
                    if(strtotime($tickets[$i]->fecha_inicio) <= strtotime($fecha)){
                        $tickets[$i]->estado = 1;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 1;
                        $ticket_hist->save();
                    }
                    if(strtotime($tickets[$i]->fecha_fin) <= strtotime($fecha)){
                        $tickets[$i]->estado = 3;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 3;
                        $ticket_hist->save();
                    }
                }
                if($tickets[$i]->estado == 1){
                    if(strtotime($fecha) > strtotime($tickets[$i]->fecha_fin)){
                        $tickets[$i]->estado = 3;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 3;
                        $ticket_hist->save();
                    }
                }
            }

            return view('frontend.alertas')
                ->with('tickets',$tickets);
        }

        public function alertadv(){

            $tickets = Ticket::where('estado','!=',0)
                    ->where('estado','!=',4)
                    ->where('estado','!=',2)
                    ->where('area_id','=',4)
                    ->where('estado','!=',6)
                    ->orderby('fecha_inicio')
                    ->get();

            $fecha = date('Y-m-d H:i:s');


            for($i=0;$i < count($tickets) ; $i++){

                if($tickets[$i]->estado == 5){
                    if(strtotime($tickets[$i]->fecha_inicio) <= strtotime($fecha)){
                        $tickets[$i]->estado = 1;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 1;
                        $ticket_hist->save();
                    }
                    if(strtotime($tickets[$i]->fecha_fin) <= strtotime($fecha)){
                        $tickets[$i]->estado = 3;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 3;
                        $ticket_hist->save();
                    }
                }
                if($tickets[$i]->estado == 1){
                    if(strtotime($fecha) > strtotime($tickets[$i]->fecha_fin)){
                        $tickets[$i]->estado = 3;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 3;
                        $ticket_hist->save();
                    }
                }
            }

            return view('frontend.alertas')
                ->with('tickets',$tickets);
        }

        public function alertasart(){

            $tickets = Ticket::where('estado','!=',0)
                    ->where('estado','!=',4)
                    ->where('estado','!=',2)
                    ->where('area_id','=',2)
                    ->where('estado','!=',6)
                    ->orderby('fecha_inicio')
                    ->get();

            $fecha = date('Y-m-d H:i:s');


            for($i=0;$i < count($tickets) ; $i++){

                if($tickets[$i]->estado == 5){
                    if(strtotime($tickets[$i]->fecha_inicio) <= strtotime($fecha)){
                        $tickets[$i]->estado = 1;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 1;
                        $ticket_hist->save();
                    }
                    if(strtotime($tickets[$i]->fecha_fin) <= strtotime($fecha)){
                        $tickets[$i]->estado = 3;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 3;
                        $ticket_hist->save();
                    }
                }
                if($tickets[$i]->estado == 1){
                    if(strtotime($fecha) > strtotime($tickets[$i]->fecha_fin)){
                        $tickets[$i]->estado = 3;
                        $tickets[$i]->save();

                        //al historial
                        $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                        $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                        $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                        $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                        $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                        $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                        $ticket_hist->estado = 3;
                        $ticket_hist->save();
                    }
                }
            }

            return view('frontend.alertas')
                ->with('tickets',$tickets);
        }

        public function horarios(){
            $colaboradores = User::where('estado','=',1)
                            ->get();
            return view('frontend.horarios')
                ->with('colaboradores',$colaboradores);
        }

        public function horario_id($id){
            $user = User::find($id);
            $dias = Dias::all();
            $colaboradores = User::where('estado','=',1)
                            ->get();
            $horarios = HorarioUser::where('user_id','=',$user->id)
                        ->orderby('dia_id')
                        ->get();
            return view('frontend.horario_user')
                ->with('user',$user)
                ->with('dias',$dias)
                ->with('horarios',$horarios)
                ->with('colaboradores',$colaboradores);
        }

        public function horario_create(request $request){
            $ee = HorarioUser::where('user_id','=',$request->id_user)
                                ->where('dia_id','=',$request->dia)
                                ->first();
            if(count($ee) == 0){
                $horario = HorarioUser::create(['user_id' => $request->id_user, 'dia_id'=>$request->dia,'entrada'=>$request->entrada, 'salida'=>$request->salida]);
                $horario->save();
                return "1";
            }
            else{
                return "Registro ya exite en el horario del trabajador";
            }

        }

        public function update_horario(request $request){
            $horario = HorarioUser::find($request->horario_id);
            $horario->entrada = $request->gentrada;
            $horario->salida = $request->gsalida;
            $horario->save();

            Session::flash('status', 'exito');
            return back();

        }
        //completarblanco
        public function ticketblank(){
            $idarea = \Auth::user()->tribu_id;
            if(\Auth::user()->tipo == "gerente"){
                $colaboradores = User::where('estado','=',1)->orderBy('name','asc')
                            ->get();
            }
            else{
                $colaboradores = User::where('estado','=',1)
                            ->where('tribu_id','=',$idarea)->orderBy('name','asc')
                            ->get();
            }
            $allTribus = Tribu::all();
            return view('frontend.ticketblank')
                ->with('colaboradores',$colaboradores)->with('allTribus',$allTribus);
        }

        public function ticketblank_id($id, $semana=null){

            if(\Auth::user()->tipo == 'jefe'){
               $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
               $idarea = \Auth::user()->area_id;
               $areas = Areas::where('id','=',$idarea)->get();
            }
            else if(\Auth::user()->tipo == 'gerente'){
                $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
                $areas = Areas::all();
            }
            else{
                // $id = \Auth::id();
                // $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
                //     ->where('clientes.activo','=',1)
                //     ->where('cliente_user.user_id','=',$id)
                //     ->orderby("clientes.nombre")
                //     ->get();
                // $areas = Areas::all();

               $clientes = Cliente::where('activo','=',1)->orderby("type",'desc')->orderby("nombre")->get();
               $idarea = \Auth::user()->area_id;
               $areas = Areas::where('id','=',$idarea)->get();
               
            }
            $hh = \Auth::user()->area_id;
            $colaboradores = User::where('area_id',$hh)
                            ->where('estado','=',1)
                            ->get();

            $user = User::find($id);

            $dia = date('d-m-Y');
            $semanaactual = $dia;
            if($semana){
                $dia = $semana;
                $semanaactual = $dia;
            }
            $dia = date("w",strtotime($dia));//5
            $diax = date('d',strtotime($semanaactual));//224
            $diaxx = $diax;
            if($dia > 1){
                $restar = ($dia - 1);
                // preguntamos si es 31
                $gddd = strtotime ( '-'.$restar.' day' , strtotime ( $semanaactual ) ) ;
                $mesantt = date ( 'd-m-Y' , $gddd );
                $diax = date ( 'd' , $gddd );

                // if($diax == 1){
                //     // preguntamos si es 31
                //     $gddd = strtotime ( '-'.$restar.' day' , strtotime ( $semanaactual ) ) ;
                //     $mesantt = date ( 'd-m-Y' , $gddd );
                //     $gddd = date ( 'd' , $gddd );

                //     $diax = $gddd - ($dia-2);
                // }
                // else{
                //     $diax = $diax - ($dia - 1);
                // }
            }
            else{
                $diax = $diax;
            }

            if(isset($mesantt)){

                $fi = date('Y-m',strtotime($mesantt));
            }
            else{
                $fi = date('Y-m',strtotime($semanaactual));
            }

            $diax = $fi.'-'.$diax;
            // dd($diax);

            $diaxant = strtotime ( '-3 day' , strtotime ( $diax ) ) ;
            $diaxant = date ( 'Y-m-d' , $diaxant );

            $lunes = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$diax)
                    ->where('estado','!=',0)
                    ->where('fecha_inicio','<',date($diax.' 19:00'))
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();
            $lunesbefore = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$diaxant)
                    ->whereDate('fecha_fin','=',$diax)
                    ->where('estado','!=',0)
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_fin','ASC')
                    ->get();
            $lunesextras = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$diax)
                    ->where('estado','!=',0)
                    ->where('observacion','like','#extra%')
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.observacion as observacion','tickets.horas_pedido as horas_pedido')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();
            //dd($lunesextras);
            $nuevafecha = strtotime ( '+1 day' , strtotime ( $diax ) ) ;
            $nuevafecha = date ( 'Y-m-d' , $nuevafecha );

            $martes = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$nuevafecha)
                    ->where('estado','!=',0)
                    ->where('fecha_inicio','<',date($nuevafecha.' 19:00'))
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();
            $martesbefore = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$diax)
                    ->whereDate('fecha_fin','=',$nuevafecha)
                    ->where('estado','!=',0)
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_fin','ASC')
                    ->get();
            $martesextras = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$nuevafecha)
                    ->where('estado','!=',0)
                    ->where('observacion','like','#extra%')
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.observacion as observacion','tickets.horas_pedido as horas_pedido')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();

            $nuevafecha1 = strtotime ( '+1 day' , strtotime ( $nuevafecha ) ) ;
            $nuevafecha1 = date ( 'Y-m-d' , $nuevafecha1 );
            $miercoles = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$nuevafecha1)
                    ->where('estado','!=',0)
                    ->where('fecha_inicio','<',date($nuevafecha1.' 19:00'))
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();
            $miercolesbefore = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$nuevafecha)
                    ->whereDate('fecha_fin','=',$nuevafecha1)
                    ->where('estado','!=',0)
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();
            $miercolesextras = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$nuevafecha1)
                    ->where('estado','!=',0)
                    ->where('observacion','like','#extra%')
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.observacion as observacion','tickets.horas_pedido as horas_pedido')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();

            $nuevafecha2 = strtotime ( '+1 day' , strtotime ( $nuevafecha1 ) ) ;
            $nuevafecha2 = date ( 'Y-m-d' , $nuevafecha2 );
            $jueves = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$nuevafecha2)
                    ->where('estado','!=',0)
                    ->where('fecha_inicio','<',date($nuevafecha2.' 19:00'))
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();
            $juevesbefore = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','<=',$nuevafecha1)
                    ->whereDate('fecha_fin','=',$nuevafecha2)
                    ->where('estado','!=',0)
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();
            $juevesextras = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$nuevafecha2)
                    ->where('estado','!=',0)
                    ->where('observacion','like','#extra%')
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.observacion as observacion','tickets.horas_pedido as horas_pedido')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();

            $nuevafecha3 = strtotime ( '+1 day' , strtotime ( $nuevafecha2 ) ) ;
            $nuevafecha3 = date ( 'Y-m-d' , $nuevafecha3 );
            $viernes = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$nuevafecha3)
                    ->where('estado','!=',0)
                    ->where('fecha_inicio','<',date($nuevafecha3.' 19:00'))
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();
            $viernesbefore = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','<=',$nuevafecha2)
                    ->whereDate('fecha_fin','=',$nuevafecha3)
                    ->where('estado','!=',0)
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.fecha_fin as fecha_fin')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();
            $viernesextras = DB::table('tickets')
                    ->join('clientes','clientes.id','=','tickets.cliente_id')
                    ->where('user_id','=',$id)
                    ->whereDate('fecha_inicio','=',$nuevafecha3)
                    ->where('estado','!=',0)
                    ->where('observacion','like','#extra%')
                    ->select('tickets.id as id','tickets.nombre as nombre','clientes.nombre as cliente','tickets.fecha_inicio as fecha_inicio','tickets.observacion as observacion','tickets.horas_pedido as horas_pedido')
                    ->orderBy('fecha_inicio','ASC')
                    ->get();

            $asistencia = Asistencia::find(1);
            // dd($asistencia);
            return view('frontend.ticket_historial')
                ->with('user',$user)
                ->with('lunes',$lunes)
                ->with('lunesbefore',$lunesbefore)
                ->with('lunesextras',$lunesextras)
                ->with('martes',$martes)
                ->with('martesbefore',$martesbefore)
                ->with('martesextras',$martesextras)
                ->with('miercoles',$miercoles)
                ->with('miercolesbefore',$miercolesbefore)
                ->with('miercolesextras',$miercolesextras)
                ->with('jueves',$jueves)
                ->with('juevesbefore',$juevesbefore)
                ->with('juevesextras',$juevesextras)
                ->with('viernes',$viernes)
                ->with('viernesbefore',$viernesbefore)
                ->with('viernesextras',$viernesextras)
                ->with('clientes',$clientes)
                ->with('areas',$areas)
                ->with('colaboradores',$colaboradores)
                ->with('semanaactual',$semanaactual)
                ->with('diaxx',$diaxx)
                ->with('asistencia',$asistencia);
        }

        public function alertasall(){

            $tickets_1 = Ticket::tickets_por_area(1);
            $tickets_2 = Ticket::tickets_por_area(2);
            $tickets_3 = Ticket::tickets_por_area(3);
            $tickets_5 = Ticket::tickets_por_area(5);
            $tickets_4 = Ticket::tickets_por_area(4);
            $tickets_7 = Ticket::tickets_por_area(7);


            return view('frontend.all_tickets')
                ->with('tickets_1',$tickets_1)
                ->with('tickets_2',$tickets_2)
                ->with('tickets_3',$tickets_3)
                ->with('tickets_4',$tickets_4)
                ->with('tickets_5',$tickets_5)
                ->with('tickets_7',$tickets_7);
        }
        public function prueba(){
            return view('frontend.prueba');
        }
        public function prueba_2(){
            return view('frontend.prueba_2');
        }

        // public static function verificar_asistencia($fecha)
        // {
        //     return "aaaa";
        // }

        public function correct_hours () {
            return view('frontend.correct_hours');
        }

        public function find_users ( ) {
            $users  = User::where('estado','1')->orderby('name','asc')->get();
            return $users;
        }

        public function find_user_to_correct_hour (Request $request) {
            $asistencia = Asistencia::where('fecha', $request->fecha)->where('user_id',$request->id_usuario)->get();
            $flag = count($asistencia) > 0 ? 1 : 0 ;
            $user = User::find($request->id_usuario);
            return view('frontend.hour_user' , compact('asistencia' , 'flag' ,'user'));
            // return $request->fecha;
        }
        public function  update_asistencia (Request $request) {
            $asistencia = Asistencia::find($request->id_asistencia);
           if (isset($request->hora_inicio)  ){
                $request->hora_inicio = $this->assign_format($request->hora_inicio);
           }
        //    if (isset($request->hora_fin)) {
        //        $request->hora_fin = $this->assign_format($request->hora_fin);
        //    }
            $asistencia->hora_inicio = $asistencia->hora_inicio == null ? $request->hora_inicio :$asistencia->hora_inicio;
            // $asistencia->hora_fin = $asistencia->hora_fin == null ? $request->hora_fin :$asistencia->hora_fin;
            $historial_asistencia = new HistorialAsistencia;
            $historial_asistencia->id_usuario = $asistencia->user_id;
            $historial_asistencia->id_asistencia= $asistencia->id;
            $historial_asistencia->fecha= $asistencia->fecha;
            $asistencia->update();
            $historial_asistencia->save();
            $message  = [
                'status' =>200,
                'message'  => 'Se actualizó correctamente'
            ];
            return $message;
        }

        private  function assign_format ($hora) {
            $flag = $hora;
            $hora = strtotime($hora);
            $hora_validar = strtotime('12:00');
            $hora_fomat = $hora < $hora_validar ? $flag.' am' : $flag.' pm';
            return $hora_fomat;
        }
        public function history_assistance_changes() {
            return view('frontend.history_assistance_changes');
        }
        public function show_history_assistance(Request $request) {
            $month  = $request->month;
            $year  = date('Y');
            $list = DB::table('historial_asistencia')
                    ->join('users','historial_asistencia.id_usuario','=' ,'users.id')
                    ->select(DB::raw('count(historial_asistencia.id_usuario) as cantidad, users.name , (SELECT nombre FROM tribus WHERE users.tribu_id = tribus.id) as tribu'))
                    ->where([
                        [DB::raw("DATE_FORMAT(historial_asistencia.fecha,'%m')"),'=', $month],
                        [DB::raw("DATE_FORMAT(historial_asistencia.fecha,'%Y')"),'=', $year]
                    ])

                    ->groupBy('historial_asistencia.id_usuario')
                    ->get();
            return  view('frontend.view_history_assistance',compact('list'));
        //   return $list;
        }
        public function reunion_marcacion() {
            $tribu = DB::table('tribus')->select('nombre')->where('id',\Auth::user()->tribu_id)->get();
            if(\Auth::user()->tipo == 'jefe'){
                $clientes = Cliente::where('activo','=',1)->orderby("type","desc")->orderby("nombre")->get();
                $idarea = \Auth::user()->area_id;
                $areas = Areas::where('id','=',$idarea)->get();
             }
             else if(\Auth::user()->tipo == 'gerente'){
                 $clientes = Cliente::where('activo','=',1)->orderby("type","desc")->orderby("nombre")->get();
                 $areas = Areas::all();
             }
             else{
                 $id = \Auth::id();
                 $clientes = Cliente::join('cliente_user', 'cliente_user.cliente_id', '=', 'clientes.id')
                     ->where('cliente_user.user_id','=',$id)
                     ->where('clientes.activo','=',1)
                     ->orderby("clientes.nombre")
                     ->get();
                 $areas = Areas::all();
             }
             // $hh = \Auth::user()->area_id;
             // $colaboradores = User::where('area_id',$hh)
             $colaboradores = User::where([
                 ['estado','=',1],
                 ['tribu_id','=',\Auth::user()->tribu_id],
                 ['id','!=',\Auth::user()->id]
             ])->get();

             return view('frontend.reunion_marcacion')
                 ->with('clientes',$clientes)
                 ->with('areas',$areas)
                 ->with('tribu',$tribu)
                 ->with('colaboradores',$colaboradores);
        }


        public function editar_reunion ($id) {

        }





        public function create_reunion_marcacion ( Request $request) {
            try {
                $reu = Idreunion::create($request->all());
                $idr = $reu->id;
                $data = json_decode($request->datajson);
                for ($i=0; $i < count($data); $i++) {
                    $reunion = Reunion::create($request->all());
                    $reunion->user_id = $data[$i]->ii;
                    $reunion->id_reunion = $idr;
                    $reunion->save();
                }

                $cliente = Cliente::find($request->cliente_id);
                $nombre_reu = $request->nombre;
                $ticket_nombre = "Reunión: ".$cliente->nombre;
                $fecha_init = explode(' ',$request->fecha_inicio);
                $date_to_assistance = date($fecha_init[0]);
                // $date_to_assistance = date("Y-m-d",strtotime($date_to_assistance.' + 1 days'));

                $dta = date($date_to_assistance);
                $dta = strtotime($dta);
                $dd = date('w', $dta);
                $hour = $request->horas;
                // $new_hour  =explode(':',$fecha_init[1]);
                // $hora_fin = 9 + $hour ;
               $date_init_to_tickets = $date_to_assistance.' 09:00:00';
               //$date_end_to_tickets = $date_to_assistance.' '.$hora_fin.':00:00';
               $us = User::find($request->user_create);
               $ee = HorarioUser::where('dia_id','=',$dd)
               ->where('user_id','=',$request->user_create)
               ->first();
               if (!empty($ee ) ) {
                   $fecha_real = $ee->entrada.':00 am';
               }
               else {
                   $fecha_real = '09:00:00 am';
               }

                $asistencia = new Asistencia ();
                $asistencia->hora_inicio = $fecha_init[1].' am';
                $asistencia->hora_inicio_real = $fecha_real;
                $asistencia->fecha = $date_to_assistance;
                $asistencia->user_id =$request->user_create;
                $asistencia->estado = 'G';
                $asistencia->save();

                $ticket = new Ticket();
                $ticket->nombre = $ticket_nombre;
                $ticket->estado = 1;
                $ticket->descripcion = '';
                $ticket->fecha_inicio = $date_init_to_tickets;
                $ticket->fecha_fin = $date_end_to_tickets;
                $ticket->fecha_limite = $date_end_to_tickets;
                $ticket->horas_pedido = $request->horas;
                $ticket->horas_supervision = $request->horas;
                $ticket->cliente_id = $request->cliente_id;
                $ticket->user_id = $request->user_create;
                $ticket->area_id= $us->area_id;
                $ticket->user_create = $request->user_create;
                $ticket->save();

                $ticket_historial = new TiketHistorial();
                $ticket_historial->nombre = $ticket_nombre;
                $ticket_historial->estado = 1;
                $ticket_historial->descripcion = '';
                $ticket_historial->fecha_inicio = $date_init_to_tickets;
                $ticket_historial->fecha_fin = $date_end_to_tickets;
                $ticket_historial->fecha_limite = $date_end_to_tickets;
                $ticket_historial->horas_pedido = $request->horas;
                $ticket_historial->horas_supervision = $request->horas;
                $ticket_historial->cliente_id = $request->cliente_id;
                $ticket_historial->user_id = $request->user_create;
                $ticket_historial->area_id= $us->area_id;
                $ticket_historial->user_create = $request->user_create;
                $ticket_historial->id_tiket = $ticket->id;
                $ticket_historial->save();

                // Asistencia

                for ($i=0; $i < count($data); $i++) {
                         $us = User::find($data[$i]->ii);
                        $ee = HorarioUser::where('dia_id','=',$dd)
                        ->where('user_id','=',$data[$i]->ii)
                        ->first();
                        if (!empty($ee )) {
                            $fecha_real = $ee->entrada.':00 am';
                        }
                        else {
                            $fecha_real = '09:00:00 am';
                         }
                            $asistencia = new Asistencia ();
                            $asistencia->hora_inicio = $fecha_init[1].' am';
                            $asistencia->hora_inicio_real = $fecha_real;
                            $asistencia->fecha = $date_to_assistance;
                            $asistencia->user_id = $data[$i]->ii;
                            $asistencia->estado = 'G';
                            $asistencia->save();

                            $ticket = new Ticket();
                            $ticket->nombre = $ticket_nombre;
                            $ticket->estado = 1;
                            $ticket->descripcion = '';
                            $ticket->fecha_inicio = $date_init_to_tickets;
                            $ticket->fecha_fin = $date_end_to_tickets;
                            $ticket->fecha_limite = $date_end_to_tickets;
                            $ticket->horas_pedido = $request->horas;
                            $ticket->horas_supervision = $request->horas;
                            $ticket->cliente_id = $request->cliente_id;
                            $ticket->user_id = $data[$i]->ii;
                            $ticket->area_id= $us->area_id;
                            $ticket->user_create = $request->user_create;
                            $ticket->save();

                            $ticket_historial = new TiketHistorial();
                            $ticket_historial->nombre = $ticket_nombre;
                            $ticket_historial->estado = 1;
                            $ticket_historial->descripcion = '';
                            $ticket_historial->fecha_inicio = $date_init_to_tickets;
                            $ticket_historial->fecha_fin = $date_end_to_tickets;
                            $ticket_historial->fecha_limite = $date_end_to_tickets;
                            $ticket_historial->horas_pedido = $request->horas;
                            $ticket_historial->horas_supervision = $request->horas;
                            $ticket_historial->cliente_id = $request->cliente_id;
                            $ticket_historial->user_id = $data[$i]->ii;
                            $ticket_historial->area_id= $us->area_id;
                            $ticket_historial->user_create = $request->user_create;
                            $ticket_historial->id_tiket = $ticket->id;
                            $ticket_historial->save();

                    }


            } catch (\Exception $e) {
                return [
                    'error' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'El código de excepción es ' =>  $e->getCode()
                ];
            }

            Session::flash('status', 'Creado exitosamente');
            return back();
        }

        // cambios genixxavier
        public function getdatosaeditar($id){
          $ticket = Ticket::find($id);
          return $ticket;
        }

        public function update_ticket2(Request $request){
          $horaa = explode(':',$request->hora_inicio);
          $horaa = $horaa[0]+1;
          $horaa = $request->fecha_inicio.' '.$horaa.':00:00';
          // dd($horaa);
          $ticket = Ticket::find($request->id_tiket);
          $keyUserid = $ticket->user_id;
          $gfi = $ticket->fecha_inicio;
          $gff = $ticket->fecha_fin;
          $fechai = $request->fecha_inicio.' '.$request->hora_inicio;
          // dd($horaa.' - '. $gfi);
          if( strtotime($horaa) >= strtotime($gfi) ){
            $variable = 'fecha_inicio';
          }
          else{
            $variable = 'fecha_fin';
            // tenemos que calcular haste hora de fin del actual ticket a editar
            $hfinal = explode(' ',$gff);
            $hfinal = explode(':',$hfinal[1]);
            $hff = $hfinal[0];
          }
          // dd($hff);
          // calculo de nueva fecha fin
          if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente'){

              $fechas_p_s = $request->horas_pedido+$request->horas_supervision;
              //obtenemos dia y hora
              $ghora = $fechas_p_s % 9;
              $gdias = intval($fechas_p_s / 9);

              $canthoras = $fechas_p_s;
              $finicio = $fechai;
              $fecha = strtotime($fechai);
              $horainicio = (int)date('H', $fecha);
              $fecha_int_d = (int)date('d', $fecha);
              $total = $horainicio+$fechas_p_s;

              $countdia = 0;
               //preguntamos el dia
              switch (date('w', $fecha)){
                  case 0: $dia = 0; break;
                  case 1: $dia = 1; break;
                  case 2: $dia = 2; break;
                  case 3: $dia = 3; break;
                  case 4: $dia = 4; break;
                  case 5: $dia = 5; break;
                  case 6: $dia = 6; break;
              }
              if($total > 19){
                  if($horainicio < 13){
                  $desc = 1;
                  }
                  else{
                      $desc = 0;
                  }

                  $hfalta = 19 - $horainicio -$desc;

                  $canthorasfaltan = $canthoras - $hfalta;

                  if($dia == 5){
                      $dia = 0;
                      $countdia++;
                      $countdia++;
                  }
                  $dia = $dia +1;
                  $countdia++;

                  //dd($dia);
                  while($canthorasfaltan >= 10){
                      if($dia == 5){
                          $dia = 0;
                          $countdia++;
                          $countdia++;
                      }
                      $dia = $dia +1;
                      $countdia++;
                      $canthorasfaltan = $canthorasfaltan-9;
                  }
                  $horafinal = 9 + $canthorasfaltan;
                  //dd($horafinal);
                  if($horafinal > 13){
                      $horafinal = $horafinal + 1;
                  }
              }
              else{
                  if($horainicio < 13 & $total > 13){
                      $desc = 1;
                  }
                  else{
                      $desc = 0;
                  }

                  $horafinal = $total + $desc;
              }

              $nuevafecha = strtotime ( '+'.$countdia.' day' , strtotime ($fechai ) ) ;
              $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
              $nfds = $nuevafecha." ".$horafinal.":00:00";
          }

          // dd($nfds);
          $dia = date("Y-m-d",strtotime($fechai));//5
          $hora = date("H", strtotime($fechai));
          $horaf = date("H", strtotime($nfds));
          $dd = $horaf-$hora;
          // dd($horaf);
          $si = 0;
          // aqui preguntamos si son horas extras
          if($hora >= 19){
              // comparamos que no pase de las 24horas
              $dd = $hora + $request->horas_pedido;

              if($dd <= 24){
                  //preguntamos si el ticket existe
                  for ($i = $hora; $i <= ($dd-1) ; $i++) {
                      $ff = $dia." ".$i.":00:00";
                      $existe = Ticket::where('fecha_inicio','=',$ff)
                                      ->where('user_id','=',$keyUserid)
                                      ->where('id','!=',$request->id_tiket)
                                      ->get();
                      if(count($existe) > 0){
                          $si = 1;
                          break;
                      }
                  }
                  if($si == 1){
                      Session::flash('status', 'existe_extra');
                      return back();
                  }
                  $update_ticket = Ticket::find($request->id_tiket);
                  $ticket_hist = TiketHistorial::where('id_tiket','=',$request->id_tiket)->first();
                  if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente'){
                      // $fechapp = date('Y-m-d H:i:s');
                      if($dd == 24){
                          $dd = 00;
                      }
                      $ffinextra = date('Y-m-d' , strtotime($fechai));
                      $ffinextra = $ffinextra." ".$dd.":00:00";

                      $update_ticket->horas_pedido = $request->horas_pedido;
                      $update_ticket->nombre = $request->nombre;
                      $update_ticket->estado = 2;
                      $update_ticket->fecha_fin = $ffinextra;
                      $update_ticket->observacion = $request->descripcion;
                      $update_ticket->save();

                      $ticket_hist->horas_pedido = $request->horas_pedido;
                      $ticket_hist->nombre = $request->nombre;
                      $ticket_hist->estado = 2;
                      $ticket_hist->fecha_fin = $ffinextra;
                      $ticket_hist->observacion = $request->descripcion;
                      $ticket_hist->area_id = $idarea;
                      $ticket_hist->save();
                  }
                  // else{
                  //     $ticket_hist->id_tiket = $ticket->id;
                  //     $ticket_hist->save();
                  // }
                  Session::flash('status', 'add_extra');
                  return back();
              }
              else{
                  Session::flash('status', 'excedio_extra');
                  return back();
              }
          }
          //comprobamos que no pase de las 18 horas
          $ffin = $hora + ($request->horas_pedido -1);
          if($hora < 13 and ($hora+$dd) > 13){
              $ffin = $ffin + 1;
          }
          if($ffin > 18){
              Session::flash('status', 'excedio');
              return back();
          }
          //preguntamos si el ticket existe
          $consultar = 0;
          // dd($horaf."-".$hff);
          if( $variable == 'fecha_fin' ){
            if( $horaf  >  $hff ){
              $horaf7 = $horaf;
              $consultar = 1;
            }
            $horaf = $hff;
          }
          // dd($hora);
          for ($i = $hora; $i <= ($horaf-1) ; $i++) {
              $ff = $dia." ".$i.":00:00";
              $existe = Ticket::where($variable,'=',$ff)
                              ->where('user_id','=',$keyUserid)
                              ->where('id','!=',$request->id_tiket)
                              ->get();
              // dd($existe);
              if(count($existe) > 0){
                  $si = 1;
                  break;
              }
          }
          // verificando de que la hora final es mayor a la hora final del ticket actual
          // dd($consultar);
          if($consultar == 1){
            for ($i = $hora; $i <= ($horaf7-1) ; $i++) {
                $ff = $dia." ".$i.":00:00";
                $existe = Ticket::where('fecha_inicio','=',$ff)
                                ->where('user_id','=',$keyUserid)
                                ->where('id','!=',$request->id_tiket)
                                ->get();
                if(count($existe) > 0){
                    $si = 1;
                    break;
                }
            }
          }
          if($si == 1){
            // dd("hay ticket creado");
              Session::flash('status', 'existe');
              return back();
          }
          // dd("creartiket");
          // $ticket = Ticket::create($request->all());
          $update_ticket = Ticket::find($request->id_tiket);
          $ticket_hist = TiketHistorial::where('id_tiket','=',$request->id_tiket)->first();
          // dd($ticket_hist);
          if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente'){
              $update_ticket->fecha_inicio = $fechai;
              $update_ticket->horas_pedido = $request->horas_pedido;
              $update_ticket->estado = 2;
              $update_ticket->fecha_fin = $nfds;
              $update_ticket->nombre = $request->nombre;
              $update_ticket->descripcion = $request->descripcion;
              $update_ticket->save();

              $ticket_hist->fecha_inicio = $fechai;
              $ticket_hist->horas_pedido = $request->horas_pedido;
              $ticket_hist->estado = 2;
              $ticket_hist->fecha_fin = $nfds;
              $ticket_hist->nombre = $request->nombre;
              $ticket_hist->descripcion = $request->descripcion;
              $ticket_hist->save();
          }
          // else{
          //     $ticket_hist->id_tiket = $update_ticket->id;
          //     $ticket_hist->save();
          // }
          Session::flash('status', 'Creado exitosamente');
          return back();
        }

        public  function showTasks ($tribu,$fecha) {
            $nameTribu = Tribu::find($tribu);
            $data = [];
            $users = User::where('tribu_id','=',$tribu)->where('estado','=',1)->orderby('name','ASC')->get();

            foreach ($users as $key => $user  ) {

                $tickets = DB::table('tickets')->select('id as id_ticket','nombre as nombre_ticket','estado','fecha_inicio','fecha_fin','horas_pedido')
                            ->whereDate('tickets.fecha_inicio','=', $fecha)->where('tickets.user_id','=',$user->id)->orderBy('fecha_inicio','ASC')
                            ->get();

                array_push($data, [
                    'id_user' => $user->id,
                    'name' => $user->name,
                    'tickets' => [],

                ]);
                if (count($tickets) > 0) {
                   foreach ($tickets as $ticket) {
                       array_push($data[$key]['tickets'], [

                           'id_ticket' => $ticket->id_ticket,
                           'nombre_ticket' => $ticket->nombre_ticket,
                           'estado_ticket' => $ticket->estado,
                           'fecha_inicio' => $ticket->fecha_inicio,
                           'fecha_fin' => $ticket->fecha_fin,
                           'horas_pedido' => $ticket->horas_pedido
                    ]);
                   }
                }


            }


                $days = [
                    'Lunes',
                    'Martes',
                    'Miercoles',
                    'Jueves',
                    'Viernes',
                    'Sabado',
                    'Domingo'
                ];
                $nameDay = $days[date('N', strtotime($fecha)) - 1 ];



                $month = explode('-',$fecha);
                $nameMonth = '';
                switch ($month[1]) {
                    case 01 :
                        $nameMonth = 'Ene';
                        break;
                    case 02 :
                        $nameMonth = 'Feb';
                        break;
                    case 03 :
                        $nameMonth = 'Mar';
                        break;
                    case 04 :
                        $nameMonth = 'Abr';
                        break;
                    case 05 :
                        $nameMonth = 'May';
                        break;
                    case 06 :
                        $nameMonth = 'Jun';
                        break;
                    case 07 :
                        $nameMonth = 'Jul';
                        break;
                    case '08' :
                        $nameMonth = 'Ago';
                        break;
                    case '09' :
                        $nameMonth = 'Sep';
                        break;
                    case 10 :
                        $nameMonth = 'Oct';
                        break;
                    case 11 :
                        $nameMonth = 'Nov';
                        break;
                    case 12 :
                        $nameMonth = 'Dic';
                        break;


                };

                $fullDate = $month[2].' '.$nameMonth;

                $nameOfDay = $nameDay.', '.$fullDate ;


            return view('frontend.showTask',compact('data','nameTribu','fecha','nameOfDay'));
        }

        public function reunion_edit ($id) {
            $idreunion = Idreunion::find($id);
            $hour = explode(' ', $idreunion->fecha_inicio);
            $date = explode('-', $hour[0]);
            $fullDate = ''.$date[1].':'.$date[2];
            $fullHour = explode(':',$hour[1]);
            $fullH = ''.$fullHour[0].':'.$fullHour[2];
            $fullDateHour = $fullDate.' '.$fullH;

            $reuniones = Reunion::where('id_reunion',$id)->get();
            $user_create_reunion = User::find($idreunion->user_create);
            $client = Cliente::find($idreunion->cliente_id);
            $participants  = [];
            $clients = Cliente::all();
            $users = User::where('estado',1)->orderby('name','ASC')->get();
            if (!empty($reuniones)) {
                foreach ($reuniones as $reunion) {
                    $user = User::find($reunion->user_id);
                    array_push($participants,$user);
                }
            }

            return view('frontend.edit_reunion',compact('fullDateHour','idreunion','reuniones','user_create_reunion','client','participants','clients','users'));

        }

        public function update_reunion (Request $request) {
            try {
                DB::beginTransaction();
                $idreunion = Idreunion::find($request->input('id_reu'));
                $idreunion->nombre = $request->input('nombre');
                $idreunion->descripcion = $request->input('descripcion');
                $idreunion->horas = $request->input('horas');
                $idreunion->cliente_id = $request->input('cliente_id');



                $idreunion->fecha_inicio = Date('Y').'-'.$request->fecha_inicio.':00';
                $idreunion->save();
                $idr = $idreunion->id;

                $allReus = Reunion::where('id_reunion',$idr)->get();
                if (!empty($allReus)) {
                    foreach ($allReus as $reu) {
                        $r = Reunion::find($reu->id);
                        $r->delete();
                    }
                }

                $data = json_decode($request->datajson);
                for ($i=0; $i < count($data); $i++) {
                    $reunion = Reunion::create($request->all());
                    $reunion->fecha_inicio = Date('Y').'-'.$request->fecha_inicio.':00';
                    $reunion->user_id = $data[$i]->ii;
                    $reunion->id_reunion = $idr;
                    $reunion->save();
                }
                DB::commit();
                Session::flash('status', 'Editado  exitosamente');
                return back();
            }
            catch ( \Exception $e) {
                DB::rollBack();
                Session::flash('status', 'No se pudo actualizar');
                return back();
            }


        }
        public function delete_reunion ($id) {
           try {
               DB::beginTransaction();
               $ir = Idreunion::find($id);
               $ir->delete();
               $allReus = Reunion::where('id_reunion',$id)->get();
               if (!empty($allReus)) {
                   foreach ($allReus as $reu) {
                       $r = Reunion::find($reu->id);
                       $r->delete();
                   }
               }
               DB::commit();
               Session::flash('status', 'Se elimino correctamente');
               return back();
           }
           catch (\Exception $e) {
               DB::rollBack();
               Session::flash('status', 'No se pudo eliminar');
               return back();
           }
        }

        public function download_excel () {
            $inicio = $_GET['inicio'];
            $fin = $_GET['fin'];
            $report = new ReportsExport($inicio,$fin);



            return Excel::download($report, 'reports.xlsx');


        }
        public function reporte_horas () {
            return view('frontend.reporte_horas');
        }

        public function horas_extras () {

            return view('frontend.contador_vacaciones');
        }
        public function content_horas_extras ($month,$year) {
            $query = $this->query_horas_extras($year,$month);

            return view ('frontend.content_horas_extras',compact('query','month','year'));
        }

        public function save_horas_extras (Request $request) {
            try {
                
                $fecha = $request->year.'-'.$request->month.'-01';
                $validar = HorasExtras::where('fecha','=',$fecha)->get();
                if (count($validar) > 0 ) {
                   
                    return [
                        'status' => 400,
                        'message' => 'Ya se registró las horas extras de este mes'
                    ];
                    
                }

                $query = $this->query_horas_extras($request->year,$request->month);
                foreach ($query as $key => $val) {
                    $he = new HorasExtras;
                    $he->horas_extras = $val->extras;
                    $he->user_id = $val->id;
                    $he->horas_deuda = $val->deuda;
                    $he->fecha = $fecha;
                    $he->save();
                }

                
               return [
                    'status' => 200,
                    'message' => 'Se registró correctamente'
               ];
            } catch (\Exception $e) {
                
                return [
                    'status' => 400,
                    'message' => $e->getMessage()
                ];
            }
          
        }
        


        public function horas_extras_detalle ($id,$year,$month) {
            $user = User::find($id);


          $query = DB::table('asistencia')
            ->join('users','users.id','=','asistencia.user_id')
            ->select(DB::raw('
        users.name,
          IF (users.horas_totales = 187,
           CASE
            WHEN asistencia.hora_fin IS NULL
                THEN   0
           WHEN asistencia.estado = "M"
               THEN 0
        
            WHEN (SUBSTRING(asistencia.hora_inicio,1,8) >= "09:00:00"  AND SUBSTRING(asistencia.hora_inicio,1,8) < "09:31:00") AND SUBSTRING(asistencia.hora_fin,1,8) > "19:00:00"
                THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 19:00:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        
            WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "09:00:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:30:00")  AND SUBSTRING(asistencia.hora_fin,1,8) >"18:30:00"
                THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 18:30:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        
            WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "08:30:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:00:00") AND SUBSTRING(asistencia.hora_fin,1,8) > "18:00:00"
                   THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 18:00:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        
            WHEN SUBSTRING(asistencia.hora_inicio,1,8) < "08:00:00"  AND  SUBSTRING(asistencia.hora_fin,1,8) > "17:30:00"
                THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 17:30:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
        

        END
        ,
        CASE
         WHEN asistencia.hora_fin IS NULL
             THEN   0
         WHEN SUBSTRING(asistencia.hora_inicio,1,8)  <= ADDTIME(CONCAT((SELECT entrada FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"),"00:30:00") AND
             (SUBSTRING(asistencia.hora_fin,1,8) >= CONCAT((SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"))
            THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",(SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00" ),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) )
        END
       )
       AS "extras",
       IF (users.horas_totales = 187,
            CASE
                WHEN asistencia.hora_fin IS NULL
                    THEN   0


      
                WHEN (SUBSTRING(asistencia.hora_inicio,1,8) >= "09:00:00"  AND SUBSTRING(asistencia.hora_inicio,1,8) < "09:31:00") AND SUBSTRING(asistencia.hora_fin,1,8) <= "19:00:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 19:00:00" )) )

                WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "09:00:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:30:00")  AND SUBSTRING(asistencia.hora_fin,1,8) <= "18:30:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 18:30:00" )) )

                WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "08:30:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:00:00") AND SUBSTRING(asistencia.hora_fin,1,8) <= "18:00:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 18:00:00" )) )

                WHEN SUBSTRING(asistencia.hora_inicio,1,8) < "08:00:00"  AND  SUBSTRING(asistencia.hora_fin,1,8) <= "17:30:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 17:30:00" )) )

                WHEN  SUBSTRING(asistencia.hora_inicio,1,8) > "09:31:00" AND SUBSTRING(asistencia.hora_fin,1,8) <= "19:00:00"
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 19:00:00" )) )  + 120

                WHEN  SUBSTRING(asistencia.hora_inicio,1,8) > "09:31:00" AND SUBSTRING(asistencia.hora_fin,1,8) >= "19:00:00"
                    THEN 120 - (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 19:00:00" ) ,  CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8)) ) )   

            END
            ,
            CASE
                WHEN asistencia.hora_fin IS NULL
                    THEN   0
                WHEN SUBSTRING(asistencia.hora_inicio,1,8) >  ADDTIME(CONCAT((SELECT entrada FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"),"00:30:00") AND
                    (SUBSTRING(asistencia.hora_fin,1,8) < CONCAT((SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"))
                    THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",(SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00" ),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) ) - 120
            END
            )
            AS "deuda",
            asistencia.fecha, asistencia.hora_inicio, asistencia.hora_fin,asistencia.estado ,WEEKDAY(asistencia.fecha) + 1 AS "Dia"
        '))->whereYear('asistencia.fecha', '=',$year)->whereMonth('asistencia.fecha','=', $month)
            ->where('users.estado','=','1')->where('users.id','=',$id)->orderBy('users.name','ASC')->get();


            return view('frontend.horas_extras',compact('query','user','month','year'));



        }


        public function clientes () {
            $clients = Client::where('activo',1)->orderby('id','DESC')->paginate(20);
            return view ('frontend.clients.index', compact('clients'));
        }

        public function clientes_edit ($id) {
            $client = Client::find($id);
            return $client;
        }
        public function clientes_create () {
           
            return view ('frontend.clients.create');
        }

        public function clientes_save (Request $request) {
      
         try {
             $message = 'Registrado correctamente';
           
            if (isset($request->id_cliente )) {
                $client = Client::find($request->id_cliente);
                $client->nombre  = $request->cliente;
                $client->razon_social  = $request->razon_social;
                $client->ruc  = $request->ruc;
                $client->type  = $request->tipo;           
                $client->save();
                $message  = 'Actualizado correctamente';
                
            }
            else {

                $client = new Client;
                $client->nombre  = $request->cliente;
                $client->razon_social  = $request->razon_social;
                $client->ruc  = $request->ruc;
                $client->type  = $request->tipo;
                $client->activo  = 1;
                $client->prioridad  = 3;
                $client->save();
            }
            return  [
                'status' => 200,
                'message' => $message
            ];
         } catch (\Exception $e) {
             return [
                 'status' => 400, 
                 'message' => 'No se pudo realizar la acción.',
                 'message_error' => $e->getMessage(), 
                 'line_error' => $e->getLine(), 
                 'code_error' => $e->getCode()
             ];
         }

        }

        public function clientes_delete (Request $request) {
          
            try {
                $client = Client::find($request->id);
                $client->activo = 0;
                $client->save();
                return [
                    'status' => 200,
                    'message' => 'Se eliminó correctamente'
                ];
            } catch (\Exception $e) {
               return [
                   'status' => 400,
                   'message' => 'No se pudo eliminar',
                   'message_error' => $e->getMessage()
               ];
            }
        }

        public function asignar_clientes () {
            $clients_users = ClienteUser::join('clientes','cliente_user.cliente_id','=','clientes.id')
                            ->join('users','cliente_user.user_id','=','users.id')
                            ->join('tribus','users.tribu_id','tribus.id')
                            ->select('cliente_user.id','clientes.id as cliente_id','users.id as user_id','users.name as usuario','clientes.nombre as cliente','clientes.razon_social','tribus.nombre as tribu')
                            ->orderby('cliente_user.id','DESC')
                            ->paginate(20);

            $clients = Cliente::where('activo','1')
                        ->orderby('type','ASC')
                        ->orderby('nombre','ASC')
                        ->get();
            $users = User::where('estado',1)
                    ->where('tipo','ejecutiva')
                    ->orderby('name','ASC')
                    ->get();

            return view('frontend.assign_clients.index', compact('clients_users','clients','users'));
        }

        public function asignar_clientes_store (Request $request) {
            try {
                $message = 'registrado correctamente ! &#128512';
                if (isset($request->id_cliente_user)) {
                    $client_user =  ClienteUser::find($request->id_cliente_user); 
                    $client_user->cliente_id = $request->cliente; 
                    $client_user->user_id = $request->user;
                    $client_user->save();
                    $message = 'actualizado correctamente ! &#128512';
                }
                else {

                    $client_user = new ClienteUser; 
                    $client_user->cliente_id = $request->cliente; 
                    $client_user->user_id = $request->user;
                    $client_user->save();
                }
                
                return [
                    'status' => 200,
                    'message' => $message
                ];
            } catch (\Exception $e) {
              return [
                        'status' => 400,
                        'message' => 'no se pudo relizar la operación &#128553', 
                        'message_error' => $e->getMessage(),
                        'message_code' => $e->getCode(),
                        'message_line' => $e->getLine()

              ];
            } 
        }

        public function asignar_clientes_show ($id) {
            $cliente_user = ClienteUser::find($id);
            return $cliente_user;
        }
        public function asignar_clientes_delete (Request $request){
           try {
                $cliente_user = ClienteUser::find($request->id);
                $cliente_user->delete();
                return [
                    'status' => 200,
                    'message' => 'Se eliminó correctamente &#128512'
                ];
           } catch (\Exception $e) {
            return [
                'status' => 400,
                'message' => 'No se pudo eliminar &#128553',
                'message_error' => $e->getMessage()
            ];
           }
        }

        public function asignar_feriado ($fecha) {
            try {
                $users = User::where('estado','1')->get();
         
            foreach ($users as $key => $user) {
                $asistencia = new Asistencia;
                $asistencia->fecha = $fecha;
                $asistencia->estado = 'J';
                $asistencia->descripcion = 'FERIADO';
                $asistencia->user_id = $user->id;
                $asistencia->save();
            }
            return [
                'code' => 200,
                'message' => 'Feriado asignado'
            ];

            } catch (\Exception $e) {
              return [
                  'code' => 200,
                  'message' => 'no se pudo asignar el feriado',
                  'message_error' => $e->getMessage(),
                  'code_error' => $e->getCode(),
                  'line_error' => $e->getLine()
              ];
            }
            



        }

        private function query_horas_extras($year,$month) {


           $query = DB::table('asistencia')
           ->join('users','users.id','=','asistencia.user_id')
           ->select(DB::raw('
           users.name,users.id,
             IF (users.horas_totales = 187,
      
            ROUND(SUM(  CASE
               WHEN asistencia.hora_fin IS NULL
                   THEN   "no marcó salida"
               WHEN asistencia.estado = "M"
                   THEN "Llego tarde"
       
                   WHEN (SUBSTRING(asistencia.hora_inicio,1,8) >= "09:00:00"  AND SUBSTRING(asistencia.hora_inicio,1,8) < "09:31:00") AND SUBSTRING(asistencia.hora_fin,1,8) > "19:00:00"
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 19:00:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
      
                   WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "09:00:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:30:00")  AND SUBSTRING(asistencia.hora_fin,1,8) >"18:30:00"
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 18:30:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
      
                   WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "08:30:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:00:00") AND SUBSTRING(asistencia.hora_fin,1,8) > "18:00:00"
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 18:00:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
      
                   WHEN SUBSTRING(asistencia.hora_inicio,1,8) < "08:00:00"  AND  SUBSTRING(asistencia.hora_fin,1,8) > "17:30:00"
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 17:30:00" )  ,   CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))   ) )
               END
           )/60),
           ROUND(SUM(    
           CASE
                   WHEN asistencia.hora_fin IS NULL
                       THEN   "no marcó salida"
                   WHEN SUBSTRING(asistencia.hora_inicio,1,8)  <= ADDTIME(CONCAT((SELECT entrada FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"),"00:30:00") AND
                       (SUBSTRING(asistencia.hora_fin,1,8) >= CONCAT((SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"))
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",(SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00" ),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) )
           END
           )/60    
           ))
          AS "extras",
          IF (users.horas_totales = 187,
               ROUND(SUM(
               CASE
                   WHEN asistencia.hora_fin IS NULL
                       THEN   "no marcó salida"
      
      
           
                   WHEN (SUBSTRING(asistencia.hora_inicio,1,8) >= "09:00:00"  AND SUBSTRING(asistencia.hora_inicio,1,8) < "09:31:00") AND SUBSTRING(asistencia.hora_fin,1,8) <= "19:00:00"
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 19:00:00" )) )
      
                   WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "09:00:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:30:00")  AND SUBSTRING(asistencia.hora_fin,1,8) <= "18:30:00"
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 18:30:00" )) )
      
                   WHEN (SUBSTRING(asistencia.hora_inicio,1,8) < "08:30:00" AND SUBSTRING(asistencia.hora_inicio,1,8) >= "08:00:00") AND SUBSTRING(asistencia.hora_fin,1,8) <= "18:00:00"
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 18:00:00" )) )
      
                   WHEN SUBSTRING(asistencia.hora_inicio,1,8) < "08:00:00"  AND  SUBSTRING(asistencia.hora_fin,1,8) <= "17:30:00"
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 17:30:00" )) )
      
                   WHEN  SUBSTRING(asistencia.hora_inicio,1,8) > "09:31:00" AND SUBSTRING(asistencia.hora_fin,1,8) <= "19:00:00"
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))  ,  CONCAT(asistencia.fecha," 19:00:00" )) )  + 120
      
                   WHEN  SUBSTRING(asistencia.hora_inicio,1,8) > "09:31:00" AND SUBSTRING(asistencia.hora_fin,1,8) >= "19:00:00"
                       THEN 120 - (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," 19:00:00" ) ,  CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8)) ) )   
      
               END
              )/60) ,
              ROUND(SUM(
               CASE
                   WHEN asistencia.hora_fin IS NULL
                       THEN   "no marcó salida"
                   WHEN SUBSTRING(asistencia.hora_inicio,1,8) >  ADDTIME(CONCAT((SELECT entrada FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"),"00:30:00") AND
                       (SUBSTRING(asistencia.hora_fin,1,8) < CONCAT((SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00"))
                       THEN (TIMESTAMPDIFF(MINUTE,CONCAT(asistencia.fecha," ",(SELECT salida FROM horarios_users WHERE dia_id = (WEEKDAY(asistencia.fecha) + 1) AND user_id = asistencia.user_id),":00" ),CONCAT(asistencia.fecha," ",SUBSTRING(asistencia.hora_fin,1,8))) ) - 120
               END
               )/60))
               AS "deuda"
           '))->whereYear('asistencia.fecha','=',$year)
             ->whereMonth('asistencia.fecha','=',$month)
             ->where('users.estado',1)->groupBy('asistencia.user_id')->orderBy('users.name','ASC')->get();

        return $query;
        }



        public  function resumen_horas_deuda_extras() {
            try {

                $myData = [];
                $query = DB::table('horas_extras')
                  ->join('users','users.id','=','horas_extras.user_id')
                  ->select(DB::raw('users.name,users.id,SUM(horas_extras.horas_extras) as extras,SUM(horas_extras.horas_deuda) as deuda'))
                  ->where('users.estado',1)
                  ->groupBy('users.id')
                  ->get();

            // recorremos las horas recompensadas de cada usuario
            
            foreach ($query as $user ) {
            $recompensado = HorasRecompensadas::select(DB::raw('SUM(horas) as horas_recompensadas'))
              ->groupby('user_id')
              ->where('user_id','=',$user->id)
              ->get();
           if (count($recompensado) > 0 ) {
             $horas_recompensadas = (int) $recompensado[0]->horas_recompensadas;
           }
           else {
             $horas_recompensadas = 0;
           }
           
           $user->deuda = $user->deuda != null ? $user->deuda : 0;
           $user->extras = $user->extras != null ? $user->extras : 0;
           $neto = $user->extras - ($user->deuda + $horas_recompensadas );
            $data   = [
             'user_id' => $user->id,
             'horas_extras' => $user->extras,
             'horas_deuda' => $user->deuda,
             'recompensadas' => $horas_recompensadas,
             'user' => $user->name,
             'neto' => $neto
            ];
            array_push($myData, $data);
          }

        DB::table('resumen')->truncate();
        foreach ($myData as  $data) {
            $resumen = new Resumen();
            
            $resumen->user_id = $data['user_id'];
            $resumen->horas_extras = $data['horas_extras'];
            $resumen->horas_deuda = $data['horas_deuda'];
            $resumen->horas_recompensadas = $data['recompensadas'];
            $resumen->user = $data['user'];

            $resumen->neto = $data['neto'];
            $resumen->save();
        }
       

        return [
            'message' => 'Success',
            'status' => 'Ok',
            'data' => $myData
        ];

        } catch (\Exception $e) {
        return [
            'message' => $e->getMessage(),
            'status' => 500
        ];
        }
        }
public function trello(){

    return view("frontend.trello");

}

    }


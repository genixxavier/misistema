<?php

namespace App\Http\Controllers;

use App\Tema;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCotizacionesRequest;
use App\Idreunion;
use App\Cliente;
use App\Cotizacion;
use App\ContactReport;
use App\Reunion;
use App\User;
use App\ServicioCotizacion;
class CotizacionesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_cotizacion = Cotizacion::orderby('id','desc')->paginate(8);
        
        
        // dd($list_cotizacion);
        return view ('frontend.cotizaciones',compact('list_cotizacion'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->select('contact_reports.id as id','idreunion.nombre as nombre','clientes.nombre as cliente','contact_reports.cr_fecha_insert as fecha')->orderby('contact_reports.id','desc')->get();
        $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->select('contact_reports.id as id','contact_reports.cr_tema as tema','clientes.nombre as cliente','contact_reports.cr_fecha_insert as fecha')->orderby('contact_reports.id','desc')->get();
        $id = '';
        $tema = '';
        $flag = '';
        $true = '';
        return view ('frontend.crear_cotizacion' ,compact('contact','id','tema','flag','true'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    
    public function store(Request $request)
    {



        function arrayToText ($data) {
            $text = '';
                foreach ($data as $item) {
                        $text = $text .$item .'.';
                    };		

            return $text;
        }
        $cotizacion = new Cotizacion;
        $nameServices = arrayToText($request->nameService);

        $cotizacion->c_services_name = $nameServices ;

        $subTotal = arrayToText($request->subTotal);
        $cotizacion->c_sub_total = $subTotal ;
        
        $descuento = arrayToText($request->descuento);
        $cotizacion->c_descuento = $descuento ;
        
        $descripcion = arrayToText($request->description);
        $cotizacion->c_descripcion = $descripcion ;
        $cotizacion->c_serie = $request->serie;
        $cotizacion->c_numero = $request->numero;
        // Calcular el total
        $sumTotal = 0; 
        for ($i = 0 ; $i < count($request->subTotal); $i++)  {
            $sumTotal = $sumTotal + ($request->subTotal[$i] - $request->descuento[$i]);

        }

        $total = Cotizacion::all();
        $cotizacion->c_num = count($total) + 1 ;
        $igv  = round (($sumTotal * 0.18),2);
        $total_neto = $sumTotal + $igv;
        
        $cotizacion->c_total = $total_neto;
        
        // dd($sumTotal);

        $cotizacion->c_nombre = $request->nombre;

        $cotizacion->contact_reports_id = $request->id_cr;

        $date_today = date('Y-m-d G-i-s');
        $fecha = date('Y-m-d');
        $date_vencimiento = strtotime ( '+1 month' , strtotime ( $fecha ) ) ;
        $date_vencimiento = date ( 'Y-m-d' , $date_vencimiento );
        
        $cotizacion->c_version = 1;
        $cotizacion->c_fecha = $date_today;
        $cotizacion->c_estado = 'Seguimiento';
        $cotizacion->c_fecha_vencimiento = $date_vencimiento;
       
        $cotizacion->save();
        return redirect()->route('cotizaciones_index');

        return $request;
    }

    /**
     * 
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        // $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->select('contact_reports.id as id','idreunion.nombre as nombre','clientes.nombre as cliente','contact_reports.cr_fecha_insert as fecha')->orderby('contact_reports.id','desc')->get();
        function returnArray ($data) {
          	
            $arr = explode('.',$data);
            array_pop($arr);
        return $arr;
        }

        $cotizacion_edit = Cotizacion::find($id);
        $nameServices = returnArray($cotizacion_edit->c_services_name);
        $subtotal = returnArray($cotizacion_edit->c_sub_total);
        $descuento = returnArray($cotizacion_edit->c_descuento);
        $descripcion = returnArray($cotizacion_edit->c_descripcion);
        $contact = ContactReport::find($cotizacion_edit->contact_reports_id);
        $reunion = Idreunion::select('nombre')->where('id',$contact->reuniones_id)->get();
        return view ('frontend.edit_cotizacion', compact('cotizacion_edit','reunion','contact','nameServices','subtotal','descuento','descripcion'));
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        function arrayToText ($data) {
            $text = '';
                foreach ($data as $item) {
                        $text = $text .$item .'.';
                    };		
              
            return $text;
        }

        $cotizacion_id_report = Cotizacion::find($id);

        $cotizacion = new Cotizacion;
        $nameServices = arrayToText($request->nameService);

        $cotizacion->c_services_name = $nameServices ;

        $subTotal = arrayToText($request->subTotal);
        $cotizacion->c_sub_total = $subTotal ;
        
        $descuento = arrayToText($request->descuento);
        $cotizacion->c_descuento = $descuento ;
        
        $descripcion = arrayToText($request->description);
        $cotizacion->c_descripcion = $descripcion ;
        $cotizacion->c_nombre = $request->nombre;
        $cotizacion->contact_reports_id = $cotizacion_id_report->contact_reports_id;
        $date_today = date('Y-m-d G-i-s');
        
        $cotizacion->c_version = $cotizacion_id_report->c_version + 1;
        $cotizacion->c_fecha = $date_today;
        // $cotizacion->c_estado = $request->estado;
        $total = Cotizacion::all();
        $cotizacion->c_num = count($total) + 1 ;
         // Calcular el total
         $sumTotal = 0; 
         for ($i = 0 ; $i < count($request->subTotal); $i++)  {
             $sumTotal = $sumTotal + ($request->subTotal[$i] - $request->descuento[$i]);
 
         }
         $igv  = round (($sumTotal * 0.18),2);
        $total_neto = $sumTotal + $igv;
    
        $cotizacion->c_total = $total_neto;
        
         $cotizacion->c_serie = $request->serie;
         $cotizacion->c_numero = $request->numero;
         $fecha = date('Y-m-d');
         $date_vencimiento = strtotime ( '+1 month' , strtotime ( $fecha ) ) ;
         $date_vencimiento = date ( 'Y-m-d' , $date_vencimiento );
         $cotizacion->c_fecha_vencimiento = $date_vencimiento;

        $cotizacion->save();
        
        return redirect()->route('cotizaciones_index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function viewContactReports () 
    {
        $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')->select('contact_reports.id','idreunion.nombre','clientes.nombre as cliente','contact_reports.cr_fecha_insert')->orderby('contact_reports.id','desc')->get();
        return $contact;
    }

    public function cotizar ($id) {
       $contact = Idreunion::join('clientes','clientes.id','=','idreunion.cliente_id')
            ->join('contact_reports','contact_reports.reuniones_id','=','idreunion.id')
            ->select('contact_reports.id as id','contact_reports.cr_tema as tema','clientes.nombre as                              cliente','contact_reports.cr_fecha_insert as fecha')
            ->orderby('contact_reports.id','desc')->get();
        $creport = ContactReport::find($id);
       // $id = $creport->id ;
      //  $tema = $creport->cr_tema;
     //  $flag = 'flag';
      // $true = 'as';
    //    dd($tema);

         //Find Contact Report
        $contact_report = ContactReport::find($id);
        //Find que reunion le pertenece el contact report
        $id_reunion = Idreunion::find($contact_report->reuniones_id);
        //Find Cliente
        $cliente = Cliente::find($id_reunion->cliente_id);
        // Find los temas del contact_report
        $temas = Tema::where('id_contact_report','=',$id)->get();
        $title_tema = '';
        foreach ($temas as $tema ) {
            $title_tema .= $tema->title.',';
        }
       $title_tema = substr($title_tema,0,-1);

        $last_contact_report =  ContactReport::orderby('id','DESC')->take(1)->get();
        $last_contact_report = collect($last_contact_report);
        $contact_num = $last_contact_report->first();

        $num = $contact_num->cr_num + 1 ;
        //Flag para mostrar estas variables en el frontend
        $flag = 1 ;
        return view ('frontend.crear_cotizacion' ,
               compact('contact_report','cliente','flag' , 'contact' ,'title_tema',
                   'num'));
    }


    public function ver_pdf ($id) {
        $cotizacion = Cotizacion::find($id);
        $contact = ContactReport::select('reuniones_id')->where('id','=',$cotizacion->contact_reports_id)->get();
       
        $reu = Idreunion::select('cliente_id')->where('id','=',$contact[0]->reuniones_id)->get();
        
        $cliente  = Cliente::select('nombre')->where('id','=',$reu[0]->cliente_id)->get();
        $name_cliente = $cliente[0]->nombre;
        
        $fecha = $cotizacion->c_fecha;
        $fecha = explode(' ',$fecha);
        $fecha_registro = $fecha[0];
       

        function returnArray ($data) {
          	
            $arr = explode('.',$data);
            array_pop($arr);
        return $arr;
        }

        
        $nameServices = returnArray($cotizacion->c_services_name);
        $subtotal = returnArray($cotizacion->c_sub_total);
        $descuento = returnArray($cotizacion->c_descuento);
       
        $descripcion = returnArray($cotizacion->c_descripcion);
        $igv  = round (($cotizacion->c_total * 0.18),2);
        $total_neto = $cotizacion->c_total + $igv;
       
        $pdf = \PDF::loadView('pdf.cotizaciones_new',[
            'nombre_cliente' => $name_cliente , 
            'cotizacion' => $cotizacion,
            'nameServices' => $nameServices,
            'subtotal' => $subtotal,
            'descuento' => $descuento,
            'descripcion' => $descripcion,
            'igv' => $igv, 
            'total_neto' => $total_neto, 
            'fecha_registro' => $fecha_registro
        ]
    )->setOptions([ 'defaultFont' => 'arial']);
        return $pdf->stream();
    }

    public function add_cotizacion(Request $request){
        //dd($request->all());

        $add = new ServicioCotizacion;
         $add->title = $request->data_title;
         $add->description = $request->data_description;
         $add->nota = $request->data_nota;
         $add->precio = $request->data_precio;
         $add->descuento = $request->data_descuento;
         $add->total = $request->data_total;
         $add->id_cotizacion = $request->id_cotizacion;
         $add->estado = 1;
         $add->save();

      // $servicio = ServicioCotizacion::find($add->id);

         return view('frontend.view_servicio');

         // $data = TemaAcuerdo::find($request->id);
         // return $data;


    }
}

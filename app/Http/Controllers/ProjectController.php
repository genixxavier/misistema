<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\{
    Areas,
    Cliente,
    PaymentFiles,
    PaymentImg,
    ProjectsAreas,
    Etapas,
    ProjectsEtapas,
    Cotizacion,
    StatusCotizacion,
    Projects,
    ServicioCotizacion,
    ContactReport,
    Idreunion
};
use Carbon\Carbon;
class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $project = DB::table('cotizaciones')
        ->select(DB::raw('c_contacto,projects.id,projects.name,projects.estado ,(SELECT clientes.nombre FROM clientes  WHERE clientes.id = cotizaciones.id_cliente) as marca,projects.id_cotizacion'))
        ->join('projects','projects.id_cotizacion','=','cotizaciones.id')
        ->orderby('id','desc')->paginate(10);

        return view('frontend.project.index')->with(compact('project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cotizaciones = Cotizacion::join('status_cotizacion','cotizaciones.id','=','status_cotizacion.id_cotizacion')
            ->join('clientes','cotizaciones.id_cliente','=','clientes.id')
            ->select('cotizaciones.c_contacto','clientes.nombre as cliente','clientes.ruc','clientes.razon_social','cotizaciones.c_num','cotizaciones.c_version','cotizaciones.c_fecha','cotizaciones.id')->where('status_cotizacion.status','=','1')->orderBy('cotizaciones.id','DESC')
            ->get();
        $areas = Areas::all();

        return view ('frontend.project.create')->with(compact('cotizaciones','areas')) ;
    }

    public function  create_project($id) {
        $coti = Cotizacion::find($id);
        $areas = Areas::all();
        //$cotizaciones = Cotizacion::join('status_cotizacion','cotizaciones.id','=','status_cotizacion.id_cotizacion')->select('cotizaciones.c_contacto','cotizaciones.c_ruc','cotizaciones.c_razon_social','cotizaciones.c_num','cotizaciones.c_version','cotizaciones.c_fecha','cotizaciones.id')->where('status_cotizacion.status','=','1')->get();
        $cotizaciones = Cotizacion::join('status_cotizacion','cotizaciones.id','=','status_cotizacion.id_cotizacion')
            ->join('clientes','cotizaciones.id_cliente','=','clientes.id')
            ->select('cotizaciones.c_nombre_cotizacion','cotizaciones.c_contacto','clientes.nombre as cliente','clientes.ruc','clientes.razon_social','cotizaciones.c_num','cotizaciones.c_version','cotizaciones.c_fecha','cotizaciones.id')->where('status_cotizacion.status','=','1')
            ->orderBy('cotizaciones.id','DESC')
            ->get();

        return view ('frontend.project.create')->with(compact('areas','cotizaciones','coti'));
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        try {
                DB::beginTransaction();

                $proyecto = new Projects;
                $proyecto->name = $request->input('name');
                $proyecto->estado = 0;
                $proyecto->id_cotizacion = $request->input('id_cotizacion');
                $proyecto->save();
                $p1 = $this->insertar_project_areas($request->area1,1,$proyecto->id);
                $p2 = $this->insertar_project_areas($request->area2,2,$proyecto->id);
                $p3 = $this->insertar_project_areas($request->area3,3,$proyecto->id);
                $p4 = $this->insertar_project_areas($request->area4,4,$proyecto->id);
                $p5 = $this->insertar_project_areas($request->area5,5,$proyecto->id);
                $p6 = $this->insertar_project_areas($request->area6,6,$proyecto->id);
                $p7 = $this->insertar_project_areas($request->area7,7,$proyecto->id);
                $etapas = $request->input('etapas');

                if (!empty($etapas)) {
                    foreach ($etapas as $key => $etapa) {
                        $etapa_project =  ProjectsEtapas::find($etapa);
                        $etapa_project->id_project = $proyecto->id;
                        $etapa_project->save();
                    }
                }
            
            
            DB::commit();
            return [
                'message' => 'Proyecto registrado !!!',
                'status' => 200
            ];
        } catch (\Exception $e) {
            DB::rollBack();
            return [
                'message' => 'No se pudo registrar :c', 
                'status' => 400,
                'error' => $e->getMessage(),
                'line' => $e->getLine(),
                'code' => $e->getCode()
            ];

        }

        
    }
    private function insertar_project_areas ($data,$id_area,$id_proyecto) {
        $project_area = new ProjectsAreas();
        $project_area->id_project = $id_proyecto;
        $project_area->percent = $data;
        $project_area->date = Carbon::now();
        $project_area->id_area = $id_area;
        $project_area->save();
        return $project_area;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $project = Projects::find($id);

        $p = ProjectsEtapas::where('id_project','=',$project->id)->get();
        $num_pa = count($p);

        $p_condicion = ProjectsEtapas::where('id_project','=',$project->id)->where('condition','=',1)->get();

        $x = (count($p_condicion) * 100 ) / $num_pa;
        $x = round($x);
        $cotizacion = Cotizacion::find($project->id_cotizacion);
        $project_areas = ProjectsAreas::join('areas','areas.id','=','projects_areas.id_area')->select('areas.name','projects_areas.percent','projects_areas.id' , 'projects_areas.id_area as id_area')->where('projects_areas.id_project','=',$id)->get();
        $project_etapas = ProjectsEtapas::join('etapas','projects_etapas.id_etapa','=','etapas.id')
            ->select(DB::raw('projects_etapas.id,DATE_FORMAT(projects_etapas.start_date,"%d-%m-%Y") as start_date,DATE_FORMAT(projects_etapas.end_date,"%d-%m-%Y") as end_date,etapas.nombre,etapas.id as id_etapa,projects_etapas.condition'))
        //->select('projects_etapas.id','projects_etapas.start_date','projects_etapas.end_date','etapas.nombre','etapas.id as id_etapa','projects_etapas.condition')
        ->where('id_project','=',$id)->get();
        $areas = Areas::all();

        //$cotizaciones = Cotizacion::join('status_cotizacion','cotizaciones.id','=','status_cotizacion.id_cotizacion')->select('cotizaciones.c_contacto','cotizaciones.c_ruc','cotizaciones.c_razon_social','cotizaciones.c_num','cotizaciones.c_version','cotizaciones.c_fecha','cotizaciones.id')->where('status_cotizacion.status','=','1')->get();
        $cotizaciones = Cotizacion::join('status_cotizacion','cotizaciones.id','=','status_cotizacion.id_cotizacion')
            ->join('clientes','cotizaciones.id_cliente','=','clientes.id')
            ->select('cotizaciones.c_contacto','clientes.nombre as cliente','clientes.ruc','clientes.razon_social','cotizaciones.c_num','cotizaciones.c_version','cotizaciones.c_fecha','cotizaciones.id')->where('status_cotizacion.status','=','1')
            ->orderBy('cotizaciones.id','DESC')
            ->get();
        return view('frontend.project.edit')->with(compact('x','id','areas','cotizaciones','project_areas','project','project_etapas','cotizacion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   
        try {
            

            $proyecto = Projects::find($request->input('id_project'));
            $proyecto->name = $request->input('name');
            $proyecto->id_cotizacion = $request->input('id_cotizacion');
            $proyecto->save();
            $p1 = $this->affected_project_area($proyecto->id,1,$request->area1);
            $p2 = $this->affected_project_area($proyecto->id,2,$request->area2);
            $p3 = $this->affected_project_area($proyecto->id,3,$request->area3);
            $p4 = $this->affected_project_area($proyecto->id,4,$request->area4);
            $p5 = $this->affected_project_area($proyecto->id,5,$request->area5);
            $p6 = $this->affected_project_area($proyecto->id,6,$request->area6);
            $p7 = $this->affected_project_area($proyecto->id,7,$request->area7);
            $etapas = $request->input('etapas');

            if (!empty($etapas)) {
                foreach ($etapas as $key => $etapa) {
                    $etapa_project =  ProjectsEtapas::find($etapa);
                    $etapa_project->id_project = $proyecto->id;
                    $etapa_project->save();
                }
            }
        
         
                return [
                    'message' => 'Proyecto Actualizado !!!',
                    'status' => 200
                ];
        } catch (\Exception $e) {
           
            return [
                'message' => 'No se pudo registrar :c', 
                'status' => 400,
                'error' => $e->getMessage(),
                'line' => $e->getLine(),
                'code' => $e->getCode()
            ];

            }
    }
    private  function affected_project_area ($id_project,$id_area,$percent) {
        $pa = ProjectsAreas::where('id_project',$id_project)->where('id_area',$id_area)->get();
        $project_area =  ProjectsAreas::find($pa[0]->id) ;
        $project_area->percent = $percent;
        $project_area->id_area = $id_area;
        $project_area->save();
        return $project_area;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function  store_area ( Request $request)  {
        try {
          
            if ($request->input('id_project_area') !== null ) {
                
                $project_area =  ProjectsAreas::find($request->input('id_project_area')) ;
                $project_area->percent = $request->input('percent');
                $project_area->id_area = $request->input('area');
                $project_area->save();
                $area = Areas::find($request->input('area'));
                $flag = 0;
                return view('frontend.project.area')->with(compact('project_area','area','flag'));    
              
            }
            else {
                $project_area = new ProjectsAreas() ;
                $project_area->percent = $request->input('percent');
                $project_area->date = Carbon::now();
                $project_area->id_area = $request->input('area');
                $project_area->save();
                $area = Areas::find($request->input('area'));
                $flag = 1;
                return view('frontend.project.area')->with(compact('project_area','area','flag'));
               
            }
           

        } catch (\Exception $e) {
            return [
                'message' => $e->getMessage(),
                'line' => $e->getLine(),
                'code' => $e->getCode()
            ];
        }
    }
    public function store_etapa(Request $request) {
        try {
            $x  = 0;

            DB::beginTransaction();
                if ($request->input('id_project_etapa') !== null)  {

                    $etapa_project =  ProjectsEtapas::find($request->input('id_project_etapa')) ; 
                    $etapa_project->start_date = $this->convert_date($request->input('date_start')) ;
                    $etapa_project->end_date =$this->convert_date($request->input('date_end')) ;
                    $etapa_project->save();
                    $etapa = Etapas::find($etapa_project->id_etapa);
                    $etapa->nombre = $request->input('name_etapa') ;
                    $etapa->save(); 

                    $date_end = $request->input('date_end');
                    
                }
                else {

                    $etapa = new Etapas();
                    $etapa->nombre = $request->input('name_etapa') ;
                    $etapa->save();

                    $etapa_project = new ProjectsEtapas() ; 
                    $etapa_project->start_date = $this->convert_date($request->input('date_start')) ;
                    $etapa_project->end_date =$this->convert_date($request->input('date_end')) ;
                    $etapa_project->date = Carbon::now();
                    $etapa_project->id_etapa = $etapa->id;
                    if (isset($request->flag_id)) {
                        $etapa_project->id_project = $request->flag_id;
                    }
                    if (isset($request->id_project)) {
                        $etapa_project->id_project = $request->id_project;
                    }
                    $etapa_project->save();
                  /*  if (isset($request->flag_id)) {
                        $p = ProjectsEtapas::where('id_project','=',$request->flag_id)->get();
                        $num_pa = count($p);
                        $p_condicion = ProjectsEtapas::where('id_project','=',$request->flag_id)->where('condition','=',1)->get();
                        $x = (count($p_condicion) * 100 ) / $num_pa;
                        $x = round($x);
                    }*/
                    $date_end = $request->input('date_end');

                   
                }
                $flag = 0;
                if ($request->edit == 1) {
                    $flag = 1;
                }
            $etapa_project->start_date = $this->convert_date($etapa_project->start_date) ;
            $etapa_project->end_date = $this->convert_date( $etapa_project->end_date) ;

            DB::commit();
            return view('frontend.project.etapa')->with(compact('x','etapa','etapa_project','date_end','flag'));
        } catch (\Exception $e) {
            DB::rollback();
            return [
                'message' => $e->getMessage(),
                'line' => $e->getLine(),
                'code' => $e->getCode()
            ];
        }
        
    } 
    public function getPercent (Request $request) {
        $id_projecto = $request->id;
        $p = ProjectsEtapas::where('id_project','=',$id_projecto)->get();
        if (count($p) > 0) {
            $num_pa = count($p);
            $p_condicion = ProjectsEtapas::where('id_project','=',$id_projecto)->where('condition','=',1)->get();
            $x = (count($p_condicion) * 100 ) / $num_pa;
            $x = round($x);
            return [
                'percent' => $x
            ];
        }
        return [
            'percent' => 0
        ];

    }
    public function delete_area (Request $request) {
        try {
            $project_areas = ProjectsAreas::find($request->id);
            $project_areas->delete();
            return [
                'message' => 'Eliminado correctamente',
                'status' => 200
            ] ;
        } catch (\Exception $e) {
            return [
                'message' => 'No se pudo eliminar',
                'status' => 400
            ] ;
        }
    }

    public function delete_etapa (Request $request) {
        try {
            
            $etapa = Etapas::find($request->id_etapa);
            $etapa->delete();
            $project_etapa = ProjectsEtapas::find($request->id_project_etapa);
            $project_etapa->delete();
            $p = ProjectsEtapas::where('id_project','=',$project_etapa->id_project)->get();
            $num_pa = count($p);

            $p_condicion = ProjectsEtapas::where('id_project','=',$project_etapa->id_project)->where('condition','=',1)->get();
            if ($num_pa != 0) {

                $x = (count($p_condicion) * 100 ) / $num_pa;
                $x = round($x);
            }
            else {
                $x = 0;
            }

            return [
                'message' => 'Eliminado correctamente',
                'x' => $x,
                'status' => 200
            ] ;

        } catch (\Exception $e) {
            return [
                'message' => 'No se pudo eliminar',
                'status' => 400,
                'e' => $e->getMessage()
            ] ;
        }
    } 

    public function change_condition (Request $request) {
        $project = ProjectsEtapas::find($request->id);

        if ($project->condition == 0) {
            $project->condition = 1;
        }
        else {
            $project->condition = 0;
        }
        $project->update();
        $p = ProjectsEtapas::where('id_project','=',$project->id_project)->get();
        $num_pa = count($p);
        $p_condicion = ProjectsEtapas::where('id_project','=',$project->id_project)->where('condition','=',1)->get();
        $x = (count($p_condicion) * 100 ) / $num_pa;
        $x = round($x);
        $project_true = Projects::find($project->id_project);
        $project_true->estado = $x;
        $project_true->save();
        return [
            'result' => $x,
            'status' => 200,
            'message' => 'se actualizo correctamente'
        ];

    }

    public function view($id) {
        $project = Projects::find($id);
        $cotizacion = Cotizacion::find($project->id_cotizacion);
        $cliente = Cliente::find($cotizacion->id_cliente);
        $p = ProjectsEtapas::where('id_project','=',$project->id)->get();
        $num_pa = count($p);

        $p_condicion = ProjectsEtapas::where('id_project','=',$project->id)->where('condition','=',1)->get();

        $x = (count($p_condicion) * 100 ) / $num_pa;
        $x = round($x);

        $project_areas = ProjectsAreas::join('areas','areas.id','=','projects_areas.id_area')->select('areas.name','projects_areas.percent','projects_areas.id' , 'projects_areas.id_area as id_area')->where('projects_areas.id_project','=',$id)->get();
        $project_etapas = ProjectsEtapas::join('etapas','projects_etapas.id_etapa','=','etapas.id')
            ->select('projects_etapas.id','projects_etapas.start_date','projects_etapas.end_date','etapas.nombre','etapas.id as id_etapa','projects_etapas.condition')
            ->where('id_project','=',$id)->get();
        $pagos = PaymentFiles::where('id_project','=',$id)->where('type','=',1)->get();
        $gastos_extras = PaymentFiles::where('id_project','=',$id)->where('type','=',2)->get();

        $count_pagos = count($pagos);
        $count_gastos_extras = count($gastos_extras);
        $resources_p = [];
        $resources_ge = [];
        $total_p = 0;
        $total_ge = 0;



        $pdf = \PDF::loadView('pdf.projects',[
                'total_p' => $total_p,
                'total_ge' => $total_ge,
                'resources_p' => $resources_p,
                'resources_ge' => $resources_ge,
                'count_pagos' => $count_pagos,
                'count_gastos_extras' => $count_gastos_extras,
                'pagos' => $pagos,
                'gastos_extras' => $gastos_extras,
                'cliente' => $cliente,
                'x' => $x,
                'id' => $id,
                'project_areas' => $project_areas,
                'project' => $project,
                'project_etapas' => $project_etapas,
                'cotizacion' => $cotizacion
            ]
        )->setOptions([ 'defaultFont' => 'arial']);
        return $pdf->stream();


       // return view('frontend.project.view')->with(compact('total_p','total_ge','resources_p','resources_ge','count_pagos','count_gastos_extras','pagos','gastos_extras','cliente','x','id','project_areas','project','project_etapas','cotizacion'));

    }



    private function convert_date ($date) {
        $d = explode('-',$date);
        $d = $d[2].'-'.$d[1].'-'.$d[0];
        return $d;
    }
    private function decode_date ($date) {
        $d = explode('-',$date);
        $d = $d[0].'-'.$d[1].'-'.$d[2];
        return $d;
    }

    public function ver_pdf ($id) {

        $cotizacion = Cotizacion::find($id);
        $cliente = '';
        if ($cotizacion->contact_reports_id != 0 || $cotizacion->contact_reports_id != null) {
            $contact_report = ContactReport::find($cotizacion->contact_reports_id);

            $reunion = Idreunion::find($contact_report->reuniones_id);
            $cliente  = Cliente::find($reunion->cliente_id);
        }

        $servicios = ServicioCotizacion::where('id_cotizacion','=',$cotizacion->id)->get();
        $precio_total = 0;
        if (count($servicios) > 0 ) {
            foreach ($servicios as $servicio) {
                $precio_total += $servicio->total;
            }
        }
        else {
            $precio_total = $servicios->total;
        }

        $fecha = $cotizacion->c_fecha;
        $fecha = explode(' ',$fecha);
        $fecha_new = explode('-' , $fecha[0]);

        $year = $fecha_new[0];
        $mes = '';
        $day = $fecha_new[2];
        switch ($fecha_new[1]) {
            case '01':
                $mes = 'Enero';
                break;
            case '02':
                $mes = 'Febrero';
                break;
            case '03':
                $mes = 'Marzo';
                break;
            case '04':
                $mes = 'Abril';
                break;
            case '05':
                $mes = 'Mayo';
                break;
            case '06':
                $mes = 'Junio';
                break;
            case '07':
                $mes = 'Julio';
                break;
            case '08':
                $mes = 'Agosto';
                break;
            case '09':
                $mes = 'Septiembre';
                break;
            case '10':
                $mes = 'Octubre';
                break;
            case '11':
                $mes = 'Noviembre';
                break;
            case '12':
                $mes = 'Diciembre';
                break;
        }
        $sum = $precio_total + $cotizacion->c_extras;
        $precio_total= number_format($precio_total,2,'.',',');
        $inversion_total = number_format($sum,2,'.',',');
        $cotizacion->c_extras = number_format($cotizacion->c_extras,2,'.',',');
        $cliente_cotizacion = Cliente::find($cotizacion->id_cliente);
        $pdf = \PDF::loadView('pdf.project_cotizacion',[
                'cliente_cotizacion' => $cliente_cotizacion,
                'cotizacion' => $cotizacion,
                'servicios' => $servicios,
                'day' => $day,
                'month' => $mes,
                'year' => $year,
                // 'cliente' => $cliente->nombre,
                'cliente' => $cliente,
                'total' => $precio_total,
                'inversion_total' => $inversion_total
            ]
        )->setOptions([ 'defaultFont' => 'arial']);
        return $pdf->stream();

    }

}

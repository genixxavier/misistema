<?php

namespace App\Http\Controllers;
use App\{
    User,
    Tribu,
    Areas
};
use Storage;
use File;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::join('tribus','users.tribu_id','=','tribus.id')
        ->join('areas','users.area_id','=','areas.id')
        ->select('users.name as nombre,users.email,users.tipo,users.horas_totales,areas.name as area, tribus.nombre as tribu, users.codigo, users.estado')
        ->where('users.estado','=',1)
        ->get();
        $tribu = Tribu::all();
        $areas = Areas::all();

        return view ('frontend.users');
    }

    public function getData () {
        
        $users = User::leftjoin('tribus','users.tribu_id','=','tribus.id')
        ->join('areas','users.area_id','=','areas.id')
        ->select('users.name as nombre','users.email','users.tipo','users.horas_totales','areas.name as area', 'tribus.nombre as tribu',
            'users.codigo', 'users.estado','users.id','areas.id as area_id','tribus.Id as tribu_id','users.fecha_ingreso','users.img'
            )
        /*  ,
            'nombres','apellidos','genero','nacionalidad','documento','n_documento','puesto','modalidad_contrato','fecha_vencimiento_contrato',
            'tipo_contrato','tiempo','banco','interbancario','email_personal','celular','tel_fijo','fecha_nacimiento','persona_emergencia',
            'telefono_persona_emergencia','domicilio','distrito','numero_cuenta_bancaria'    
        */
        ->where('users.estado','=',1) 
        ->orderby('users.name','ASC')
        ->get();

        // dd($users);
        
        
        $tribu = Tribu::all();
        $areas = Areas::all();
        return [
            'users' => $users,
            'tribus' => $tribu,
            'areas' => $areas
        ];
    }

    
    public function destroy(Request $request)
    {
       
        try {
            $user = User::find($request->id);
            $user->estado = 0;
            $user->save();
            return [
                'status' => 200,
                'message' => 'Eliminado correctamente',
              
            ];
     } catch (\Exception $e) {
        return [
            'status' => 400,
            'message' => 'No se pudo eliminar',
            'message_error' => $e->getMessage(),
            'code_error' => $e->getCode()
        ];
     }
    }

    public function updateUser (Request $request) {
       try {
        
        $user = User::find($request->id);
        if (\Auth::user()->id != $user->id) {
            return [
                'status' => 401,
                'message' => 'Solo puedes actualizar tus datos'
               
            ];
        }
        $user->name = $request->nombres.' '.$request->apellidos;
        $user->email = $request->email;
        $user->area_id = $request->area;
        $user->tribu_id = $request->tribu;
        $user->fecha_ingreso = $request->fecha_ingreso;
        // $user->nombres = $request->nombres;
        // $user->apellidos = $request->apellidos;
        // $user->genero = $request->genero;
        // $user->nacionalidad = $request->nacionalidad;
        // $user->documento = $request->documento;
        // $user->n_documento = $request->n_documento;
        // $user->puesto = $request->puesto;
        // $user->modalidad_contrato = $request->modalidad_contrato;
        // $user->fecha_vencimiento_contrato = $request->fecha_vencimiento_contrato;
        // $user->tipo_contrato = $request->tipo_contrato;
        // $user->tiempo = $request->tiempo;
        // $user->banco = $request->banco;
        // $user->interbancario = $request->interbancario;
        // $user->email_personal = $request->email_personal;
        // $user->numero_cuenta_bancaria = $request->numero_cuenta_bancaria;
        // $user->celular = $request->celular;
        // $user->tel_fijo = $request->tel_fijo;
        // $user->fecha_nacimiento = $request->fecha_nacimiento;
        // $user->persona_emergencia = $request->persona_emergencia;
        // $user->telefono_persona_emergencia = $request->telefono_persona_emergencia;
        // $user->domicilio = $request->domicilio;
        // $user->distrito = $request->distrito;
        if ($request->file('img')) {
            $file = $request->file('img');
            $extension = $file->getClientOriginalExtension();
            $fname = $file->getFilename().'.'.$extension;
            Storage::disk('img_user')->put($fname,File::get($file));
           $user->img = $fname;
        }
        $user->save();
        return [
            'status' => 200,
            'message' => 'Actualizado correctamente' 
        ];
       } catch (\Exception $e) {
          return [
              'status' => 400,
              'message' => 'No se pudo actualizar',
              'message_error' => $e->getMessage(),
              'code_error' => $e->getCode()
          ];
       }

      
    }
}

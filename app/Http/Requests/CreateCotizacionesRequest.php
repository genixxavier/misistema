<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCotizacionesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'nombre' => ['required'], 
            'nameService' => ['required'],
            'subTotal' => ['required'],
            'descuento' => ['required'],
            'description' => ['required'],
            'serie' => ['required'], 
            'numero' => ['required'], 
            'id_cr' => ['required'],
            
        ];
    }
    public function messages () {
        return [
            'nombre.required' => 'Ingrese el nombre de contacto',
            'nameService.required' => 'Ingrese el nombre del servicio',
            'subTotal.required' => 'Ingrese el precio',
            'descuento.required' => 'Ingrese el descuento de la cotización',
            'description.required' => 'Ingrese la descripción del servicio',
            'serie.required' => 'Ingrese la serie de la factura', 
            'numero.required' => 'Ingrese el número de la factura',
            'id_cr.required' => 'Seleccioné el Contact Report '
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateContactReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tema' =>  ['required'],
            'acuerdosM' => ['required'],
            'acuerdosC' => ['required'],
            'integrantes_c' => ['required'],
            // 'cliente' => ['required']
        ];
    }
    public function messages() {
        return [
            'tema.required' => 'Por favor, escribe el tema ',
            'acuerdosM.required' => 'Por favor, escribe los acuerdos ',
            'acuerdosC.required' => 'Por favor, escribe los acuerdos ',
            'integrantes_c.required' => 'Por favor, escribe los integrantes ',
            // 'cliente.required' => 'Por favor, seleccione el cliente '
        ];
    }
}

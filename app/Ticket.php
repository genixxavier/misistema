<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{

   	protected $table = 'tickets';

    protected $primary = 'id';
    
    protected $fillable = [
    	'nombre','estado','descripcion','fecha_inicio','fecha_fin','fecha_limite','monto','horas_pedido','horas_supervision','cliente_id','user_id','area_id','user_create'
    ];

    public function colaborador()
    {
        return $this->belongsTo('App\Colaborador');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }


    public function area()
    {
        return $this->belongsTo('App\Areas');
    }
    public static function narea($id)
    {
        $dd =  Areas::where('id','=',$id)->first();
        return $dd;
    }
    public static function gettickets($id)
    {
        return "aaaa";
    }


    public static function tickets_por_area($area){

        $tickets = Ticket::where('estado','!=',0)
                ->where('estado','!=',4)
                ->where('estado','!=',2)
                ->where('area_id','=',$area)
                ->where('estado','!=',6)
                ->orderby('fecha_inicio')
                ->get();

        $fecha = date('Y-m-d H:i:s');


        for($i=0;$i < count($tickets) ; $i++){
        
            if($tickets[$i]->estado == 5){
                if(strtotime($tickets[$i]->fecha_inicio) <= strtotime($fecha)){
                    $tickets[$i]->estado = 1;
                    $tickets[$i]->save();
                    
                    //al historial
                    $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                    $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                    $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                    $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                    $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                    $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                    $ticket_hist->estado = 1;
                    $ticket_hist->save();
                }
                if(strtotime($tickets[$i]->fecha_fin) <= strtotime($fecha)){
                    $tickets[$i]->estado = 3;
                    $tickets[$i]->save();

                    //al historial
                    $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                    $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                    $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                    $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                    $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                    $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                    $ticket_hist->estado = 3;
                    $ticket_hist->save();
                }
            }
            if($tickets[$i]->estado == 1){
                if(strtotime($fecha) > strtotime($tickets[$i]->fecha_fin)){
                    $tickets[$i]->estado = 3;
                    $tickets[$i]->save();

                    //al historial
                    $ticket_hist = TiketHistorial::create(['id_tiket' => $tickets[$i]->id, 'nombre'=>$tickets[$i]->nombre,'descripcion'=>$tickets[$i]->descripcion, 'cliente_id'=>$tickets[$i]->cliente_id,'user_id'=>$tickets[$i]->user_id,'area_id'=>$tickets[$i]->area_id, 'user_create' => $tickets[$i]->user_create]);
                    $ticket_hist->fecha_inicio = $tickets[$i]->fecha_inicio;
                    $ticket_hist->fecha_fin =  $tickets[$i]->fecha_fin;
                    $ticket_hist->fecha_limite = $tickets[$i]->fecha_limite;
                    $ticket_hist->horas_pedido = $tickets[$i]->horas_pedido;
                    $ticket_hist->horas_supervision = $tickets[$i]->horas_supervision;
                    $ticket_hist->estado = 3;
                    $ticket_hist->save();
                }
            }
        }

        return $tickets;
    }  
}

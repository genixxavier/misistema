<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactReport extends Model
{
    protected $table = 'contact_reports';

    public function listar_reunion () {
        return $this->hasMany(Idreunion::class);
    }

    public function reunion () {
        return $this->belongsto(Idreunion::class);
    }
}

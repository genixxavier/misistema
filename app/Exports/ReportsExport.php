<?php

namespace App\Exports;

use App\Tickets;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;

class ReportsExport implements FromCollection
{
   protected  $inicio ;
   protected $fin;
   public function __construct($start,$end )
   {
       $this->inicio = $start;
       $this->fin = $end;
   }

    public function collection()
    {

        $query = DB::table('tickets')->join('users','tickets.user_id','=','users.id')
            ->join('clientes','tickets.cliente_id','=','clientes.id')
            ->select(DB::raw('users.name,clientes.nombre, SUM(tickets.horas_pedido) as Horas'))
            ->where('tickets.fecha_inicio', '>=' , $this->inicio)
            ->where('tickets.fecha_inicio','<=',$this->fin)
            ->where('users.estado','=','1')
            ->groupBy('tickets.user_id')
            ->groupBy('tickets.cliente_id')
            ->orderBy('users.name','ASC')
            ->orderBy('clientes.nombre','ASC')
            ->get();
        return $query;
    }
}

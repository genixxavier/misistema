<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorarioUser extends Model
{
    //
    protected $table = 'horarios_users';

    protected $primary = 'id';
    
    protected $fillable = [
    	'user_id','dia_id','entrada','salida'
    ];

    public function dia()
    {
        return $this->belongsTo('App\Dias');
    }
}

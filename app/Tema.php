<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tema extends Model
{
    //
    protected $table = 'temas';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'title','estado','id_contact_report'
    ];

    public static function temas_acuerdos($id,$t)
    {
        $acuerdos = TemaAcuerdo::where('type','=',$t)
                                ->where('id_tema','=',$id)
                                ->get();
        return $acuerdos;
    }

    public static function temas_acuerdos_pdf($id)
    {
        $acuerdos = TemaAcuerdo::where('id_tema','=',$id)
                                ->get();
        return $acuerdos;
    }
}

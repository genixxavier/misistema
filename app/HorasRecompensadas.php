<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HorasRecompensadas extends Model
{
    //
    protected $table = 'horas_recompensadas';
    public $timestamps = false;
}

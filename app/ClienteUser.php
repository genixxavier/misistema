<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteUser extends Model
{
    //
    protected $table = 'cliente_user';

    protected $primary = 'id';
    
    protected $fillable = [
    	'cliente_id','user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }
}

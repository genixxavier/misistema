<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiketHistorial extends Model
{

    protected $table = 'tickets_historial';

    protected $primary = 'id';
    
    protected $fillable = [
    	'id_tiket','nombre','estado','descripcion','fecha_inicio','fecha_fin','fecha_limite','monto','horas_pedido','horas_supervision','cliente_id','user_id','area_id','user_create'
    ];

    public function colaborador()
    {
        return $this->belongsTo('App\Colaborador');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }

}

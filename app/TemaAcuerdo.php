<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemaAcuerdo extends Model
{
    //
    protected $table = 'temas_acuerdos';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'description','category','type','estado','id_tema'
    ];
}

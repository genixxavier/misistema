<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Etapas extends Model
{
    protected $table = 'etapas';
    public  $timestamps = false;
}

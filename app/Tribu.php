<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tribu extends Model
{
    protected $table = 'tribus';
    protected $primarykey = 'id';
    protected $fillabel = [
    	'id','nombre'
    ];

    public static function datos($id,$mes){
    	return PuntosExtras::where('area_id',$id)->where('mes',$mes)->get();
    }

}

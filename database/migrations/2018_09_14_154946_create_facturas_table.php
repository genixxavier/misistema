<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facturas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('f_fecha');
            $table->integer('proyecto_id')->unsigned();
            $table->foreign('proyecto_id')->references('id')->on('proyectos');
            $table->integer('orden_compras_id')->unsigned();
            $table->foreign('orden_compras_id')->references('id')->on('orden_compras');
            $table->integer('constancia_id')->unsigned();
            $table->foreign('constancia_id')->references('id')->on('constancias');
            // $table->integer('file_comp_id')->unsigned();
            // $table->foreign('file_comp_id')->references('id')->on('file_complementarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facturas');
    }
}

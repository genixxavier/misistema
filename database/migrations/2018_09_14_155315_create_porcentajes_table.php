<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePorcentajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('porcentajes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('proyectos_id')->unsigned();
            $table->foreign('proyectos_id')->references('id')->on('proyectos');
            $table->integer('areas_id')->unsigned();
            $table->foreign('areas_id')->references('id')->on('areas');
            $table->string('porcentaje',45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('porcentajes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemasAcuerdosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temas_acuerdos', function (Blueprint $table) {
            $table->increments('id');

            $table->text('description');
            $table->string('category',1);
            $table->string('type',1);
            $table->integer('estado');
            $table->integer('id_tema')->unsigned();
            $table->foreign('id_tema')->references('id')->on('temas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temas_acuerdos');
    }
}

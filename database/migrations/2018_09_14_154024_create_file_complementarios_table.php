<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileComplementariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_complementarios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('fc_name');
            $table->date('fc_fecha');
            $table->string('fc_ruta_image');
            $table->integer('id_factura')->unsigned();
            $table->foreign('id_factura')->references('facturas')->on('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_complementarios');
    }
}

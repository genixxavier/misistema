<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdreunionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('idreunion', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre');
            $table->boolean('estado')->nullable();
            $table->mediumtext('descripcion')->nullable();
            $table->integer('horas')->nullable();

            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('user_create')->unsigned()->nullable();
            $table->foreign('user_create')->references('id')->on('users');

            $table->timestamp('fecha_inicio')->nullable();

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('idreunion');
    }
}

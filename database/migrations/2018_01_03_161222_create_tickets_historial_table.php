<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsHistorialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets_historial', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tiket');
           // $table->integer('id_tiket')->unsigned()->nullable();
           // $table->foreign('id_tiket')->references('id')->on('tickets');

            $table->string('nombre');
            $table->boolean('estado')->nullable();
            $table->text('descripcion')->nullable();

            $table->timestamp('fecha_inicio')->nullable();
            $table->timestamp('fecha_fin')->nullable();
            $table->timestamp('fecha_limite')->nullable();

            $table->decimal('monto', 7, 2)->nullable();

            $table->integer('horas_pedido')->nullable();
            $table->integer('horas_supervision')->nullable();

            $table->integer('cliente_id')->unsigned();
            $table->foreign('cliente_id')->references('id')->on('clientes');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');



            $table->integer('area_id')->unsigned()->nullable();
            $table->foreign('area_id')->references('id')->on('areas');

            $table->integer('user_create');
        //    $table->foreign('user_create')->references('id')->on('users');
            $table->text('observacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets_historial');
    }
}

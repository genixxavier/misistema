<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGastosExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gastos_extras', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('ge_desc',100);
            $table->decimal('ge_monto',7,2);
            $table->integer('proyectos_id')->unsigned();
            $table->foreign('proyectos_id')->references('id')->on('proyectos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gastos_extras');
    }
}

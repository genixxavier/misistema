<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizaciones', function (Blueprint $table) {
            $table->increments('id');
          
            $table->decimal('c_monto',7,2);
            $table->decimal('c_descuento',7,2);
            $table->longText('c_desc_descuento');
            $table->longText('c_des');
            $table->integer('c_version');
            $table->string('c_nombre',100);
            $table->string('c_estado',100);
            $table->timestamp('c_fecha');
            $table->integer('contact_reports_id')->unsigned();
            $table->foreign('contact_reports_id')->references('id')->on('contact_reports');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizaciones');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_reports', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('cr_fecha_reunion');
            $table->timestamp('cr_fecha_insert');
            // $table->longText('cr_contenido');
            $table->longtext('cr_acuerdosC');
            $table->longtext('cr_acuerdosM');
            $table->longtext('cr_integrantesC');
            $table->longtext('checkElementsM');
            $table->longtext('checkElementsC');
            $table->integer('reuniones_id')->unsigned();
            $table->foreign('reuniones_id')->references('id')->on('idreunion');
            
            $table->longtext('cr_total_percent_m');
            $table->longtext('cr_total_percent_c');
            $table->string('cr_tema',100);
            $table->integer('cr_num');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_reports');
    }
}

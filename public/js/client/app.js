let  myForm = document.getElementById('myForm')

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// REGISTRO CLIENTES 
myForm.addEventListener('submit' , e => {
    e.preventDefault()
    let data = {
        'cliente' : e.target.cliente.value, 
        'razon_social' : e.target.razon_social.value,
        'ruc' : e.target.ruc.value,
        'tipo' : e.target.tipo.value
    }
    uri = '/cliente/save'
    if (e.target.id_cliente.value ) {
       
        data.id_cliente = e.target.id_cliente.value
    }

    $.ajax({
        method: 'POST',
        url : uri ,
        data : data,
      
        
        success(response) {

            if (response.status !== 200 ) {
                alertify.error(response.message)
            }
            else {
                alertify.success(response.message)
                location.href = '/clientes'
            }
           
        }
    })
})



// EDITAR CLIENTE


const showModal = id => {
    let myForm = document.getElementById('myForm')
    let uri = '/clientes/' + id
    $.ajax({
        method: 'GET',
        url : uri , 
        success(response) {
            myForm.cliente.value = response.nombre 
            myForm.id_cliente.value = response.id 
            myForm.razon_social.value = response.razon_social 
            myForm.ruc.value = response.ruc 
            myForm.tipo.value = response.type 
            myForm.btn_submit.innerHTML = 'Actualizar' 
            
          $('#myModal').modal('show')
         
        }
    })
}

const closeModal = () => {
    let myForm = document.getElementById('myForm')
    myForm.cliente.value =  ''
    myForm.id_cliente.value ='' 
    myForm.razon_social.value = '' 
    myForm.ruc.value =  ''
    myForm.tipo.value = ''
    myForm.btn_submit.texContent = 'Guardar' 
}


 // Eliminar cliente 

 const deleteClient = (id)  => {
    let  uri = '/cliente/delete', 
        data = {
            id
        }
     
    alertify.confirm('Cliente', 'Esta segur@  de eliminar al cliente ?',
     function(){ 
        
        $.ajax({
            method: 'POST',
            url : uri , 
            data : data ,
            success(response) {
            
                if (response.status != 200) {
                    alertify.error(response.message)
                    
                }
                else {
                    alertify.success(response.message) 
                    location.href = '/clientes'
                }
             
             
            }
        })
        
       
    }
    , function(){
         alertify.error('Cancelado')
        
    });
}
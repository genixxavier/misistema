// Globales
const divArea  = document.getElementById('divAreas')
const formArea = document.getElementById('myAddArea')
const formEtapa = document.getElementById('myAddEtapa')
const myForm = document.getElementById('myForm')
// Config Ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// Config Datatables
$(document).ready( function () {
    $('#dataTable').DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    } );
})

// FUNCION PARA CALCULAR LA VALIDACION DE PORCENTAJE
const validatePercent = (array, num) => {
    let flag = 0,
        faltante = 0,
        percents = []
    if (  array.length > 1) {
        array.forEach(element => {
            percents.push(Number(element.value))
        })

        let sumPorcentaje = percents.reduce( (a,b) => a+ b)
        let sum = Number(sumPorcentaje) + Number(num)
        // console.log(sumPorcentaje)
        // console.log(sum)
        faltante = 100 - Number(sumPorcentaje)
        if (sumPorcentaje == 100) {
            flag = 3
        }
        else if (sum  > 100) {

            flag = 2
        }
    }
    return {
        flag,
        faltante
    }
}
// AGREGAR AREAS
formArea.addEventListener('submit', e => {
    e.preventDefault()
    let id_project_area = e.target.id_project_area.value
    let arraysAreasValidate = Array.from(document.querySelectorAll('#arrayAreas'))
    let arrayPorcentaje = Array.from(document.querySelectorAll('#arrayPorcentaje'))
    let myAreas = []
    arraysAreasValidate.forEach(element => {
        myAreas.push(element.value)
    });

    // Validar que no se repitan las areas
    if (myAreas.includes(e.target.area.value)) {
        if (id_project_area == '' || id_project_area == null ) {

            return alertify.warning('El área ya ha sido  seleccionada')
        }
    }


    // Validar que el porcentaje no supere el 100 %
    let validar = validatePercent(arrayPorcentaje,event.target.percent.value)
    if (validar.flag == 2) {
        return alertify.warning('El porcentaje seleccionado supera el 100%, Faltan ' + validar.faltante)
    }

    else if (validar.flag == 3) {

        if (id_project_area == '' || id_project_area == null ) {
            return alertify.warning('100% completado, no puede agregar')
        }
    }
    let data = new FormData(formArea),
        uri = '/funnel/projects/store_area'


    $.ajax({
        method: 'POST',
        url : uri ,
        data : data,
        contentType: false,
        processData : false,
        success(response) {
            /*  validar cuando se esta guardando y cuando se esta editando */

            if (response.message) {
                alertify.error('Ups ! Algo inesperado paso')
                $('#addAreas').modal('hide')
            }
            else {
                if (id_project_area == '' || id_project_area == null ) {

                    $('#divAreas').append(response)
                    alertify.success('Agregado Correctamente')
                }
                else {

                    $(`#area_id${id_project_area}`).replaceWith(response)
                    alertify.success('Actualizado Correctamente')
                }

                $('#addAreas').modal('hide')
            }

            limpiar()

        }
    })
})



// AGREGAR ETAPAS
formEtapa.addEventListener('submit', e => {
    e.preventDefault()
    let id_project_etapa =e.target.id_project_etapa.value,
        id_etapa = e.target.id_etapa.value
    let data = new FormData(formEtapa),
        uri = '/funnel/projects/store_etapa',
        flag = false

    let arraysEtapaValidate = Array.from(document.querySelectorAll('#arraysDate'))

    if (id_project_etapa == '' || id_project_etapa == null) {
        arraysEtapaValidate.forEach(element => {

            let d = e.target.date_start.value
            d  = d.split('-')
            let da = element.value
            da = da.split('-')
            let date = new Date(`${d[2]},${d[1]   },${d[0]}`),
                date_comparacion = new Date(`${da[2]},${da[1]  },${da[0]}`)

            if (date  <= date_comparacion) {
                flag = true
            }
        })
    }

    if (flag) {
        return alertify.warning('La fecha no puede estar dentro de otra etapa')
    }

    $.ajax({
        method: 'POST',
        url : uri ,
        data : data,
        contentType: false,
        processData : false,
        success(response) {


            if (id_project_etapa == '' || id_project_etapa == null) {
                $('#divEtapas').append(response)
                alertify.success('Agregado Correctamente')
            }
            else {
                $(`#etapa_project_id${id_etapa}`).replaceWith(response)
                alertify.success('Se actualizo  Correctamente')
            }

            $('#addEtapas').modal('hide')
            limpiarEtapa()

        }
    })
})

// AGREGAR PROYECTO
myForm.addEventListener('submit', e => {
    e.preventDefault()

    let data = new FormData(myForm),
        uri = '/funnel/projects/store',
        areas = Array.from( document.querySelectorAll('#areas'));
        collect =[];
        areas.forEach(input => {

            collect[`area${input.dataset.area}`] = input.value
        })


        data.append('areas', collect);

    $.ajax({
        method: 'POST',
        url : uri ,
        data : data,
        contentType: false,
        processData : false,
        success(response) {
            console.log(response)
            if (response.status !== 200 ) {
                alertify.error(response.message)
            }
            else {

                alertify.success(response.message)
                location.href = '/funnel/projects/'
            }

        }
    })
})

// FECHAS
$(function() {
    $('#date_start').daterangepicker({
        singleDatePicker: true,
        locale : {
            format : 'DD-MM-YYYY'
        }

    }, function(start) {

        document.getElementById('date_start').value = start.format('DD-MM-YYYY')
    });
});
$(function() {
    $('#date_end').daterangepicker({
        singleDatePicker: true,
        locale : {
            format : 'DD-MM-YYYY'
        }

    }, function(start) {

        document.getElementById('date_end').value = start.format('DD-MM-YYYY')
    });
});

// AGREGAR COTIZACIONES AL INPUT HIDDEN
const addCotizacion = (id,contacto, nombre) => {

    let id_cotizacion = document.getElementById('id_cotizacion'),
        form_contacto = document.getElementById('contacto'),
        name = document.getElementById('name')
    id_cotizacion.value = id
    form_contacto.value = contacto
    name.value = nombre
    $('#modalCotizaciones').modal('hide');
}
const limpiar = ()=> {
    document.getElementById('area').value = 1
    document.getElementById('percent').value = ''
    document.getElementById('id_project_area').value = null
    document.getElementById('btn_add_percent').textContent = 'Guardar'
}

const limpiarEtapa =  () => {
    document.getElementById('name_etapa').value = ''
    document.getElementById('date_start').value = ''
    document.getElementById('date_end').value = ''
    document.getElementById('id_etapa').value = null
    document.getElementById('id_project_etapa').value = null
    document.getElementById('btn_etapa').textContent = 'Guardar'
}

// EDIT ETAPA
const editEtapa = (id_etapa,start_date,end_date,name,id_project_etapa) => {

    document.getElementById('name_etapa').value = name
    document.getElementById('date_start').value = start_date
    document.getElementById('date_end').value = end_date
    document.getElementById('id_project_etapa').value = id_project_etapa
    document.getElementById('id_etapa').value = id_etapa
    document.getElementById('btn_etapa').textContent = 'Actualizar'
    $('#addEtapas').modal('show')

}

// EDITAR AREA
const editArea = (area,percent,id_area) => {
    document.getElementById('area').value = area
    document.getElementById('percent').value = percent
    document.getElementById('id_project_area').value = id_area
    document.getElementById('btn_add_percent').textContent = 'Actualizar'
    percentGlobal = percent
    $('#addAreas').modal('show')
}

// Eliminar Area

const deleteArea = (id,id_area) => {
    console.log(id)
    data = {
        id
    }
    let uri = '/funnel/projects/delete_area'
    alertify.confirm("Areas", "Desea eliminar esta area ?",
        function(){

            $.ajax({
                method: 'POST',
                url : uri ,
                data : data,
                success(response) {

                    if (response.status !== 200 ) {
                        alertify.error(response.message)
                    }
                    else {
                        document.getElementById('area_id'+id).remove()
                        alertify.success(response.message)

                    }

                }
            })

        },
        function(){

        });
}

// Delete Etapa
const deleteEtapa = (id_project_etapa,id_etapa) =>  {


    data = {
        id_project_etapa,
        id_etapa
    }
    let uri = '/funnel/projects/delete_etapa'
    alertify.confirm("Etapas", "Desea eliminar esta etapa ?",
        function(){

            $.ajax({
                method: 'POST',
                url : uri ,
                data : data,
                success(response) {

                    if (response.status !== 200 ) {
                        alertify.error(response.message)
                    }
                    else {
                        document.getElementById('etapa_project_id'+id_project_etapa).remove()

                        alertify.success(response.message)


                    }

                }
            })

        },
        function(){

        });

}

document.getElementById('container_areas').addEventListener('focusout', e => {
    let percentTotal = 0,
        error_percent = document.getElementById('error_percent')
    areas = Array.from(document.querySelectorAll('#areas'))
    areas.forEach( input => percentTotal += Number(input.value))
    error_percent.textContent = percentTotal > 100 ? 'Superaste el 100%' : ''
})
// Globales
const divArea  = document.getElementById('divAreas')
const formArea = document.getElementById('myAddArea')
const formEtapa = document.getElementById('myAddEtapa')
const myForm = document.getElementById('myForm')
let percentGlobal = 0
// Config Ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// Config Datatables
$(document).ready( function () {
    $('#dataTable').DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    } );
})

// FUNCION PARA CALCULAR LA VALIDACION DE PORCENTAJE
const validatePercent = (array, num) => {
    let flag = 0,
        faltante = 0,
        percents = []

    if (  array.length > 1) {
        array.forEach(element => {
            percents.push(Number(element.value))
        })

        let sumPorcentaje = percents.reduce( (a,b) => a+ b)
        // Aqui validamos si el usuario quiere editar, le restamos el porcentaje que quiere editar
        // al total(sumPorcentaje) para que ejecute la funcion correctamente
        // y valide si pasa los 100
        sumPorcentaje = Number(sumPorcentaje) - percentGlobal
        let sum = Number(sumPorcentaje) + Number(num)
        faltante = 100 - Number(sumPorcentaje)

        if (sumPorcentaje == 100) {
            flag = 3
        }
        else if (sum  > 100) {

            flag = 2
        }
    }
    return {
        flag,
        faltante


    }
}
// AGREGAR AREAS
formArea.addEventListener('submit', e => {
    e.preventDefault()

    let id_project_area = e.target.id_project_area.value
    let arraysAreasValidate = Array.from(document.querySelectorAll('#arrayAreas'))
    let arrayPorcentaje = Array.from(document.querySelectorAll('#arrayPorcentaje'))
    let myAreas = []
    arraysAreasValidate.forEach(element => {
        myAreas.push(element.value)
    });

    // Validar que no se repitan las areas siempre siempre y cuando no se quiera actualizar
    // id_project_area permite saber si se quiere actualizar

    if (myAreas.includes(e.target.area.value)) {
        if (id_project_area == '' || id_project_area == null ) {

            return alertify.warning('El área ya ha sido  seleccionada')
        }
    }


    // Validar que el porcentaje no supere el 100 %
    let validar = validatePercent(arrayPorcentaje,event.target.percent.value)
    if (validar.flag == 2) {
        return alertify.warning('El porcentaje seleccionado supera el 100%, Faltan ' + validar.faltante)
    }

    else if (validar.flag == 3) {

        if (id_project_area == '' || id_project_area == null ) {
            return alertify.warning('100% completado, no puede agregar')
        }

    }
    let data = new FormData(formArea)


    if (e.target.id_project_area.value !== '') {
        uri =  './../store_area'

    }
    else {
        uri = '/funnel/projects/store_area'

    }

    $.ajax({
        method: 'POST',
        url : uri ,
        data : data,
        contentType: false,
        processData : false,
        success(response) {
            console.log(response)
            if (response.message) {
                alertify.error('Ups ! Algo inesperado paso')
                $('#addAreas').modal('hide')
            }
            else {


                if (id_project_area == '' || id_project_area == null ) {

                    $('#divAreas').append(response)
                    alertify.success('Agregado Correctamente')
                }
                else {

                    $(`#area_id${id_project_area}`).replaceWith(response)
                    alertify.success('Actualizado Correctamente')
                }

                $('#addAreas').modal('hide')
                limpiar()
            }

        }
    })
})



// AGREGAR ETAPAS
formEtapa.addEventListener('submit', e => {
    e.preventDefault()
    let id_project_etapa =e.target.id_project_etapa.value,
        id_etapa = e.target.id_etapa.value
        id_project = document.getElementById('id_project').value
    let data = new FormData(formEtapa),
        uri = './store_etapa',
        flag = false
        data.append('id_project',id_project)
    if (e.target.id_project_etapa.value == '' || e.target.id_project_etapa.value == null  ) {
        let arraysEtapaValidate = Array.from(document.querySelectorAll('#arraysDate'))
        arraysEtapaValidate.forEach(element => {

            let d = e.target.date_start.value
            d  = d.split('-')
            let da = element.value
            da = da.split('-')
            let date = new Date(`${d[2]},${d[1]  },${d[0]}`),
                date_comparacion = new Date(`${da[2]},${da[1]  },${da[0]}`)

            if (date  <= date_comparacion) {
                flag = true


            }
        });
    }


    if (flag) {
        return alertify.warning('La fecha no puede estar dentro de otra etapa')
    }
    if (e.target.id_project_etapa.value !== '' || e.target.id_project_etapa.value !== null  ) {
        uri = '/funnel/projects/store_etapa'
    }
    data.append('edit',1)

    $.ajax({
        method: 'POST',
        url : uri ,
        data : data,
        contentType: false,
        processData : false,
        success(response) {
            if (response.message) {
                alertify.error('Ups ! Algo inesperado paso')
                $('#addEtapas').modal('hide')
            }
            else {
                if (id_project_etapa == '' || id_project_etapa == null) {
                        $('#divEtapas').append(response)
                     /*   document.getElementById('progress').style.width = `${max}%`
                          document.getElementById('number_progress').textContent = `${max}%`

                      },500)*/

                    alertify.success('Agregado Correctamente')
                }
                else {
                    console.log(id_project_etapa)
                    $(`#etapa_project_id${id_etapa}`).replaceWith(response)
                    alertify.success('Se actualizo  Correctamente')
                }
                $.ajax({
                    method: 'POST',
                    url: '/funnel/projects/getPercent',
                    data: {id: id_project},
                    success(re) {
                        document.getElementById('progress').style.width = `${re.percent}%`
                        document.getElementById('number_progress').textContent = `${re.percent}%`
                    }
                } )
                limpiarEtapa()
                $('#addEtapas').modal('hide')
            }

        }
    })
})

// AGREGAR PROYECTO
myForm.addEventListener('submit', e => {
    e.preventDefault()

    let data = new FormData(myForm),
        uri = '/funnel/projects/update'


    $.ajax({
        method: 'POST',
        url : uri ,
        data : data,
        contentType: false,
        processData : false,
        success(response) {
            console.log(response)
            if (response.status !== 200 ) {
                alertify.error(response.message)
            }
            else {

                 alertify.success(response.message)
                location.href = '/funnel/projects/'

             }

        }
    })
})

// FECHAS
$(function() {
    $('#date_start').daterangepicker({
        singleDatePicker: true,
        locale : {
            format : 'DD-MM-YYYY'
        }

    }, function(start) {

        document.getElementById('date_start').value = start.format('DD-MM-YYYY')
    });
});
$(function() {
    $('#date_end').daterangepicker({
        singleDatePicker: true,
        locale : {
            format : 'DD-MM-YYYY'
        }

    }, function(start) {

        document.getElementById('date_end').value = start.format('DD-MM-YYYY')
    });
});

// AGREGAR COTIZACIONES AL INPUT HIDDEN
const addCotizacion = (id,contacto) => {
    let id_cotizacion = document.getElementById('id_cotizacion'),
        form_contacto = document.getElementById('contacto')
    id_cotizacion.value = id
    form_contacto.value = contacto
    $('#modalCotizaciones').modal('hide');
}

// EDITAR AREA
const editArea = (area,percent,id_area) => {
    document.getElementById('area').value = area
    document.getElementById('percent').value = percent
    document.getElementById('id_project_area').value = id_area
    document.getElementById('btn_add_percent').textContent = 'Actualizar'
    percentGlobal = percent
    $('#addAreas').modal('show')
}

const limpiar = ()=> {
    document.getElementById('area').value = 1
    document.getElementById('percent').value = ''
    document.getElementById('id_project_area').value = null
    document.getElementById('btn_add_percent').textContent = 'Guardar'
}

// Eliminar Area

const deleteArea = (id,id_area) => {
    console.log(id)
    data = {
        id
    }
    let uri = '/funnel/projects/delete_area'
    alertify.confirm("Areas", "Desea eliminar esta area ?",
        function(){

            $.ajax({
                method: 'POST',
                url : uri ,
                data : data,
                success(response) {

                    if (response.status !== 200 ) {
                        alertify.error(response.message)
                    }
                    else {
                        document.getElementById('area_id'+id).remove()
                        alertify.success(response.message)

                    }

                }
            })

        },
        function(){

        });
}

// EDIT ETAPA
const editEtapa = (id_etapa,start_date,end_date,name,id_project_etapa) => {

    document.getElementById('name_etapa').value = name
    document.getElementById('date_start').value = start_date
    document.getElementById('date_end').value = end_date
    document.getElementById('id_project_etapa').value = id_project_etapa
    document.getElementById('id_etapa').value = id_etapa
    document.getElementById('btn_etapa').textContent = 'Actualizar'
    $('#addEtapas').modal('show')

}

const limpiarEtapa =  () => {
    document.getElementById('name_etapa').value = ''
    document.getElementById('date_start').value = ''
    document.getElementById('date_end').value = ''
    document.getElementById('id_etapa').value = null
    document.getElementById('id_project_etapa').value = null
    document.getElementById('btn_etapa').textContent = 'Guardar'
}
//Vlidar percent
const validarPercent = (id_project) => {
    $.ajax({
        method: 'POST',
        url: '/funnel/projects/getPercent',
        data: {id: id_project},
        success(re) {

            document.getElementById('progress').style.width = `${re.percent}%`
            document.getElementById('number_progress').textContent = `${re.percent}%`

        }
    } )
}
// Delete Etapa
const deleteEtapa = (id_project_etapa,id_etapa) =>  {
    let val = validarPercent
    let id_project = document.getElementById('id_project').value
    data = {
        id_project_etapa,
        id_etapa
    }
    let uri = '/funnel/projects/delete_etapa'
    alertify.confirm("Etapas", "Desea eliminar esta etapa ?",
        function(){

            $.ajax({
                method: 'POST',
                url : uri ,
                data : data,
                success(response) {
                   val(id_project)
                    if (response.status !== 200 ) {
                        alertify.error(response.message)
                    }
                    else {
                       
                        document.getElementById('etapa_project_id'+id_etapa).remove()
                        alertify.success(response.message)
                        



                    }

                }
            })

        },
        function(){

        });

}

const changeCondition = (id) => {
    console.log(id)
    $.ajax({
        method: 'POST',
        url : '/funnel/projects/change_condition' ,
        data : {
            id
        },
        success(response) {
            console.log(response)
            if (response.status !== 200 ) {
                alertify.error(response.message)
            }
            else {
                document.getElementById('progress').style.width = `${response.result}%`
                document.getElementById('number_progress').textContent = `${response.result}%`
                alertify.success(response.message)

            }

        }
    })
}
    // Actualizar el progress

const obtenerMaximo = data => {
    numbers = []
    data.forEach(element => {

        console.log(element.value)
        numbers.push(element.value)
    })

    return numbers
}

document.getElementById('container_areas').addEventListener('focusout', e => {
    let percentTotal = 0,
        error_percent = document.getElementById('error_percent')
    areas = Array.from(document.querySelectorAll('#areas'))
    areas.forEach( input => percentTotal += Number(input.value))
    error_percent.textContent = percentTotal > 100 ? 'Superaste el 100%' : ''
})
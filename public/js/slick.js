$(document).ready(function(){
    // $('.variable-width').slick({
    //   prevArrow: false,
    //   nextArrow: false,			
    //   dots: true,
    //   infinite: true,
    //   speed: 300,
    // //   slidesToShow: 1,
    // //   centerMode: true,
    //   variableWidth: true
    // });	    
    
    $('.responsive').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,    // se muestra [n] elementos
        slidesToScroll: 1, // avanza en [n] en [n]
        // variableWidth: true,
        responsive: [
          {
            breakpoint: 992, // < 1024
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 768, // < 768
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 480, //<480
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });

      $('.responsive2').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,    // se muestra [n] elementos
        slidesToScroll: 1, // avanza en [n] en [n]
        responsive:
        [
          {
            breakpoint: 992,
            settings:{
              slidesToShow: 3,
              slidesToScroll: 1,
              dots: true
            }
          },
          {
            breakpoint: 768,
            settings:{
              slidesToShow: 2,
              slidesToScroll: 2

            }
          }
        ]
      });

      $('.responsive2_2').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 4,    // se muestra [n] elementos
        slidesToScroll: 1, // avanza en [n] en [n]
        responsive:
        [
          {
            breakpoint: 992,
            settings:{
              slidesToShow: 3,
              slidesToScroll: 1,
              dots: true
            }
          },
          {
            breakpoint: 768,
            settings:{
              slidesToShow: 2,
              slidesToScroll: 2

            }
          }
        ]
      });

      
      $data = $(".slider.responsive .slick-slide.slick-active").data("slick-index");
      console.log("data: "+$data);


  });

  // $('button[id=btnCancelarFactura]').on('load',function () {

  // });


//   $('.multiple-items').slick({
//     prevArrow: false,
//     nextArrow: false,
//     fade: true,
//     speed: 650,
//     dots: true,
//     autoplay: true,
//     autoplaySpeed: 3000,
//     slidesToShow: 4,
//     slidesToScroll: 4,				
//     responsive: [
//         {
//             breakpoint: 1024,
//             settings: {
//                 slidesToShow: 3,
//                 slidesToScroll: 3,
//                 infinite: true,
//                 dots: true
//             }
//         },
//         {
//             breakpoint: 600,
//             settings: {
//                 slidesToShow: 2,
//                 slidesToScroll: 2
//             }
//         },
//         {
//             breakpoint: 480,
//             settings: {
//             slidesToShow: 1,
//             slidesToScroll: 1
//             }
//         }
//     ]				
// });

// document.addEventListener('load',function(event){
//   // $hola = $('.slider.responsive .slick-slide.slick-active').css('margin-left','60px');
//   console.log("hola ");
// });

$(window).on("load", function(){
  
  // $slick = $(".slider.responsive .slick-slide.slick-active");
  // $ancho = $(".slider.responsive .slick-slide.slick-active").width();
  // // console.log($ancho);
  // $ancho = $ancho - 30;
  // // $(".slider.responsive .slick-slide.slick-active").css('width',$ancho);
  // $(".slider.responsive .slick-slide.slick-active").width($ancho);
  // console.log("ancho: "+$ancho);
  // setTimeout(function(){
  //   $slick = $(".slider.responsive .slick-slide.slick-active");
  //   $ancho = $(".slider.responsive .slick-slide.slick-active").width();
  //   $ancho = $ancho - 30;
  //   $(".slider.responsive .slick-slide").width($ancho);

  //   $('.slider.responsive').on('beforeChange', function(event, slick, currentSlide, nextSlide){
  //     console.log(nextSlide);
  //     $(".slider.responsive .slick-slide.slick-active").width(50);
  //   });
  // }, 100);

});

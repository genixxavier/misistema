// Calcular el total

const sumTotalStaffTable = () => {
  let allTotalStaff = Array.from(document.querySelectorAll('#allTotalCotizador'))
  
  let sumTotal = 0
  let myArray = []
  allTotalStaff.forEach( total => {
    myArray.push(total.value)
    sumTotal += parseFloat(total.value)
  })
 
  return sumTotal.toFixed(2)
 
}

// Cotizador

document.getElementById('btnCotizador').addEventListener('click', e =>  {
  e.preventDefault()
  let horas = document.getElementById('horas').value,
    cotizador_id = document.getElementById('staff').value
  let data  = {
    horas,
    cotizador_id
  }
  let uri = '/add_staff'
  $.ajax({
    method: 'POST',
    url: uri,
    data: data,
    success: function(data){
      document.getElementById('content-staff').innerHTML += data
      let resultSumTotal = sumTotalStaffTable()
      document.getElementById('sumTotalTableStaff').textContent = `S/ ${resultSumTotal}`
    }
  });
  
})

const deleteStaffCotizador = (idStaffCotizador) => {
  let uri = `/deleteStaffCotizador/${idStaffCotizador}`
 
  $.ajax({
    method: 'DELETE',
    url: uri,
    
    success: function(response){
      if (response.status == 200) {
  
        document.getElementById(`staffCotizador-${idStaffCotizador}`).remove()
        alertify.success(response.message)
        let resultSumTotal = sumTotalStaffTable()
        document.getElementById('sumTotalTableStaff').textContent = `S/ ${resultSumTotal}`
      }
      else {
        alertify.error(response.message)
      }
    }
  });
}


// Store project
myForm.addEventListener('submit', e => {
    e.preventDefault()
    let id_payment = document.getElementById('id_payment').value

    let data = new FormData(myForm),
        uri = ''
        if ( id_payment != '') {

            uri = '/funnel/projects/payment/update'
             }
        else {
            console.log('eno')
            uri = '/funnel/projects/payment/store'
         }


    $.ajax({
        method: 'POST',
        url : uri ,
        data : data,
        contentType: false,
        processData : false,
        success(response) {

            if (response.status !== 200 ) {
                alertify.error(response.message)
            }
            else {

                alertify.success(response.message)
            }
            location.href = '/funnel/projects/payment'
        }
    })
})

// Read files
const readFile = (event, id) => {
    let size = event.target.files[0].size
        size = parseInt((size/1024)/1024)
   /*
    if (size >=2) {

        document.getElementById('oc').value = ''

        return event.preventDefault()
    } */
    let div = document.getElementById(id),
        e = event
    let read_extension = e.target.value.split('.'),
        flag_extension = false
    if (read_extension[1] == 'jpg' || read_extension[1] == 'png' || read_extension[1] == 'jpeg') {
        flag_extension = true
    }
    div.innerHTML = ''
    div.style.display = 'flex'

    // Creamos el objeto de la clase FileReader

    let reader = new FileReader();


    // Leemos el archivo subido y se lo pasamos a nuestro fileReader
    reader.readAsDataURL(e.target.files[0]);

    // Le decimos que cuando este listo ejecute el código interno
    reader.onload = function(){
         if (flag_extension) {
             let image = document.createElement('img');
             let div_img = document.createElement('div')
             div_img.setAttribute('class','text-center ')


             image.src = reader.result
             image.style.width = '300px'
             image.style.height = '300px'
             image.setAttribute('class','img-thumbnail')
             div_img.appendChild(image)

             div.appendChild(div_img)
         }
    };

}

// show description
document.getElementById('type').addEventListener('change', e =>  {
    let value = e.target.value
    if (value == 2 ) {
        document.getElementById('content_gastos_extras').style.display  = 'block'
        document.getElementById('content_description').style.display  = 'block'
        document.getElementById('title_oc').textContent = 'Seleccione archivo:'
        document.getElementById('title_factura').textContent = 'Seleccione archivo:'
        document.getElementById('title_codigo_entrada').textContent = 'Seleccione archivo:'
    }
    else {
        document.getElementById('content_gastos_extras').style.display  = 'none'
        document.getElementById('content_description').style.display  = 'none'
        document.getElementById('title_oc').textContent = 'Seleccione OC:'
        document.getElementById('title_factura').textContent = 'Seleccione Factura:'
        document.getElementById('title_codigo_entrada').textContent = 'Seleccione codigo de entrada:'
    }
})

const addProject = (id,proyecto,id_cotizacion) => {
    let id_project = document.getElementById('id_proyecto'),
        project = document.getElementById('proyecto')
    id_project.value = id
    project.value = proyecto
    document.getElementById('uri_oc').href = `/funnel/reporte_cotizacion/${id_cotizacion}`
    $('#modalProyecto').modal('hide');
}




let  myForm = document.getElementById('myForm')

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// REGISTRO EJECUTIV@S - CLIENTES 
myForm.addEventListener('submit' , e => {
    e.preventDefault()
    let data = {
        'user' : e.target.user.value, 
        'cliente' : e.target.cliente.value,
    }
    uri = '/asignar_clientes/store'
    if (e.target.id_cliente_user.value ) {
       
        data.id_cliente_user = e.target.id_cliente_user.value
    }

    $.ajax({
        method: 'POST',
        url : uri ,
        data : data,
      
        
        success(response) {

            if (response.status !== 200 ) {
                alertify.error(response.message)
            }
            else {
                alertify.success(response.message)
                location.href = '/asignar_clientes'
            }
           
        }
    })
})



// EDITAR CLIENTE


const showModal = id => {
    let myForm = document.getElementById('myForm')
    let uri = '/asignar_clientes/' + id
    $.ajax({
        method: 'GET',
        url : uri , 
        success(response) {
            myForm.cliente.value = response.cliente_id 
            myForm.id_cliente_user.value = response.id 
            myForm.user.value = response.user_id 
            myForm.btn_submit.innerHTML = 'Actualizar' 
            
          $('#myModal').modal('show')
         
        }
    })
}

const closeModal = () => {
    let myForm = document.getElementById('myForm')
    myForm.cliente.value = response.cliente_id 
    myForm.id_cliente_user.value = response.id 
    myForm.user.value = response.user_id 
    myForm.btn_submit.texContent = 'Guardar' 
}


 // Eliminar cliente 

 const deleteClient = (id)  => {
    let  uri = '/asignar_clientes/delete', 
        data = {
            id
        }
     
    alertify.confirm('Clientes  | Ejecutiv@', 'Esta segur@  de eliminar el registro ?',
     function(){ 
        
        $.ajax({
            method: 'POST',
            url : uri , 
            data : data ,
            success(response) {
            
                if (response.status != 200) {
                    alertify.error(response.message)
                    
                }
                else {
                    alertify.success(response.message) 
                    location.href = '/clientes'
                }
             
             
            }
        })
        
       
    }
    , function(){
         alertify.error('Cancelado')
        
    });
}
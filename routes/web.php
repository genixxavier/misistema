<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::post('/login', 'PageController@home');

Route::get('/asistencia_diaria/{fecha?}', 'PageController@asistencia_diaria');

Route::get('/loginfake/{id}', 'PageController@loginfake');
Route::get('/bravo', 'PageController@bravo');
Route::group(['middleware' => 'auth'], function () {
  Route::get('/inicio', 'PageController@inicio')->name('inicio');
  Route::get('/historial', 'PageController@historial')->name('historial');
  Route::get('/detalle_tiket', 'PageController@detalle_tiket');
  Route::get('/tiket', 'PageController@tiket');
  Route::get('/trello', 'PageController@trello')->name('trello');;
  Route::get('/tiket_completo', 'PageController@tiket_completo');
  Route::get('/asignar/{id}', 'PageController@asignar')->name('asignar');
  Route::get('/culminar/{id}', 'PageController@culminar')->name('culminar');
  Route::get('/home', 'HomeController@index')->name('home');
  Route::get('/pausar_tk/{id}', 'PageController@pausar_tk')->name('pausar_tk');
  Route::get('/play_tk/{id}', 'PageController@play_tk')->name('play_tk');
  //Route::get('/validar/{id}/4', 'PageController@validar')->name('validar');
  Route::post('/validar/', 'PageController@validar')->name('validar');
  Route::post('/asignar_post/{id}', 'PageController@asignar_post')->name('asignar_post');
  Route::get('/asistencia', 'PageController@asistencia');
  Route::get('/eliminar/{id}', 'PageController@eliminar')->name('eliminar');
  Route::get('/historial/{id}', 'PageController@ordenar')->name('ordenar');
  Route::get('/historial/{colaborador}/{filtro}', 'PageController@filtro_c')->name('filtro_c');
  Route::get('/historial/{colaborador}/{filtro}/{orden}', 'PageController@filtro_c')->name('filtro_c');
  Route::get('/crear_reunion', 'PageController@crear_reunion')->name('crear_reunion');
  Route::post('/create_reunion', 'PageController@create_reunion')->name('create_reunion');
  Route::get('/reuniones', 'PageController@reuniones')->name('reuniones');
  Route::get('/reunion/{id}', 'PageController@detalle_reunion')->name('detalle_reunion');
  Route::get('/reuniones/{id}', 'PageController@reunion_edit')->name('editar_reunion');
  Route::post('/update_reunion', 'PageController@update_reunion')->name('update_reunion');
  Route::post('/delete_reunion/{id}', 'PageController@delete_reunion')->name('eliminar_reunion');
  Route::get('/horarios', 'PageController@horarios')->name('horarios');
  Route::get('/horario/{id}', 'PageController@horario_id');
  Route::post('/horario/create', 'PageController@horario_create');
  Route::post('/horario/update_horario', 'PageController@update_horario')->name('update_horario');

  Route::get('/ticket/{id}', 'PageController@detalle_tiket')->name('detalle_ticket');
  Route::get('/edit/ticket/{id}', 'PageController@edit_ticket')->name('editar_ticket');
  Route::get('/crear_ticket', 'PageController@crear_ticket')->name('crear_ticket');
  Route::post('/create_ticket', 'PageController@create_ticket')->name('create_ticket');
  Route::post('/create_asistencia', 'PageController@create_asistencia')->name('create_asistencia');
  Route::post('/historial/culminarticket', 'PageController@culminarticket')->name('culminarticket');
  Route::post('/postcolaborador', 'PageController@postcolaborador')->name('postcolaborador');

  Route::get('/crear_documento', 'PageController@crear_documento')->name('crear_documento');
  Route::post('/create_documento', 'PageController@create_documento')->name('create_documento');
  Route::get('/documentos', 'PageController@documentos')->name('documentos');
  Route::get('/informes', 'PageController@informes')->name('informes');
  Route::get('/documento/{id}', 'PageController@detalle_documento')->name('detalle_documento');
  Route::get('/edit/documento/{id}', 'PageController@edit_documento')->name('edit_documento');
  Route::post('/update_documento', 'PageController@update_documento')->name('update_documento');
  Route::get('/delete_documento/{id}', 'PageController@delete_documento')->name('delete_documento');

  Route::get('/ticketblank', 'PageController@ticketblank')->name('ticketblank');
  Route::get('/ticket_historial/{id}', 'PageController@ticketblank_id');
  Route::post('/create_ticket2', 'PageController@create_ticket2')->name('create_ticket2');
  Route::get('/ticket_historial/{id}/{semana}', 'PageController@ticketblank_id');
  // Route::post('/horario/update_horario', 'PageController@update_horario')->name('update_horario');
  Route::get('/podio/{mes}', 'PageController@puntajes');
  Route::get('/puntajes/{mes}', 'PageController@puntajes_individuales');

  // cambios genixxavier
  Route::get('/getdatosaeditar/{id}', 'PageController@getdatosaeditar');
  Route::post('/update_ticket2', 'PageController@update_ticket2')->name('update_ticket2');

  // Ver pendientes de las tribus

  Route::get('/showTasks/{tribu}/{fecha}', 'PageController@showTasks')->name('showTasks');


  // new calendar v2
  // Route::get('/calendario', 'TaskController@index');
  Route::get('/calendario/v2', 'CalendarController@index');
  Route::get('/calendario', 'CalendarController@index');
  Route::get('/api/list_tasks', 'CalendarController@list_task');
  Route::get('/api/list_clients', 'CalendarController@list_clients');
  Route::post('/api/add_task_back', 'CalendarController@add_task_back');
  Route::get('/api/list_task_back', 'CalendarController@list_task_back');
  Route::post('/api/add_task', 'CalendarController@add_task');
  Route::post('/api/update_task', 'CalendarController@update_task');
  Route::get('/api/task/{id}', 'CalendarController@task_id');
  Route::post('/api/update_edit_task', 'CalendarController@update_edit_task');
  Route::get('/api/delete_task/{id}', 'CalendarController@delete_task_id');
});

Auth::routes();
Route::get('/', function () {

  return view('welcome');
});
Route::get('/login', 'PageController@home')->name('login');
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/alertas', 'PageController@alerta');
Route::get('/alertascreatividad', 'PageController@alertacm');
Route::get('/alertasadv', 'PageController@alertadv');
Route::get('/alertasart', 'PageController@alertasart');
Route::get('/alertassocialmedia', 'PageController@alertassocialmedia');
// Corregir horas de entrada vs salida
Route::get('/correct_hours', 'PageController@correct_hours')->name('correct_hours');
Route::get('/reunion_marcacion', 'PageController@reunion_marcacion')->name('reunion_marcacion');

Route::get('/find_users', 'PageController@find_users')->name('find_users');
Route::get('/history_assistance_changes', 'PageController@history_assistance_changes')->name('history_assistance_changes');
Route::get('/show_history_assistance', 'PageController@show_history_assistance')->name('show_history_assistance');
Route::post('/find_user_to_correct_hour', 'PageController@find_user_to_correct_hour')->name('find_user_to_correct_hour');
Route::post('/update_asistencia', 'PageController@update_asistencia')->name('update_asistencia');
Route::post('/create_reunion_marcacion', 'PageController@create_reunion_marcacion')->name('create_reunion_marcacion');


// Funnel Cotizacion


Route::group(['prefix' => 'funnel'], function () {
  Route::get('contact_report', 'ContactReportController@contact_report_index')->name('contact_report_index');
  Route::get('download_file/{file}', 'CotizacionesController@download_file')->name('download_file');
  Route::get('cotizaciones', 'CotizacionesController@index')->name('cotizaciones_index');
  Route::get('crear_contact_report', 'ContactReportController@crear_contact_report')->name('crear_contact_report');
  Route::get('crear_cotizacion', 'CotizacionesController@create')->name('cotizaciones_create');
  Route::get('crear_cotizacion_c', 'CotizacionesController@create_2')->name('cotizaciones_create_2');
  Route::post('save_cotizacion', 'CotizacionesController@store')->name('cotizacion_store');
  Route::post('save_cotizacion_2', 'CotizacionesController@store_2')->name('cotizacion_store_"');
  Route::post('save_contact_report', 'ContactReportController@save_contact_report')->name('save_contact_report');
  Route::get('proyectos', 'ContactReportController@proyectos_index')->name('proyectos_index');
  Route::post('clientes_reu_show', 'ContactReportController@clientes_reu_show')->name('clientes_reu_show');
  Route::get('contact_report/{id}', 'ContactReportController@contact_report_edit')->name('contact_report_edit');
  Route::get('contact_report/destroy/{id}', 'ContactReportController@contact_report_eliminar')->name('contact_report_eliminar');
  Route::post('viewReunion', 'ContactReportController@viewReunion')->name('viewReunion');

  Route::post('viewReu', 'ContactReportController@viewReu')->name('viewReu');
  Route::post('put_contact_report', 'ContactReportController@put_contact_report')->name('put_contact_report');
  Route::get('cotizacion/{id}', 'CotizacionesController@edit')->name('edit_cotizaciones');
  Route::get('edit_cotizacion', 'CotizacionesController@edit_cotizacion');
  Route::get('nueva_version/{id}', 'CotizacionesController@edit_2')->name('edit_cotizaciones_2');

  Route::post('cotizacion/{id}', 'CotizacionesController@update')->name('cotizaciones_update');
  Route::post('delete_service', 'CotizacionesController@delete_service')->name('delete_service');
  Route::get('reporte_contact/{id}', 'ContactReportController@ver_pdf')->name('reporte_contact');
  Route::get('cotizar/{id}', 'CotizacionesController@cotizar')->name('cotizar');
  Route::get('reporte_cotizacion/{id}', 'CotizacionesController@ver_pdf')->name('reporte_cotizacion');
  Route::post('cotizacion_cliente', 'CotizacionesController@cotizacion_cliente')->name('cotizacion_cliente');
  Route::get('status_cotizacion', 'CotizacionesController@status_cotizacion')->name('status_cotizacion');
  Route::get('observacion', 'CotizacionesController@observacion')->name('ver_observacion');
  Route::post('editar_acuerdo', 'ContactReportController@editar_acuerdo')->name('editar_acuerdo');
  Route::post('editar_tema', 'ContactReportController@editar_tema')->name('editar_tema');
  Route::post('add_tema', 'ContactReportController@add_tema')->name('add_tema');
  Route::post('add_tema_acuerdo', 'ContactReportController@add_tema_acuerdo')->name('add_tema_acuerdo');
  Route::post('delete_acuerdo_item', 'ContactReportController@delete_acuerdo_item')->name('delete_acuerdo_item');
  Route::post('eliminar_tema', 'ContactReportController@eliminar_tema')->name('eliminar_tema');
  Route::post('update_item_acuerdo', 'ContactReportController@update_item_acuerdo')->name('update_item_acuerdo');
  Route::post('add_cotizacion', 'CotizacionesController@add_cotizacion')->name('add_cotizacion');
  Route::post('show_service_cotizacion', 'CotizacionesController@show_service_cotizacion')->name('show_service_cotizacion');
  Route::post('update_service_cotizacion', 'CotizacionesController@update_service_cotizacion')->name('update_service_cotizacion');
  Route::post('save_cotizacion_in_edit', 'CotizacionesController@save_cotizacion_in_edit')->name('save_cotizacion_in_edit');
  Route::post('update_cotizacion', 'CotizacionesController@update_cotizacion')->name('update_cotizacion');
  Route::post('delete_cotizacion', 'CotizacionesController@delete_cotizacion')->name('delete_cotizacion');
  Route::post('update_status_cotizacion', 'CotizacionesController@update_status_cotizacion')->name('update_status_cotizacion');

  // Projets
  Route::get('projects/', 'ProjectController@index')->name('project.index');
  Route::get('projects/view_pdf/{id}', 'ProjectController@ver_pdf')->name('project.ver_pdf');
  Route::get('projects/create', 'ProjectController@create')->name('project.create');
  Route::get('projects/create_project/{id}', 'ProjectController@create_project')->name('project.create_project');
  Route::get('projects/edit/{id}', 'ProjectController@edit')->name('project.edit');
  Route::get('projects/view/{id}', 'ProjectController@view')->name('project.view');
  Route::post('projects/store_area', 'ProjectController@store_area')->name('project.store_area');
  Route::post('projects/store_etapa', 'ProjectController@store_etapa')->name('project.store_etapa');
  Route::post('projects/update', 'ProjectController@update')->name('project.update');
  Route::post('projects/store', 'ProjectController@store')->name('project.store');
  Route::post('projects/destroy', 'ProjectController@destroy')->name('project.destroy');
  Route::post('projects/delete_area', 'ProjectController@delete_area')->name('project.destroy.area');
  Route::post('projects/delete_etapa', 'ProjectController@delete_etapa')->name('project.destroy.etapa');
  Route::post('projects/change_condition', 'ProjectController@change_condition')->name('project.change_condition');
  Route::post('projects/getPercent', 'ProjectController@getPercent')->name('project.getPercent');

  // Payment
  Route::get('projects/payment', 'PaymentController@index')->name('payment.index');
  Route::get('projects/payment/create', 'PaymentController@create')->name('payment.create');
  Route::get('projects/payment/{id}/edit', 'PaymentController@edit')->name('payment.edit');
  Route::post('projects/payment/store', 'PaymentController@store')->name('payment.store');
  Route::post('projects/payment/update', 'PaymentController@update')->name('payment.update');
  Route::get('projects/payment/generate_payment', 'PaymentController@generate_payment')->name('payment.generate_payment');
  Route::get('projects/add_payment/{id}', 'PaymentController@add_payment')->name('payment.add_payment');


});
Route::get('alertasall', 'PageController@alertasall');
Route::get('/prueba', 'PageController@prueba');
Route::get('/prueba_2', 'PageController@prueba_2');
Route::get('/download_excel', 'PageController@download_excel')->name('download_excel');
Route::get('/reporte_horas/', 'PageController@reporte_horas')->name('reporte_horas');
Route::get('/horas_extras/', 'PageController@horas_extras')->name('horas_extras');
Route::post('/save_horas_extras', 'PageController@save_horas_extras')->name('save_horas_extras');
Route::get('/content_horas_extras/{month}/{year}', 'PageController@content_horas_extras')->name('content_horas_extras');
Route::get('/horas_extras_detalle/{id}/{year}/{month}', 'PageController@horas_extras_detalle')->name('horas_extras_detalle');
Route::get('/clientes', 'PageController@clientes')->name('clientes');
Route::get('/clientes/{id}', 'PageController@clientes_edit')->name('clientes.edit');
Route::get('/cliente/crear', 'PageController@clientes_create')->name('clientes.create');
Route::post('/cliente/save', 'PageController@clientes_save')->name('clientes.save');
Route::post('/cliente/delete', 'PageController@clientes_delete')->name('clientes.delete');

// Clientes - Ejecutivas 

Route::get('/asignar_clientes', 'PageController@asignar_clientes')->name('asignar_clientes');
Route::get('/asignar_clientes/{id}', 'PageController@asignar_clientes_show')->name('asignar_clientes.show');
Route::post('/asignar_clientes/store', 'PageController@asignar_clientes_store')->name('asignar_clientes.store');
Route::post('/asignar_clientes/update', 'PageController@asignar_clientes_update')->name('asignar_clientes.update');
Route::post('/asignar_clientes/delete', 'PageController@asignar_clientes_delete')->name('asignar_clientes.delete');

Route::get('/asignar_feriado/{fecha}', 'PageController@asignar_feriado')->name('asignar_feriado');
Route::get('/usuarios', 'UserController@index')->name('usuarios');
Route::get('/getData', 'UserController@getData')->name('getData');
Route::post('/updateUser', 'UserController@updateUser')->name('updateUser');
Route::post('/deletUser', 'UserController@destroy')->name('deletUser');

//Horas recompensadas
Route::get('/horas_recompensadas', 'HorasRecompensadasController@index')->name('horas_recompensadas');
Route::get('/horas_recompensadas_data', 'HorasRecompensadasController@data');
Route::post('/horas_recompensadas', 'HorasRecompensadasController@store');
Route::delete('/horas_recompensadas/{id}', 'HorasRecompensadasController@destroy');

//Horas resumen

Route::get('/horas_totales', 'HorasRecompensadasController@resumen')->name('resumen');
Route::get('/resumen_horas', 'PageController@resumen_horas_deuda_extras')->name('resumen');

Route::post('/add_staff', 'CotizacionesController@addStaffToCotizacion')->name('add_staff');
Route::delete('/deleteStaffCotizador/{id}','CotizacionesController@deleteStaffCotizador')->name('deleteStaffCotizador');

Route::get('balance_horas','HorasRecompensadasController@balance_horas')->name('balance_horas');
Route::get('getStaffs','CotizacionesController@getStaffs')->name('getStaffs');

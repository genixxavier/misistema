<?php
// Home
Breadcrumbs::register('contact', function ($breadcrumbs) {
    $breadcrumbs->push('Contact Report ', route('contact_report_index'));
});
Breadcrumbs::register('historial', function ($breadcrumbs) {
    $breadcrumbs->parent('contact');
    $breadcrumbs->push('Historial', route('historial'));
});

Breadcrumbs::register('reuniones', function ($breadcrumbs) {
    $breadcrumbs->parent('contact');
    $breadcrumbs->push('Reuniones', route('reuniones'));
});

Breadcrumbs::register('crear_reunion', function ($breadcrumbs) {
    $breadcrumbs->parent('reuniones');
    $breadcrumbs->push('Crear Reunión', route('crear_reunion'));
});

Breadcrumbs::register('editar_reunion', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('reuniones');
    $breadcrumbs->push('Editar Reunión', route('editar_reunion',$id));
});

Breadcrumbs::register('crear_contact_report', function ($breadcrumbs) {
    $breadcrumbs->parent('contact');
    $breadcrumbs->push('Crear Contact Report', route('crear_contact_report'));
});
Breadcrumbs::register('contact_report_edit', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('contact');
    $breadcrumbs->push('Editar Contact Report', route('contact_report_edit',$id));
});

Breadcrumbs::register('cotizaciones', function ($breadcrumbs) {
    $breadcrumbs->parent('contact');
    $breadcrumbs->push('Cotizaciones', route('cotizaciones_index'));
});

Breadcrumbs::register('crear_cotizacion', function ($breadcrumbs) {
    $breadcrumbs->parent('cotizaciones');
    $breadcrumbs->push('Crear Cotización', route('cotizaciones_create'));
});

Breadcrumbs::register('crear_cotizacion_c', function ($breadcrumbs) {
    $breadcrumbs->parent('cotizaciones');
    $breadcrumbs->push('Crear Cotización sin CR', route('cotizaciones_create_2'));
});

Breadcrumbs::register('editar_cotizacion', function ($breadcrumbs,$id,$flag) {
    $breadcrumbs->parent('cotizaciones');
    if ($flag != 1) {
        $breadcrumbs->push('Editar Cotización', route('edit_cotizaciones',$id));
    }
    else {
        $breadcrumbs->push('Corregir  observación', route('edit_cotizaciones',$id));
    }

});
Breadcrumbs::register('nueva_version', function ($breadcrumbs,$id) {
    $breadcrumbs->parent('cotizaciones');
    $breadcrumbs->push('Generar nueva versión', route('edit_cotizaciones_2',$id));
});

Breadcrumbs::register('ver_estado', function ($breadcrumbs) {
    $breadcrumbs->parent('cotizaciones');
    $breadcrumbs->push('Cambiar estados', route('status_cotizacion'));
});


Breadcrumbs::register('justificacion', function ($breadcrumbs) {
    $breadcrumbs->push('Justificación', route('crear_documento'));
});
Breadcrumbs::register('documentos', function ($breadcrumbs) {
    $breadcrumbs->parent('justificacion');
    $breadcrumbs->push('Documentos', route('documentos'));
});

Breadcrumbs::register('correcion_por_reu', function ($breadcrumbs) {
    $breadcrumbs->parent('reuniones');
    $breadcrumbs->push('Corrección por reunión', route('reunion_marcacion'));
});
require('./bootstrap');
import store from './store'
window.Vue = require('vue');
require('./plugins/index')
Vue.component('lista-component', require('./components/ListaComponent.vue'));
// Vue.component('user-component', require('./components/UserComponent.vue'));
import CotizadorMi from './components/CotizadorMi'
import user_component from './components/UserComponent';
import horas_recompensadas from './components/HorasRecompensadas';
const app = new Vue({
    store,
    el: '#app',
    components: {
        user_component,
        horas_recompensadas,
        cotizador_mi : CotizadorMi
    }
});

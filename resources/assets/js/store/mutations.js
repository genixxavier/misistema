const mutations = {
    SETDATA(state, data) {
        state.users = data.users 
        state.areas = data.areas
        state.tribus = data.tribus
    },
    GETUSERBYID(state, id) {
        
        state.userID =  state.users.find(u => u.id == id)
    },
    UPDATEUSER(state, data ) {
        
        axios.post('/updateUser',data
        )
        .then(function (response) {
            console.log(response)
            if (response.data.status != 200) {
                alertify.error(response.data.message)
            }
            else {
                alertify.success(response.data.message)
                document.getElementById('dismiss').click()
            }
        })
        .catch(function (error) {
          console.log(error);
        });
    }, 
    DELETEUSER (state, id) {
       
        axios.post('/deletUser',{id}
        )
        .then(function (response) {
            console.log(response)
            if (response.data.status != 200) {
                alertify.error(response.data.message)
            }
            else {
                alertify.success(response.data.message)
               let key =  state.users.findIndex(user => user.id == id)
                state.users.splice(key,1)
                document.getElementById('dismiss').click()
                
            }
        })
        .catch(function (error) {
          console.log(error);
        });
    },
    GETHORAS_RECOMPENSADAS (state, data ) {
        state.horas_recompensadas = data.horas
    },
    GETHORAS_RECOMPENSADAS_BY_ID(state, id) {
        state.horas_recompensadas_id = state.horas_recompensadas.filter(h => h.id == id)
    }
}
export default mutations
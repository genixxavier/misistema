const actions = {
    getData ({commit}) {
        axios.get('/getData')
          .then(function (response) {
            commit('user/SETDATA',response.data,{root:true})
           
          })
          .catch(function (error) {
            console.log(error);
          });
    }, 
    updateUser({commit},data) {
        
        commit('user/UPDATEUSER',data,{root:true})
      
    }, 
    deleteUser ({commit}, id ) {
       
        commit('user/DELETEUSER', id, {root:true})
    },

    //HORAS RECOMPENSADAS
    getHorasRecompensadas ({commit } , id ) {
        axios.get('/horas_reconpensadas')
            .then(response  => {
                commit('user/GETHORAS_RECOMPENSADAS', response.data, {root:true})
            })
    }
}
export default actions
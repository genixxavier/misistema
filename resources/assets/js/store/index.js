import mutations from './mutations'
import getters from './getters'
import state from './state'
import actions from './actions'
const namespaced = true
export default {
    namespaced,
    mutations,
    getters,
    state,
    actions
}
const actions = {
    getData ({commit}) {
      axios.get('/horas_recompensadas_data')
        .then(response  => {
            commit('horas_recompensadas/SETDATA', response.data, {root:true})
        })
  },
  saveData ({commit}, data) {
      commit('horas_recompensadas/SAVEDATA', data , {root:true})
  }


}
export default actions
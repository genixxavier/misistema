const mutations = {
  SETDATA(state, data) {
    state.horas_recompensadas = data.horas
    state.users = data.users
  },
  GETHORAS_RECOMPENSADAS_BY_ID(state, id) {

    state.horas_recompensadas_id = state.horas_recompensadas.filter(h => h.id == id)
  },
  SAVEDATA (state, data) {

    axios.post('/horas_recompensadas',data)
        .then(res => {
          if (res.data.success == 'ok' && res.data.flag != 'ok') {
              state.horas_recompensadas.unshift(res.data.data)


          }
          else {
            console.log(res.data.data)
            let hora = state.horas_recompensadas.find( obj => obj.id === res.data.data.id )
            console.log(hora)
            hora.horas = res.data.data.horas
            hora.fecha = res.data.data.fecha
            hora.user_id = res.data.data.user_id
          }
          alertify.success(res.data.message)
          document.getElementById('dismiss').click()
        })
        .catch(err => {
          alertify.error('No se pudo realizar la operacion ' + err)
        })
  },
  DELETEDATA (state, id) {

    axios.delete(`/horas_recompensadas/${id}`
    )
        .then(function (response) {
          console.log(response)
          if (response.data.status != 200) {
            alertify.error(response.data.message)
          }
          else {
            alertify.success(response.data.message)
            let key =  state.horas_recompensadas.findIndex(obj => obj.id == id)
            state.horas_recompensadas.splice(key,1)
            document.getElementById('dismiss').click()

          }
        })
        .catch(function (error) {
          console.log(error);
        });
  },
  
   GETHORASDEUDA (state) {
   axios.get('/horas_totales')
      .then( (res) => {
        if (res.status == 200) {
          state.horas_deuda = res.data
         
        }
      })
      .catch( err   => console.log(err.message))
   
   
  }

}
export default mutations
import Vue from 'vue';
import Vuex from 'vuex';
import user from './store/index'
import  horas_recompensadas from './store/horas_recompensadas/index'
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
       user,
      horas_recompensadas

    }
    
});
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte Proyecto  </title>
    <style>
        *{
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
        }
        .container {
            position: fixed;
            width: 794px;
            height: 1070px;
            z-index: -1000;
        }
        body {
            padding: 180px 150px 100px 150px;
            color: rgb(25,25,25) !important;
        }
        .container img {
            width: 100%;
            height: 100%;
        }
        .main-title {
            text-align: center;
        }
        .areas_participantes, .main-title, .etapas {
            margin-top: 10px;
        }
        .areas_participantes p {
            text-align: left;
            margin-left: 50px;
            margin-top: 5px;


        }
        .etapas {
            margin-bottom: 10px;
        }
        .etapas_container, .etapas_container table{
            width: 100%;
        }
        .etapas_container table thead tr th {
            width: 25%;
        }
        .etapas_container table thead, .etapas_container table tbody tr td {
            text-align: center !important;
        }
    </style>
</head>
<body>
<div class="container">
    <img src="img/backgroundpdf.png" alt="imagen">
</div>

<div class="content">
    <div class="description">
        <p >Cliente: {{$cliente->nombre}}  </p>
        <p>Razón social: {{$cliente->razon_social}}</p>
        <p>Ruc: {{$cliente->ruc}}</p>
    </div>
    <h3 class="main-title">{{$project->name}}</h3>

    <div class="areas_participantes">
        <label>Áreas participantes: </label>
            @foreach($project_areas as $area)
                <p>{{$area->name}}: {{$area->percent}} %</p>
            @endforeach
    </div>
    <div class="etapas">
        <label for="">Etapas: </label>

    </div>
    <div class="etapas_container">

        <table>
            <thead>
                <tr>
                    <th>Nombre: </th>
                    <th>F. Inicio</th>
                    <th>F. Fin</th>
                    <th>Estado</th>
                </tr>
            </thead>
            <tbody>
            @foreach($project_etapas as $etapa)
                <tr>
                    <td>{{$etapa->nombre}}</td>
                    <td>{{date('d/m',strtotime($etapa->start_date))}}</td>
                    <td>{{date('d/m',strtotime($etapa->end_date))}}</td>
                    <td>
                        @if($etapa->condition)
                            <p>Culminado</p>
                        @else
                            <p>En proceso</p>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>



    </div>
</div>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte Cotización  </title>
    <style>
        *{
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
        }
        .container {
            position: fixed;
            width: 794px;
            height: 1100px;
            z-index: -1000;
        }
        body {
            padding: 180px 150px 100px 150px;
        }
        .container img {
            width: 100%;
            height: 100%;
        }
        .title_contact {
            margin-top: 20px;
        }
        .temas {
            margin-top: 20px;
        }
        .temas h4{
            margin-bottom: 10px;
        }
        .title_tema {
            font-size: 20px;
        }
        .participantes h4{
            margin: 0px;
            margin-bottom: 10px;
            font-size: 16px;
        }
        .col-md-6{
            display: inline-block;
            width: 300px;
            float: left;
        }
        .participantes h5{
            margin: 0px;
            display: block;
            font-size: 15px;
            margin-bottom: 10px;
        }
        .participantes ul{
            margin-left: 15px;
            font-size: 14px;
        }
        .content_report  h4{
            margin-top: 40px;
            margin-bottom: 20px;
        }
        .acuerdos {
            margin-top: 15px;

        }
        .fecha{
            font-size: 14px;
        }
        ol{
            margin-left: 25px;
            font-size: 16px;
        }
        .lista_items{
            margin-left: 15px;
        }
        .item_temas h4{
            margin: 0px;
            font-size: 16px;
            margin-bottom: 20px;
            margin-top: 20px;
        }
        .item_temas h5{
            margin: 0px;
            font-size: 16px;
            margin-bottom: 5px;
        }
        .item_temas ul{
            margin: 0px;
            margin-left: 25px;
            padding: 0px;
        }
        .item_temas ul li{
            margin-bottom: 5px;
        }

        .services {
            text-align: center;

        }
        .description {
            text-align: left;
            margin-top: 20px;
            font-size: 15px;

        }
        .title-servicio {
            margin-top: 20px;
            font-size: 16px;
            font-weight: bold;
        }

        .consideraciones {
            margin-top: 30px;
            text-align: center;
            font-size: 11px;
            /* page-break-before: always; */


        }
        .align {
            text-align: left;
            margin-left: 30px;
        }
        .title-consideraciones {

            margin-left: -15px;



            font-weight: bold;
            font-size: 13px;
            margin-bottom: 5px;
            margin-top: 8px;
        }
        .consideraciones-description {
            margin-top: 3px;
        }
        .consideraciones_items {
            font-size: 12px;
        }

    </style>
</head>
<body>
<div class="container">
    <img src="img/backgroundpdf.png" alt="imagen">
</div>

<div class="content">
    <div class="fecha">
        <p>Lima, {{ $day }} de {{ $month }} del {{ $year }}</p>
        @if (isset($cliente_cotizacion->razon_social))
            <p style="margin-top:10px">
                Razón Social: {{$cliente_cotizacion->razon_social }}
            </p>
            <p>
                RUC : {{$cliente_cotizacion->ruc}}
            </p>
        @endif
        <p>
            Cliente: {{$cotizacion->c_contacto}}
        </p>
        <p style="margin-top:10px">Datos del proveedor: </p>
        <p>Razón Social: MEDIA IMPACT PERU S.A.C</p>
        <P>RUC: 20556395820</P>
    </div>
    <div class="title" style="text-align:center">
        <h3 class="title_contact">COTIZACIÓN #{{$cotizacion->c_num}}</h3>
    </div>
    <div class="services" >
        @foreach($servicios as $val => $servicio)
            <p class="title-servicio"> {{$val + 1}}.  {{$servicio->title}}</p>
            <div class="description" >
                {!!  $servicio->description!!}
            </div>
            <h5 style="text-align: right;margin-top:10px">Inversión S/. {{number_format($servicio->total,2,'.',',')}} + IGV <br>
                {{$servicio->nota}}</h5>
        @endforeach
        @if (floatval($cotizacion->c_extras) > 0)
                <h5 style="text-align: right;margin-top: 40px">Inversión Total: S/. {{$total}} </h5>
                <h5 style="text-align: right;margin-top: 40px">Gastos extras: S/. {{$cotizacion->c_extras}}  </h5>
                <h5 style="text-align: right;margin-top: 40px">Inversión Final: S/. {{$inversion_total}} + IGV </h5>
        @else
                <h5 style="text-align: right;margin-top: 40px">Inversión Final: S/. {{$total}} + IGV </h5>
        @endif

    </div>
    <div class="consideraciones">
        <div class="align">
            <h4 class="title-consideraciones" style="margin-bottom:10px">CONSIDERACIONES DEL SERVICIO</h4>
            {{-- <li class="consideraciones-description"> Tipo de Documento:Factura.</li>
            <li class="consideraciones-description"> El presupuesto no incluye los impuestos de ley (18% IGV).</li>
           --}}
            <div class="consideraciones_items">
                {!!$cotizacion->c_consideraciones!!}
            </div>
            <h4 class="title-consideraciones" style="margin-top:10px;margin-bottom:10px" >ACCIONES NO INCLUIDAS EN EL PRESUPUESTO</h4>

            {{-- <li class="consideraciones-description">Compra de videos, audios o imágenes, entre otros materiales.</li>
            <li class="consideraciones-description">Servicio de imprenta.</li>
            <li class="consideraciones-description">Cobertura de eventos, generación de entrevistas, toma de fotos, reportajes, alquiler de equipos, focus groups, costos asociados a la contratación de terceros, trámites de permisos para promociones, concursos y otros trámites legales necesarios para cualquier tipo de promoción.</li> --}}
            <div class="consideraciones_items">
                {!!$cotizacion->c_acciones!!}
            </div>
        </div>


    </div>

</div>
</body>
</html>
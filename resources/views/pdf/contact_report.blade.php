<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte Contact Report</title>
    <style>
        *{
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
        }
        .container {
        position: fixed;
        width: 794px;
        height: 1100px;
        z-index: -1000;
        }
        body {
            padding: 180px 150px 100px 150px;
            color: rgb(25,25,25) !important;
        }
        .container img {
            width: 100%;
            height: 1070px;
        }
        .title_contact {
            margin-top: 20px;
        }
        .temas {
            margin-top: 20px;
        }
        .temas h4{
            margin-bottom: 10px;
        }
        .title_tema {
            font-size: 20px;
        }
        .participantes h4{
            margin: 0px;
            margin-bottom: 10px;
            font-size: 16px;
        }
        .col-md-6{
            display: inline-block;
            width: 300px;
            float: left;
        }
        .participantes h5{
            margin: 0px;
            display: block;
            font-size: 15px;
            margin-bottom: 10px;
        }
        .participantes ul{
            margin-left: 15px;
            font-size: 14px;
        }
       .content_report  h4{
            margin-top: 40px;
            margin-bottom: 20px;
       }
       .acuerdos {
           margin-top: 15px;
           
       }
       .fecha{
           font-size: 14px;
       }
       ol{
            margin-left: 25px;
            font-size: 16px;
       }
       .lista_items{
           margin-left: 15px;
       }
       .item_temas h4{
           margin: 0px;
           font-size: 16px;
           margin-bottom: 20px;
           margin-top: 20px;
       }
       .item_temas h5{
           margin: 0px;
           font-size: 16px;
           margin-bottom: 5px;
       }
       .item_temas ul{
           margin: 0px;
           margin-left: 25px;
           padding: 0px;
       }
       .item_temas ul li{
           margin-bottom: 5px;
       }
    </style>
</head>
<body>
<div class="container">
    <img src="img/backgroundpdf.png" alt="imagen">
</div>

 <div class="content">
    <div class="fecha">
        <p>Lima , {{ $dia }} de {{ $mes }} del {{ $year }}</p>
        <p>Cliente: <b>{{ $nombre_cliente }}</b></p>
    </div>
    <div class="title" style="text-align:center">
            <h3 class="title_contact">Contact Report</h3>
    </div>
    <div class="temas">
        <h4>Temas tratados:</h4>
        <ol>
            @foreach($temas as $tema)
                <li><b>{{$tema->title}}</b></li>
            @endforeach
        </ol>
    </div>
    <div class="content_report">
        <h4>Detalle de los acuerdos por temas conversados:</h4>
        <div class="item_temas">
            <?php $i = 1;?>
            @foreach($temas as $tema)
                <h4><b>{{$i}}. {{$tema->title}}</b></h4>
                <div class="lista_items">
                    <h5><i>Acuerdos Media Impact:</i></h5>
                    <ul>
                        @foreach($tema->temas_acuerdos_pdf($tema->id) as $item)
                           @if($item->type == 1)
                                <li>{{ $item->description}}</li>
                            @endif
                        @endforeach
                    </ul>
                    <h5><i>Acuerdos {{$nombre_cliente}}:</i></h5>
                    <ul>
                        @foreach($tema->temas_acuerdos_pdf($tema->id) as $item)
                            @if($item->type == 2)
                                <li>{{ $item->description}}</li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <?php $i++;?>
            @endforeach
        </div>

        <div class="participantes">
            <h4>Participantes:</h4>
            <div class="col-md-6">
                <h5>Media Impact:</h5>
                <ul>
                    <li>{{ $dataUserCreate->name  }}</li>
                    @foreach ($integrantesm as $item)
                        @if($item[0]->id !== $dataUserCreate->id)
                            <li>{{  $item[0]->name }}  </li>
                        @endif
                        
                    @endforeach
                </ul>
            </div>
            <div class="col-md-6">
                <h5> {{ $nombre_cliente }}:</h5>
                <ul>
                    @foreach ($integrantesc as $item)
                      @if(!strlen($item) == 0)
                          <li>{{ $item }}</li>
                      @endif
                    @endforeach
                </ul>
            </div>
        </div>
        
    </div>
</div>
</body>
</html>
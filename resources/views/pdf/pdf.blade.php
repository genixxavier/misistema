<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte Contact Report</title>
    <style>
        *{
            margin: 0;
            padding: 0;
        }
          .container {
            position: fixed;
            width: 794px;
            height: 1100px;
            z-index: -1000;
            
          }
          .container img {
              width: 100%;
              height: 100%;
          }
    .content {
        position: absolute;
        top: 200px;
        left: 150px;
        right: 150px;
      
        }
        .title_contact {
            margin-top: 20px;
        }
        .cliente {
            /* font-weight: bold; */
            /* font-size: 20px; */
            
        }
        .tema {
            margin-top: 20px;
        }
        .title_tema {
            font-size: 20px;
        }
        .participantes {
            font-size: 18px;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .integrantes {
            font-size: 18px;
            /* margin-top: 10px; */
        }
       .content_report {
           margin-left: 25px;
         
       }
       .acuerdos {
           margin-top: 15px;
           
       }
     
    </style>
</head>
<body>
<div class="container">
    <img src="img/header.png" alt="imagen">
</div>

 <div class="content">
    <div class="fecha">
    <h3>Lima ,{{  date('d') }} de {{ date('F') }} de {{  date('Y')  }}</h3>
    <h4>Cliente: <span class="cliente">{{ $nombre_cliente }}</span></h4>

    </div>
    <div class="title" style="text-align:center">
            <h3 class="title_contact">Contact Report</h3>
    </div>
    <div class="tema">
    <h3>Tema: <span class="title_tema">{{ $contact->cr_tema }}</span></h3>
    </div>
    <div class="content_report">
      
        <div class="mediaimpact">
                <p class="participantes">
                        Participantes de media impact :
                </p >

                @foreach ($integrantesm as $item)
                <p class="">
                    {{  $item[0]->name }}
                </p>
                @endforeach
        </div>
        <div class="cliente">
                <p class="participantes">
                        Participantes de  {{ $nombre_cliente }} :
                </p>
                @foreach ($integrantesc as $item)
                    <p class="">
                        {{ $item }}

                    </p>
                @endforeach
        </div>
        
        <div class="acuerdosm">
            <h3 class="acuerdos">Acuerdos de Media Impact</h3>
            @foreach ($acuerdosm as $item)
                
            <li>
                {{ $item }}
            </li>

            @endforeach
        </div>
        <div class="acuerdosc">
           <h3 class="acuerdos">Acuerdos de {{ $nombre_cliente }}</h3>
            @foreach ($acuerdosc as $item)
                
            <li>
                {{ $item }}
            </li>

            @endforeach
        </div>

    </div>
</div>
</body>
</html>
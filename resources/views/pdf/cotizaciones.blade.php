<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte Cotización  </title>
    <style>
        *{
            margin: 0;
            padding: 0;
            font-family: Arial, Helvetica, sans-serif;
          

        }

          .container {
            position: fixed;
            width: 794px;
            height: 1100px;
            z-index: -1000;
            
          }
          body {
              padding: 180px 150px 100px 150px;
           
          }
          .container img {
              width: 100%;
              height: 100%;
          }
   
        .title_contact {
            margin-top: 20px;
        }
        
        .tema {
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .title_tema {
            font-size: 20px;
        }
       
       
  
     .title-consideraciones {
         font-weight: bold;
         
     }
     .consideraciones {
         opacity: 0.5;
         page-break-before: always;
         
     }
     .content-item {
         margin: 20px 0 ;
     }
     .item-description , .inversion_total{
         margin: 15px 0;
     }
     li {
         margin-left: 20px;
     }
     .inversion, .inversion_total {
         font-weight: bold;
        text-align: right;
     }
     
    </style>
</head>
<body>
<div class="container">
    <img src="img/header.png" alt="imagen">
</div>

 <div class="content">
    <div class="fecha">
    <p>Lima , {{ $dia }} de {{ $mes }} del {{ $year }}</p>
    <p>Cliente: {{ $nombre_cliente }}</p>

    </div>
    <div class="title" style="text-align:center">
    <h3 class="title_contact">COTIZACIÓN - 000{{ $cotizacion->c_num  }}</h3>
    </div>
    <div class="tema">
    <h4> <span class="title_tema">{{ $cotizacion->c_nombre }}</span></h4>
    </div>
    <div class="contenido">

            @php
            for ($i = 0 ; $i < count($nameServices) ;$i ++ ) {
               echo ' <div class="" >
                                <div class="content-item">
                                
                                <h4>'.$nameServices[$i].' </h4> 
                                </div>
                                <div class=" ">
                                   <p class="item-description"> '.$descripcion[$i].'   </p>
                                </div>
                                <div class="">
                                    <p class="inversion">Inversión S/ '.intval($subtotal[$i]  - $descuento[$i]) .'.00  </p>
                                </div>
                        </div>';
            }
        @endphp

        <div>
            <h4 class="inversion_total">
                Inversión total S/ {{  $cotizacion->c_total }}
            </h4>
        </div>
        
     
    </div>

    <div class="consideraciones">
             <h4 class="title-consideraciones">CONSIDERACIONES DEL SERVICIO</h4>   
            <li> Tipo de Documento Factura.</li>
            <li> La tasa de cambio  de acuerdo al BCP es de S/. 3.30  </li>
            <li> Elpresupuesto NO incluye los impuestos de ley (18% IGV).</li>
            <h4 class="title-consideraciones">ACCIONES NO INCLUIDAS EN EL PRESUPUESTO</h4>

                <li>Compra de videos,audios o imágenes, entre otros materiales.</li>
                <li>Cobertura de eventos, generación de entrevistas, toma de fotos, reportajes.</li>
                <li>El precio indicado en la presente propuesta está sujeto  a la aprobación o modificación de las especificaciones planteadas.</li>
                <li>El tiempo de aceptación final de la solución por parte del cliente no debe excederse 5 días útiles.</li>

            
         
    </div>
</div>
</body>
</html>
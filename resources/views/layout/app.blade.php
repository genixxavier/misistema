<!doctype html>
<html lang="es">
  <head>
    <title>@yield('titulo')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="_token" content="{!! csrf_token() !!}"/>
	  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favi/apple-icon-57x57.png')}}">
	  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favi/apple-icon-60x60.png')}}">
	  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favi/apple-icon-72x72.png')}}">
	  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favi/apple-icon-76x76.png')}}">
	  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favi/apple-icon-114x114.png')}}">
	  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favi/apple-icon-120x120.png')}}">
	  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favi/apple-icon-144x144.png')}}">
	  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favi/apple-icon-152x152.png')}}">
	  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favi/apple-icon-180x180.png')}}">
	  <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('favi/android-icon-192x192.png')}}">
	  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favi/favicon-32x32.png')}}">
	  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favi/favicon-96x96.png')}}">
	  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favi/favicon-16x16.png')}}">
	  <link rel="manifest" href="{{asset('favi/manifest.json')}}">
	  <meta name="msapplication-TileColor" content="#ffffff">
	  <meta name="msapplication-TileImage" content="{{asset('favi/ms-icon-144x144.png')}}">
	  <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	{{-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> --}}
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/funnel.css')}}">
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">

  	<link href="https://fonts.googleapis.com/css?family=Catamaran:400,500,700,900" rel="stylesheet">

	@yield('css')
	  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
      <style>
          .top_menu {
              top: 47px !important;
          }
		 section .container {

		  }
		  @media (max-width: 767px) {
			  #main .main-content {
				   margin-top: 0;
			  }
		
		  }
			@media only screen and (max-width: 992px) {
			  .container_img {
				   display: none;
			  }
			}
	  </style>

  </head>
  <body onload="mueveReloj()">


@php
	$data = array (7,9,10,35,36,61,50,71,5,78);
@endphp
	<header class="d-flex align-items-center">
			
	    <div class="container"  >
	    	<div class="container-fluid">
	    		<div class="row align-items-center">
	    			<div class="col-md-2 offset-sm-2 col-sm-2 col-4 offset-lg-1 col-lg-2 offset-xl-1 col-xl-2 responsi">
						<a href="{{route('historial')}}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
					</div>
	    			{{-- <div class="col-6 col-sm-6 col-md-7 col-lg-7   text-right align-middle  time"> --}}
					<div class="col-6 col-sm-6 col-md-7 offset-lg-2 col-lg-5 offset-xl-2 col-xl-5 text-right align-middle  time">
						<div>
						<span class="fecha">{{ date('D')." ".date('d')." de ".date('F').", " }}
	    					<form name="form_reloj" style="display: inline;">
									<input type="text" name="reloj" size="10" onfocus="window.document.form_reloj.reloj.blur()" style="background-color: transparent;border: none;color: white" id="reloj_header">
						 			</form>
							</span>
	    				<a class="btn" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
						</form>
					</div>
					</div>
					
	    		</div>
	    	</div>
		</div>

		{{-- <figure class="header__imagen">
			<div class="header__imagen_div__logo">
				<img src="{{asset('img_users/phpE455.tmp.png')}}" alt="" class="header__imagen__logo">
			</div>
			<p class="header__imagen__desc">Hola {Erick}</p>
		</figure> --}}
	</header>


	
	{{-- @if(\Auth::user()->img != '')  --}}
	<div class="container_img" style="position:absolute;top:0;right:0">
		<div class="card">
		@if(\Auth::user()->img != '')
			<img class="rounded" src="/img_users/{{\Auth::user()->img}}" width="140px" height="170px" alt="{{Auth::user()->name}}">
		@else
			<img class="rounded" src="{{asset("img/user-img.png")}}" width="140px" height="170px" alt="{{Auth::user()->name}}" style="width: 45px !important;margin-top:3px !important;">
		@endif
			<div class="card-body">
				<h5 class="card-title">{{\Auth::user()->name}}</h5>
			</div>
		</div>
		@php
			$nombreUsuario = \Auth::user()->name;
			$nomUs = explode(" ",$nombreUsuario);
		@endphp
		{{-- <h5 class="card-title">Hola, {{\Auth::user()->name}}</h5> --}}
		<h5 class="card-title">Hola, {{$nomUs[0]}}</h5>
	</div>
	{{-- @endif --}}
{{-- <img class="rounded" src="/img_users/{{\Auth::user()->img}}" width="140px" height="120px" alt="{{Auth::user()->name}}"> --}}

	<section id="main" style="min-height:135vh !important">
		@php
			$url= $_SERVER["REQUEST_URI"];
		@endphp
	@if($url=='/historial')
		<div class="container-fluid main__container__slide" style="background-color: rgba(205,205,205,.2);">
			<div class="container">
				<div class="col-12">
					<iframe src="https://wall.equipowik.com/" frameborder="0" width="100%" style="width:100%" id="ifame_wik"></iframe>
				</div>
			</div>
		</div>
	@endif

		<div class="container container__botonera">
			<div class="row">
				<div class="menu box despegable top_menu">
					<ul class="nav flex-column tops__menu__burger" style="cursor: pointer;">
							<li class="nav-item text-center">
							<a class="nav-link respo-boton" id="m1" style="">
							<i class="fa fa-bars" aria-hidden="true"></i></a>
						</li>
					</ul>
					<ul class="nav flex-column contenido tops__menu__burger__items" id="myDIV">

						@if(\Auth::user()->tipo != 'colaborador')
							<li class="nav-item text-center">
								<a class="nav-link" id="m9" href="/ticketblank" data-toggle="tooltip" data-placement="right" title="Completa tus tickets para ser el ganador del mes y darle puntos a tu tribu"><img src="{{asset('img/tickets.png')}}" ><span>MI Tickets</span></a>
							</li>
							<li class="nav-item text-center">
								<a class="nav-link" id="m9" href="/calendario" data-toggle="tooltip" data-placement="right" title="Completa tus tickets para ser el ganador del mes y darle puntos a tu tribu"><img src="{{asset('img/tickets.png')}}" ><span>Tickets 2.0</span></a>
							</li>
							<li class="nav-item text-center">
								<a class="nav-link" id="m4" href="{{ route('reuniones') }}" data-toggle="tooltip" data-placement="right" title=" Agenda reuniones con tus clientes o miembros de tribu"><i class="fa fa-users" aria-hidden="true" ></i><span>Reunión</span></a>
							</li>

							@if (\Auth::user()->tipo != 'jefe')

							<li class="nav-item text-center">
								<a class="nav-link" id="m21" href="/funnel/contact_report" data-toggle="tooltip" data-placement="right" title="Ingresa los acuerdos realizados con tus clientes"><i class="fas fa-funnel-dollar"></i><span>Contact | Cotización</span></a>
						</li>
							@endif

						@endif
						<li class="nav-item text-center">
							<a class="nav-link" id="m3" href="/asistencia" data-toggle="tooltip" data-placement="right" title="No olvides de marcar tu entrada y tu salida"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Check In / Out</span></a>
						</li>



						<li class="nav-item text-center">
							<a class="nav-link" id="m7" href="/informes" data-toggle="tooltip" data-placement="right" title="Reportes de tu asistencia, tickets completados y desempeño del mes"><i class="fa fa-file" aria-hidden="true"></i><span>MI Historial</span></a>
						</li>
						<li class="nav-item text-center">

							<a class="nav-link" id="m8" href="/podio/8" target="_blank" data-toggle="tooltip" data-placement="right" title="Llega temprano, completa tus tickets y podrás ayudar a tu tribu a ganar premios increíbles">
							<img src="{{asset('img/icono-mb.png')}}"  class="nav_bravo"  width="35%" height="35%" alt="">
								<span>Media Bros</span></a>
					</li>
						<li class="nav-item text-center">
							<a class="nav-link" id="m8" href="https://rockstar.equipowik.com" target="_blank" data-toggle="tooltip" data-placement="right" title="Cada día tenemos muchas responsabilidades, pero son esas acciones adicionales las que nos permiten ser mejores y todos debemos reconocerlas."><img
										src="{{asset('img/rockstar.png')}}" alt="rockstar"><span>MI Rockstar</span></a>
						</li>
						<li class="nav-item text-center">

							<a class="nav-link " id="m8" href="https://docs.google.com/spreadsheets/d/1TCNMbjylTNd4ojuR1oAvvyK1KDQfxcQJiHWzCSSoL7U/edit?usp=asogm&ouid=116965352010057924204" target="_blank" data-toggle="tooltip" data-placement="right" title="Cada Rockstar tiene su premio. Completa con tu regalo deseado para que podamos hacerlo realidad."><i class="fas fa-list-alt"></i><span>Wishlist</span></a>
						</li>
						<li class="nav-item text-center">

							<a class="nav-link" id="m8" href="https://docs.google.com/spreadsheets/d/1SmRAQ8UgMLr1eXKCKcOSXNLh85BotdlSwnVT8u722-4/edit?usp=asogm&ouid=116965352010057924204" target="_blank" data-toggle="tooltip" data-placement="right" title="Cumpleaños, aniversarios y datos importantes de todos nosotros a nuestra disposición" >
								<i class="fas fa-database"  ></i>
									<span>Base de Datos</span>
							</a>
						</li>
						<li class="nav-item text-center">

							<a class="nav-link" id="m8" href="https://docs.google.com/document/d/1N49WXta1ziJ4vywJyUkFHLALF0sYlvfKGxRW4AxC6aU/edit" target="_blank" data-toggle="tooltip" data-placement="right" title="Las reglas para que tengamos un buen ambiente laboral" ><i class="fas fa-align-justify"></i><span>Reglamento</span></a>
						</li>
						<li class="nav-item text-center">

							<a class="nav-link" id="m8" href="{{route('reporte_horas')}}"  data-toggle="tooltip" data-placement="right" title="Reporte de las horas que ha trabajado el usuario con los clientes "  ><i class="fas fa-clipboard-list"></i><span>Reporte</span></a>
						</li>
						{{-- <li class="nav-item text-center">

							<a class="nav-link" id="m8" href="{{route('horas_extras')}}"  data-toggle="tooltip" data-placement="right" title="Contador horas extras"  ><i class="fas fa-hourglass-half"></i><span>Horas extras</span></a>
						</li> --}}
						<li class="nav-item text-center">

							<a class="nav-link" id="m8" href="{{route('horas_recompensadas')}}"  data-toggle="tooltip" data-placement="right" title="Contador horas recompensadas"  ><i class="fas fa-user-check"></i><span>Horas recompensadas</span></a>
						</li>
						<li class="nav-item text-center">

							<a class="nav-link" id="m8" href="{{route('clientes')}}"  data-toggle="tooltip" data-placement="right" title="registro de clientes"  ><i class="fas fa-user-tie"></i><span>Clientes</span></a>
						</li>
						<li class="nav-item text-center">

							<a class="nav-link" id="m8" href="{{route('usuarios')}}"  data-toggle="tooltip" data-placement="right" title="actualizar datos"  ><i class="fas fa-user"></i><span>Actualizar datos</span></a>
						</li>

					</ul>
				</div>
			</div>
				@yield('content')
		</div>

	</section>

	@yield('content_extras')

	
	<section id="footer" >
		<div class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						 Copyright 2019 © Powered by <a href="https://www.mediaimpact.pe/" target="_blank">Media Impact</a>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<script language="JavaScript">
			$("#m1").click(function(){
		$("#myDIV").toggle();
	});
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		});
	function mueveReloj(){
	   	momentoActual = new Date()
	   	hora = momentoActual.getHours()
	   	minuto = momentoActual.getMinutes()
	   	segundo = momentoActual.getSeconds()

	   	str_segundo = new String (segundo)
	   	if (str_segundo.length == 1)
	      	segundo = "0" + segundo

	   	str_minuto = new String (minuto)
	   	if (str_minuto.length == 1)
	      	minuto = "0" + minuto

	   	str_hora = new String (hora)
	   	if (str_hora.length == 1)
	      	hora = "0" + hora

	   	horaImprimible = hora + " : " + minuto + " : " + segundo

	   	document.form_reloj.reloj.value = horaImprimible

	   	setTimeout("mueveReloj()",1000)
	}
	</script>
    @yield('js')
  </body>
</html>

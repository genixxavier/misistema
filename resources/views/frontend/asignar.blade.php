@extends('layout.app')
@section('titulo','Asignar Tiket')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="{{ asset('css/asignar.css') }}">
@endsection
@section('content')
 
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				@if (session('status'))

				    <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-success">{{ session('status') }}</p>
					        <div class="col-md-12">
					        	<button type="button" class="btn btn-primary btn-ok" >OK</button>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
				@endif
				<form method="post" action="{{route('asignar_post',$ticket->id)}}" id="frm-ticket">
					{{csrf_field()}}
					<div class="form-row">
					  <div class="form-group col-md-6">
					    <label for="nombre">Nombre:</label>
					    <input type="text" class="form-control" name="nombre_value" id="nombre" value="{{$ticket->nombre}}" disabled="">
					  </div>
					  <div class="form-group col-md-6">
					    <label for="cliente">Cliente:</label>
						<input type="text" class="form-control" name="cliente__value" value="{{$ticket->cliente->nombre}}" disabled="">
					  </div>
					</div>
					<div class="form-row">
					  <div class="form-group col-md-6 box-calendario">
					    <label for="fecha">Fecha de entrega tentativa:</label>
					    <input type="text" class="form-control" name="fecha_value"  value="{{$ticket->fecha_limite}}" disabled="">
					    <i class="fa fa-calendar"></i>
					  </div>
					</div>

					<div class="form-row">
					  <div class="form-group col-md-12 box-calendario">
					    <label for="fecha">Descripción de pedido:</label>
					    <div id="descripcion" class="form-control text-left" disabled>
					    	{!! $ticket->descripcion !!}
					    </div>
					  </div>
					</div>
					<hr>
					<p>Asignar lo siguiente</p>
					<div class="form-row">
						<div class="form-group col-md-6">
						    <label for="colaborador">Colaborador:</label>
						    <select class="form-control" name="user_id" value="2" id="colaborador">
						    	@foreach($colaboradores as $colaborador)
						    		<option value="{{$colaborador->id}}" @if($ticket->user_id == $colaborador->id) selected="" @endif>{{$colaborador->name}}</option>
						    	@endforeach
						    </select>
						</div>
					</div>
					
					<div class="form-row">
					  <div class="form-group col box-calendario">
					    <label for="dia">Dia:</label>
					    <input type="text" class="form-control" name="fecha_inicio" id="dia" value="{{$ticket->fecha_inicio}}">
					    <i class="fa fa-calendar" ></i>
					  </div>
					  <div class="form-group col">
					    <label for="hora">Horas del pedido:</label>
					    <input type="text" class="form-control" name="horas_pedido" id="hora" value="{{$ticket->horas_pedido}}">
					  </div>
					  <div class="form-group col">
					    <label for="hora_s">Horas de supervición:</label>
					    <input type="text" class="form-control" name="horas_supervision" id="hora_s" value="{{$ticket->horas_supervision}}">
					  </div>
					</div>

					<div class="form-row">
						<div class="col-md-12 text-center">
					  		<button type="submit" class="btn btn-primary">Asignar</button>
 					  		<a href="javascript:history.back(1)" class="btn btn-dark">Volver</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@section('content_extras')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>
<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript">
	$("#alerta_popup").modal("show");
	$(".btn-ok").click(function(){
		location.href ="/historial";
	});
	$("#frm-ticket").validate({
		rules: {
			nombre_value: "required",
			cliente__value: "required",
			user_id: "required",
			fecha_inicio: "required",
			fecha_limite: "required",
			horas_pedido: {
				required: true,
				number: true,
				minlength: 1,
				min: 1
			},
			horas_supervision: {
				required: true,
				number: true,
				minlength: 1,
				min: 0
			},
		},
		messages: {
			nombre_value: "Ingresar nombre",
			cliente__value: "Escoga cliente",
			user_id: "Escoga Colaborador",
			fecha_inicio: "Escoga fecha",
			fecha_limite: "Escoga fecha",
			horas_pedido: {
				required: "Ingrese horas",
				number: "Ingrese numeros",
				min: "Las horas deber ser mayor a 0"
			},
			horas_supervision: {
				required: "Ingrese horas",
				number: "Ingrese numeros",
				min: "Las horas deber ser mayor a 0"
			}
		}
	});

    $(function () {
        $('#fecha').datetimepicker({
        	sideBySide: true
        });
        //  $('#dia').datetimepicker({
        // 	format: 'YYYY-MM-DD HH:mm:ss',
        // 	sideBySide: true
        // });

        var dateToday = new Date(); 
        $('#dia').datetimepicker({
      		format: 'YYYY-MM-DD HH:00:00',
        	sideBySide: true,
        	daysOfWeekDisabled:[0,6],
        	minDate: dateToday
        });
    });
    $('#m1').addClass('active');
</script>
@endsection
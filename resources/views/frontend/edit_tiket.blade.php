@extends('layout.app')
@section('titulo','Tiket')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
@endsection
@section('content')
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				@if (session('status'))
				    <!-- <div class="alert alert-success">
				    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
				        {{ session('status') }}
				    </div> -->

				    <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-success">{{ session('status') }}</p>
					        <div class="col-md-12">
					        	<button type="button" class="btn btn-primary btn-ok" >OK</button>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
				@endif

				<form action="{{route('create_ticket')}}" method="POST" id="frm-ticket">
					{{ csrf_field() }}
					<input type="hidden" name="idtk" value="{{ $ticket->id }}">
					<input type="hidden" name="estado" value="{{ $ticket->estado }}">
					<input type="hidden" name="user_create" value="{{ \Auth::user()->id }}">
					<div class="form-row">
					  <div class="form-group col-md-6">
					    <label for="nombre">Nombre de pedido:</label>
					    <input type="text" class="form-control" name="nombre" id="nombre" value="{{ $ticket->nombre }}" required>
					  </div>
					  <div class="form-group col-md-6">
					    <label for="cliente">Cliente:</label>
					    <select class="form-control" name="cliente_id" id="cliente">	
					    	@foreach($clientes as $cliente)
					    		 @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
					    		 	<?php 
						    			if($cliente->id == $ticket->cliente_id){
						    				$selected = "selected";
						    			}
						    			else{
						    				$selected = "";
						    			}
						    		?>
					    		 	<option value="{{$cliente->id}}" {{$selected}}>{{$cliente->nombre}}</option>
					    		 @else
					    		 	<?php 
						    			if($cliente->cliente_id == $ticket->cliente_id){
						    				$selected = "selected";
						    			}
						    			else{
						    				$selected = "";
						    			}
						    		?>
					    			<option value="{{$cliente->cliente_id}}" {{$selected}}>{{$cliente->nombre}}</option>
					    		 @endif
					    	@endforeach
					    </select>
					  </div>
					</div>
					@if(\Auth::user()->tipo == 'jefe')
					<div class="form-row">
						<div class="form-group col-md-6">
						    <label for="cliente">Colaborador:</label>
						    <select class="form-control" name="user_id" id="cliente">
						    	@foreach($colaboradores as $colaborador)
						    		<?php 
						    			if($colaborador->id == $ticket->user_id){
						    				$selected = "selected";
						    			}
						    			else{
						    				$selected = "";
						    			}
						    		?>
						    		<option value="{{$colaborador->id}}" {{ $selected }}>{{$colaborador->name}}</option>
						    	@endforeach
						    </select>
						</div>
						<!-- <div class="form-group col-md-6">
						    <label for="fecha">Area:</label>
						    <select class="form-control" name="area_name" id="area_name">
						    	@foreach($areas as $area)
						    		<option value="{{$area->id}}">{{$area->name}}</option>
						    	@endforeach
						    </select>
						</div> -->
					</div>
					
					<div class="form-row">
					  <div class="form-group col box-calendario">
					    <label for="dia">Día: </label>
					    <!-- <input type="text" class="form-control " name="getfecha" value="{{ $ticket->fecha_inicio }}" disabled>
					    <input type="hidden" class="form-control " name="fecha_inicio" value="{{ $ticket->fecha_inicio }}">
					    <i class="fa fa-calendar"></i> -->

					    <div class='input-group date' id='editfecha1'>
		                    <input type='text' class="form-control" name="fecha_inicio" value="{{ $ticket->fecha_inicio }}"/>
		                    <span class="input-group-addon">
		                        <span class="fa fa-edit"></span>
		                    </span>
		                </div>
		                <div class="input-group date" id="getfecha1" style="display: none">
			                <div class='input-group date' id='fecha'>
			                    <input type='text' class="form-control" value="{{ $ticket->fecha_inicio }}"/>
			                    <span class="input-group-addon">
			                        <span class="fa fa-calendar"></span>
			                    </span>
			                </div>
			                <span class="input-group-addon cerrarfech1">
		                        <span class="fa fa-times"></span>
		                    </span>
		                </div>
					  </div>
					  <div class="form-group col">
					    <label for="hora">Horas del pedido:</label>
					    <input type="number" class="form-control" name="horas_pedido" id="hora" value="{{ $ticket->horas_pedido }}">
					  </div>
					  <div class="form-group col">
					    <label for="hora_s">Horas de supervición:</label>
					    <input type="number" class="form-control" name="horas_supervision" id="hora_s" value="{{ $ticket->horas_supervision }}">
					  </div>
					</div>
					@else
					<div class="form-row">
					  <div class="form-group col-md-6 box-calendario" >
					    <label for="fecha">Fecha de entrega:</label>
					    <div class='input-group date' id='editfecha'>
		                    <input type='text' class="form-control" name="fecha_limite" value="{{ $ticket->fecha_limite }}"/>
		                    <span class="input-group-addon">
		                        <span class="fa fa-edit"></span>
		                    </span>
		                </div>
		                <div class="input-group date" id="getfecha" style="display: none">
			                <div class='input-group date' id='fecha'>
			                    <input type='text' class="form-control" value="{{ $ticket->fecha_limite }}"/>
			                    <span class="input-group-addon">
			                        <span class="fa fa-calendar"></span>
			                    </span>
			                </div>
			                <span class="input-group-addon cerrarfech">
		                        <span class="fa fa-times"></span>
		                    </span>
		                </div>
					    <!-- <input type="text" class="form-control date" name="fecha_limite" id="fecha" value="{{ $ticket->fecha_limite }}">
					    <i class="fa fa-calendar"></i> -->
					  </div>

					  <div class="form-group col-md-6">
					    <label for="fecha">Área:</label>
					    <select class="form-control" name="area_id" id="area_id">
					    	@foreach($areas as $area)
					    		<?php 
					    			if($area->id == $ticket->area_id){
					    				$selected = "selected";
					    			}
					    			else{
					    				$selected = "";
					    			}
					    		?>
					    		<option value="{{$area->id}}" {{ $selected }}>{{$area->name}}</option>
					    	@endforeach
					    </select>
					  </div>
					</div>

					@endif
					<div class="form-row">
						<div class="form-group col-md-12">
						    <label for="fecha">Descripción del cambio:</label>
						    <textarea  id="descripcion" name="descripcion" ckeditor="editorOptions" required>
						    	{{ $ticket->descripcion }}
						    </textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 text-center">
					  		<button type="submit" class="btn btn-primary">Actualizar</button>
					  		<a href="/historial" class="btn btn-dark">Volver</a> 
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content_extras')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>

<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
<script type="text/javascript">
	$("#alerta_popup").modal("show");
	$(".btn-ok").click(function(){
		location.href ="/historial";
	});
	$('#descripcion').summernote({
        placeholder: 'Descripción del cambio ...',
        tabsize: 2,
        height: 200
      });
	$("#frm-ticket").validate({
		rules: {
			nombre: "required",
			descripcion: "required",
			cliente_id: "required",
			user_id: "required",
			fecha_inicio: "required",
			fecha_limite: "required",
			horas_pedido: {
				required: true,
				number: true,
				minlength: 1,
				min: 1
			},
			horas_supervision: {
				required: true,
				number: true,
				minlength: 1,
				min: 0
			},
		},
		messages: {
			nombre: "Ingresar nombre",
			descripcion: {
				required: "Ingresar Descripción del cambio"
			},
			cliente_id: "Escoga cliente",
			user_id: "Escoga Colaborador",
			fecha_inicio: "Escoga fecha",
			fecha_limite: "Escoga fecha",
			horas_pedido: {
				required: "Ingrese horas",
				number: "Ingrese numeros",
				min: "Las horas deber ser mayor a 0"
			},
			horas_supervision: {
				required: "Ingrese horas",
				number: "Ingrese numeros",
				min: "Las horas deber ser mayor o igual 0"
			}
		},
		ignore: ":hidden:not(#descripcion),.note-editable.panel-body"
	});

    $(function () {
    	var dateToday = new Date(); 
        $('#fecha').datetimepicker({
      		format: 'YYYY-MM-DD HH:00:00',
        	sideBySide: true,
        	daysOfWeekDisabled:[0,6],
        	minDate: dateToday
        });
    });
    $("#editfecha span").click(function(){
    	$("#editfecha").hide();
    	$("#editfecha input").removeAttr('name');

    	$("#getfecha").show();
    	$("#fecha input").attr('name','fecha_limite');
    });

    $(".cerrarfech").click(function(){
    	$("#getfecha").hide();
    	$("#getfecha input").removeAttr('name');

    	$("#editfecha").show();
    	$("#editfecha input").attr('name','fecha_limite');
    });

    $("#editfecha1 span").click(function(){
    	$("#editfecha1").hide();
    	$("#editfecha1 input").removeAttr('name');

    	$("#getfecha1").show();
    	$("#fecha input").attr('name','fecha_inicio');
    });

    $(".cerrarfech1").click(function(){
    	$("#getfecha1").hide();
    	$("#getfecha1 input").removeAttr('name');

    	$("#editfecha1").show();
    	$("#editfecha1 input").attr('name','fecha_inicio');
    });
    $('#m2').addClass('active');
</script>
@endsection
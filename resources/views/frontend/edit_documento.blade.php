@extends('layout.app')
@section('titulo','Editar Justificación -  Vacaciones')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
@endsection
@section('content')
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				@if (session('status'))
					@if(session('status') == 'exito')
				    <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-success">Su registro fue Actualizado con éxito</p>
					        <div class="col-md-12">
					        	<button type="button" class="btn btn-primary btn-ok" >OK</button>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
					@else
					<div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-danger">Error: tiene marcado asistencia el día: {{ session('dia') }}, verificar el rango de dias</p>
					        <div class="col-md-12">
					        	<a href="javascript:void(0)" class="btn btn-primary btn-close" >Cerrar</a>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
					@endif
				@endif
				<h2>Editar Documento</h2>
				<form action="{{route('update_documento')}}" method="POST" id="frm-ticket" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-row">
					  <div class="form-group col-md-6">
					    <label for="tipo">Tipos:</label>
					    <select class="form-control" name="tipo" id="tipo">	
			    		 	<option ></option>
			    		 	<option value="V" @if($documento->estado == "V") selected @endif >Vacaciones</option>
							<option value="J" @if($documento->estado == "J") selected @endif >Justificación</option>
							<option value="N" @if($documento->estado == "N") selected @endif >No existio</option>
					    </select>
					    <input type="hidden" name="iddoc" value="{{ $documento->id }}">
					  </div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
						    <label for="user_id">Nombre:</label>
					    	@foreach($colaboradores as $colaborador)
					    		@if($documento->user_id == $colaborador->id)
					    		<input type="text" readonly class="form-control" value="{{ $colaborador->name }}">
								@endif
					    	@endforeach
						</div>
					</div>
					<div class="form-row">
					  <div class="form-group col-md-6 box-calendario">
					    <label for="dia">Fecha:</label>
					    <input type="text" readonly class="form-control" name="fecha_inicio" value="{{ $documento->fecha }}">
					    <i class="fa fa-calendar"></i>
					  </div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
						    <label for="fecha">Justificación de documento:</label>
						    <div>{!! $documento->descripcion !!}</div>
						    <!-- <textarea  id="descripcion" name="descripcion" ckeditor="editorOptions" required></textarea> -->
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 text-center">
					  		<button type="submit" class="btn btn-primary">Actualizar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content_extras')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>

<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
<script type="text/javascript">
	$("#alerta_popup").modal("show");
	$(".btn-close").click(function(){
		$("#alerta_popup").modal("hide");
	});
	$(".btn-ok").click(function(){
		location.href ="/documentos";
		//$("#alerta_popup").modal("hide");
	});

	$("#frm-ticket").validate({
		rules: {
			tipo: "required",
		},
		messages: {
			tipo: "Seleccione tipo",
		}
	});
    $('#m6').addClass('active');
</script>
@endsection
@extends('layout.app')
@section('titulo','Contact Report')
@section('css')
{{-- Icon Fontastic  --}}
<link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- <link rel="stylesheet" href="{{ asset('css/funnel.css') }}"> --}}
  <style>
      .btn-crear:hover {
	background-color: #f4511e;
}
.btn-crear{

	display: inline-block;
	padding: 7px 15px;
	cursor: pointer;
	font-size: 13px;
	line-height: 13px;
	text-decoration: none;
	border-radius: 5px;
	color: #fff;
	margin-left: 5px;
	margin-right: 5px;
	margin-bottom: 20px;
}

table {
    font-size: 14px;
}

@media only screen and (max-width: 768px) {
    .margin_div {
        margin-top: 30px;
    }
}

      .bc a {
          color: #52ACDE;
      }
      .bc a:hover {
          text-decoration: underline;
      }
  </style>
@endsection
@section('content')
@php
     function convertDate($date) {
        $fecha = explode(' ',$date);
        $new_fecha = explode('-',$fecha[0]);
        $fecha_end = $new_fecha[2].'/'.$new_fecha[1];
        $hour = explode(':',$fecha[1]);
        $hour_end = $hour[0].':'.$hour[1];

       return  ' '.$fecha_end.' '.$hour_end.' ';
    }
@endphp
<div class="container-fluid">
    {{ Breadcrumbs::render('contact') }}
    <div class="row">
        <div class="col-12 col-md-12 text-center ">
            <h1 class="main-title ">Contact Report</h1>
            <p class="title-description">Ingresa los acuerdos realizados con tus clientes.
            </p>
        </div>
    </div>
    <div class="row text-center margin_div">
        <div class="col-6 col-md-2 mt-3">
            <a href="{{route('crear_reunion')}}" class="btn btn-primary btn-crear btn-custom">Crear  Reunión</a>
        </div>
        <div class="col-6 col-md-3 mt-3">
            <a href="/funnel/crear_contact_report" class="btn btn-primary btn-crear btn-custom">Crear Contact Report</a>
        </div>
        <div class="col-6 col-md-2 mt-3">
        <a href="{{  route('cotizaciones_create') }}" class="btn btn-primary btn-crear btn-custom">Crear Cotización</a>
        </div>
        <div class="col-6 col-md-3 mt-3">
            <a href="{{  route('cotizaciones_create_2') }}" class="btn btn-primary btn-crear btn-custom">Crear Cotización sin CR</a>
        </div>
        <div class="col-6 col-md-2 mt-3 ">
            @if(Auth::user()->tipo == 'gerente')
            <a href="/funnel/status_cotizacion" class="btn btn-primary btn-crear btn-custom" >Ver Cotizaciones</a>
            @else
            <a href="/funnel/cotizaciones" class="btn btn-primary btn-crear btn-custom" >Ver Cotizaciones</a>
            @endif
        </div>
        {{-- <div class="col-6 col-md-3 mt-3">
            <a href="/funnel/proyectos" class="btn-crear">Ver Proyectos</a>
        </div> --}}
    </div>
    <div class="row mt-4">

        <div class="col-md-12">
            <table class="table table-bordered table-responsive-lg mitabla custom-table">
                <thead class="">
                    <tr>
                        <th>
                            Tema
                        </th>
                        <th>
                            Cliente
                        </th>
                        <th>
                            Fecha
                        </th>
                    
                        <th>% Cliente</th>
                        <th>% MI</th>
                        <th>
                            Opciones
                        </th>
                    </tr>
                    
                </thead>

                <tbody>
                 
                  @foreach ($list_contact_report as $item)
                      <tr>
                          <td>
                              <?php 
                              $i = 0;
                              $list="";
                              ?>
                              @foreach( $item->get_temas($item->id) as $tema)
                                @if($i > 0)
                                    <?php $list .= ", ";?>
                                @endif
                                    <?php $list .= $tema->title;?>
                                <?php $i++;?>
                              @endforeach
                              <?php 
                                $t = strlen($list);
                                if($t > 50){
                                    $text = substr($list,0,50);
                                    echo $text."...";
                                }
                                else{
                                    echo $list;
                                }
                              ?>
                          </td>
                          <td> {{$item->nombre}}</td>
                          <td width="115px"> 
                            @php
                                echo "".convertDate($item->cr_fecha_reunion)."";
                            @endphp    
                           
                        </td>
                         
                          <td> {{$item->cr_total_percent_c}} %</td>
                          <td> {{$item->cr_total_percent_m}} %</td>
                          <td>
                    
                            <div class="dropdown">
                                <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-cog"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item"  target="_blank" href="{{ route('reporte_contact', $item->id)}}"> <i class="fas fa-file-pdf"></i>  Ver</a> 
                                <a class="dropdown-item" href=" {{route('contact_report_edit' , $item->id)}} "><i class="fas fa-edit"></i> Editar</a>
                                
                                
                                @if (! in_array($item->id,$coti_realizadas))
                                <a class="dropdown-item" href="{{route("cotizar",$item->id)}}"> <i class="fas fa-file-invoice-dollar"></i> Cotizar</a>
                                <a class="dropdown-item" href=" {{route('contact_report_eliminar' , $item->id)}} "><i class="fas fa-edit"></i> Eliminar</a>
                                    
                                @endif
                                </div>
                            </div>
                        </td>
                      </tr>
                  @endforeach
               
                </tbody>
            </table>
            {!!$list_contact_report->render()!!}
        </div>
    
    </div>
</div>
@endsection
@section('content_extras')    
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script>
    //    function  mostrarId (item)  {
    //        console.log(item)
    //    }
    $('#m21').addClass('active');
  </script>
@endsection
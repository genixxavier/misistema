@extends('layout.app')
@section('titulo','Detalle Ticket')

@section('css')
	<link rel="stylesheet" href="{{ asset('css/detalle_tiket.css') }}">
@endsection
@section('content')
 
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				<p><b>Nombre de Reunión:</b> {{$reunion->nombre}}</p>
				<p><b>Responsable:</b>
					@foreach($users as $uu)
                    @if($uu->id == $reunion->user_create)
                      {{$uu->name}}                            
                    @endif
                  @endforeach
				</p>
				<p><b>Cliente:</b> @if($reunion->cliente) {{$reunion->cliente->nombre}} @endif</p>
				<p><b>Descripción de reunión:</b> {!! $reunion->descripcion !!}</p>
				<p><b>Fecha de inicio:</b> {{$reunion->fecha_inicio}}</p>
				<p><b>Colaboradores:</b>
					<ul id="lista_colaborador">
					@foreach( $colaboradores as $colaborador)
						<li>{{ $colaborador->name }}</li>
					@endforeach
					</ul>
				</p>
				<p><b>Tiempo /horas:</b> {{$reunion->horas}}</p>
				<p class="text-center">
					<a href="/reuniones" class="btn btn-primary">Volver</a>
				</p>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content_extras')
@endsection

@section('js')
	<script>
	    $('#m1').addClass('active');
	</script>
@endsection
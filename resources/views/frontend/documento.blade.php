@extends('layout.app')
@section('titulo','Crear Justificación -  Vacaciones')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
@endsection
@section('content')
<div class="col main-content">
	<div class="row">
		<div class="col-12">
			{{ Breadcrumbs::render('justificacion') }}
		</div>
		<div class="col-12 col-md-12 text-center custom-div">
			<h1 class="main-title ">Justificación</h1>
			<p class="title-description">Subes los documentos necesarios para justificar tu inasistencia.
		</div>
		<div class="col-12 col-md-12 text-center mt-5">
			<a class="btn btn-custom" href="/documentos">Ver documentos </a>

		</div>
		<div class="col-md-12">
			<div class="detalle_tiket">
				@if (session('status'))
					@if(session('status') == 'exito')
				    <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-success">Su registro fue creado con éxito</p>
					        <div class="col-md-12">
					        	<button type="button" class="btn btn-primary btn-ok" >OK</button>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
					@else
					<div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-danger">Error: tiene marcado asistencia el día: {{ session('dia') }}, verificar el rango de dias</p>
					        <div class="col-md-12">
					        	<a href="javascript:void(0)" class="btn btn-primary btn-close" >Cerrar</a>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
					@endif
				@endif

				<form action="{{route('create_documento')}}" method="POST" id="frm-ticket" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-row">
					  <div class="form-group col-md-6">
					    <label for="tipo">Tipo:</label>
					    <select class="form-control" name="tipo" id="tipo">	
							 <option ></option>
							 
						    @if(\Auth::user()->tipo == 'gerente')
			    		 	<option value="V">Vacaciones</option>
							<option value="J">Justificación</option>
							<option value="N">No existio</option>
							@else 
							<option value="V">Vacaciones</option>
							<option value="J">Justificación</option>
							@endif
					    </select>
					  </div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
						    <label for="user_id">Nombre:</label>
						    <select class="form-control" name="user_id" id="user_id">
						    	@foreach($colaboradores as $colaborador)
						    		<option value="{{$colaborador->id}}">{{$colaborador->name}}</option>
						    	@endforeach
						    </select>
						</div>
					</div>
					<div class="form-row">
					  <div class="form-group col box-calendario">
					    <label for="dia">Fecha Inicio:</label>
					    <input type="text" class="form-control" name="fecha_inicio" id="fecha1">
					    <i class="fa fa-calendar"></i>
					  </div>
					  <div class="form-group col box-calendario">
					    <label for="dia">Fecha Fin:</label>
					    <input type="text" class="form-control" name="fecha_fin" id="fecha2">
					    <i class="fa fa-calendar"></i>
					  </div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12">
						    <label for="fecha">Justificación de documento:</label>
						    <textarea  id="descripcion" name="descripcion" ckeditor="editorOptions" required></textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 text-center">
					  		<button type="submit" class="btn btn-primary">Crear</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content_extras')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>

<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
<script type="text/javascript">
	$("#alerta_popup").modal("show");
	$(".btn-close").click(function(){
		$("#alerta_popup").modal("hide");
	});
	$(".btn-ok").click(function(){
		location.href ="/documentos";
	});
	$('#descripcion').summernote({
        placeholder: 'Descripción del cambio ...',
        tabsize: 2,
        height: 200
      });
	$("#frm-ticket").validate({
		rules: {
			tipo: "required",
			user_id: "required",
			fecha_inicio: "required",
			fecha_fin: "required",
			descripcion: "required",
		},
		messages: {
			tipo: "Seleccione tipo",
			user_id: "Seleccione usuario",
			fecha_inicio: "Escoga fecha Inicio",
			fecha_fin: "Escoga fecha Fin",
			descripcion: {
				required: "Ingresar Descripción del cambio"
			},
		},
		ignore: ":hidden:not(#descripcion),.note-editable.panel-body"
	});

    $(function () {
    	var dateToday = new Date(); 
        $('#fecha1').datetimepicker({
      		format: 'YYYY-MM-DD',
        	sideBySide: true,
        	daysOfWeekDisabled:[0,6],
        	// minDate: dateToday
        });
        $('#fecha2').datetimepicker({
        	format: 'YYYY-MM-DD',
        	sideBySide: true,
        	daysOfWeekDisabled:[0,6],
            useCurrent: false //Important! See issue #1075
        });
        $("#fecha1").on("dp.change", function (e) {
            $('#fecha2').data("DateTimePicker").minDate(e.date);
        });
        $("#fecha2").on("dp.change", function (e) {
            $('#fecha1').data("DateTimePicker").maxDate(e.date);
        });
    });
    $('#m5').addClass('active');
</script>
@endsection
@extends('layout.app')
@section('titulo','Descripción proyecto')
@section('css')
    {{-- Icon Fontastic  --}}
    <link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    {{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"> --}}

    <style>
        .btn-crear:hover {
            background-color: #f4511e;
        }
        .btn-crear{
            background: #263238;
            display: inline-block;
            padding: 7px 15px;
            cursor: pointer;
            font-size: 13px;
            line-height: 13px;
            text-decoration: none;
            border-radius: 5px;
            color: #fff;
            margin-left: 5px;
            margin-right: 5px;
            margin-bottom: 20px;
        }
        .btn {
            cursor: unset;
        }
        .mitabla tr th {
            text-align: center;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid">

        <div class="row mb-4">
            <div class="col-12 mt-3"><h3 class="text-center"> {{$project->name}} </h3></div>

        </div>
        <div class="row">
            <div class="col-12">
                <label for="" class=""><strong>Estado: </strong> {{$project->estado}}%</label><br>
                <label for=""><strong>Cliente:</strong> {{$cliente->nombre}}</label> <br>
                <label for=""><strong>Razon social:</strong> {{$cliente->razon_social}}</label> <br>
            </div>
            <div class="col-12">
                <label for="" class=""><strong>Areas participantes: </strong> </label>
                <ul>
                    @foreach($project_areas as $area)
                        <li>{{$area->name}}: {{$area->percent}} %</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-12">
                <label for="" class=""><strong>Etapas: </strong> </label>
                <ul>
                    @foreach($project_etapas as $etapa)
                        <li><strong>Nombre:</strong> {{$etapa->nombre}}
                            <ul>
                                <li><strong>Inicio:</strong> {{$etapa->start_date}} </li>
                               <li><strong>Fin:</strong> {{$etapa->end_date}}</li>
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </div>
         {{--    <div class="col-12">
                <label for=""><strong>Pagos del cliente:</strong> {{$total_p}}</label> <br>
                @if ($count_pagos > 0 )
                    @foreach($resources_p as $rp)

                        <a href="{{asset('uploads/'.$rp->path)}}" target="_blank"> Ver archivo </a>
                    @endforeach
                @endif
                <label for=""><strong>Gastos Extras:</strong> {{$total_ge}}</label> <br>
                @if ($count_gastos_extras > 0 )
                    @foreach($resources_ge as $ge)

                        <a href="{{asset('uploads/'.$ge->path)}}" class="badge badge-primary" target="_blank"> Ver archivo </a>
                    @endforeach
                @endif
            </div>
            --}}
        </div>


    </div>
    </div>
@endsection


@section('js')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('js/project/edit.js')}}"></script>

@endsection
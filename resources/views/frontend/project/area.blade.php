{{-- @if($flag == 1) --}}
    <div class="col-md-4 text-center mt-4" id="area_id{{$project_area->id}}">
        <div class="offset-8 col-2  bg-success rounded-circle" onclick="editArea('{{$area->id}}','{{$project_area->percent}}','{{$project_area->id}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  >
            <i class="fas fa-edit "></i> 
        </div>
        <div class="col-2  bg-danger rounded-circle" onclick="deleteArea('{{$project_area->id}}','{{$area->id}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  >
                <i class="fas fa-minus"></i>
        </div>
        
        <label for="" >Área: {{$area->name}}</label>
        <p >Porcentaje : <span>{{$project_area->percent}}%</span> </p>
    <input type="hidden" name="areas[]" value="{{ $project_area->id }}">
    <input type="hidden" id="arrayAreas" value="{{ $area->id }}">
    <input type="hidden" id="arrayPorcentaje" value="{{$project_area->percent}}">
    </div>
{{-- else
    <div class="col-md-4 text-center mt-4" id="area_id{{$project_area->id}}">
    <div class="offset-8 col-2  bg-success rounded-circle" onclick="editArea('{{$area->id}}','{{$project_area->percent}}','{{$project_area->id}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  >
        <i class="fas fa-edit "></i> 
    </div>
    <div class="col-2  bg-danger rounded-circle" onclick="deleteArea('{{$project_area->id}}','{{$area->id}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  >
    <i class="fas fa-minus"></i>
    </div>
    <label for="" >Área: {{$area->name}}</label>
    <p >Porcentaje : <span>{{$project_area->percent}}%</span> </p>
    <input type="hidden" name="areas[]" value="{{ $project_area->id }}">
    <input type="hidden" id="arrayAreas" value="{{ $area->id }}">
    <input type="hidden" id="arrayPorcentaje" value="{{$project_area->percent}}">
    </div>
@endif
--}}
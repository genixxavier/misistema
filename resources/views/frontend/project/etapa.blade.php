{{-- @if ($flag == 1) --}}
<div class="col-4 text-center mt-3" id="etapa_project_id{{$etapa_project->id_etapa}}">
            <div class="offset-8 col-2  bg-success rounded-circle" onclick="editEtapa('{{$etapa_project->id_etapa}}','{{$etapa_project->start_date}}','{{$etapa_project->end_date}}','{{$etapa->nombre}}','{{$etapa_project->id}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  ><i class="fas fa-edit "></i>    
            </div>
            <div class="col-2  bg-danger rounded-circle" onclick="deleteEtapa('{{$etapa_project->id}}','{{$etapa_project->id_etapa}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  ><i class="fas fa-minus"></i>
            </div>
        <label for="">Nombre: {{ $etapa->nombre }}</label>
        <p>Fecha de Inicio : <span>{{$etapa_project->start_date}}</span></p>
        <p>Fecha Fin : <span>{{$etapa_project->end_date}}</span></p>
        <input type="hidden" value="{{$date_end}}" id="arraysDate">
        <input type="hidden" value="{{$etapa_project->id}}" name="etapas[]">
        @if($flag == 1)
            <div class="offset-10  col-2 rounded-circle" onclick="changeCondition('{{ $etapa_project->id }}')" style="width:45px;height:45px;color:white;padding: 0px 6px;display:inline-block"  ><input class="form-check-input" id="chekboxes"  @if($etapa_project->condition) {{'checked'}} @endif style="width:20px;height:20px;"  type="checkbox"></i>
            </div>
            <div class="message_div mt-0" id="message_div" style="display:none">
                <p class="text-primary" id="message_text">Actualizando ....</p>
            </div>
            </div>
        @endif
        @if($x > 0)
            <input type="hidden" name="values" id="values" value="{{$x}}">
        @endif

    </div>
{{--
@else
    <div class="col-4 text-center mt-3" id="etapa_id{{$etapa_project->id_etapa}}">
    <div class="offset-8 col-2  bg-success rounded-circle" onclick="editEtapa('{{$etapa_project->id_etapa}}','{{$etapa_project->start_date}}','{{$etapa_project->end_date}}','{{$etapa->nombre}}','{{$etapa_project->id}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  ><i class="fas fa-edit "></i>    
    </div>
    <div class="col-2  bg-danger rounded-circle" onclick="deleteEtapa('{{$etapa_project->id}}','{{$etapa_project->id_etapa}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  ><i class="fas fa-minus"></i>
    </div>
   
        <label for="">Nombre: {{ $etapa->nombre }}</label>
        <p>Fecha de Inicio : <span>{{$etapa_project->start_date}}</span></p>
        <p>Fecha Fin : <span>{{$etapa_project->end_date}}</span></p>
        <input type="hidden" value="{{$date_end}}" id="arraysDate">
        <input type="hidden" value="{{$etapa_project->id}}" name="etapas[]">
    </div>
  
@endif
--}}
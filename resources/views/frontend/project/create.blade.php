@extends('layout.app')
@section('titulo','crear proyecto')
@section('css')

  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"> --}}

<style>
    .btn-crear:hover {
  background-color: #f4511e;
}
.btn-crear{
  background: #263238;
  display: inline-block;
  padding: 7px 15px;
  cursor: pointer;
  font-size: 13px;
  line-height: 13px;
  text-decoration: none;
  border-radius: 5px;
  color: #fff;
  margin-left: 5px;
  margin-right: 5px;
  margin-bottom: 20px;
}
.btn {
    cursor: unset;
}
.mitabla tr th {
    text-align: center;
}
</style>
@endsection
@section('content')

<div class="container-fluid">
    <div class="row">
      
    </div>
    <div class="row mb-4">
        <div class="col-12 mt-3"><h3 class="text-center main-title"> Agregar Proyecto </h3></div>
    </div>
    
    @include('frontend.project._form')
</div>

@endsection
@section('content_extras')
  
@include('frontend.project._modals')

    
@endsection

@section('js')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="{{asset('js/project/create.js')}}"></script>
   
@endsection
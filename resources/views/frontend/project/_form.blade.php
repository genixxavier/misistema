<div class="row mt-3">
    <div class="col-12">
        <form action="" id="myForm" class="col-12">
            {{ csrf_field()}}
            <div class="form-row">
                    <div class="col-md-12 mb-1">
                        <input type="hidden" id="flagUri" value="0">
                        <button type="button"  class="icon-btn btn btn-info btn-custom "
                                data-toggle="modal"   data-target="#modalCotizaciones "
                                id="btn-modal" >
                            <i class="icon-eye" ></i>
                            Buscar Cotización
                        </button>
                    </div>
                    <div class="col-12 col-sm-12 col-md-5 col-lg-5  mt-2" >
                        <label for="cotizacion">Contacto: </label>
                        
                        @if (isset($id))
                    <input type="text" disabled  class="form-control mt-1" id="contacto" value="{{$cotizacion->c_contacto}}">   
                            <input type="hidden" name="id_project" id="id_project" value="{{$id}}">
                            <input type="hidden" name="id_cotizacion" id="id_cotizacion" value="{{$project->id_cotizacion}}">
                        @elseif (isset($coti))
                        <input type="hidden" name="id_cotizacion" id="id_cotizacion" value="{{$coti->id}}">
                        <input type="text" disabled  class="form-control mt-1" id="contacto" value="{{$coti->c_contacto}}">
                        @else 
                            <input type="hidden" name="id_cotizacion" id="id_cotizacion">
                            <input type="text" disabled  class="form-control mt-1" id="contacto">
                        @endif
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-2">
                        <label for="cotizacion"  class="col-6"  >Nombre: </label>
                        @if (isset($id))
                             <input type="text" class="form-control" id="name" placeholder="" value="{{$project->name}}" name="name">
                        @elseif (isset($coti))
                            <input type="text" class="form-control" id="name" placeholder="" value="{{$coti->c_nombre_cotizacion}}"  name="name">
                        @else
                            <input type="text" class="form-control" id="name" placeholder="" value=""  name="name">
                        @endif
                    </div>

                <div class="col-12 mt-3" >
                    <hr>
                  {{--   <button type="button" class="btn btn-primary  mt-2 btn-custom"  data-toggle="modal"   data-target="#addAreas" id="">
                        <i class="fas fa-plus"></i> Agregar áreas
                    </button> --}}
                    <div class="row text-center">
                        <h4 class="col-12">Áreas participantes <span class="error_percent" id="error_percent"></span></h4>
                    </div>
                    <div class="row " id="container_areas">
                        @if (isset($id) &&  !empty($project_areas))
                            @foreach ($project_areas as $key => $area)
                                <div class="col-3 mt-3">
                                    <label for="areas"> {{$area->name}}: </label>
                                    <input type="text" class="form-control" id="areas"  value="{{$area->percent}}" data-area="{{$area->id_area}}" name="area{{$area->id_area}}" >
                                </div>
                            @endforeach
                            @else
                            @foreach($areas as $area )
                                <div class="col-3 mt-3">
                                    <label for="areas"> {{$area->name}}: </label>
                                    <input type="text" class="form-control" id="areas"  value="0" data-area="{{$area->id}}" name="area{{$area->id}}" >
                                </div>
                            @endforeach
                        @endif

                    </div>
                </div>
                <div class="col-12 mt-3" >
                    <hr>
                    <button type="button" class="btn btn-primary  mt-2 btn-custom"  data-toggle="modal"   data-target="#addEtapas" >
                        <i class="fas fa-plus"></i> Agregar etapas (sprint)
                    </button>
                </div>
                @if (isset($id))
                    <div class="col-12 mt-3" >
                        <label for="progress">Progreso: <strong id="number_progress">{{$x}}%</strong></label>
                        <div class="progress" style="height: 6px">

                            <div class="progress-bar bg-success"  id="progress" role="progressbar" style="width: {{$x}}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                @endif
                <div class="col-12">
                    <div class="row mt-3" id="divEtapas">
                            @if (isset($id) &&  !empty($project_etapas))
                            @foreach ($project_etapas as $key => $etapa) 
                    <div class="col-4 text-center mt-3"  id="etapa_project_id{{$etapa->id_etapa}}">
                                    <div class="offset-8 col-2  bg-success rounded-circle" onclick="editEtapa('{{$etapa->id_etapa}}','{{$etapa->start_date}}','{{$etapa->end_date}}','{{$etapa->nombre}}','{{$etapa->id}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  ><i class="fas fa-edit "></i>    
                                    </div>
                                    <div class="col-2  bg-danger rounded-circle" onclick="deleteEtapa('{{$etapa->id}}','{{$etapa->id_etapa}}')" style="width:25px;height:25px;color:white;padding: 0px 6px;display:inline-block"  ><i class="fas fa-minus"></i>
                                    </div>
                                    <label for="">Nombre: {{ $etapa->nombre }}</label>
                                    <p>Fecha de Inicio : <span>{{$etapa->start_date}}</span></p>
                                    <p>Fecha Fin : <span>{{$etapa->end_date}}</span></p>

                                    <input type="hidden" value="{{$etapa->end_date}}" id="arraysDate">
                                    <input type="hidden" value="{{$etapa->id}}" name="etapas[]">
                                    
                                <div class="offset-10  col-2 rounded-circle" onclick="changeCondition('{{ $etapa->id }}')" style="width:45px;height:45px;color:white;padding: 0px 6px;display:inline-block"  ><input class="form-check-input" id="chekboxes"  @if($etapa->condition) {{'checked'}} @endif style="width:20px;height:20px;"  type="checkbox"></i>
                                    </div>
                                    <div class="message_div mt-0" id="message_div" style="display:none">
                                            <p class="text-primary" id="message_text">Actualizando ....</p>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <hr>
                    @if(isset($id))
                        <button  type="submit" class="btn btn-primary btn-custom">Actualizar Proyecto</button>
                    @else
                        <button  type="submit" class="btn btn-primary btn-custom">Crear Proyecto</button>
                    @endif
                <a href="{{route('project.index')}}" class="btn btn-primary ml-5" >Cancelar</a>
                </div>
            </div>
        </form>
    </div>
</div>

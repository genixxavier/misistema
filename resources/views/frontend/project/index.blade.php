@extends('layout.app')
@section('titulo','Proyectos')
@section('css')

  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
<style>
    .btn-crear:hover {
  background-color: #f4511e;
}
.btn-crear{
  background: #263238;
  display: inline-block;
  padding: 7px 15px;
  cursor: pointer;
  font-size: 13px;
  line-height: 13px;
  text-decoration: none;
  border-radius: 5px;
  color: #fff;
  margin-left: 5px;
  margin-right: 5px;
  margin-bottom: 20px;
}
.btn {
    cursor: unset;
}
.mitabla tr th {
    text-align: center;
}
</style>
@endsection
@section('content')

<div class="container-fluid">
    
    <div class="row">
        <div class="col-12 mt-3"><h1 class="text-center"> Proyectos</h1></div>
    </div>
    <div class="row text-center">
        <div class="col-12 col-md-4 mt-3">
         
        <a href="{{route('project.create')}}" class="btn-crear btn-custom">Crear Proyecto </a>
        </div>
        <div class="col-12 col-md-4 mt-3">

            <a href="{{route('payment.index')}}" class="btn-crear btn-custom"> Facturaciones  </a>
        </div>
        <div class="col-12 col-md-4 mt-3 ">
       
            <a href="{{ route('cotizaciones_index')}}" class="btn-crear btn-custom">Volver a cotización </a>
        </div>

    </div>
   
    <div class="row mt-5">
    
        <div class="col-md-12">
            <table class="table table-bordered table-responsive-lg mitabla custom-table">
                <thead class="">
                    <tr>
                        <th>
                            Marca
                        </th>
                      
                        <th>
                           Proyecto
                        </th>

                        <th> 

                           Estado
                        </th>


                        <th>
                            Opciones
                        </th>
                    </tr>
                    
                </thead>
                <tbody>
                        @foreach ($project as $item)
                        <tr class="text-center">
                            <td> 
                                {{$item->marca}}
                            </td>
                            <td>
                              {{$item->name}}
                            </td>
                            <td> 
                                {{$item->estado}}%
                            </td>


                            <td>
                                <div class="dropdown">
                                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-cog"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item"  href="{{route('project.view', $item->id) }}"><i class="fas fa-eye"></i> Ver</a>
                                            <a class="dropdown-item"  href="{{route('project.edit', $item->id) }}"><i class="far fa-edit"></i> Editar</a>
                                            <a class="dropdown-item"  href="{{route('payment.add_payment', $item->id) }}"><i class="fas fa-money-check-alt " ></i> Añadir pago</a>
                                            <a class="dropdown-item" target="_blank"  href="{{route('project.ver_pdf', $item->id_cotizacion) }}"><i class="fas fa-file-pdf"></i> Ver cotización</a>
                                        </div>
                                </div>
                            </td>
                        </tr>
                     @endforeach
                </tbody>
            </table>
            {!! $project->render() !!}

        </div>
   
    </div>
</div>
@endsection
@section('content_extras')
  

    
  

   
    
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>


   
@endsection
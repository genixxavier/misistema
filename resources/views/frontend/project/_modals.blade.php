
<div class="modal fade" id="addAreas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Áreas</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="limpiar()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           <div class="container">
                <form action="" id="myAddArea" method="POST">
                        <div class="form-group row">
                            <label for="">Área : </label> 
                            <select name="area" id="area" class="form-control">
                                @if(!empty($areas))
                                    @foreach ($areas as $area)
                                        <option value="{{$area->id}}">{{$area->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group row">
                            <label for="">Porcentaje (Solo número) : </label> 
                            <input type="number" class="form-control" name="percent" id="percent">
                            <input type="hidden" name="id_project_area" id="id_project_area"  value="">
                        </div>
                       
                        <div class="text-center mt-3">
                                <button type="submit" class="btn btn-primary" id="btn_add_percent">Agregar </button>
                                <button type="button" class="btn btn-secondary" onclick="limpiar()"  data-dismiss="modal">Cerrar</button>
                        </div>
                    </form>
           </div>
        </div>
        <div class="modal-footer">
         
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="addEtapas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Etapas</h5>
              <button type="button" class="close" data-dismiss="modal" onclick="limpiarEtapa()" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
               <div class="container">
                    <form action="" id="myAddEtapa" method="POST">
                            <div class="form-group row">
                                <label for="">Nombre : </label> 
                                <input type="text" class="form-control" name="name_etapa" id="name_etapa">
                               
                            </div>
                            <div class="form-group row">
                                <label for="">Fecha de inicio : </label> 
                                <input type="text" class="form-control" autocomplete="off"  id="date_start" name="date_start">
                            </div>
                            <div class="form-group row">
                                    <label for="">Fecha de culminación : </label> 
                                    <input type="text" class="form-control" autocomplete="off"  name="date_end" id="date_end">
                                    <input type="hidden" id="id_project_etapa" name="id_project_etapa">
                                    <input type="hidden" id="id_etapa" name="id_etapa">
                                    @if(isset($id))
                                    <input type="hidden" id="flag_id" name="flag_id" value="{{$id}}">
                                    @endif
                                </div>
                            <div class="text-center mt-3">
                                    <button type="submit" class="btn btn-primary" id="btn_etapa">Agregar </button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"  onclick="limpiarEtapa()" >Cerrar</button>
                            </div>
                        </form>
               </div>
            </div>
            <div class="modal-footer">
             
            </div>
          </div>
        </div>
      </div>
   
      {{-- Buscar cotizaciones aprobadas --}}
      <div class="modal fade" id="modalCotizaciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cotizaciones</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <table class="table table-striped table-bordered mitabla custom-table" id="dataTable" >
                        <thead> 
                            <tr>
                                <th>Opciones</th>
                                <th>Cliente</th>
                                <th>Contacto</th>
                                <th>Nombre</th>
                                <th>Razón Social</th>
                                <th>RUC</th>
                               {{-- <th>Fecha</th> --}}

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cotizaciones as $cotizacion)
                                
                            <tr>
                                <td> 
                                    <button class="btn btn-primary icon-btn" 
                                    onclick="addCotizacion('{{ $cotizacion->id}}','{{$cotizacion->c_contacto}}','{{$cotizacion->c_nombre_cotizacion}}')">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </td>
                                <td>{{$cotizacion->cliente}}</td>
                                <td>{{$cotizacion->c_contacto}}</td>
                                <td>{{$cotizacion->c_nombre_cotizacion}}</td>
                                <td>{{$cotizacion->razon_social}}</td>
                                <td>{{$cotizacion->ruc}}</td>
                                {{-- <td>{{$cotizacion->c_fecha}}</td> --}}

                            </tr>
                            
                            @endforeach 
                    
    
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<!doctype html>
<html lang="es">
  <head>
    <title>Alerta</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">

  <!-- <meta  http-equiv="refresh" content="5"/> -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/alertas.css')}}">
  </head>
  <body>
  <!-- <header> -->
<!--       <div class="container">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 col-sm-2 col-6 responsi">
              <img src="http://localhost:8000/img/logo.png" alt="">
            </div>
            <div class="col-6 col-sm-9 text-right align-middle time">
              <span class="fecha">Wed 03 de October, 
                <form name="form_reloj" style="display: inline;"> 
                <input type="text" name="reloj" size="10" onfocus="window.document.form_reloj.reloj.blur()" style="background-color: transparent;border: none;color: white"> 
              </form>         
            </span>
              <a class="btn" href="http://localhost:8000/logout"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                        <form id="logout-form" action="http://localhost:8000/logout" method="POST" style="display: none;">
                            <input type="hidden" name="_token" value="ZbkF2zOsDwXyeDZL39qBbjYArwUOgQChNK1RxyS9">
                        </form>
            </div>
          </div>
        </div>
      </div>
  </header> -->

    <div id="alertas">
  		<div class="container-fluid">
        <div class="row">
          <div class="col-md-12 text-center">
            <h2 class="parpadea" style="padding: 10px">AVANCE: <b>100%</b></h2>
          </div>
        </div>
  			<div class="row">
  					@foreach($tickets as $ticket)
<!--   						<div class="col-md-3 cardx 
  						@if($ticket->estado == 0 ) bg-light
              @elseif($ticket->estado == 1 ) bg-info
    					@elseif($ticket->estado == 2 ) bg-warning
              @elseif($ticket->estado == 3 ) bg-danger
              @elseif($ticket->estado == 4 ) bg-success
    					@elseif($ticket->estado == 5 ) bg-primary
              @elseif($ticket->estado == 10 ) bg-morado
    					@endif"> -->
              <div class="col-md-4 col-lg-4 col-xl-3 col-sm-6 cuadro">
                <div class="cardx 
              @if($ticket->estado == 0 ) bg-light
              @elseif($ticket->estado == 1 ) bg-info
              @elseif($ticket->estado == 2 ) bg-warning
              @elseif($ticket->estado == 3 ) bg-danger
              @elseif($ticket->estado == 4 ) bg-success
              @elseif($ticket->estado == 5 ) bg-primary
              @elseif($ticket->estado == 10 ) bg-morado
              @endif">
    							<div class="card-block">
    								<h2>{{$ticket->nombre}}</h2>
    								<p class="cliente">[{{$ticket->cliente->nombre}}]</p>
    								<p class="nombre">{{$ticket->user->name}}</p>
    								<p class="fecha">INICIO : <?php echo date('d/m H:i', strtotime($ticket->fecha_inicio));?> <br>
                                    FIN : <?php echo date('d/m H:i', strtotime($ticket->fecha_fin));?></p>
    							</div>
                </div>
              </div>
  						<!-- </div> -->
  					@endforeach
  			</div>
  		</div>
    </div>
    <div id="colores">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <ul class="indicaciones">
              <li><span class="azul"></span>En Proceso</li>
              <li><span class="rojo"></span>Atrasado</li>
              <li><span class="verde"></span>Terminado</li>
            </ul>
            
          </div>
        </div>
      </div>
    </div>
    <section id="footer">
      <div class="container-fluid">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center">
               Copyright 2018 © Powered by <a href="https://www.mediaimpact.pe/" target="_blank">Media Impact</a>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.js"></script>
  </body>
</html>
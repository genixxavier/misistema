@extends('layout.app')
@section('titulo','Crear Ticket')

@section('css')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
@endsection
@section('content')
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				@if (session('status'))
					@if(session('status') == 'Creado exitosamente')
				    <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-body text-center">
										<p class="text-success">{{ session('status') }}</p>
										<div class="col-md-12">
											<button type="button" class="btn btn-primary btn-ok" >OK</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					@endif
					@if(session('status') == 'existe')
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
					  <strong>Error,</strong> las horas puestas en el ticket estan ocupadas por otro ticket, verificar horas del pedido.
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
					@endif
				@endif

				<form action="{{route('create_ticket')}}" method="POST" id="frm-ticket">
					{{ csrf_field() }}
					<input type="hidden" name="estado" value="0">
					<input type="hidden" name="user_create" value="{{ \Auth::user()->id }}">
					<div class="form-row">
					  <div class="form-group col-md-6">
					    <label for="nombre">Nombre de pedido:</label>
					    <input type="text" class="form-control" name="nombre" id="nombre" required>
					  </div>
					  <div class="form-group col-md-6">
					    <label for="cliente">Cliente:</label>
					    <select class="form-control" name="cliente_id" id="cliente">	
					    	@foreach($clientes as $cliente)
					    		 @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
					    		 	<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
					    		 @else
					    			<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
					    		 @endif
					    	@endforeach
					    </select>
					  </div>
					</div>
					@if(\Auth::user()->tipo == 'jefe')
					<div class="form-row">
						<div class="form-group col-md-6">
						    <label for="cliente">Colaborador:</label>
						    <select class="form-control" name="user_id" id="cliente">
						    	@foreach($colaboradores as $colaborador)
						    		<option value="{{$colaborador->id}}">{{$colaborador->name}}</option>
						    	@endforeach
						    </select>
						</div>
					</div>
					
					<div class="form-row form-row-2">
					  <div class="form-group col box-calendario">
					    <label for="dia">Día:</label>
					    <input type="text" class="form-control form-control-2" name="fecha_inicio" id="fecha">
					    <i class="fa fa-calendar"></i>
					  </div>
					  <div class="form-group col">
					    <label for="hora">Horas del pedido:</label>
					    <input type="number" class="form-control" name="horas_pedido" id="hora" value="1">
					  </div>
					  <div class="form-group col">
					    <label for="hora_s">Horas de supervisión:</label>
					    <input type="number" class="form-control" name="horas_supervision" id="hora_s" value="0">
					  </div>
					</div>
					@else
					<div class="form-row">
					  <div class="form-group col-md-6 box-calendario">
					    <label for="fecha">Fecha de entrega:</label>
					    <input type="text" class="form-control" name="fecha_limite" id="fecha">
					    <i class="fa fa-calendar"></i>
					  </div>

					  <div class="form-group col-md-6">
					    <label for="fecha">Área:</label>
					    <select class="form-control" name="area_id" id="area_id">
					    	@foreach($areas as $area)
					    		<option value="{{$area->id}}">{{$area->name}}</option>
					    	@endforeach
					    </select>
					  </div>
					</div>

					@endif
					<div class="form-row">
						<div class="form-group col-md-12">
						    <label for="fecha">Descripción:</label>
						    <textarea  id="descripcion" name="descripcion" ckeditor="editorOptions" required></textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 text-center">
					  		<button type="submit" class="btn btn-primary">Crear</button>
{{-- 					  		<button type="submit" class="btn btn-dark">Volver</button> --}}
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content_extras')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>

<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
<script type="text/javascript">
	$("#alerta_popup").modal("show");
	$(".btn-ok").click(function(){
		location.href ="/historial";
	});
	$('#descripcion').summernote({
        placeholder: 'Descripción del cambio ...',
        tabsize: 2,
        height: 200
      });
	$("#frm-ticket").validate({
		rules: {
			nombre: "required",
			descripcion: "required",
			cliente_id: "required",
			user_id: "required",
			fecha_inicio: "required",
			fecha_limite: "required",
			horas_pedido: {
				required: true,
				number: true,
				minlength: 1,
				min: 1
			},
			horas_supervision: {
				required: true,
				number: true,
				minlength: 1,
				min: 0
			},
		},
		messages: {
			nombre: "Ingresar nombre",
			descripcion: {
				required: "Cuéntanos más sobre el proyecto"
			},
			cliente_id: "Escoga cliente",
			user_id: "Escoga Colaborador",
			fecha_inicio: "Escoga fecha",
			fecha_limite: "Escoga fecha",
			horas_pedido: {
				required: "Ingrese horas",
				number: "Ingrese numeros",
				min: "Las horas deber ser mayor a 0"
			},
			horas_supervision: {
				required: "Ingrese horas",
				number: "Ingrese numeros",
				min: "Las horas deber ser mayor o igual 0"
			}
		},
		ignore: ":hidden:not(#descripcion),.note-editable.panel-body"
	});

    $(function () {
    	var dateToday = new Date(); 
        $('#fecha').datetimepicker({
      		format: 'YYYY-MM-DD HH:00:00',
        	sideBySide: true,
        	daysOfWeekDisabled:[0,6],
        	minDate: dateToday
        });
    });
    $('#m2').addClass('active');
</script>
@endsection
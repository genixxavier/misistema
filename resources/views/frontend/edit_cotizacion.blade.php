@extends('layout.app')
@section('titulo','Editar Cotización')
@section('css')
    {{-- Icon Fontastic  --}}
    <link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=374b9rm3q420nrmdtfwnqf3miwqwvrto3p2dgt51spdlozup"></script>

    <style>
        a {
            color: white;
            text-decoration: none;
        }
        a:hover {
            color: white;
            text-decoration: none;
        }
        .mitabla {
            margin-top: 20px;

        }
        .mitabla thead tr {
            background: #263238;
            color: #fff;

        }
        .mitabla thead tr th {
            font-weight: 400;
            color: #fff;
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .mitabla tbody tr td {
            font-size: 14px;
        }
        .myDiv {
            border: 1px solid black;
            padding: 25px;
        }
        @media screen and (max-width: 576px) {
            .button-res {
                height: 35px;
                width: 60px;
                margin: 10px auto;
                margin-right: 35px;
            }

        }
        @media screen and (max-width: 452px) {
            .res {
                margin-right: 35px
            }
        }
        .show_service {
            background-color: whitesmoke;
            border: 1px solid #6b9dbb;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid">
        <div class="row">
            {{ Breadcrumbs::render('editar_cotizacion',$cotizacion->id,$flag) }}
        </div>
        <div class="row">
            <div class="col-12">
                <div class="detalle_tiket">
                    <h2>Formulario Cotización</h2>
                    <form id="myForm" enctype="multipart/form-data">
                        {{ csrf_field()}}
                        <div class="form-row">

                            @if ($title_tema !== '')
                            <div class="col-md-12">
                                <input type="hidden" id="flagUri" value="0">
                                <button type="button"  class="icon-btn btn btn-info "
                                        data-toggle="modal"   data-target="#myModal "
                                        id="btn-modal" >
                                    <i class="icon-eye" ></i>
                                    Buscar Contact Report
                                </button>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6  mt-2" >
                                <label for="cotizacion">Tema:  </label>
                                <input type="text" disabled  class="form-control mt-1" id="reunion"  value="{{$title_tema}}"                                                  >
                                <input type="hidden" name="id_cr" id="id_cr"    value="{{$contact_report->id}}"                                                                       >
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-2">
                                <label for="cotizacion">Cliente:  </label>
                                <input type="text" readonly name="cliente" id="cliente"  class="form-control mt-1" value="{{$cliente->nombre}}"  >
                            </div>


                                <div class="col-md-4 mt-3">
                                    <label for="cotizacion" class="col-6">Contacto: </label>
                                    <input type="text" class="form-control" id="contacto" placeholder=""  value="{{$cotizacion->c_contacto}}" onkeypress="return soloLetras(event)"  name="nombre">
                                </div>

                                <div class="col-md-4 mt-3">
                                    <label for="razon_social" class="col-6">Razón Social:</label>
                                    <input type="text" class="form-control" id="razon_social" placeholder=""  readonly name="razon_social" value="{{$cliente->razon_social}}">
                                </div>
                                <div class="col-md-4 mt-3">
                                    <label for="ruc" class="col-6">RUC:</label>
                                    <input type="number" class="form-control"  readonly id="ruc"  onkeypress="return maximo(event)"  placeholder=""  name="ruc" value="{{$cliente->ruc}}">
                                </div>



                            @else
                                <div class="col-md-4 mt-3">
                                    <label for="cotizacion" class="col-6">Contacto: </label>
                                    <input type="text" class="form-control" value="{{$cotizacion->c_contacto}}" onkeypress="return soloLetras(event)"    id="contacto" placeholder=""   name="nombre">
                                </div>
                                <div class="col-md-4 mt-3">
                                    <label for="" class="">Cliente: </label>
                                    <select name="cliente" id="cliente"  class="form-control">
                                        <option value="seleccionar">-- Seleccione --</option>


                                        <optgroup label="Fees">
                                                @foreach($clientes as $cliente)
                                                    @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
                                                        @if($cliente->type == 1)
                                                        <option value="{{$cliente->id}}"
                                                                @if($cliente->id  == $cotizacion->id_cliente)
                                                                    {{'selected'}}
                                                                @endif  >{{$cliente->nombre}}</option>
                                                        @endif
                                                    @else
                                                        @if ($cliente->type == 1)
                                                        <option value="{{$cliente->id}}"
                                                                @if($cliente->id  == $cotizacion->id_cliente)
                                                                    {{'selected'}}
                                                                @endif  >{{$cliente->nombre}}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                        </optgroup>
                                            <optgroup label="Spots A">
                                                @foreach($clientes as $cliente)
                                                    @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
                                                        @if($cliente->type == 2   )
                                                        <option value="{{$cliente->id}}"
                                                                @if($cliente->id  == $cotizacion->id_cliente)
                                                                    {{'selected'}}
                                                                @endif  >{{$cliente->nombre}}</option>
                                                        @endif
                                                    @else
                                                        @if ($cliente->type == 2)
                                                        <option value="{{$cliente->id}}"
                                                                @if($cliente->id  == $cotizacion->id_cliente)
                                                                    {{'selected'}}
                                                                @endif  >{{$cliente->nombre}}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                        </optgroup>
                                        <optgroup label="Spots B">
                                                @foreach($clientes as $cliente)
                                                    @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
                                                        @if($cliente->type == 3  )
                                                        <option value="{{$cliente->id}}"
                                                                @if($cliente->id  == $cotizacion->id_cliente)
                                                                    {{'selected'}}
                                                                @endif  >{{$cliente->nombre}}</option>
                                                        @endif
                                                    @else
                                                        @if ($cliente->type == 3)
                                                        <option value="{{$cliente->id}}"
                                                                @if($cliente->id  == $cotizacion->id_cliente)
                                                                    {{'selected'}}
                                                                @endif  >{{$cliente->nombre}}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                        </optgroup>
                                        <optgroup label="Otros">
                                                @foreach($clientes as $cliente)
                                                    @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
                                                        @if($cliente->type !== 3 && $cliente->type !== 2 && $cliente->type !== 1  )
                                                        <option value="{{$cliente->id}}"
                                                                @if($cliente->id  == $cotizacion->id_cliente)
                                                                    {{'selected'}}
                                                                @endif  >{{$cliente->nombre}}</option>
                                                        @endif
                                                    @else
                                                        @if ($cliente->type !== 3 && $cliente->type !== 2 && $cliente->type !== 1)
                                                        <option value="{{$cliente->id}}"
                                                                @if($cliente->id  == $cotizacion->id_cliente)
                                                                    {{'selected'}}
                                                                @endif  >{{$cliente->nombre}}</option>
                                                        @endif
                                                    @endif
                                                @endforeach
                                                {{-- <option value="otros">Añadir nuevo</option> --}}
                                        </optgroup>

                                    </select>
                                </div>

                                <div class="col-md-4 mt-3">
                                    <label for="ruc" class="col-6">RUC:</label>
                                    <input type="text" class="form-control"  value="{{$cliente->ruc}}"   disabled   onkeypress="return maximo(event)"   id="ruc" placeholder=""  name="ruc">
                                    <input type="hidden" id="flag" name="flag" value="@if(isset($flag)){{$flag}}@endif">
                                </div>
                                <div class="col-12" style="display: none;" id="conten_new_client">
                                    <div class="row mt-3">
                                        <div class="col-4">
                                            <label for="">Nombre Comercial: </label>
                                            <input type="text" id="nombre_comercial"  class="form-control"  name="nombre_comercial">
                                        </div>
                                        <div class="col-4">
                                            <label for="">Razon Social: </label>
                                            <input type="text" id="razon_social" class="form-control" name="razon_social">
                                        </div>
                                        <div class="col-4">
                                            <label for="">Tipo</label>
                                            <select name="tipo_cliente" id="tipo_cliente" class="form-control">
                                                <option value="1">Fee</option>
                                                <option value="2">Spot</option>
                                                <option value="3">Otro</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            @endif



                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 mt-3">
                                <label for="nombre_cotizacion">Nombre :</label>
                                <input type="text"  class="form-control" id="nombre_cotizacion" name="nombre_cotizacion" value="{{$cotizacion->c_nombre_cotizacion}}">
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 mt-3">
                                <label for="tipo_comprobante">Tipo de Comprobante :</label>
                                <select name="tipo_combrobante" id="tipo_comprobante" class="form-control">
                                    <option value="1"  <?php
                                            if ($cotizacion->c_tipo_comprobante == 1 ) {
                                                echo "selected";
                                            }               
                                        ?> 
                                    >Boleta</option>
                                    <option value="2"   <?php
                                    if ($cotizacion->c_tipo_comprobante == 2) {
                                        echo "selected";
                                    }
                                    
                                ?> >Factura</option>
                                   
                                </select>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 mt-3">
                                <label for="">Número :</label>
                                <input type="text" name="numero"  readonly  value="{{$cotizacion->c_num}}" class="form-control"
                                       value="">
                                <input type="hidden" name="id_cotizacion" id="id_cotizacion" value="{{$cotizacion->id}}" >
                                <input type="hidden" name="id_cliente" id="id_cliente" value="{{$cotizacion->id_cliente}}">
                                @if ($errors->has('numero'))
                                    @foreach ($errors->get('numero') as $error)

                                        <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 mt-3">
                                <label for="" class="color-input ">Seleccione la fecha  <span style="color:black"><i class="far fa-calendar-alt"></i></span></label>
                            <input id="datepicker" class="form-control" value="{{$fecha}}" name="fecha" >
                            </div>
                            <div class="col-12 col-md-4 mt-3">
                                <label for="">Tipo de moneda: </label>
                                <select name="moneda"  class="form-control">
                                    <option value="1"  <?php
                                        if ($cotizacion->c_tipo_moneda == 1 ) {
                                            echo "selected";
                                        }
                                        ?>>Sol  </option>
                                    <option value="2" <?php
                                        if ($cotizacion->c_tipo_moneda == 2 ) {
                                            echo "selected";
                                        }
                                        ?>>Dólar </option>

                                </select>
                            </div>
                            <div class="col-12 col-md-4 mt-3">
                                <label for="">Seleccione el tipo de adjunto</label>
                            <select name="adjunto" id="adjunto"  class="form-control"  >
                                    <option value="1"   <?php
                                    if ($cotizacion->c_type_file == 1 ) {
                                        echo "selected";
                                    }               
                                ?> >Archivo  </option>
                                    <option value="2" <?php
                                    if ($cotizacion->c_type_file == 2 ) {
                                        echo "selected";
                                    }               
                                ?> >Link de google docs</option>
                                    <option value="3" <?php
                                    if ($cotizacion->c_type_file == 3 ) {
                                        echo "selected";
                                    }               
                                ?> >No</option>
                                </select>
                            </div>
                            <div class="col-12 col-md-4 mt-3"  id="container_files">
                                <div class="col-12" id="files" style="<?php
                                if ($cotizacion->c_type_file == 1 ) {
                                    echo "display:block";
                                } 
                                else {
                                    echo "display:none";
                                }              
                            ?> " >
                                    {{-- <div class="col-12 col-sm-12 col-md-4 col-lg-4 mt-3"> --}}
                                        <label for="">pdf | word | excel:</label>
                                        <input type="file" onchange="getImage(event)" class="form-control" accept=".xlsx , .doc ,.pdf ,.docx"  name="me_files" id="me_files">
                                        <input type="hidden" name="file" id="file">
                                    {{-- </div> --}}
                                </div>
                                <div class="col-12" id="link" style="<?php
                                if ($cotizacion->c_type_file == 2 ) {
                                    echo "display:block";
                                } 
                                else {
                                    echo "display:none";
                                }              
                            ?>">
                                    <label for="">Ingrese el link</label>
                                <input type="text" id="link_google" name="link_google" value="{{ $cotizacion->c_file }}" class="form-control" >
                                </div>
                            </div>
                            <div class="mt-3 mb-2 col-12 form-check" style="margin-left:25px">
                                <input class="form-check-input" name="inversion_total"  @if ($cotizacion->c_boolean_inversion) {{'checked'}}
                                    
                                @endif  type="checkbox" value="1" id="inversion_total" style="width:15px;height:15px">
                                <label class="form-check-label" for="inversion_total">
                                  Poner inversión total 
                                </label>
                            </div>
                            <div class="mt-3 mb-2 col-12 form-check" style="margin-left:25px">
                                <input class="form-check-input" name="igv"  @if ($cotizacion->boolean_igv) {{'checked'}}
                                    
                                @endif  type="checkbox" value="1" id="igv" style="width:15px;height:15px">
                                <label class="form-check-label" for="igv">
                                  Poner IGV 
                                </label>
                            </div>
                            <div class="mt-3 col-12"   style="display:none">
                                    <div class="row " >
                                        <div class="col-12">
                                            <h3 class="text-center">Cotizador</h3>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center col-12 mb-3"  >
                                        <select name="staff" id="staff" class="form-control col-4">
                                            @foreach($staff as $person)
                                                <option value="{{$person->id}}">{{$person->persona}}</option>

                                            @endforeach
                                        </select>
                                        <input type="number" placeholder="Horas"  id="horas" name="horas" class="form-control col-2">
                                        <input type="submit" class="btn btn-primary col-2"  id="btnCotizador" value="Añadir">

                                    </div>
                                    <div class="col-12  text-center mt-2" >
                                        <table class="table table-striped  ">
                                            <thead>
                                            <th>Staff</th>
                                            <th>Hora</th>
                                            <th>Total</th>
                                            <th></th>
                                            </thead>
                                            <tbody class="content-staff" id="content-staff">
                                           @if(!empty($staffCotizacion))
                                               @foreach($staffCotizacion as $staff)
                                                   <tr class="staff-item"  id="staffCotizador-{{$staff->id}}">
                                                       <td>
                                                           <input type="hidden" id="listStaffCotizador" name="listStaffCotizador[]" value="{{$staff->id}}" >
                                                           {{$staff->persona}}
                                                       </td>
                                                       <td>{{$staff->horas}}</td>
                                                       <td >S/ <span id="staff-item-total">{{$staff->total}}</span></td>
                                                       <td> <i style="cursor: pointer" class="fas fa-trash" onclick="deleteStaffCotizador({{$staff->id}})" ></i></td>
                                                   </tr>
                                               @endforeach
                                           @endif
                                            </tbody>
                                            <tfood>
                                                <tr  class="table-primary">
                                                    <td colspan="1"></td>
                                                    <td>Total : </td>
                                                    <td id="sumTotalTableStaff">S/ {{$sumTotalStaffCotizacion}}</td>
                                                    <td></td>
                                                </tr>
                                            </tfood>
                                        </table>
                                    </div>

                                </div>
                            <div class="row center mt-3" id="service">

                                @foreach( $service_cotizacion as $service)

                                    <div class="col-12  mt-5 show_service" id="{{$service->id}}">
                                        <div class="col-12 mt-3">
                                            <a href="javascript:void(0)" class="eliminar_tema rounded" data-id=""  onclick="delete_service(this
                                                    )"  id="delete_service" >X</a>
                                            <a href="javascript:void(0)" class="eliminar_tema bg-success text-white rounded mr-5" data-id=""  data-toggle="modal"  data-target="#addservicio" onclick="edit_service({{$service->id}})" id="edit_service" ><i class="far fa-edit"></i></a>
                                            <div class="col-md-12">
                                                <label >Servicio : <span class="font-weight-bold text-primary"> {{$service->title}}</span> </label>
                                                <input type="hidden" name="data_get_tema_id" value="">
                                            </div>
                                            <div class="col-md-12">
                                                <label>Descripción del servicio :</label> <br>

                                                <div class="mb-3">
                                                    {!!$service->description!!}
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="" class="col-2 col-form-label">Precio :</label>
                                                <div class="col-2">
                                                    <input type="text" class="form-control"  readonly value="{{$service->precio}}">
                                                </div>
                                                <label for="" class="col-2 col-form-label">Descuento % :</label>

                                                <div class="col-2">
                                                    <input type="text" class="form-control"  readonly value="{{$service->descuento}}">
                                                </div>
                                                <label for="" class="col-2 col-form-label">Total: S/:</label>
                                                <div class="col-2">
                                                    <input type="text" class="form-control" readonly value="{{$service->total}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                            </div>
                            <div class="col-12 mt-3" >
                                <hr>
                                <button type="button" class="btn btn-primary  mt-2"  data-toggle="modal"                                       data-target="#addservicio" id="addService"> <i class="fas                                                  fa-plus"></i> Agregar servicio</button>
                            </div>
                            <div class="col-12 mt-4">
                                <label for=""  class="text-dark font-weight-bold">Consideraciones del servicio: </label>
                                <textarea name="consideraciones_servicio" class="form-control"   id="consideraciones_servicio" cols="30"></textarea>
                            <input type="hidden" id="data_consideraciones" value="{{$cotizacion->c_consideraciones}}">
                            </div>
                            <div class="col-12 mt-4">
                                <label for="" class="text-dark font-weight-bold" >Acciones no incluidas en el presupuesto: </label>
                                <textarea name="acciones_no_incluidas" class="form-control"   id="acciones_no_incluidas" cols="30"></textarea>
                            <input type="hidden" id="data_acciones" value="{{$cotizacion->c_acciones}}">
                            </div>
                           
                            <div class="col-md-12 text-center">
                                <hr>
                                <button  type="submit" class="btn btn-primary" >Editar cotización</button>
                                <a href="/funnel/cotizaciones" class="btn btn-secondary ml-5" >Cancelar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('content_extras')
    {{-- MODAL --}}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Contact Reports</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center">
                    <table class="table table-striped table-bordered mitabla " id="dataTable" >
                        <thead>
                        <tr>
                            <th> Opciones </th>
                            <th>Tema </th>
                            <th>Cliente </th>
                            <th> Fecha</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($contact as $item)
                            <?php
                            $i = 0;
                            $list="";
                            ?>
                            @foreach( $item->get_temas($item->id) as $tema)
                                @if($i > 0)
                                    <?php $list .= ", ";?>
                                @endif
                                <?php $list .= $tema->title;?>
                                <?php $i++;?>
                            @endforeach
                            <?php
                            $t = strlen($list);
                            if($t > 50){
                                $text = substr($list,0,50);
                                $ttt =  $text."...";
                            }
                            else{
                                $ttt = $list;
                            }
                            ?>
                            <tr>
                                <td>
                                    <button class="btn btn-primary icon-btn" onclick="view_contact('{{ $item->id}}','{{ $ttt }}','{{ $item->cliente }}','{{ $item->id_cliente }}')"> <i class="icon-plus"></i> </button>
                                </td>
                                <td>
                                    {{$ttt}}
                                </td>
                                <td>
                                    {{ $item->cliente }}
                                </td>
                                <td>
                                    {{ $item->fecha}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
    {{-- add servicio --}}
    <div class="modal fade" id="addservicio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="limpiar()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body center" id="">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="modal-title" id="modal_title">Nuevo Servicio</h4>
                        </div>
                        <div class="col-md-12">
                            <form id="add_servicio">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Servicio:</div>
                                            </div>
                                            <input type="text" class="form-control" name="data_title" id="modal_service" placeholder="Nombre servicio" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title">Descripción del servicio:</label>
                                    <textarea name="data_description" class="form-control"   id="modal_description" cols="30"></textarea>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        {{-- <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Nota:</div>
                                            </div>
                                            <input type="text" class="form-control" name="data_nota" id="modal_nota">
                                        </div> --}}
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Precio:</div>
                                            </div>
                                            <input type="number" class="form-control" name="data_precio" id="modal_precio" placeholder="00.00" required>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Descuento: %</div>
                                            </div>
                                            <input type="number" class="form-control" name="data_descuento" id="modal_descuento" placeholder="00">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">Total:</div>
                                            </div>
                                            <input type="number" class="form-control disabled" readonly name="data_total" id="modal_total" placeholder="00.00">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-right" id="">
                                    <hr>
                                    <div class="alerta_add_service"></div>
                                    <button type="submit" class="btn btn-primary mb-2" id="modal_button" onclick="operation(this,event.preventDefault())" >Crear servicio</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
    <script src="{{asset('js/cotizador.js')}}"></script>
    <script>

        //Helpers
        let c = console.log ,
            byId = document.getElementById
     

        let id_cotizacion ;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // ============= MODAL ====================
        function  view_contact(id,reunion,cliente, id_cliente) {
        /*    document.getElementById('reunion').value= reunion
            document.getElementById('id_cr').value = id
            document.getElementById('cliente').value = cliente
            $('#modal_servicio').modal('hide');*/

            let uri = '/funnel/cotizacion_cliente',
                myData = {
                    id : id_cliente
                }
            $.ajax({
                method: 'POST',
                url: uri,
                data: myData,
                success: function(data){
                    document.getElementById('razon_social').value = data.razon_social
                    document.getElementById('ruc').value = data.ruc
                    document.getElementById('reunion').value= reunion
                    document.getElementById('id_cr').value = id
                    document.getElementById('cliente').value = cliente
                    document.getElementById('id_cliente').value = data.id
                    $('#myModal').modal('hide');
                }
            });
        }
        // =============  END MODAL ====================
        $(document).ready( function () {

            let test = document.getElementById('data_consideraciones').value
            let test2= document.getElementById('data_acciones').value

            tinymce.init({
                selector: '#modal_description',
                height: 400,
                menubar: false,

                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',

            })
            tinymce.init({
                selector: '#consideraciones_servicio',
                height: 200,
                menubar: false,

                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',

            })
            tinymce.init({
                selector: '#acciones_no_incluidas',
                height: 200,
                menubar: false,

                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',

            })

            setTimeout(() => {
                tinymce.get('consideraciones_servicio').setContent(test);
                tinymce.get('acciones_no_incluidas').setContent(test2);

            },1000)

            $('#dataTable').DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            } );
            function calularpreciot(precio,descuento){
                if(descuento){
                    var x = (precio*descuento) / 100;
                    var x = precio - x;
                }
                else{
                    var x = precio;
                }
                $("#modal_total").val(x);
            }
            $("#modal_precio").on('keyup',function(){
                calularpreciot( $('#modal_precio').val(),$('#modal_descuento').val() );
            })
            $("#modal_descuento").on('keyup',function(){
                calularpreciot( $('#modal_precio').val(),$('#modal_descuento').val() );
            })

            
        

        })


        //Formulario Cotizacion

        let form  = document.getElementById('myForm')

        form.addEventListener('submit', (e) => {
            e.preventDefault()


            // /let data = $("#myForm").serialize();
            let data =  new FormData(form)
            data.id = document.getElementById('id_cotizacion').value
            data.append('consideraciones_servicios_html',tinymce.get('consideraciones_servicio').getContent())
            data.append('acciones_no_incluidas_html',tinymce.get('acciones_no_incluidas').getContent())
            $.ajax({
                method: 'POST',
                url: '../update_cotizacion',
                data: data,
                contentType: false,
                processData: false,

                success: function(data){
                console.log(data)
                    if (data.status !== 200) {
                        alertify.error(data.message)
                    }
                    alertify.success(data.message)
                   window.location.href = '../cotizaciones'

                }
            });


        })
        function calcular (e,id) {
            e.preventDefault()
            let sub = document.getElementById('sub'+id),
                des = document.getElementById('des'+id),
                total = document.getElementById('total'+id),
                calcularTotal = sub.value - des.value
            total.textContent = `Inversión S/ ${calcularTotal}.00`


        }

        const edit_service = (id) =>  {
            let uri = '../show_service_cotizacion'
            $.ajax({
                method: 'POST',
                url: uri,
                data: {
                    id
                },

                success: function(data){
                    console.log(data)
                    document.getElementById('modal_title').textContent = 'Editar Servicio'
                    document.getElementById('modal_service').value = data.title
                    tinymce.get('modal_description').setContent(data.description);
                    // document.getElementById('modal_nota').value = data.nota
                    document.getElementById('modal_precio').value = data.precio
                    document.getElementById('modal_descuento').value = data.descuento
                    document.getElementById('modal_total').value = data.total
                    document.getElementById('modal_button').textContent = 'Editar'
                    document.getElementById('modal_button').parentNode.id = data.id
                }
            });

        }
        const delete_service = a => {

            let id = a.parentNode.parentNode.id,
                div = a.parentNode.parentNode
            let  uri = '/funnel/delete_service'
            
            $.ajax({
                method: 'POST',
                url: uri,
                data: {
                    id
                },

                success: function(data){

                    if (data.status == 200) {
                        div.remove()
                        alertify.success(data.message)
                    }
                    else {
                        alertify.error(data.message)
                    }
                }
            });

        }
        const operation = (t) => {
            
            let id = t.parentNode.id !== '' ? t.parentNode.id : 0

            let service = document.getElementById('modal_service').value
            let description = tinymce.get('modal_description').getContent()
            // let nota = document.getElementById('modal_nota').value
            let precio = document.getElementById('modal_precio').value
            let descuento = document.getElementById('modal_descuento').value
            let total = document.getElementById('modal_total').value
            let  uri = ''

            let data = {
                service,
                description,
                // nota,
                precio,
                descuento,
                total
            }

            if (id !== 0) {
                data.id = id
                uri = '/funnel/update_service_cotizacion'
            }
            else {
                data.id = document.getElementById('id_cotizacion').value
                uri = '/funnel/save_cotizacion_in_edit'
            }


            $.ajax({
                method: 'POST',
                url: uri,
                data: data,
                beforeSend: function(){
                    $(".alerta_add_service").html('<p class="text-info">Agregando servicio ...</p>');
                },
                success: function(data){

                    if (id !== 0) {
                        let div = document.getElementById(`${id}`)
                        // div.innerHTML = data 
                        $(`#${id}`).replaceWith(data)
                        alertify.success('Se actualizo correctamente')

                    }
                    else {
                        $('#service').append(data)
                        alertify.success('Se agrego correctamente')
                    }

                    $('#addservicio').modal('hide')
                    limpiar()

                }

            });


        }
       /* const update_cotizacion  = () => {
            let id = document.getElementById('id_cotizacion').value
            $.ajax({
                method: 'POST',
                url: 'update_cotizacion',
                data: {
                    id
                },

                success: function(data){

                    if (data.status !== 200) {
                        alertify.error(data.message)
                    }
                    alertify.success(data.message)

                }
            });
        }*/

    const limpiar = () => {
         document.getElementById('modal_service').value = ''
        tinymce.get('modal_description').setContent('');
        //  document.getElementById('modal_nota').value = ''
       
        document.getElementById('modal_precio').value = ''
        document.getElementById('modal_descuento').value = ''
        document.getElementById('modal_total').value = ''
        document.getElementById('modal_button').textContent = 'Agregar Servicio'
        document.getElementById('modal_button').parentNode.id = ''
        document.getElementById('modal_title').textContent = 'Nuevo Servicio'
    }
    $(function() {
    $('#datepicker').daterangepicker({
        singleDatePicker: true,
       locale : {
           format : 'DD-MM-YYYY'
       }
        
    }, function(start) {
        // console.log(start.format('DD-MM-YYYY'))

        document.getElementById('datepicker').value = start.format('DD-MM-YYYY')
    });
    });

    document.getElementById('adjunto').addEventListener('change', e =>  {
        let number = e.target.value 
        document.getElementById('files').style.display = 'none'
        document.getElementById('link').style.display = 'none'
        document.getElementById('file').value = ''
        document.getElementById('link_google').value = ''
        // console.log(e.target.value)
        if (number == 1  ) {
            document.getElementById('files').style.display = 'block'
        }
        else if (number == 2){
            document.getElementById('link').style.display = 'block'
            document.getElementById('file').value = null
        }
        else {
            document.getElementById('files').style.display = 'none'
            document.getElementById('link').style.display = 'none'
        }
       
    })

      // Validar que solo se escriban letras 
      function soloLetras(e){
       key = e.keyCode || e.which;
       tecla = String.fromCharCode(key).toLowerCase();
       letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
       especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }

    }

     // Maximo de un input
    function maximo(e){
      if (e.target.value.length > 10) 
        {
          return false
        }
    }
    const getImage = event => {
    //    console.log(event.target.value)
       document.getElementById('file').value = event.target.value
    }

        document.getElementById('cliente').addEventListener('change', e => {
            let cliente = e.target.value
            if (cliente == 'otros') {
                document.getElementById('conten_new_client').style.display = 'block'
                document.getElementById('ruc').disabled = false
            }
            else {
                console.log(cliente)
                document.getElementById('ruc').disabled = true
                document.getElementById('conten_new_client').style.display = 'none'
                $.ajax({
                    method: 'POST',
                    url: '/funnel/cotizacion_cliente',
                    data: {
                        id : cliente
                    },

                    success: function(data){

                        document.getElementById('ruc').value = data.ruc
                        document.getElementById('id_cliente').value = data.id
                    }
                });
            }

        })


    </script>
@endsection
<!doctype html>
<html lang="es">
  <head>
    <title>Alerta</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">

	<meta  http-equiv="refresh" content="32"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favi-ticket/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favi-ticket//apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favi-ticket//apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favi-ticket//apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favi-ticket//apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favi-ticket//apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favi-ticket//apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favi-ticket//apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favi-ticket//apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('favi-ticket//android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favi-ticket//favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favi-ticket//favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favi-ticket//favicon-16x16.png')}}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
  <link rel="manifest" href="{{asset('favi-ticket//manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 
    <link rel="stylesheet" href="{{ asset('css/alertas.css')}}">
  </head>
  <body onload="mueveReloj()">
  <header>
      <div class="container">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-2 col-sm-2 col-6 responsi">
              <img src="{{ asset('img/logo.png') }}" alt="">
            </div>
            <div class="col-6 col-sm-9 text-right align-middle time">
              <span class="fecha">{{ date('D')." ".date('d')." de ".date('F').", " }}
                <form name="form_reloj" style="display: inline;"> 
                <input type="text" name="reloj" size="10" onfocus="window.document.form_reloj.reloj.blur()" style="background-color: transparent;border: none;color: white"> 
              </form>         
            </span>
              <a class="btn" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
            </div>
          </div>
        </div>
      </div>
  </header>
  
<div id="alertas_all">
<!--   <div class="menu box despegable">
    <ul class="nav flex-column" style="cursor: pointer;">
        <li class="nav-item text-center">
        <a class="nav-link respo-boton" id="m1" style="">
        <i class="fa fa-bars" aria-hidden="true"></i></a>
      </li>
    </ul>
    <ul class="nav flex-column contenido" id="myDIV">
      @if(\Auth::user()->tipo != 'colaborador')
      <li class="nav-item text-center">
        <a class="nav-link" id="m1" href="/historial"><i class="fa fa-list-ol" aria-hidden="true"></i><span>Historial</span></a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link" id="m2" href="/crear_ticket"><i class="fa fa-plus" aria-hidden="true"></i><span>Crear</span></a>
      </li>

      <li class="nav-item text-center">
        <a class="nav-link" id="m4" href="/crear_reunion"><i class="fa fa-users" aria-hidden="true"></i><span>Reunión</span></a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link" id="m9" href="/ticketblank"><i class="fa fa-clipboard" aria-hidden="true"></i><span>Completar</span></a>
      </li>
      @endif
      @if(\Auth::user()->area_id == 6)
      <li class="nav-item text-center">
        <a class="nav-link" id="m5" href="/crear_documento"><i class="fa fa-file" aria-hidden="true"></i><span>Justificación</span></a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link" id="m6" href="/documentos"><i class="fa fa-copy" aria-hidden="true"></i><span>Documentos</span></a>
      </li>
      @endif
      <li class="nav-item text-center">
        <a class="nav-link" id="m3" href="/asistencia" data-toggle="tooltip" data-placement="right" title="Asistencia"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Asistencia</span></a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link" id="m7" href="/informes" data-toggle="tooltip" data-placement="right" title="Informes"><i class="fa fa-file" aria-hidden="true"></i><span>Informes</span></a>
      </li>
      <li class="nav-item text-center">
        <a class="nav-link" id="m8" href="https://bravo.equipowik.com/" target="_blank" data-toggle="tooltip" data-placement="right" title="Programa Bravo"><i class="fa fa-star" aria-hidden="true"></i><span>Bravo</span></a>
      </li>
    </ul>
  </div> -->
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center text-md-right">
        <h2 class="parpadea" style="padding: 10px">META: 49%  :D</h2>
      </div>
    </div>
  </div>
  <div class="container">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
    </ol>
    <div class="carousel-inner" id="alertas">
      @for($a = 1;$a<=6;$a++)
      <div class="carousel-item @if($a == 01) active @endif">
        <div class="container-fluid">
          <div class="row">
              <?php 
              if($a == 1){
                $tickets = $tickets_1;
                $titulo = 'PROGRAMACIÓN';
              }elseif($a == 2){
                $tickets = $tickets_2;
                $titulo = 'DISEÑO';
              }elseif($a == 3){
                $tickets = $tickets_3;
                $titulo = 'CREATIVIDAD';
              }elseif($a == 4){
                $tickets = $tickets_4;
                $titulo = 'AUDIOVISUAL';
  	    }elseif($a == 5){
                $tickets = $tickets_7;
                $titulo = 'SOCIAL MEDIA';
              }elseif($a == 6){
  	    	$tickets = $tickets_5;
  		$titulo = 'CUENTAS';
              }
              ?>
              <div class="col-lg-12">
                <h4>{{$titulo}}</h4>
              </div>
              @foreach($tickets as $ticket)
                
                <div class="col-md-4 col-lg-4 col-xl-3 col-sm-6 cuadro">
                  <div class="cardx 
                @if($ticket->estado == 0 ) bg-light
                @elseif($ticket->estado == 1 ) bg-info
                @elseif($ticket->estado == 2 ) bg-warning
                @elseif($ticket->estado == 3 ) bg-danger
                @elseif($ticket->estado == 4 ) bg-success
                @elseif($ticket->estado == 5 ) bg-primary
                @elseif($ticket->estado == 10 ) bg-morado
                @endif" >
                    <div class="card-block">
                      <h2>{{$ticket->nombre}}</h2>
                      <p class="cliente">[{{$ticket->cliente->nombre}}]</p>
                      <p class="nombre">{{$ticket->user->name}}</p>
                      <p class="fecha">INICIO : <?php echo date('d/m H:i', strtotime($ticket->fecha_inicio));?> <br>
                                      FIN : <?php echo date('d/m H:i', strtotime($ticket->fecha_fin));?></p>
                    </div>
                  </div>
                </div>
              @endforeach
          </div>
        </div>
      </div>
      @endfor
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
    </div>
  </div>
</div>
  <div id="colores" class="container">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <a href="">Regresar al Dashboard</a>
          <ul class="indicaciones">
            <li><span class="azul"></span>En Proceso</li>
            <li><span class="rojo"></span>Atrasado</li>
            <li><span class="verde"></span>Terminado</li>
          </ul>
          
        </div>
      </div>
    </div>
  </div>
  <section id="footer">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
             Copyright 2018 © Powered by <a href="https://www.mediaimpact.pe/" target="_blank">Media Impact</a>
          </div>
        </div>
      </div>
    </div>
  </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.js"></script>
    <script>
    $('.carousel').carousel({
      interval: 8000
      // interval: 30000
    })
    </script>
    <script>
  function mueveReloj(){ 
      momentoActual = new Date() 
      hora = momentoActual.getHours() 
      minuto = momentoActual.getMinutes() 
      segundo = momentoActual.getSeconds() 

      str_segundo = new String (segundo) 
      if (str_segundo.length == 1) 
          segundo = "0" + segundo 

      str_minuto = new String (minuto) 
      if (str_minuto.length == 1) 
          minuto = "0" + minuto 

      str_hora = new String (hora) 
      if (str_hora.length == 1) 
          hora = "0" + hora 

      horaImprimible = hora + " : " + minuto + " : " + segundo 

      document.form_reloj.reloj.value = horaImprimible 

      setTimeout("mueveReloj()",1000) 
  } 
  </script>       
    </script>    
    <style>
/*    .carousel-control-next-icon, .carousel-control-prev-icon{
      background: #263238;
    }*/
    .carousel-indicators{
      display: none !important;
    }
      .carousel-control-next-icon,.carousel-control-prev-icon{
        background-color: rgba(0,0,0,.7);
        border-radius: 20px;
      }

    @media(max-width: 767px){
      .carousel-control-next-icon{
        position: absolute;
        right: -10px;
      }

      .carousel-control-prev-icon{
        position: absolute;
        left: -10px;
      }
    }    
    @media(min-width: 768px){
      .carousel-control-next-icon{
        position: absolute;
        right: -15px;
      }

      .carousel-control-prev-icon{
        position: absolute;
        left: -15px;
      }
    }
    </style>
  </body>
</html>

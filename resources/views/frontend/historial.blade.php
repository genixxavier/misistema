@extends('layout.app')
@section('titulo','Bienvenid@ a Media Impact')
@section('css')
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('content')

<style>
@media (min-width: 576px) {
  iframe{
    width: 100%;
    height: 350px;
  }
}

@media (min-width: 768px) { 
  iframe{
    width: 100%;
    height: 450px;
  }
}
@media (min-width: 992px) { 
  iframe{
    width: 100%;
    height: 550px;
  }
}

@media (min-width: 1200px) { 
  iframe{
    width: 100%;
    height: 600px;
  }
}

@media (max-width: 768px) {
  .align_text {
    text-align: center;
  }

}
</style>

<div class="col main-content">
  
	<div class="row">
    <div class="col-md-12">
      <div class="row justify-content-md-center box-icons-options">
       <div class="col-12">
       <div class="slider responsive">
         <div class="text-center">
          <a class="nav-link" id="m8" href="{{route('clientes')}}" style="background-image: url({{asset('img/botones_historial/clientes.jpg')}});">
            <div class="slick_contenido">
              <i class="fas fa-user-tie"></i>
              <div class="texto">
                <span>Clientes</span>
                <span class="texto__descripcion">Agenda reuniones con tus clientes o miembros de tribu</span>
                {{-- Agenda reuniones con tus clientes o miembros de tribu --}}
              </div>
            </div>
          </a>
        </div>
        <div class="text-center">
          <a class="nav-link" id="m4" href="{{route('reuniones')}}" style="background-image: url({{asset('img/botones_historial/reunion.jpg')}});">
            <div class="slick_contenido">
                <i class="fa fa-users" aria-hidden="true"></i>
              <div class="texto">
                <span>Reunión</span>
                <span class="texto__descripcion">Agenda reuniones con tus clientes o miembros de tribu</span> 
                 {{-- Agenda reuniones con tus clientes o miembros de tribu --}}
               </div>
            </div>
          </a>
        </div>
        @if (\Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente')
        <div class="text-center">
          <a class="nav-link" id="m3" href="/funnel/contact_report" style="background-image: url({{asset('img/botones_historial/contact_cotizacion.jpg')}});">
            <div class="slick_contenido">
                <i class="fas fa-funnel-dollar"></i>
              <div class="texto">
                <span>Contact | Cotización </span>
                <span class="texto__descripcion">Ingresa los acuerdos realizados con tus clientes</span>
                {{-- Ingresa los acuerdos realizados con tus clientes --}}
              </div>
            </div>
          </a>
        </div>
      @endif
      <div class="text-center">
        <a class="nav-link" id="m7" href="/trello" style="background-image: url({{asset('img/trello.jpg')}});">
          <div class="slick_contenido">
            <i class="fa fa-file" aria-hidden="true"></i>
            <div class="texto">
              <span>Listado de Trello</span>
              <span class="texto__descripcion">Listado de usuarios, tickets y terllo</span>
            </div>
          </div>
        </a>
      </div>
        {{-- <div class="text-center">
            <a class="nav-link" id="m9" href="/calendario" style="background-image: url({{asset('img/botones_historial/_tickets.jpg')}});">
              <div class="slick_contenido">
                <img src="{{asset('img/tickets_50.png')}}" width="50px" height="46px" style="display: block;margin: 0 auto;" alt="MI Tickets" class="slick__contenido__img">
                <div class="texto">
                  <span>Tickets 2.0</span>
                  <span class="texto__descripcion">Completa tus tickets para ser el ganador del mes</span> --}}
                  {{-- y darle puntos a tu tribu --}}
              {{-- </div>
              </div>
            </a>
          </div> --}}

        {{-- @if(\Auth::user()->area_id == 6 || \Auth::user()->tipo == 'gerente' )
          <div class="text-center">
            <a class="nav-link" id="m3" href="/asistencia" style="background-image: url({{asset('img/botones_historial/check_in_out.jpg')}});;background-size: 100%;background-position-y: 25%;">
              <div class="slick_contenido">
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <div class="texto">
                  <span>Check In / Out</span>
                  <span class="texto__descripcion">No olvides de marcar tu entrada y tu salida</span>
                </div>
              </div>
            </a>
          </div>
        @else
          <div class="text-center">
            <a class="nav-link" id="m3" href="/asistencia" style="background-image: url({{asset('img/botones_historial/check_in_out.jpg')}});background-size: 100%;background-position-y: 25%;">
              <div class="slick_contenido">
                <i class="fa fa-clock-o" aria-hidden="true"></i>
                <div class="texto">
                  <span>Asistencia</span>
                  <span class="texto__descripcion">No olvides de marcar tu entrada y tu salida</span>
                </div>
              </div>
            </a>
          </div>
        @endif --}}
          {{-- <div class="text-center">
            <a class="nav-link" id="m8" href="https://rockstar.equipowik.com/login" style="background-image: url({{asset('img/botones_historial/rockstar.jpg')}});">
              <div class="slick_contenido">
                <img src="{{asset('img/rockstar_50.png')}}" width="50px" height="46px" style="display: block;margin: 0 auto" alt="rockstar" class="slick__contenido__img">
                <div class="texto">
                  <span>MI Rockstar</span>
                  <span class="texto__descripcion">Cada día tenemos muchas responsabilidades</span> --}}
                  {{-- Cada día tenemos muchas responsabilidades, pero son esas acciones adicionales las que nos permiten ser mejores y todos debemos reconocerlas --}}
                {{-- </div>
              </div>
            </a>
          </div> --}}
        </div>
      </div>

      {{-- aqui van los grandes --}}
      <div class="col-12">
        <div class="slider responsive2">
          <div class="text-center">
            <a class="nav-link" id="m8" target="_blank"  href="https://docs.google.com/document/d/1N49WXta1ziJ4vywJyUkFHLALF0sYlvfKGxRW4AxC6aU/edit" style="background-image: url({{asset('img/botones_historial/reglas.jpg')}});">
              <div class="slick_contenido">
                <i class="fas fa-align-justify" aria-hidden="true"></i>
                <div class="texto">
                  <span>Reglamento</span>
                  <span class="texto__descripcion">Las reglas para que tengamos un buen ambiente laboral</span>
                  {{-- Las reglas para que tengamos un buen ambiente laboral --}}
                </div>
              </div>
            </a>
          </div>
          <div class="text-center">
            <a class="nav-link" id="m7" href="/informes" style="background-image: url({{asset('img/botones_historial/historial.jpg')}});">
              <div class="slick_contenido">
                <i class="fa fa-file" aria-hidden="true"></i>
                <div class="texto">
                  <span>MI Historial</span>
                  <span class="texto__descripcion">Reportes de tu asistencia, tickets y desempeño del mes</span>
                </div>
              </div>
            </a>
          </div>
        {{-- @if(\Auth::user()->tipo != 'colaborador')
          <div class="text-center">
            <a class="nav-link" id="m4" href="{{route('reuniones')}}" style="background-image: url({{asset('img/botones_historial/reunion.jpg')}});">
              <div class="slick_contenido">
                  <i class="fa fa-users" aria-hidden="true"></i>
                <div class="texto">
                  <span>Reunión</span>
                  <span class="texto__descripcion">Agenda reuniones con tus clientes o miembros de tribu</span>  --}}
                   {{-- Agenda reuniones con tus clientes o miembros de tribu --}}
                 {{-- </div>
              </div>
            </a>
          </div> 
        @endif --}}
    
          {{-- <div class="text-center">
            <a class="nav-link" id="m8" href="{{route('clientes')}}" style="background-image: url({{asset('img/botones_historial/clientes.jpg')}});">
              <div class="slick_contenido">
                <i class="fas fa-user-tie"></i>
                <div class="texto">
                  <span>Clientes</span>
                  <span class="texto__descripcion">Agenda reuniones con tus clientes o miembros de tribu</span> --}}
                  {{-- Agenda reuniones con tus clientes o miembros de tribu --}}
                {{-- </div>
              </div>
            </a>
          </div> --}}
          {{-- <div class="text-center">
              <a class="nav-link" id="m8" href="{{route('reporte_horas')}}" style="background-image: url({{asset('img/botones_historial/reporte.png')}});">
                <div class="slick_contenido">
                  <i class="fas fa-clipboard-list"></i>
                  <div class="texto">
                    <span>Reporte</span>
                    <span class="texto__descripcion">Horas que ha trabajado el usuario con los clientes</span> --}}
                    {{-- Reporte de las horas que ha trabajado el usuario con los clientes --}}
                  {{-- </div>
                </div>
              </a>
            </div>        
        </div>
      </div> --}}

      {{-- <div class="col-12">
        <div class="slider responsive2_2">         
          <div class="text-center">
            <a class="nav-link" id="m8" href="{{route('usuarios')}}" style="background-image: url({{asset('img/botones_historial/Mis_Datos.jpg')}});">
              <div class="slick_contenido">
                <i class="fas fa-user"></i>
                <div class="texto"> --}}
                  {{-- <span>Actualizar datos</span> --}}
                  {{-- <span>MIs datos</span>
                  <span class="texto__descripcion">Actualizar datos</span> --}}
                  {{-- Actualizar datos --}}
                {{-- </div>
              </div>
            </a>
          </div> --}}
          {{-- <div class="text-center">
            <a class="nav-link" id="m7" href="/trello" style="background-image: url({{asset('img/trello.jpg')}});">
              <div class="slick_contenido">
                <i class="fa fa-file" aria-hidden="true"></i>
                <div class="texto">
                  <span>Listado de Trello</span>
                  <span class="texto__descripcion">Listado de usuarios, tickets y terllo</span>
                </div>
              </div>
            </a>
          </div> --}}

          {{-- <div class="text-center">
            <a class="nav-link" id="m8" href="{{asset('podio/5')}}" target="_blank" style="background-image: url({{asset('img/botones_historial/media_bros.jpg')}});">
              <div class="slick_contenido">
                <img src="{{asset('img/icono-mb.png')}}" alt="" style="width: 41px;display: block;margin: 0 auto;" class="slick__contenido__img">
                <div class="texto">
                  <span>Media Bros</span>
                  <span class="texto__descripcion">Completa tus tickets y podrás ganar premios increíbles</span> --}}
                  {{-- Llega temprano, completa tus tickets y podrás ayudar a tu tribu a ganar premios increíbles --}}
                {{-- </div>
              </div>
            </a>
          </div>  --}}
{{-- <a class="nav-link" id="m8" href="{{route('horas_recompensadas')}}"  ><i class="fas fa-user-check"></i><span>Horas recompensadas</span></a> --}}
          {{-- <div class="text-center">
            <a class="nav-link" id="m8" href="{{route('horas_recompensadas')}}" style="background-image: url({{asset('img/botones_historial/horas_recompensadas.jpg')}});">
              <div class="slick_contenido">
                <i class="fas fa-user-check"></i>
                <div class="texto">
                  <span>Horas recompensadas</span>
                  <span class="texto__descripcion">Contador horas recompensadas</span> --}}
                  {{-- Contador horas recompensadas --}}
                {{-- </div>
              </div>
            </a>
          </div> --}}
{{-- <a class="nav-link" id="m8"   target="_blank"  href="https://docs.google.com/document/d/1N49WXta1ziJ4vywJyUkFHLALF0sYlvfKGxRW4AxC6aU/edit"><i class="fas fa-align-justify" aria-hidden="true"></i><span>Reglamento</span></a>           --}}
          {{-- <div class="text-center">
            <a class="nav-link" id="m8" target="_blank"  href="https://docs.google.com/document/d/1N49WXta1ziJ4vywJyUkFHLALF0sYlvfKGxRW4AxC6aU/edit" style="background-image: url({{asset('img/botones_historial/reglas.jpg')}});">
              <div class="slick_contenido">
                <i class="fas fa-align-justify" aria-hidden="true"></i>
                <div class="texto">
                  <span>Reglamento</span>
                  <span class="texto__descripcion">Las reglas para que tengamos un buen ambiente laboral</span> --}}
                  {{-- Las reglas para que tengamos un buen ambiente laboral --}}
                {{-- </div>
              </div>
            </a>
          </div> --}}

        </div>
      </div>

{{-- originales --}}
        {{-- <div class="col-md-3 text-center">
          <a class="nav-link" id="m9" href="/calendario" ><img src="{{asset('img/tickets_50.png')}}" width="50px" height="46px" style="display: block;margin: 0 auto" alt="MI Tickets"><span>Tickets 2.0</span></a>
        </div>
        @if(\Auth::user()->tipo != 'colaborador')
        <div class="col-md-3 text-center">
          <a class="nav-link" id="m4" href="{{route('reuniones')}}"><i class="fa fa-users" aria-hidden="true"></i><span>Reunión</span></a>
        </div>
        @endif
        @if (\Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente')
        <div class="col-md-3 text-center">
          <a class="nav-link" id="m3" href="/funnel/contact_report"><i class="fas fa-funnel-dollar"></i><span>Contact | Cotización </span></a>
      </div>
        @endif     
      @if(\Auth::user()->area_id == 6 || \Auth::user()->tipo == 'gerente' )
        <div class="col-md-3 text-center">
          <a class="nav-link" id="m3" href="/asistencia"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Check In / Out</span></a>
        </div>
      @else 
        <div class="col-md-3 text-center">
          <a class="nav-link" id="m3" href="/asistencia"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Asistencia</span></a>
        </div>
      @endif
        @if(\Auth::user()->area_id !== 6)
        @endif      
        <div class="col-md-3 text-center">
          <a class="nav-link" id="m7" href="/informes"><i class="fa fa-file" aria-hidden="true"></i><span>MI Historial</span></a>
        </div>
        <div class="col-md-3 text-center col-md-offset-3">
             <a class="nav-link" id="m8" href="{{asset('podio/5')}}" target="_blank">
             <img src="{{asset('img/icono-mb.png')}}" alt="" style="width: 41px;display: block;margin: 0 auto;">
              <span>Media Bros</span></a>
        </div>
        <div class="col-md-3 text-center">
             <a class="nav-link" id="m8" href="https://rockstar.equipowik.com/login" target="_blank"><img src="{{asset('img/rockstar_50.png')}}" width="50px" height="46px" style="display: block;margin: 0 auto" alt="rockstar"><span>MI Rockstar</span></a>
        </div>

        <div class="col-md-3 text-center">
          <a class="nav-link" id="m8"  target="_blank"   href="https://docs.google.com/spreadsheets/d/1TCNMbjylTNd4ojuR1oAvvyK1KDQfxcQJiHWzCSSoL7U/edit?usp=asogm&ouid=116965352010057924204"><i class="fas fa-list-alt" aria-hidden="true"></i><span>Wishlist</span></a>
        </div>
        <div class="col-md-3 text-center">
          <a class="nav-link" id="m8" target="_blank"   href="https://docs.google.com/spreadsheets/d/1SmRAQ8UgMLr1eXKCKcOSXNLh85BotdlSwnVT8u722-4/edit#gid=1891176735"><i class="fas fa-database" aria-hidden="true"></i><span>Base de Datos</span></a>
        </div>
        <div class="col-md-3 text-center">
          <a class="nav-link" id="m8"   target="_blank"  href="https://docs.google.com/document/d/1N49WXta1ziJ4vywJyUkFHLALF0sYlvfKGxRW4AxC6aU/edit"><i class="fas fa-align-justify" aria-hidden="true"></i><span>Reglamento</span></a>
        </div>
        <div class="col-md-3 text-center">
          <a class="nav-link" id="m8" href="{{route('reporte_horas')}}"     ><i class="fas fa-clipboard-list"></i><span>Reporte</span></a>
        </div>
        <div class="col-md-3 text-center">
            <a class="nav-link" id="m8" href="{{route('horas_recompensadas')}}"  ><i class="fas fa-user-check"></i><span>Horas recompensadas</span></a>
        </div>
        <div class="col-md-3 text-center">
            <a class="nav-link" id="m8" href="{{route('clientes')}}"  data-toggle="tooltip" data-placement="right" title="registro de clientes"  ><i class="fas fa-user-tie"></i><span>Clientes</span></a>
        </div>      
        <div class="col-md-3 text-center">
            <a class="nav-link" id="m8" href="{{route('usuarios')}}"  data-toggle="tooltip" data-placement="right" title="actualizar datos"  ><i class="fas fa-user"></i><span>Actualizar datos</span></a>
        </div> --}}

      </div>
    </div>
  </div>
</div>
@endsection
        {{-- <div class="col-md-3 text-center">
          <a class="nav-link" id="m8" href="{{route('horas_extras')}}"  ><i class="fas fa-hourglass-half"></i><span>Horas extras</span></a>
        </div> --}}
@section('content_extras')
    <!-- Modal -->
    <div class="modal fade" id="validar_tiket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <h4>¿Se cumplió con los requisitos?</h4>
            <form class="form-horizontal" method="POST" action="{{ route('validar',2) }}">
              {{ csrf_field() }}
              <input type="hidden" value="" name="idvalidar" id="idvalidar">
              <button class="btn btn-primary yes" >Si</button>
              <a href="javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">No estoy seguro</a>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
{{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script> --}}
  <script type="text/javascript" src="{{asset('js/slick_slider.js')}}"></script>
  <script>   
    $('#m1').addClass('active');
  </script>
  <script src="{{asset('js/slick.js')}}"></script>
@endsection
@extends('layout.app')
@section('titulo','Historial')
@section('css')
{{-- Icon Fontastic  --}}
<link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
  <link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
    <style> 
        h6{
            text-decoration: underline;
        }
        h4{
            color: #17a2b8;
        }
        a {
            color: white; 
            text-decoration: none;
        }
        a:hover {
            color: white; 
            text-decoration: none;
        }
        .mitabla {
            margin-top: 20px;

        }
        .mitabla thead tr {
            background: #0a0025;
            color: #fff;

        }
        .mitabla thead tr th {
            font-weight: 400;
            color: #fff;
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .mitabla tbody tr td {
            font-size: 14px;
        }

          .icon-btn{ 
            font-size: 0.9em;        
        }
        
        .integrantes_c {
            
            display: grid;
            grid-template-columns:   100%;
            grid-row-gap: 10px;
            text-align: center;
        }
        .acuerdos {
            text-align: right;
            position: relative;
        }
    
        @media screen and (max-width : 576px) {
            .button {
                margin-left: 50px;
            }
            .tema-responsive {
                margin-left: 20px;
            }
            .cliente-responsive {
                margin-left: 7px;
            }
        }
        .editar-tema{
            position: absolute;
            top: 0px;
            right: 50px;
            background: #28A745;
            color: #fff;
            padding: 10px 15px;
            font-size: 10px;
            z-index: 2;

        }
        .input_tema {
            display: none;
        }


 
    </style>
@endsection
@section('content')
<div class="col main-content">
    {{ Breadcrumbs::render('crear_contact_report') }}
    <div class="row">
        <div class="col-12 text-center custom-div">
            <h1>Contact Report</h1>
        </div>
        <div class=" col-12 col-md-12">
            <div class="detalle_tiket">
                @if (session('status'))
					@if(session('status') == 'exito')
				    <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-success">Contact report creado con éxito</p>
					        <div class="col-md-12">
					        	<button type="button" class="btn btn-primary btn-ok" >OK</button>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
					@else
					<div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-danger">Error: Contact report no creado</p>
					        <div class="col-md-12">
					        	<a href="javascript:void(0)" class="btn btn-primary btn-close" >Cerrar</a>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
					@endif
				@endif

                {{-- <form action="{{route('save_contact_report')}}" id="cr_register" method="POST"> --}}
                <div>
                    {{csrf_field()}}
                    <div class="form-row">
                        
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 form-group button-responsive ">
                            {{-- <label for="" class=""> Reunión:</label> --}}
                            <button type="button" class="icon-btn btn btn-info btn-custom " data-toggle="modal" data-target="#myModal " ><i class="fa fa-eye"></i> Buscar reunión registrada</button>
                            <input type="text" disabled  class="form-control mt-3" id="reunion" >
                            <input type="hidden" name="id" id="id">
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 form-group ">
                            <label for="" class=""><h6 class="mb-1">Cliente:</h6></label>
                            <input type="text"  disabled class="form-control cliente-responsive  mt-3" id="cliente" name="cliente">
                            @if ($errors->has('cliente'))
                            @foreach ($errors->get('cliente') as $error)
                                <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                            @endforeach
                            @endif
                        </div>
                        
                        <div class="col-12 mt-3" >
                            <hr>
                            <h4>Integrantes: </h4>
                        </div>
    
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-3">
                            <label for=""><h6>Media Impact:</h6></label>
                            <div class="integrantes_m" id="integrantes_m"></div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-3  " id="divAddIntC">
                            <h6 class="button"><span class="getcliente">Cliente</span>: <button id="addIntC" class="btn btn-info icon-btn button ml-3"><i class="fas fa-plus"></i></button></h6>
                            @if ($errors->has('integrantes_c'))
                            @foreach ($errors->get('integrantes_c') as $error)
                            <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                                @endforeach
                            @endif 
                            <div class="integrantes_c mt-3"  id="integrantes_c" ></div>
                        </div>
                        <div class="col-md-12 mt-5"><hr></div>
                        <div class="col-md-12 ajax_loader"></div>
                        <div class="col-12 col-sm-12 form-group mt-3">
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Agregar Tema:</h4>
                                </div>
                                <div class="col-md-6">
                                    <form id="add_tema">
                                        <div class="int-cliente input-group">
                                            <input type="text" id="data_tema" class="form-control" required>
                                            <div class="input-group-prepend">
                                                <button type="submit" class="btn btn-info input-group-text">Agregar</button>
                                            </div>
                                        </div>
                                        <p class="alerta_add"></p>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="col-lg-12"><hr></div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-4 ">
                            <h6 class="button">Media Impact: <span class="btn btn-info ml-3" id="addAcuerdosM"> <i class="fas fa-plus"></i></span></h6>
                            @if($errors->has('acuerdosM'))
                                @foreach ($errors->get('acuerdosM') as $error)
                                <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                                @endforeach
                                @endif
                            <div class="integrantes_c mt-4" id="acuerdos_m"></div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-4" id="AddAcuerdoIntC">
                            <h6 class="button"><span class="getcliente">Cliente</span>:  <span class="btn btn-info ml-3 " id="addAcuerdosC"> <i class="fas fa-plus"></i></span></h6>
                            @if($errors->has('acuerdosC'))
                                @foreach ($errors->get('acuerdosC') as $error)
                                <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                                @endforeach
                            @endif
                            <div class="integrantes_c mt-4" id="acuerdos_c"></div>
                        </div>-->

                        <div class="col-12 mt-3 text-center buttons">
                            <hr>
                            <a href="javascript:void(0)" id="add_contact_report" class="btn btn-primary btn-custom mr-5"> Crear </a>
                            <a class="btn btn-primary ml-5" id="cancelar" href="{{route('contact_report_index')}}">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   
@endsection
@section('content_extras')    
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reuniones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body center">
                <table class="table table-striped table-bordered mitabla " id="dataTable" > 
                    <thead> 
                        <tr> 
                            <th> Opciones </th>
                            <th>Nombre </th>
                            <th>Cliente </th>
                            <th> Fecha</th>
                
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($lis_idreunion as $item)
                            
                        <tr> 
                            <td> 
                            <button class="btn btn-primary icon-btn btn-custom" onclick="buscarParticipantes('{{ $item->id}}','{{$item->nombre_reunion}}','{{ $item->nombre }}')"> <i class="fas fa-plus"></i> </button>
                            </td>
                            <td>
                                {{ $item['nombre_reunion']}}
                            </td>
                            <td> 
                                {{ $item['nombre'] }}
                            </td>
                            <td>
                                {{ $item['fecha_inicio']}}
                            </td>
                            

                        </tr>
                        
                        @endforeach 
                

                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Tema</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="form_edit_tema">
                    <div class="form-group">
                        <label for="">
                            Tema
                        </label>
                        <input type="text" class="form-control"  id="input_tema_edit">
                        <input type="hidden" class="hidden-edit-id" id="hidden_edit_id" >
                    </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary btn-custom">Actualizar</button>
                </div>
                </form>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_acuerdo_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Acuerdo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="form_edit_acuerdo">
                    <div class="form-group">
                        <label for="">
                            Acuerdo:
                        </label>
                        <textarea name="" id="input_acuerdo_edit" cols="50" rows="3" class="form-control"></textarea>
                        <input type="hidden" class="hidden-edit-id" id="hidden_acuerdo_edit_id" >
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
<script>
    $.ajaxSetup({
          headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
    })

          const addIntC  = document.getElementById('addIntC')

            // ============== AGREGAR INTEGRANTES DEL CLIENTE ================

            addIntC.addEventListener('click' , (e) => {
                e.preventDefault()
                $("#integrantes_c").append('<div class="int-cliente input-group">'
                +'<input type="text" name="integrantes_c" class="form-control h32" >'
                    +'<div class="input-group-prepend">'
                        +'<div class="btn bg-danger2 input-group-text deleteic">X</div>'
                    +'</div>'
                +'</div>');

               
            })

            // Remover integrantes clientes 
            $('body').on('click',".deleteic",function(){
                $(this).parent().parent().remove();
            });
            function removeIntCliente (e, name) {       
                e.preventDefault()
                name.parentElement.remove()
            }
            

            $(document).ready( function () {
                $('#dataTable').DataTable({
                    language: {
                        "decimal": "",
                        "emptyTable": "No hay información",
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                        "infoPostFix": "",
                        "thousands": ",",
                        "lengthMenu": "Mostrar _MENU_ Entradas",
                        "loadingRecords": "Cargando...",
                        "processing": "Procesando...",
                        "search": "Buscar:",
                        "zeroRecords": "Sin resultados encontrados",
                        "paginate": {
                            "first": "Primero",
                            "last": "Ultimo",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        }
                    }
                } );
             })
            // ========== ELIMINAR INTEGRANTES DEL CLIENTE ===================



           
         



            // ============= MODAL ====================

            function  buscarParticipantes(id,reunion,cliente) {
                console.log(id,reunion,cliente)
                    $.ajaxSetup({
                        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
                    })

                    $.ajax({
                        url : 'viewReunion',
                        method : 'POST', 
                        data : { 'id' : id },
                        success : function (res) {
                            let user_create = res.length - 1 ;
                     
                            function removeAllChilds(a)
                            {
                                var a=document.getElementById(a);
                                while(a.hasChildNodes())
                                    a.removeChild(a.firstChild);	
                            }
                            if (integrantes_m.childElementCount > 0) {
                                removeAllChilds('integrantes_m')
                        
                            }    
                        
                            $("#integrantes_m").append('<p><i class="fas fa-circle"></i> '+res[user_create]+'</p>');
                            for(i=0;i< (res.length-1) ;i++){
                                $("#integrantes_m").append('<p><i class="fas fa-circle"></i> '+res[i][0].name+'</p>');
                            }

                         

                            document.getElementById('reunion').value= reunion
                            document.getElementById('cliente').value = cliente
                            document.getElementById('id').value = id
                            $(".getcliente").html(cliente);
                            $('#myModal').modal('hide');
                        }
                    })
         } 
            // =============  END MODAL ====================

        
    $(document).ready(function(){
        $("#alerta_popup").modal("show");
        $(".btn-close").click(function(){
            $("#alerta_popup").modal("hide");
        });
        $(".btn-ok").click(function(){
            location.href ="/funnel/contact_report";
        });
        $("#add_contact_report").click(function(){
            var myArray = [];
            $( ".int-cliente input[name=integrantes_c]" ).each(function( index ) {
                myArray.push($(this).val());
            });
            var myArray2 = [];
            $( ".items_temas input[name=data_get_tema_id]" ).each(function( index ) {
                myArray2.push($(this).val());
            });
            data = {
                'integrantes_c': myArray,
                'temas': myArray2,
                'id': $("#id").val()
            };

            console.log(data);
            $.ajax({
                method: 'POST',
                url: 'save_contact_report',
                data: data,
                beforeSend: function(){
                    console.log("procesando");
                  
                },
                success: function(data){
                    location.reload();
             
                }
            });
        });

        $("#add_tema").submit(function(e){
            e.preventDefault();
            data = {'tema': $("#data_tema").val(), 'type': 'N'}
            $.ajax({
                method: 'POST',
                url: 'add_tema',
                data: data,
                beforeSend: function(){
                    $(".alerta_add").html('<span class="text-info">Agregando tema ...</span>');
                },
                success: function(data){
                    $(".ajax_loader").append(data);
                    $("#add_tema")[0].reset();
                    $(".alerta_add").html('<span class="text-success">Éxito ...</span>');
                    setTimeout(function(){
                        $(".alerta_add").html("");
                    },1000);
                }
            });
        });

        $(document).on('submit',".form_add",function(e){
            console.log("entro")
            e.preventDefault();
            var data = $(this).serialize();
            var data2 = $(this).serializeArray();
            let self = this;
            var div;
            let it = data2[3].value;
            if(data2[2].value == 1){
                var tt = 'mi';
            }
            else{
                var tt = 'cl';
            }
            div = $('.alerta_acuerdo_'+tt+'_'+it);
            $.ajax({
                method: 'POST',
                url: 'add_tema_acuerdo',
                data: data,
                beforeSend: function(){
                    div.html('<span class="text-info">Agregando acuerdo ...</span>');
                },
                success: function(data){
                    let id = data.id_tema;
                    let t = data.type;
                    if( t == 1){
                        var type = 'mi';
                    }
                    else{
                        var type = 'cl';
                    }
                    $("#acuerdos_"+type+"_"+id).append('<div class="acuerdos">'
                        +'<p id="acuerdo_description_'+data.id+'">'+data.description+'</p>'
                        +'<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_acuerdo_edit"  class="btn btn-success icon-btn ml-2  mr-4" data-id="'+data.id+'" onclick="editAcuerdo('+ data.id +')" ><i class="fas fa-edit"></i></a>'
                        +'<a href="javascript:void(0)" class="btn btn-danger icon-btn ml-2 delete_acuerdo" data-id="'+data.id+'">X</a>'
                        +'</div>');
                    $(self)[0].reset();
                    div.html('<span class="text-success">Éxito</span>');
                    setTimeout(function(){
                        div.html("");
                    },1000);
                }
            });
        });

        $(document).on('click','.delete_acuerdo',function(){
            let id = $(this).attr('data-id');
            var self = this;
            $.confirm({
                icon: 'fa fa-warning',
				type: 'red',
                title: 'Alerta',
                content: 'Desea eliminar acuerdo?',
                buttons: {
                    Si: function () {
                        data = {'id': id }
                        $.ajax({
                            method: 'POST',
                            url: 'delete_acuerdo_item',
                            data: data,
                            // beforeSend: function(){
                            //     $(".alerta_add").html('<span class="text-info">Agregando tema ...</span>');
                            // },
                            success: function(data){
                                if(data == 1){
                                    $(self).parent().remove();
                                    $.alert({
                                        type: 'green',
                                        title: 'Éxito',
                                        content: 'Item acuerdo eliminado',
                                    });
                                }
                                else{
                                    $.alert({
                                        icon: 'fa fa-warning',
                                        type: 'red',
                                        title: 'Error',
                                        content: 'Item acuerdo no eliminado',
                                    });
                                }
                            }
                        });
                    },
                    No: function () {
                    }
                }
            });
        });

        $(document).on('click','.eliminar_tema',function(){
            let id = $(this).attr('data-id');
            var self = this;
            $.confirm({
                icon: 'fa fa-warning',
				type: 'red',
                title: 'Alerta',
                content: 'Desea eliminar acuerdo?',
                buttons: {
                    Si: function () {
                        data = {'id': id }
                        $.ajax({
                            method: 'POST',
                            url: 'eliminar_tema',
                            data: data,
                            success: function(data){
                                if(data == 1){
                                    $(self).parent().remove();
                                    $.alert({
                                        type: 'green',
                                        title: 'Éxito',
                                        content: 'Tema eliminado',
                                    });
                                }
                                else{
                                    $.alert({
                                        icon: 'fa fa-warning',
                                        type: 'red',
                                        title: 'Error',
                                        content: 'Tema no eliminado',
                                    });
                                }
                            }
                        });
                    },
                    No: function () {
                    }
                }
            });
        });
    });

      //ASSETS
    const findElement = id => {
        let element = document.getElementById(`${id}`)
        return element
    }

            //MY FUNCTIONS
    const showInputTema = id => {

        let title = `tema-title-${id}`
        let element = findElement(`${title}`)
        let id_form = `hidden_edit_id`
        let form_hidden = findElement(`${id_form}`)
        form_hidden.setAttribute('value' , id)
        let id_edit = `input_tema_edit`
        let input_edit = findElement(`${id_edit}`)
        flag = element.textContent
        flag = flag.split(':')
        input_edit.value = flag[1]
    }
    const editAcuerdo = id => {

        let title = `acuerdo_description_${id}`
        let element = findElement(`${title}`)
        let id_form = `hidden_acuerdo_edit_id`
        let form_hidden = findElement(`${id_form}`)
        form_hidden.setAttribute('value' , id)
        let id_edit = `input_acuerdo_edit`
        let input_edit = findElement(`${id_edit}`)
        input_edit.value = element.textContent
    }

    // formulario edit
    let formulario = findElement('form_edit_tema')
    formulario.addEventListener('submit' , e => {
        e.preventDefault()
        let input  = e.target.input_tema_edit.value,
            id = e.target.hidden_edit_id.value
        console.log('detenido')

        $.ajax({
            method: 'POST',
            url: 'editar_tema',
            data: {
                'id' : id ,
                'text' : input
            },

            success: function(data){
                if (data.status == 200) {
                    let title = `tema-title-${id}`
                    let element = findElement(`${title}`)
                    element.textContent =`Tema : ${input} `
                    alertify.success(data.message)
                    $('#modal_edit').modal('hide')

                }
            }
        });
    })

    let formulario_acuerdo = findElement('form_edit_acuerdo')
    formulario_acuerdo.addEventListener('submit' , e => {
        e.preventDefault()
        let input  = e.target.input_acuerdo_edit.value,
            id = e.target.hidden_acuerdo_edit_id.value

        $.ajax({
            method: 'POST',
            url: 'editar_acuerdo',
            data: {
                'id' : id ,
                'text' : input
            },

            success: function(data){
                if (data.status == 200) {
                    let title = `acuerdo_description_${id}`
                    let element = findElement(`${title}`)
                    element.textContent = input
                    alertify.success(data.message)
                    $('#modal_acuerdo_edit').modal('hide')

                }
            }
        });
    })


    
  </script>
@endsection
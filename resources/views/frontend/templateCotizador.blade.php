{{-- <div class="staff-item row"  id="staffCotizador-{{$staffCotizador->id}}">
    <label for="staff" class="col-5">{{$cotizador->persona}}</label>
    <p class="col-2">{{$staffCotizador->horas}}</p>
    <p class="col-2">S/ {{$staffCotizador->total}}</p>
    <div class="delete-div col-3" style="cursor: pointer">
        <i class="fas fa-trash"></i>
    </div>

</div>
--}}
<tr class="staff-item"  id="staffCotizador-{{$staffCotizador->id}}">
    <td>
        <input type="hidden" id="listStaffCotizador" name="listStaffCotizador[]" value="{{$staffCotizador->id}}" >
        <input type="hidden" id="allTotalCotizador" name="allTotalCotizador" value="{{$notFormatMoney}}">
        {{$cotizador->persona}}
    </td>
    <td>{{$staffCotizador->horas}}</td>
    <td >S/ <span id="staff-item-total">{{$staffCotizador->total}}</span></td>
    <td> <i style="cursor: pointer" class="fas fa-trash" onclick="deleteStaffCotizador({{$staffCotizador->id}})" ></i></td>
</tr>
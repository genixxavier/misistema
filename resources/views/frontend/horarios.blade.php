@extends('layout.app')
@section('titulo','Asistencia')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="{{ asset('css/asistencia.css') }}">
@endsection
@section('content')
 
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				<div class="row">
					<div class="col-md-4">
						Trabajadores:
						<form>
							{{  csrf_field() }}
							<select  class="form-control" name="iduser" id="trabajador">
									<option></option>
								@foreach($colaboradores as $colaborador)
									<option value="{{ $colaborador->id }}">{{ $colaborador->name }}</option>
								@endforeach
							</select>
							<br>
							<a href="" id="urlh" class="btn btn-primary">Cargar Horario</a>
						</form>
					</div>
					<div class="col-md-8">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('content_extras')
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function(){
		$("#trabajador").change(function(e){
			var data= $("#trabajador").val();
			var urlg = 'horario/'+data;
			$("#urlh").attr('href',urlg);
		});

	});
	

    $('#m3').addClass('active');
</script>
@endsection
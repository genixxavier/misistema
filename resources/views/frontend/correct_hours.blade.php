@extends('layout.app')
@section('titulo','Corregir horas')
@section('css')

{{-- Icon Fontastic  --}}
<link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>

<style>
 .btn-per  {
    background-color: #263238;
    color: white;
 }
 .color-input {
   color: #263238;
   font-weight: bold;
 }
</style>
@endsection
@section('content')


<div class="container text-center">
    <div class="row">
        <div class="col-12 text-center">
            <h1>Entrada | Salida</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-4 offset-md-4 text-center mt-2 mb-4  ">
        <a class="btn btn-per btn-custom" href="{{route('history_assistance_changes')}}">Ver Historial de Cambios</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <form action="" id="users" class="form-row">
                <div class="form-group col-4 mr-2">
                    <label for="" class="color-input">Seleccioné el usuario: </label>
                    <select name="user" id="user" class="form-control">
                      <option value="0" selected>-- Usuarios --</option>
                    </select>
                </div>
                
                <div class=" form-group col-4">
                    <label for="" class="color-input ">Seleccioné la fecha  <span style="color:black"><i class="far fa-calendar-alt"></i></span></label>
                    <input id="datepicker" class="form-control" value="">
                    
                </div>
                <div class="form-group col-3">
                    <label for="" style="opacity: 0">hello</label>
                   <button class=" btn btn-primary btn-block btn-custom">Buscar</button>
                </div>

            </form>
        </div>
        <div class="col-12" id="form_to_user_change">

        </div>
    </div>
</div>
@endsection
@section('content_extras')
  

    
  

   
    
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>

    document.addEventListener('DOMContentLoaded' ,  e =>  {
        let select_to_users = document.getElementById('user') 
        // Cargamos la data al select 
        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        
                $.ajax({
                    method: 'GET',
                    url: 'find_users',
                    success: function(data){
                       
                        for (let i = 0; i < data.length ; i++ ) {
                            let option = document.createElement('option')
                            option.value = data[i].id 
                            option.text = data[i].name
                            select_to_users.add(option)
                        }
                     


                    }
        });
    })
    $(function() {
    $('#datepicker').daterangepicker({
        singleDatePicker: true,
       locale : {
           format : 'DD-MM-YYYY'
       }
        
    }, function(start) {
        // console.log(start.format('DD-MM-YYYY'))

        document.getElementById('datepicker').value = start.format('DD-MM-YYYY')
    });
    });
    const delete_cotizacion = (id) => {

        alertify.confirm("Cotización","Desea eliminar la cotización ?",
            function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    method: 'POST',
                    url: 'delete_cotizacion',
                    data: {
                        id
                    },

                    success: function(data){
                        console.log(data)
                        if (data.status !== 200) {
                            alertify.error(data.message)
                        }
                        else {

                        alertify.success(data.message)
                            location.reload()
                        }


                    }
                });


            },
            function () {
            alertify.error('Cancelado !')
            }
           )
    }
    
    // Buscar user 
    document.getElementById('users').addEventListener('submit', e => {
        e.preventDefault() 
        let id_usuario = e.target.user.value, 
            fecha = e.target.datepicker.value 
       let new_date = fecha.split('-') 
      
      fecha = `${new_date[2]}/${new_date[1]}/${new_date[0]}`
      
    //   console.log(id_usuario)
        // Buscamos al usuario 
        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    method: 'POST',
                    url: 'find_user_to_correct_hour',
                    data: {
                        id_usuario, 
                        fecha
                    },

                    success: function(data){
                         let div = document.getElementById('form_to_user_change')
                        div.innerHTML = data
                        


                    }
                });


    })
    // Update asistencia 
    // document.getElementById('update_asistencia').addEventListener('submit' , e => {
    //     e.preventDefault()
    //     let hora_inicio = e.target.hora_inicio.value, 
    //         hora_fin = e.target.hora_fin.value, 
    //         id_asistencia = e.target.id_asistencia.value, 
    //         data = {
    //             hora_fin,
    //             hora_inicio,
    //             id_asistencia
    //         }
    //         $.ajaxSetup({
    //                 headers: {
    //                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //                 }
    //             });

    //         $.ajax({
    //                 method: 'POST',
    //                 url: 'update_asistencia',
    //                 data: data,
    //                 success: function(data){

    //                     if (data.status !== 200) {
    //                         alertify.error(data.message)
    //                     }
    //                     else {

    //                     alertify.success(data.message)
    //                         location.reload()
    //                     }


    //                 }
    //             });

    // })
    // Helper Ajax
    const ajax = (request, data) => {
                return new Promise ( (resolve, reject ) => {
                    const xhr = new XMLHttpRequest () 
                    xhr.open(request.method, request.url ,true)
                    xhr.addEventListener('load' ,  e => {
                    resolve(e.target)
                    })
                    xhr.send(data)
                })
            }
    function updateAsistencia(id) {
        let hora_inicio = document.getElementById('hora_inicio').value, 
            hora_fin = document.getElementById('hora_fin').value,
            id_asistencia = id ,
            data = {
                hora_fin,
                hora_inicio,
                id_asistencia
            }
        
          
            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

            $.ajax({
                    type: 'POST',
                    url: '/update_asistencia',
                    data: data,
                    success: function(data){

                        if (data.status !== 200) {
                            alertify.error(data.message)
                        }
                        else {

                        alertify.success(data.message)
                        location.reload()
                        }


                    }
                });
    }
    $('#m22').addClass('active');
   </script>
@endsection
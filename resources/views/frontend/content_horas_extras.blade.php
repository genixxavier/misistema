<div class="col-12" style="" >
<form action=""  method="POST"   >
    {{csrf_field()}}
        <div class="form-group">
        <input type="hidden" name="month" id="month" value="{{$month}}">
        <input type="hidden" name="year" id="year" value="{{$year}}">
      
        <button type="button" class="btn btn-primary btn-custom" onclick="registrarHorasExtras({{$month}},{{$year}})"> Guardar  </button>
        </div>
    </form>
</div>
<div class="col-12">
    <table class=" table table-bordered table-responsive-lg mitabla custom-table">
        <thead class="">
        <tr>
            <th>
                Nombre
            </th>
            <th>
                Horas Extras
            </th>
            <th>
                Horas Deuda
            </th>
            <th>
                Opciones
            </th>

        </tr>

        </thead>
        <tbody>
        @foreach($query as $item)

            <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->extras}}</td>
                <td>{{$item->deuda}}</td>
                <td>

                    <div class="dropdown">
                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-cog"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"   href="{{route('horas_extras_detalle',
                            ['id' => $item->id,
                                'year' => $year,
                                'month' => $month

                            ]) }}"><i class="fas fa-eye"></i> Ver detalle</a>
                        </div>
                    </div>


                </td>
            </tr>

        @endforeach
        </tbody>

    </table>
</div>
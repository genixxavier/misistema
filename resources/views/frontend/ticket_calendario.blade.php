@extends('layout.app')
@section('titulo','Completar Tickets')

@section('css')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js">
<link href="{{asset('calendar/core/main.css')}}" rel='stylesheet' />
<link href="{{asset('calendar/daygrid/main.css')}}" rel='stylesheet' />
<link href="{{asset('calendar/timegrid/main.css')}}" rel='stylesheet' />
<link href="{{asset('calendar/list/main.css')}}" rel='stylesheet' />
<style>
	body{
		padding: 0px !important;
	}
	#wrap {
	  width: 1100px;
	  margin: 0 auto;
	}

	#external-events {
	  float: left;
	  width: 150px;
	  padding: 0 10px 15px;
	  border: 1px solid #ccc;
	  background: #eee;
	  text-align: left;
	}

	#external-events h4 {
	  font-size: 16px;
	  margin-top: 0;
	  padding-top: 1em;
	}

	#external-events .fc-event {
	  margin: 10px 0;
	  cursor: pointer;
		padding: 5px;
		border: 1px solid #c8c7c7;
	}

	#external-events p {
	  margin: 0px;
	  font-size: 11px;
	}

	#external-events p input {
	  margin: 0;
	  vertical-align: middle;
	}

	#calendar {
	  float: right;
	  width: 900px;
	}
	a.fc-time-grid-event{
		font-size: 11px;
	}
	.fc-time-grid-event .fc-time{
		font-size: 11px;
	}
	.fc-title {
	    color: #fff;
			font-size: 11px;
	}
	.fc-event, .fc-event:hover {
	    color: #fff !important;
	    text-decoration: none;
	}
	.fc-time-grid .fc-slats td {
    height: 1.5em;
}
	.loadercontent {
	  position: fixed;
	  background: #00000030;
	  top: 0;
	  left: 0;
	  right: 0;
	  bottom: 0;
	  z-index: 99999;
	  align-items: center;
	  justify-content: center;
	  display: none;
	}
	.loader {
	    background: #ffff;
	    padding: 20px;
	    width: 300px;
	    display: block;
	}
	.loader h2{
	    font-size: 20px;
	}
  .tooltip{
  	opacity: unset;
  }
  .fc-time-grid .fc-slats td, .fc-time-grid .fc-slats .fc-widget-content {
	  
	  font-size: 0.9em;
  }
</style>
@endsection
@section('content')
<div class="col main-content" id="app">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				<div class="center_div">
						@if (session('status'))
						@if(session('status') == 'Creado exitosamente')
						<div class="alert alert-success alert-dismissible fade show" role="alert">
						  <strong>Éxito,</strong> Ticket creado.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>
						@endif
						@if(session('status') == 'existe')
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
						  <strong>Error,</strong> las horas puestas en el ticket estan ocupadas por otro ticket, verificar horas del pedido.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>
						@endif
						@if(session('status') == 'excedio')
						<div class="alert alert-warning alert-dismissible fade show" role="alert">
						  <strong>Error,</strong> excedió las horas del día en el ticket.
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						    <span aria-hidden="true">&times;</span>
						  </button>
						</div>
						@endif
						@if(session('status') == 'excedio_extra')
							<div class="alert alert-warning alert-dismissible fade show" role="alert">
								<strong>Error,</strong> excedió las horas extras del día en el ticket.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								  <span aria-hidden="true">&times;</span>
								</button>
							  </div>
						@endif
						@if(session('status') == 'existe_extra')
							<div class="alert alert-danger alert-dismissible fade show" role="alert">
								<strong>Error,</strong> las horas puestas en el ticket extra estan ocupadas por otro ticket, verificar horas del pedido.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								  <span aria-hidden="true">&times;</span>
								</button>
							
							  </div>
						@endif
						@if(session('status') == 'add_extra')
							<div class="alert alert-success alert-dismissible fade show" role="alert">
								<strong>Éxito,</strong> Ticket extra creado.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								  <span aria-hidden="true">&times;</span>
								</button>
							  </div>
						@endif
						@if(session('status') == 'eliminado')
							<div class="alert alert-success alert-dismissible fade show" role="alert">
								<strong>Éxito,</strong> Ticket eliminado.
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								  <span aria-hidden="true">&times;</span>
								</button>
							</div>
						@endif
					@endif
				</div>
				<div class="row">
					<div class="col-md-6 align_button">
						{{-- <p><a href="{{ route('ticketblank') }}" class="btn btn-primary btn-custom">Atrás</a></p> --}}
					</div>
					<div class="col-md-6 text-right">
						<div class="leyenda">
						    <p><span class="alert_puntual"></span> Puntual</p>
							<p><span class="alert_muytarde"></span> Muy tarde</p>
							<p><span class="alert_vacaciones"></span> Vacaciones</p>
							<p><span class="alert_justificacion"></span> Justificado</p>
							<p><span class="alert_noexistio"></span> No existio</p>
							<input type="hidden" id='trello_id' value="{{ $id_trello }}">
							<input type="hidden" id='trello_key' value="{{ $id_key }}">
							<input type="hidden" id='trello_token' value="{{ $id_token }}">
							<input type="hidden" id='list_cards' value="{{ $list_card }}">

						</div>
					</div>
					<div class="col-md-12 b-box">
						<p class="text-center"><span class="main-title">Tickets: {{ \Auth::user()->name }}   </span></p>

						<div class="row tablahoras">

							<div>
								<div id='wrap'>
									<div id='external-events'>
										<h4>Personal Backlog </h4>
										<div id='external-events-list'>
												<div class="custom-control custom-switch">
														<input type="checkbox" class="custom-control-input" id="checkk">
														<label class="custom-control-label" for="checkk">Duplicar Tarea</label>
													  </div>
											@foreach($listTaskBack as $item)
											<div class='fc-event' data-trello="fail" id="{{$item->id}}"style="{{ $item->color }}">
												{{$item->title}}

												<p>({{$item->cliente}})</p>
												
											</div>
											@endforeach
										</div>
										<hr>
										<a href="javascipt:void(0)" class="btn btn-primary exampleModalLong">Añadir Tareasss</a>
									</div>
									<div id='calendar'></div>
									<div style='clear:both'></div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('content_extras')
<!-- preload -->
    <div class="loadercontent">
      <div class="loader">
        <h2>Cargando ...</h2>
      </div>
    </div>

<!-- Modal view -->
    <div class="modal fade show" id="modal_edit_task" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Editar  tarea</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <form id="edit_task">
                  <input type="hidden" class="form-control" name="edit_id_task">
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <label >Cliente</label>
                      <select class="form-control" name="task_cliente">
												@foreach($clientes as $cliente)
                        	<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
												@endforeach		
                      </select>
                    </div>
                    <div class="form-group col-md-12">
                      <label >Tarea</label>
                      <input type="text" class="form-control" name="edit_name_task">
                    </div>
                    <div class="form-group col-md-12">
							<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" name="isChangeTicket" id="default">
									<label class="custom-control-label" for="default">corrección</label>
								</div>
                    </div>

                    <div class="form-group col-md-6">
                      <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>

                    <div class="form-group col-md-6 text-right">
					  <a href="javascipt:void(0)" class="btn btn-danger delete_task_id" data-idtask="">Eliminar</a>
                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

		<div class="modal fade show" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Nueva Tarea</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <form id="new_task">
                  <div class="form-row">
                    <div class="form-group col-md-12">
                      <label >Cliente</label>
                      <select class="form-control" name="task_cliente" required>

                        	{{--<option value="{{$cliente->id}}" data-name="$client->nombre">{{$cliente->nombre}}</option> --}}

							  <optgroup label="Fees">
								  @foreach($clientes as $cliente)
									  @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
										  @if($cliente->type == 1)
											  <option value="{{$cliente->id}}" >{{$cliente->nombre}}</option>
										  @endif
									  @else
										  @if ($cliente->type == 1)
											  <option value="{{$cliente->id}}"> {{$cliente->nombre}}</option>
										  @endif
									  @endif
								  @endforeach
							  </optgroup>
							  <optgroup label="Spots A">
								  @foreach($clientes as $cliente)
									  @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
										  @if($cliente->type == 2   )
											  <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										  @endif
									  @else
										  @if ($cliente->type == 2)
											  <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										  @endif
									  @endif
								  @endforeach
							  </optgroup>
							  <optgroup label="Spots B">
								  @foreach($clientes as $cliente)
									  @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
										  @if($cliente->type == 3  )
											  <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										  @endif
									  @else
										  @if ($cliente->type == 3)
											  <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										  @endif
									  @endif
								  @endforeach
							  </optgroup>
							  <optgroup label="Otros">
								  @foreach($clientes as $cliente)
									  @if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
										  @if($cliente->type !== 3 && $cliente->type !== 2 && $cliente->type !== 1  )
											  <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										  @endif
									  @else
										  @if ($cliente->type !== 3 && $cliente->type !== 2 && $cliente->type !== 1)
											  <option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										  @endif
									  @endif
								  @endforeach
							  </optgroup>

                      </select>
                    </div>
                    <div class="form-group col-md-4" style="display:none">
                      <label >Duration</label>
                      <input type="text" class="form-control" name="task_duration" value="1" required>
                    </div>
                    <div class="form-group col-md-12">
                      <label >Tarea</label>
                      <input type="text" class="form-control" name="name_task" required>
                    </div>
                    <div class="form-group col-md-12">
						<!-- Default unchecked -->
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" name="isChangeTicket" id="defaultUnchecked">
						<label class="custom-control-label" for="defaultUnchecked">corrección</label>
					</div>
                    </div>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                  </div>	
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('js')

<script src="{{asset('calendar/core/main.js')}}"></script>

<script src="{{asset('calendar/interaction/main.js')}}"></script>
<script src="{{asset('calendar/daygrid/main.js')}}"></script>
<script src="{{asset('calendar/timegrid/main.js')}}"></script>
<script src="{{asset('calendar/core/locales-all.js')}}"></script>
<!-- <script src='https://unpkg.com/popper.js/dist/umd/popper.min.js'></script> -->
<script src="https://unpkg.com/tooltip.js@1.3.1/dist/umd/tooltip.min.js"></script>

<!-- <script src="{{asset('calendar/list/main.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"> </script>
<script type="text/javascript">
	$(document).ready(function(){
		
		let id_trello = document.getElementById('trello_id').value
		let trello_key = document.getElementById('trello_key').value
		let trello_token = document.getElementById('trello_token').value
		let list_cards = document.getElementById('list_cards').value
		

		axios.get(`https://api.trello.com/1/members/me/boards?key=${trello_key}&token=${trello_token}`)
		.then( response  => {
			    
				
				let divcontainer = document.getElementById('external-events-list')
				response.data.forEach( async board =>  {
				await 	axios.get(`https://api.trello.com/1/boards/${board.id}/cards/?=name&members=true&member_fields=fullName&dateLastActivity=true&key=${trello_key}&token=${trello_token}`)
					.then( response => {
						let cards = response.data
							
						cards.forEach( card => {
							console.info( list_cards.includes(card.id) );
						
							if (card.members.length > 0) {
								card.members.forEach(member => {

									if (member.id == id_trello &&  !list_cards.includes(card.id) ) {
										console.log(card)
										let div = document.createElement('div')
										div.className = 'fc-event'
										div.dataset.trello = 'ok'
										div.id = card.id
										

										div.innerHTML = `
										
										${card.name}
										 
										`

										divcontainer.appendChild(div)
									
									}
								})
							}


						} )


					} )
					

				} )


		} )

		// fetch("https://api.trello.com/1/members/me/boards?key=5f39fd32ce375d936870391efedad435&token=0ec0194b227b52871b7fb9004877a5a6dbd78d9d00e7c3f9d604c9c742168831")
		// .then(response => response.json())
		// .then(response =>  {
		
			
		// 	response.forEach( board => {

		// 	let div = document.createElement("div")
		// 	div.innerHTML = `

				
		// 	<div>
		// 		<div class="card" style="width: 18rem;">
				
		// 		<div class="card-body">
		// 			<h5 class="card-title">${board.name}</h5>
		// 			<button class="btn btn-success" onclick="addCard('${board.id}')">
		// 			ver cards
		// 			</button>
		// 			<div id="${board.id}">
		// 			</div>
		// 		</div>
		// 		</div>


		// 	` 
		// 	divContent.appendChild(div)

		// 	} )


		// })
		
		$.ajaxSetup({
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
		});

	})

	document.addEventListener('DOMContentLoaded', function(listtt=null) {
    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable

    /* initialize the external events
    -----------------------------------------------------------------*/

    var containerEl = document.getElementById('external-events-list');
    new Draggable(containerEl, {
      itemSelector: '.fc-event',
      eventData: function(eventEl) {
        return {
          title: eventEl.innerText.trim()
        }
      }
    });

    //// the individual way to do it
    // var containerEl = document.getElementById('external-events-list');
    // var eventEls = Array.prototype.slice.call(
    //   containerEl.querySelectorAll('.fc-event')
    // );
    // eventEls.forEach(function(eventEl) {
    //   new Draggable(eventEl, {
    //     eventData: {
    //       title: eventEl.innerText.trim(),
    //     }
    //   });
    // });

    /* initialize the calendar
    -----------------------------------------------------------------*/

    var calendarEl = document.getElementById('calendar');
    var calendar = new Calendar(calendarEl, {
		
      // get datos ajax
      plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list' ], //'momentTimezone',
      // locale: 'es',
      defaultView: 'timeGridWeek',
      header: {
        left: 'prev,next',
        center: 'title',
        // right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
        // right: 'timeGridWeek',
      },
		views: {
			timeGridWeek: {
				titleFormat: {  month: 'long', day: '2-digit' }
			}
		},
			slotDuration:'00:15',
			agenda: 'H:mm:ss A ',
			timeFormat: 'H(:mm)',
			slotLabelFormat: {
				hour: 'numeric',
			  minute: '2-digit',
			  omitZeroMinute: true,
			  meridiem: 'short'
			},
      allDaySlot: false,
      editable: true,
		locale: 'es',
      droppable: true, // this allows things to be dropped onto the calendar
      drop: function(arg) {
		
		 let description = arg.draggedEl.innerText
		 let checking = document.getElementById('checkk').checked
         
        const task_start = arg.dateStr
				let ends = task_start.split('T')
				let task_end = ends[0]+'T';
				ends = ends[1].split(':')
				task_end = task_end+ (parseInt(ends[0])+1)+':'+ends[1]+':00'
				if( ends[0] != '13'){
	        const task_id = arg.draggedEl.id
	        console.log(task_id)
	        var datos = {
			  'checkin' : checking,
	          'task_end': task_end,
	          'task_start': task_start,
	          'task_id': task_id,
			  'isTrello': arg.draggedEl.attributes['data-trello'].value,
			  description
	        }
	        $.ajax({
						url: '../api/add_task',
						type: 'POST',
	          data: datos,
						beforeSend: function(){
	            $(".loadercontent").css({'display':'flex'})
						},
						success: function(response){
	            // console.log(response)
	            if(response == 'exito'){
	              $(".loadercontent .loader h2").html('Added Task.')
	              setTimeout( () => {
					  if (checking == true){
						calendar.refetchEvents()
	                $(".loadercontent").css({'display':'none'})
	                $(".loadercontent .loader h2").html('Cargando ...')
					  }else{

	                 arg.draggedEl.parentNode.removeChild(arg.draggedEl);
									calendar.refetchEvents()
	                $(".loadercontent").css({'display':'none'})
	                $(".loadercontent .loader h2").html('Cargando ...')
	              }},400)
	            }
							else if(response == 'almuerzo'){
	              $(".loadercontent .loader h2").html('Es hora de almuerzo, no puede agregar tarea.')
								calendar.refetchEvents()
	              setTimeout( () => {
	                $(".loadercontent").css({'display':'none'})
	                $(".loadercontent .loader h2").html('Cargando ...')
	              },600)	
	            }
	            else{
	              $(".loadercontent .loader h2").html('Task not added.')
	              setTimeout( () => {
	                $(".loadercontent").css({'display':'none'})
	                $(".loadercontent .loader h2").html('Cargando ...')
	              },600)
	            }
						}
					});
				}
        else{
					$(".loadercontent").css({'display':'flex'})
					$(".loadercontent .loader h2").html('Es hora de almuerzo, no puede agregar tarea.')
					// calendar.refetchEvents()
					setTimeout( () => {
						$(".loadercontent").css({'display':'none'})
						$(".loadercontent .loader h2").html('Cargando ...')
					},1000)
				}
      },
      eventOverlap: function(stillEvent, movingEvent) {
        return stillEvent.allDay && movingEvent.allDay;
      },
      eventDrop: function(info) {
        
		
           const task_start = info.event.start.toISOString()
           const task_end = info.event.end.toISOString()
           const task_id = info.event.id
           let datos = {
             'task_end': task_end,
             'task_start': task_start,
             'task_id': task_id,
           }
           $.ajax({
   					url: '../api/update_task',
   					type: 'POST',
            data: datos,
   					beforeSend: function(){
               $(".loadercontent").css({'display':'flex'})
   					},
   					success: function(response){
               console.log(response)
               if(response == 'exito'){
                 $(".loadercontent .loader h2").html('Updated Task.')
                 setTimeout( () => {
                   $(".loadercontent").css({'display':'none'})
                   $(".loadercontent .loader h2").html('Cargando ...')
                 },400)
               }
				 else if (response == 'almuerzo') {
					$(".loadercontent .loader h2").html('Hora de inicio no puede ser a las 13:00h.')
					 setTimeout( () => {
						 info.revert();
						 $(".loadercontent").css({'display':'none'})
						 $(".loadercontent .loader h2").html('Cargando ...')
					 },1200)
				}
				else if (response == 'finalmuerzo') {
					$(".loadercontent .loader h2").html('Hora fin, no puede ser a las 14:00h.')
					 setTimeout( () => {
						 info.revert();
						 $(".loadercontent").css({'display':'none'})
						 $(".loadercontent .loader h2").html('Cargando ...')
					 },1200)
				}
               else{
                 $(".loadercontent .loader h2").html('Task not updated.')
                 setTimeout( () => {
                   $(".loadercontent").css({'display':'none'})
                   $(".loadercontent .loader h2").html('Cargando ...')
					 location.reload()
                 },400)
               }
   					}
   				});

      },
      hiddenDays: [ 0, 6 ],
      minTime: '08:00:00',
      maxTime: '24:00:00',
      timeZone: 'UTC',
      // timeZone: 'America/Lima',
      events: {
        url: '../api/list_tasks',
        cache: true
      },
      eventClick: function(calEvent, jsEvent, view) {
        // traemos datos del task
        $.ajax({
             url: '../api/task/'+calEvent.event.id,
             type: 'GET',
             beforeSend: function(){
                $(".loadercontent").css({'display':'flex'})
             },
             success: function(response){
                console.log(response)
                if(response != 'error'){
                  $(".loadercontent").css({'display':'none'})
                  $("select[name='task_cliente']").val(response.id_cliente)
                  $("input[name='edit_name_task']").val(response.title)
                  $("textarea[name='edit_description_task']").val(response.description)
                  $("input[name='edit_id_task']").val(calEvent.event.id)
                  $(".delete_task_id").attr('data-idtask',calEvent.event.id)
				  
				  if(response.correccion){
					  if(response.correccion == 1){
						  $('#default').prop( "checked", true );
					  }
					  else{
						$('#default').prop( "checked", false );
					  }
				  }
				  else{
					$('#default').prop( "checked", false );
				  }
                  $("#modal_edit_task").modal('show')
                  $(".modal-backdrop").addClass('show')
                }
                else{
                  $(".loadercontent .loader h2").html('Task not updated.')
                  setTimeout( () => {
                    $(".loadercontent").css({'display':'none'})
                    $(".loadercontent .loader h2").html('Cargando ...')
                  },400)
                }
             }
          });
      },
			eventReceive: function(info){
        var event = calendar.getEventById('') // an event object!
        event.remove()
      },
      eventRender: function(event, element) {
				// console.log('render',event.el)
				let div = event.el
				div.append('('+event.event.extendedProps.cliente+')')
      },
      eventResize: function(info) {

          const task_start = info.event.start.toISOString()
          const task_end = info.event.end.toISOString()
          const task_id = info.event.id
          let datos = {
            'task_end': task_end,
            'task_start': task_start,
            'task_id': task_id,
          }
          $.ajax({
           url: '../api/update_task',
           type: 'POST',
           data: datos,
           beforeSend: function(){
              $(".loadercontent").css({'display':'flex'})
           },
           success: function(response){
             console.log(response)
              if(response == 'exito'){
                $(".loadercontent .loader h2").html('Updated Task.')
                setTimeout( () => {
                  $(".loadercontent").css({'display':'none'})
                  $(".loadercontent .loader h2").html('Cargando ...')
                },400)
              }
							else if (response == 'almuerzo') {
								$(".loadercontent .loader h2").html('Hora de inicio no puede ser a las 13:00h.')
								 setTimeout( () => {
									 info.revert();
									 $(".loadercontent").css({'display':'none'})
									 $(".loadercontent .loader h2").html('Cargando ...')
								 },1200)
							}
							else if (response == 'finalmuerzo') {
								$(".loadercontent .loader h2").html('Hora fin no puede ser a las 14:00h.')
								 setTimeout( () => {
									 info.revert();
									 $(".loadercontent").css({'display':'none'})
									 $(".loadercontent .loader h2").html('Cargando ...')
								 },1200)
							}
              else{
                $(".loadercontent .loader h2").html('Task not updated.')
                setTimeout( () => {
                  $(".loadercontent").css({'display':'none'})
                  $(".loadercontent .loader h2").html('Cargando ...')
                },400)
              }
           }
         });

      },
    });

    // var event = calendar.getEventById('a') // an event object!
    // var start = event.start // a property (a Date object)
    // console.log(start.toISOString()) // "2018-09-01T00:00:00.000Z"

    calendar.render();

    $(document).ready(function(){
			$(".exampleModalLong").click(function(){
				$("#exampleModalLong").modal('show')
				$(".modal-backdrop").addClass('show')
			})
			$("#new_task").submit(function(e){
        		e.preventDefault()
				let datos = $("#new_task").serialize()
				$.ajax({
				 url: '../api/add_task_back',
				 type: 'POST',
				 data: datos,
				 beforeSend: function(){
						// $(".loadercontent").css({'display':'flex'})
				 },
				 success: function(response){
					 console.log(response)
						if(response != 0){
							var html = '<div class="fc-event" data-trello="fail" id="'+response.id+'" style="'+response.color+'">'
								+response.title+'<p>('+response.cliente+')</p>'
							+'</div>'
							$("#external-events-list").append(html)
							// <div id='external-events-list'>

							$("#exampleModalLong").modal('hide')
							$("#new_task")[0].reset()
						}
						else{
						}
				 }
			 });
			})
      $("#edit_task").submit(function(e){
        e.preventDefault()
        let datos = $("#edit_task").serialize()
        $.ajax({
           url: '../api/update_edit_task',
           type: 'POST',
           data: datos,
           beforeSend: function(){
              $(".loadercontent").css({'display':'flex'})
           },
           success: function(response){
              console.log(response)
              if(response != 'error'){
                $(".loadercontent .loader h2").html('Updated Task.')
                calendar.refetchEvents()
                setTimeout( () => {
                  $(".loadercontent").css({'display':'none'})
                  $(".loadercontent .loader h2").html('Cargando ...')
                },400)
              }
              else{
                $(".loadercontent .loader h2").html('Task not updated.')
                setTimeout( () => {
                  $(".loadercontent").css({'display':'none'})
                  $(".loadercontent .loader h2").html('Cargando ...')
                },400)
              }
           }
         });
         console.log(datos)
      });

      $(".delete_task_id").click(function(){
        const task_id = $(this).attr('data-idtask')
        if (confirm("¿Desea Eliminar Tarea?")) {
          console.log(task_id)
          $.ajax({
             url: '../api/delete_task/'+task_id,
             type: 'GET',
             beforeSend: function(){
               $("#modal_edit_task").modal('hide')
                $(".loadercontent").css({'display':'flex'})
             },
             success: function(response){
                if(response == 'exito'){
                  $(".loadercontent .loader h2").html('Remove Task.')
                  calendar.refetchEvents()
                  setTimeout( () => {
                    $(".loadercontent").css({'display':'none'})
                    $(".loadercontent .loader h2").html('Cargando ...')
                  },500)
                }
                else{
                  $(".loadercontent .loader h2").html('Task not remove.')
                  setTimeout( () => {
                    $(".loadercontent").css({'display':'none'})
                    $(".loadercontent .loader h2").html('Cargando ...')
                  },400)
                }
             }
           });
        }
        else{

        }
      })

    })

  });

    $('#m9').addClass('active');
		console.log(window.innerWidth + 'inner width')
		if (window.innerWidth <= 772) {
			let elements =  Array.from(document.querySelectorAll('#style_depends_screen'))
				elements.forEach(element => {
					element.style.display = 'block'
				})
		}
</script>
@endsection

<!doctype html>
<!doctype html>
<html lang="es">
  <head>
    <title>Alerta</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">

	<meta  http-equiv="refresh" content="5"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/alertas.css')}}">
  </head>
  <body>

    <div id="alertas">
		<div class="container-fluid">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2 class="parpadea" style="padding: 10px">META:  100%  :)</h2>
        </div>
      </div>
			<div class="row">
					@foreach($tickets as $ticket)
						<div class="col-md-3 cardx 
						@if($ticket->estado == 0 ) bg-light
			            @elseif($ticket->estado == 1 ) bg-info
      					@elseif($ticket->estado == 2 ) bg-warning
                        @elseif($ticket->estado == 3 ) bg-danger
                        @elseif($ticket->estado == 4 ) bg-success
      					@elseif($ticket->estado == 5 ) bg-primary
                @elseif($ticket->estado == 10 ) bg-morado
      					@endif" >
							<div class="card-block">
								<h2>{{$ticket->nombre}}</h2>
								<p class="cliente">[{{$ticket->cliente->nombre}}]</p>
								<p class="nombre">{{$ticket->user->name}}</p>
								<p class="fecha">INICIO : <?php echo date('d/m H:i', strtotime($ticket->fecha_inicio));?> <br>
                                FIN : <?php echo date('d/m H:i', strtotime($ticket->fecha_fin));?></p>
							</div>
						</div>
					@endforeach
			</div>
		</div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.js"></script>
  </body>
</html>
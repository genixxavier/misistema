{{-- <!doctype html>
<html lang="es">
  <head>
    <title>Login</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="_token" content="{!! csrf_token() !!}"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>
		html,body{
			height: 100%;
		}
		.site-wrapper {
		    display: table;
		    width: 100%;
		    height: 100%;
		    min-height: 100%;
		    box-shadow: inset 0 0 5rem rgba(0,0,0,.5);
		}
		.site-wrapper-inner {
		    display: table-cell;
		    vertical-align: middle;
		}
	</style>
  </head>
  <body>

    <div class="site-wrapper">

		<div class="site-wrapper-inner">

			<div class="container-fluid">
				<div class="row">
					<div class="col"></div>
					<div class="col-md-6 text-center">
						<h1><img src="{{asset('img/logo-mi.gif')}}" alt=""></h1>
						<p>Iniciar sesión con google +</p>
						<p><a href="{{asset('login/google')}}" class="btn btn-danger btn-google"><i class="fa fa-google-plus"></i> Google +</a></p>
					</div>
					<div class="col"></div>
				</div>
			</div>

		</div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html> --}}
@extends('layout.app')
@section('titulo','Historial')
@section('css')
{{-- Icon Fontastic  --}}
<link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
   
   <style> 
        a {
            color: white; 
            text-decoration: none;
        }
        a:hover {
            color: white; 
            text-decoration: none;
        }
        .mitabla {
            margin-top: 20px;

        }
        .mitabla thead tr {
            background: #263238;
            color: #fff;

        }
        .mitabla thead tr th {
            font-weight: 400;
            color: #fff;
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .mitabla tbody tr td {
            font-size: 14px;
        }
    </style>
@endsection
@section('content')
    
    <div class="container-fluid">
            <div class="row">
                    <div class="col-12 mt-3"><h3 class="text-center">Formulario Cotizacion</h1></div>
                </div>
        <div class="row mt-5">
          
            <div class="col-12">
                 
                    <form action="{{route('cotizacion_store')}}"  method="POST">
                        {{ csrf_field()}}
                        <div class="form-row">
                         
                          
                  @if(!$flag = '')
                                    <div class="col-6" >
                                        <label for="" class="col-4"> Contact Report</label>
                                    
                                        <button type="button"  class="icon-btn btn btn-info " data-toggle="modal"   data-target="#myModal " id="btn-modal" ><i class="icon-eye" ></i>  ver</button>
                                    
                                     
                              
                                    
                                        <input type="text" disabled  class="form-control mt-1" id="reunion" 
                                    value="{{$tema}}">
                                    <input type="hidden" name="id_cr" id="id_cr" value="{{$id}}" >
        
                                </div>
            
                @endif
                          <div class="col-6 mt-2  ">
                             <label for="cotizacion" class="col-6">Nombre Cotización</label>
                             <input type="text" class="form-control" id="cotizacion" placeholder=""  name="nombre">
                             @if ($errors->has('nombre'))
                             @foreach ($errors->get('nombre') as $error)
                             
                            <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                             @endforeach
                             @endif
                          </div>
                          <div class="col-6 mt-2">
                                <label for="monto" class="col-6">Monto: </label>
                                <input type="text" class="form-control" id="monto" placeholder="" name="monto">
                                @if ($errors->has('monto'))
                                @foreach ($errors->get('monto') as $error)
                                
                               <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                                @endforeach
                                @endif
                             </div>
                             <div class="col-6 mt-2">
                                    <label for="descuento" class="col-6">Descuento: </label>
                                    <input type="text" name="descuento" class="form-control" id="descuento" placeholder="" >
                                    @if ($errors->has('descuento'))
                                    @foreach ($errors->get('descuento') as $error)
                                    
                                   <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                                    @endforeach
                                    @endif
                                 </div>
                            <div class="col-6 mt-3" >
                                <label for="estado" class="col-6">Estado: </label>
                                <input type="text" name="estado" class="form-control" id="estado"  value="seguimiento" >
                                @if ($errors->has('estado'))
                                @foreach ($errors->get('estado') as $error)
                                
                               <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                                @endforeach
                                @endif
                                </div>
                          <div class="col-6 mt-3">
                              <label for="des-descuento" class="mr-5">Descripción Descuento: </label>
                              <textarea name="descuento_desc"  class="form-control" id="des-descuento" cols="50" rows="3"></textarea>
                              @if ($errors->has('descuento_desc'))
                                @foreach ($errors->get('descuento_desc') as $error)
                                
                               <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                                @endforeach
                                @endif
                          </div>
                          <div class="col-12 mt-3">
                                <label for="descripcion" class="mr-5">Descripción: </label>
                                <textarea name="descripcion" class="form-control" id="descripcion" cols="100" rows="3"></textarea>
                                @if ($errors->has('descripcion'))
                                @foreach ($errors->get('descripcion') as $error)
                                
                               <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                                @endforeach
                                @endif
                            </div>
                          <button  type="submit" class="btn btn-primary mt-5">Crear</button>
                         <a href="/funnel/cotizaciones" class="btn btn-secondary ml-5 mt-5" >Cancelar</a>
                        </div>
                      </form>
                            
            </div>
          
         </div>



         {{-- MODAL --}}

         <div class="row"> 
            <div class="col-10"> 
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Contact Reports</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body center">
                                    <table class="table table-striped table-bordered mitabla " id="dataTable" > 
                                        <thead> 
                                             <tr> 
                                                 <th> Opciones </th>
                                                 <th>Tema </th>
                                                 <th>Cliente </th>
                                                 <th> Fecha</th>                                        
                                             </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($contact as $item)
                                             
                                            <tr> 
                                                <td> 
                                                <button class="btn btn-primary icon-btn" onclick="view_contact('{{ $item->id}}','{{$item->tema}}')"> <i class="icon-plus"></i> </button>
                                                </td>
                                                <td>
                                                    {{ $item->tema}}
                                                </td>
                                                <td> 
                                                    {{ $item->cliente }}
                                                </td>
                                                <td>
                                                    {{ $item->fecha}}
                                                </td>
                                            
                                            </tr>
                                            
                                            @endforeach 
                                   
    
                                           
                                        </tbody>
                                    
                                    </table>
                                   
                                </div>
                              
                              </div>
                            </div>
                          </div>
                      
            </div>
        </div>
     </div>

@endsection
@section('content_extras')    
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
  <script>
       

  
       // ============= MODAL ====================
  

            function  view_contact(id,reunion) {
                console.log(id,reunion)
                document.getElementById('reunion').value= reunion
                
                document.getElementById('id_cr').value = id
                        
                        }
                     
            // =============  END MODAL ====================



           $(document).ready( function () {
                        $('#dataTable').DataTable({
                            language: {
                                "decimal": "",
                                "emptyTable": "No hay información",
                                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                                "infoPostFix": "",
                                "thousands": ",",
                                "lengthMenu": "Mostrar _MENU_ Entradas",
                                "loadingRecords": "Cargando...",
                                "processing": "Procesando...",
                                "search": "Buscar:",
                                "zeroRecords": "Sin resultados encontrados",
                                "paginate": {
                                    "first": "Primero",
                                    "last": "Ultimo",
                                    "next": "Siguiente",
                                    "previous": "Anterior"
                                }
                            }
                    } ); })
         











  </script>
@endsection
@extends('layout.app')
@section('titulo','Historial')
@section('css')
<!-- JavaScript -->
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
{{-- Icon Fontastic  --}}
<link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
<link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
    <style> 
        h6{
            text-decoration: underline;
        }
        h4{
            color: #17a2b8;
        }
        .check {
            width: 20px;
            height: 20px;
            margin-left: 10px;
        }
        a {
            color: white; 
            text-decoration: none;
        }
        a:hover {
            color: white; 
            text-decoration: none;
        }
        .mitabla {
            margin-top: 20px;

        }
        .mitabla thead tr {
            background: #263238;
            color: #fff;

        }
        .mitabla thead tr th {
            font-weight: 400;
            color: #fff;
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .mitabla tbody tr td {
            font-size: 14px;
        }
        .icon-btn{ 
            text-align: center;
            padding: 5px 8px ;
                
        }
        .integrantes_c {
            
            display: grid;
            grid-template-columns:   100%;
        grid-row-gap: 10px;
            text-align: center;
        }

        @media screen and (max-width : 576px) {
            .button {
                margin-left: 50px;
            }
            .tema-responsive {
                margin-left: 20px;
            }
            .cliente-responsive {
                margin-left: 7px;
            }
        }
        .editar-tema{
            position: absolute;
            top: 0px;
            right: 50px;
            background: #28A745;
            color: #fff;
            padding: 10px 15px;
            font-size: 10px;
            z-index: 2;

        }
    </style>
@endsection
@section('content')

 <div class="container">
     {{ Breadcrumbs::render('contact_report_edit', $edit_report->id) }}
    <div class="row">
        <div class="col-12">
            <div class="detalle_tiket">
                @if (session('status'))
					@if(session('status') == 'exito')
				    <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-success">Contact report creado con éxito</p>
					        <div class="col-md-12">
					        	<button type="button" class="btn btn-primary btn-ok" >OK</button>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
					@else
					<div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-danger">Error: Contact report no creado</p>
					        <div class="col-md-12">
					        	<a href="javascript:void(0)" class="btn btn-primary btn-close" >Cerrar</a>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
					@endif
				@endif
                <h2>Editar Contact Report <a href="/funnel/contact_report" class="float-right btn btn-primary">Volver</a> </h2>
                {{-- <form action="{{route('put_contact_report')}}" id="cr_register" method="POST"> --}}
                <div>
                    {{csrf_field()}}
                    <div class="form-row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-3">
                            <h6>Reunión:</h6>
                            {{-- <button type="button" class="icon-btn btn btn-info ml-3" data-toggle="modal" data-target="#myModal " ><i class="fa fa-eye"></i>  Reuniones</button> --}}
                            <input type="text" disabled  class="form-control mt-3" id="reunion" value="{{$cliente_reu[0]->nombre}}" >
                            <input type="hidden" name="id" id="id" value="{{ $id_reu }}" >
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-3 ">
                            <h6>Cliente:</h6>
                            <input type="text"  disabled class="form-control cliente-responsive  mt-3" id="cliente"  name="cliente" value="{{ $name_cliente[0]->nombre}}">
                        </div>
                        <div class="col-12 mt-3" > 
                            <hr>
                            <h4>Integrantes: </h4>
                        </div>

                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-4">
                            <h6>Media Impact:</h6>
                            <div class="integrantes_m" id="integrantes_m">   
                                <p><i class="fas fa-circle"></i> {{$name_user}}</p>
                                @foreach ($name as $item)
                                    <p><i class="fas fa-circle"></i> {{$item[0]->name}}</p>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 mt-3 " id="divAddIntC">
                            <h6 class="button">{{ $name_cliente[0]->nombre }}: <button id="addIntC" class="btn btn-info icon-btn ml-3"><i class="fas fa-plus"></i></button></h6>
                            @if ($errors->has('integrantes_c'))
                            @foreach ($errors->get('integrantes_c') as $error)
                            <p class="mt-3 ml-3 text-danger">{{ $error }}</p>
                            @endforeach
                            @endif 
                            <div class="integrantes_c mt-3" id="integrantes_c">
                            
                            @foreach ($integrantesc as  $item )
                                <div class="int-cliente input-group">
                                    <input type="text" name="integrantes_c" class="form-control h32" value="{{ $item}}">
                                    <div class="input-group-prepend">
                                        <div class="btn bg-danger2 input-group-text deleteic">X</div>
                                    </div>
                                </div>
                            @endforeach
                        
                            </div>
                        </div>
                        <div class="col-md-12 mt-5"><hr></div>
                        <div class="col-md-12">
                            @foreach($temas as $tema)
                                <div class="row items_temas">
                                    <a href="javascript:void(0)" class="eliminar_tema" data-id="{{ $tema->id }}" >X</a>
                                    <a href="javascript:void(0)" class="editar-tema rounded" data-toggle="modal" data-target="#modal_edit"  onclick="showInputTema({{ $tema->id }})" ><i class="fas fa-edit"></i></a>
                                    <div class="col-md-12">
                                        <h4 id="tema-title-{{$tema->id}}">Tema : {{ $tema->title }}</h4>
                                        <input type="hidden" name="data_get_tema_id" value="{{ $tema->id }}">
                                      
                                    </div>
                                    <div class="col-md-12">
                                        <h5>Acuerdos</h5>
                                    </div>
                                    <div class="col-md-6 mt-1">
                                        <h6 class="button">Media Impact: </h6>
                                        <label class="title_acuerdos">Progreso : <span id="porcentaje_mi_{{$tema->id}}">0</span> % de acuerdos</label>
                                        <div class="progress">
                                            <div id="porcent_mi_{{$tema->id}}" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="mt-4" id="acuerdos_mi_{{ $tema->id }}">
                                            @foreach($tema->temas_acuerdos($tema->id,1) as $acuerdo)
                                            <div class="acuerdos">
                                            <p id="acuerdo_description_{{$acuerdo->id}}">{{ $acuerdo->description }}</p>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal_acuerdo_edit"  class="btn btn-success icon-btn ml-2  mr-4" data-id="{{$acuerdo->id}}" onclick="editAcuerdo({{$acuerdo->id}})" ><i class="fas fa-edit"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-danger icon-btn ml-2 delete_acuerdo" data-id="{{$acuerdo->id}}">X</a>
                                                <input type="checkbox" name="checkbox" <?php if($acuerdo->estado == 1){ echo "checked";}?> data-tema="{{$tema->id}}" value="{{$acuerdo->id}}" data-type="1" class="checkbox_all check check-align">
                                            </div>
                                            @endforeach
                                        </div>
                                        <form class="form_add">
                                            <hr>
                                            <div class="form-group">
                                                <textarea class="form-control" name="data_description" cols="2" required></textarea>
                                            </div>
                                            <input type="hidden" value="2" name="data_category">
                                            <input type="hidden" value="1" name="data_type">
                                            <input type="hidden" value="{{ $tema->id }}" class="data_tema_id" name="data_tema_id">
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary">Agregar</button>
                                                </div>
                                                <div class="col-md-6 text-right"><div class="alerta_acuerdo_mi_{{ $tema->id }}"></div></div>
                                            </div>
                                        </form>
                                    </div>
                                
                                    <div class="col-md-6 mt-1 border-l">
                                        <h6 class="button">Cliente: </h6>
                                        <label class="title_acuerdos">Progreso : <span id="porcentaje_cl_{{$tema->id}}">0</span> % de acuerdos</label>
                                        <div class="progress">
                                            <div id="porcent_cl_{{$tema->id}}" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <div class="mt-4" id="acuerdos_cl_{{ $tema->id }}">
                                            @foreach($tema->temas_acuerdos($tema->id,2) as $acuerdo)
                                            <div class="acuerdos">
                                                <p id="acuerdo_description_{{$acuerdo->id}}">{{ $acuerdo->description }}</p>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#modal_acuerdo_edit"  class="btn btn-success icon-btn ml-2  mr-4" data-id="{{$acuerdo->id}}" onclick="editAcuerdo({{$acuerdo->id}})" ><i class="fas fa-edit"></i></a>
                                                <a href="javascript:void(0)" class="btn btn-danger icon-btn ml-2 delete_acuerdo" data-id="{{$acuerdo->id}}">X</a>
                                                <input type="checkbox" name="checkbox" <?php if($acuerdo->estado == 1){ echo "checked";}?> value="{{$acuerdo->id}}" data-tema="{{$tema->id}}" data-type="2" class="checkbox_all check check-align">
                                            </div>
                                            @endforeach
                                        </div>
                                        <form class="form_add">
                                            <hr>
                                            <div class="form-group">
                                                <textarea class="form-control" name="data_description" cols="2" required></textarea>
                                            </div>
                                            <input type="hidden" value="2" name="data_category">
                                            <input type="hidden" value="2" name="data_type">
                                            <input type="hidden" value="{{ $tema->id }}" class="data_tema_id" name="data_tema_id">
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary">Agregar</button>
                                                </div>
                                                <div class="col-md-6 text-right"><div class="alerta_acuerdo_cl_{{ $tema->id }}"></div></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="col-md-12 ajax_loader"></div>
                        <div class="col-12 col-sm-12 form-group mt-3">
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Agregar Tema:</h4>
                                </div>
                                <div class="col-md-6">
                                    <form id="add_tema">
                                        <div class="int-cliente input-group">
                                            <input type="text" id="data_tema" class="form-control" required>
                                            <div class="input-group-prepend">
                                                <button type="submit" class="btn btn-info input-group-text">Agregar</button>
                                            </div>
                                        </div>
                                        <p class="alerta_add"></p>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12 mt-3 text-center buttons">
                            <hr>
                            <a href="javascript:void(0)" id="update_contact_report" class="btn btn-primary mr-5"> Actualizar </a>
                            <a class="btn btn-secondary ml-5" id="cancelar" href="{{route('contact_report_index')}}">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Tema</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="form_edit_tema">
                    <div class="form-group">
                        <label for="">
                            Tema
                        </label>
                        <input type="text" class="form-control"  id="input_tema_edit">
                        <input type="hidden" class="hidden-edit-id" id="hidden_edit_id" >
                    </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                </div>
                </form>
            </div>

        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal_acuerdo_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Editar Acuerdo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" id="form_edit_acuerdo">
                    <div class="form-group">
                        <label for="">
                            Acuerdo:
                        </label>
                        <textarea name="" id="input_acuerdo_edit" cols="50" rows="3" class="form-control"></textarea>
                        <input type="hidden" class="hidden-edit-id" id="hidden_acuerdo_edit_id" >
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection
@section('content_extras')
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
  <script>
      
        //  ============ JUEGO CHECKBOX ================
        let acuerdos_mi = document.getElementById('acuerdos_mi')
        
         function calcularPercentM () {
             let array = document.querySelectorAll('#checkAcuerdosM')
             array = Array.from(array)
             if (array.length === 0 ) {
                return acuerdos_mi.textContent = 'Progreso : 0 % de acuerdos'
            }
             i = 0 
             for (let j  of array) {
                 if (j.checked) {
                     i += 1
                 }
             }

             let x = (i*100) / array.length
             x = Math.round(x)
             document.getElementById('acuerdos_mi_total').value = x
             $("#proggressmi").css('width',x+'%');
             return acuerdos_mi.textContent = `Progreso : ${x} %  de acuerdos`
         }
        
        let acuerdos_cl = document.getElementById('acuerdos_cl')
        
        function calcularPercentC () {
            var ck = 0;
            // var tck = $("#acuerdos_c .acuerdos .check:checked").length;
            // var tlist = $("#acuerdos_c .acuerdos .check").length;
            let array = document.querySelectorAll('#checkAcuerdosC')
            array = Array.from(array)

            if (array.length === 0 ) {
                return acuerdos_cl.textContent = 'Progreso : 0 % de acuerdos'
            }
            else {
                i = 0 
                for (let j  of array) {
                    if (j.checked) {
                        i += 1
                    }
                }
                let x = (i*100) / array.length
                x = Math.round(x)
                document.getElementById('acuerdos_cl_total').value = x
                $("#proggresscl").css('width',x+'%');
                return acuerdos_cl.textContent = `Progreso : ${x} %  de acuerdos`
            }
        }
       
        //  ============ END JUEGO CHECKBOX ================
        
        $.ajaxSetup({
          headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
        });

        $(document).ready(function(){
            function cal_init(){
                $(".items_temas").each(function(index){
                    var data = $(this).children('.col-md-6');
                    data.each(function(index){
                        var data2 = $(this).children('.mt-4').attr('id');
                        calular_porcentaje2(data2);
                    });
                });
            }
            cal_init();

            function calular_porcentaje(data){
                let id = data.id_tema;
                let t = data.type;
                if( t == 1){
                    var type = 'mi';
                }
                else{
                    var type = 'cl';
                }
                var div = "#acuerdos_"+type+"_"+id;
                /////////////
                var ac = 0;
                var cant = 0;
                $( div+" input[name=checkbox]" ).each(function( index ) {
                    cant ++;
                    if( $(this).is(':checked')){
                        ac ++;
                    }
                });
                //calculando el %
                let x = (ac*100) / cant;
                x = Math.round(x);
                console.log(ac+" -- "+cant);
                $("#porcent_"+type+"_"+id).css('width',x+'%');
                $("#porcentaje_"+type+"_"+id).html(x);
            }

            function calular_porcentaje2(data){
                var res = data.split("_");
                let id = res[2];
                let type = res[1];
                var div = "#"+data;
                /////////////
                var ac = 0;
                var cant = 0;
                $( div+" input[name=checkbox]" ).each(function( index ) {
                    cant ++;
                    if( $(this).is(':checked')){
                        ac ++;
                    }
                });
                //calculando el %
                let x = (ac*100) / cant;
                x = Math.round(x);

                console.log(ac+" -- "+cant);
                if(cant == 0){
                    $("#porcent_"+type+"_"+id).css('width','0%');
                    $("#porcentaje_"+type+"_"+id).html(0);
                }
                else{
                    $("#porcent_"+type+"_"+id).css('width',x+'%');
                    $("#porcentaje_"+type+"_"+id).html(x);
                }
            }
            

            $("#alerta_popup").modal("show");
            $(".btn-close").click(function(){
                $("#alerta_popup").modal("hide");
            });
            $(".btn-ok").click(function(){
                // location.href ="/funnel/contact_report";
                $("#alerta_popup").modal("hide");
            });
            $("#update_contact_report").click(function(){
                var myArray = [];
                $( ".int-cliente input[name=integrantes_c]" ).each(function( index ) {
                    myArray.push($(this).val());
                });
                var myArray2 = [];
                $( ".items_temas input[name=data_get_tema_id]" ).each(function( index ) {
                    myArray2.push($(this).val());
                });
                data = {
                    'integrantes_c': myArray,
                    'temas': myArray2,
                    'id': $("#id").val()
                };

                $.ajax({
                    method: 'POST',
                    url: '../put_contact_report',
                    data: data,
                    beforeSend: function(){
                        console.log("procesando");
                    },
                    success: function(data){
                        location.reload();
                    }
                });
            });

            $(document).on('change',".checkbox_all",function(){
                let self = this;
                let estado;
                if( $(this).is(':checked')){
                    estado = 1;
                }
                else{ estado = 0;}
                data = {
                    'id' : $(this).val(),
                    'estado' : estado,
                }

                let it = $(this).attr("data-tema");
                if($(this).attr("data-type") == 1){
                    var tt = 'mi';
                }
                else{
                    var tt = 'cl';
                }
                var alt = $('.alerta_acuerdo_'+tt+'_'+it);
                $.ajax({
                    method: 'POST',
                    url: '../update_item_acuerdo',
                    data: data,
                    beforeSend: function(){
                        alt.html('<span class="text-info">Actualizando estado ...</span>');
                    },
                    success: function(data){
                        //calculamos %
                        calular_porcentaje(data);
                        alt.html('<span class="text-success">Éxito</span>');
                        setTimeout(function(){
                            alt.html("");
                        },1000);
                    }
                });
                
            });

            $(document).on('click','.delete_acuerdo',function(){
                let id = $(this).attr('data-id');
                var self = this;
                let dat = $(this).parent().parent().attr("id");
                $.confirm({
                    icon: 'fa fa-warning',
                    type: 'red',
                    title: 'Alerta',
                    content: 'Desea eliminar acuerdo?',
                    buttons: {
                        Si: function () {
                            data = {'id': id }
                            $.ajax({
                                method: 'POST',
                                url: '../delete_acuerdo_item',
                                data: data,
                                success: function(data){
                                    if(data == 1){
                                        $(self).parent().remove();
                                        calular_porcentaje2(dat);
                                        $.alert({
                                            type: 'green',
                                            title: 'Éxito',
                                            content: 'Item acuerdo eliminado',
                                        });
                                    }
                                    else{
                                        $.alert({
                                            icon: 'fa fa-warning',
                                            type: 'red',
                                            title: 'Error',
                                            content: 'Item acuerdo no eliminado',
                                        });
                                    }
                                }
                            });
                        },
                        No: function () {
                        }
                    }
                });
            });

            $(document).on('submit',".form_add",function(e){
                e.preventDefault();
                var data = $(this).serialize();
                var data2 = $(this).serializeArray();
                let self = this;
                var div;
                let it = data2[3].value;
                if(data2[2].value == 1){
                    var tt = 'mi';
                }
                else{
                    var tt = 'cl';
                }
                div = $('.alerta_acuerdo_'+tt+'_'+it);
                $.ajax({
                    method: 'POST',
                    url: '../add_tema_acuerdo',
                    data: data,
                    beforeSend: function(){
                        div.html('<span class="text-info">Agregando acuerdo ...</span>');
                    },
                    success: function(data){
                        let id = data.id_tema;
                        let t = data.type;
                        if( t == 1){
                            var type = 'mi';
                        }
                        else{
                            var type = 'cl';
                        }
                        $("#acuerdos_"+type+"_"+id).append('<div class="acuerdos">'
                            +'<p id="acuerdo_description_'+data.id+'">'+data.description+'</p>'
                            +'<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_acuerdo_edit"  class="btn btn-success icon-btn ml-2  mr-4" data-id="'+data.id+'" onclick="editAcuerdo('+ data.id +')" ><i class="fas fa-edit"></i></a>'
                            +'<a href="javascript:void(0)" class="btn btn-danger icon-btn ml-2 delete_acuerdo" data-id="'+data.id+'">X</a>'
                            +'<input type="checkbox" name="checkbox" data-tema="'+data.id_tema+'" value="'+data.id+'" data-type="'+data.type+'" class="checkbox_all check check-align">'
                            +'</div>');
                        $(self)[0].reset();
                        calular_porcentaje(data);
                        div.html('<span class="text-success">Éxito</span>');
                        setTimeout(function(){
                            div.html("");
                        },1000);
                    }
                });
            });

            $("#add_tema").submit(function(e){
                e.preventDefault();
                data = {'tema': $("#data_tema").val(), 'type': 'U'}
                $.ajax({
                    method: 'POST',
                    url: '../add_tema',
                    data: data,
                    beforeSend: function(){
                        $(".alerta_add").html('<span class="text-info">Agregando tema ...</span>');
                    },
                    success: function(data){
                        $(".ajax_loader").append(data);
                        $("#add_tema")[0].reset();
                        $(".alerta_add").html('<span class="text-success">Éxito ...</span>');
                        setTimeout(function(){
                            $(".alerta_add").html("");
                        },1000);
                    }
                });
            });
            
            $(document).on('click','.eliminar_tema',function(){
                let id = $(this).attr('data-id');
                var self = this;
                $.confirm({
                    icon: 'fa fa-warning',
                    type: 'red',
                    title: 'Alerta',
                    content: 'Desea eliminar acuerdo?',
                    buttons: {
                        Si: function () {
                            data = {'id': id }
                            $.ajax({
                                method: 'POST',
                                url: '../eliminar_tema',
                                data: data,
                                success: function(data){
                                    if(data == 1){
                                        $(self).parent().remove();
                                        $.alert({
                                            type: 'green',
                                            title: 'Éxito',
                                            content: 'Tema eliminado',
                                        });
                                    }
                                    else{
                                        $.alert({
                                            icon: 'fa fa-warning',
                                            type: 'red',
                                            title: 'Error',
                                            content: 'Tema no eliminado',
                                        });
                                    }
                                }
                            });
                        },
                        No: function () {
                        }
                    }
                });
            });
        });



          const addIntC  = document.getElementById('addIntC')

            // ============== AGREGAR INTEGRANTES DEL CLIENTE ================

            addIntC.addEventListener('click' , (e) => {
                e.preventDefault()
                $("#integrantes_c").append('<div class="int-cliente input-group">'
                +'<input type="text" name="integrantes_c" class="form-control h32" >'
                    +'<div class="input-group-prepend">'
                        +'<div class="btn bg-danger2 input-group-text deleteic">X</div>'
                    +'</div>'
                +'</div>');
            })
            
            $('body').on('click',".deleteic",function(){
                $(this).parent().parent().remove();
            });

            // Remover integrantes clientes 
            let  checkM = 0 
            let  checkC = 0 
            function removeIntCliente (e, name) {
                e.preventDefault()
                let dataName = name.parentElement.parentElement.id
                name.parentElement.remove()

                calcularPercentC()
                calcularPercentM()
                console.log(calcularPercentM())
                checkM -=1
                checkC -=1
            } 


      //ASSETS
      const findElement = id => {
        let element = document.getElementById(`${id}`)
        return element
    }


                 //MY FUNCTIONS
    const showInputTema = id => {

    let title = `tema-title-${id}`
    let element = findElement(`${title}`)
    let id_form = `hidden_edit_id`
    let form_hidden = findElement(`${id_form}`)
    form_hidden.setAttribute('value' , id)
    let id_edit = `input_tema_edit`
    let input_edit = findElement(`${id_edit}`)
    flag = element.textContent
    flag = flag.split(':')
    input_edit.value = flag[1]
    }

    const editAcuerdo = id => {

    let title = `acuerdo_description_${id}`
    let element = findElement(`${title}`)
    let id_form = `hidden_acuerdo_edit_id`
    let form_hidden = findElement(`${id_form}`)
    form_hidden.setAttribute('value' , id)
    let id_edit = `input_acuerdo_edit`
    let input_edit = findElement(`${id_edit}`)
    input_edit.value = element.textContent
    }

    // formulario edit
    let formulario = findElement('form_edit_tema')
    formulario.addEventListener('submit' , e => {
    e.preventDefault()
    let input  = e.target.input_tema_edit.value,
        id = e.target.hidden_edit_id.value
    console.log('detenido')

    $.ajax({
        method: 'POST',
        url: '../editar_tema',
        data: {
            'id' : id ,
            'text' : input
        },

        success: function(data){
            if (data.status == 200) {
                let title = `tema-title-${id}`
                let element = findElement(`${title}`)
                element.textContent =`Tema : ${input} `
                alertify.success(data.message)
                $('#modal_edit').modal('hide')

            }
        }
    });
    })

    let formulario_acuerdo = findElement('form_edit_acuerdo')
    formulario_acuerdo.addEventListener('submit' , e => {
    e.preventDefault()
    let input  = e.target.input_acuerdo_edit.value,
        id = e.target.hidden_acuerdo_edit_id.value

    $.ajax({
        method: 'POST',
        url: '../editar_acuerdo',
        data: {
            'id' : id ,
            'text' : input
        },

        success: function(data){
            if (data.status == 200) {
                let title = `acuerdo_description_${id}`
                let element = findElement(`${title}`)
                element.textContent = input
                alertify.success(data.message)
                $('#modal_acuerdo_edit').modal('hide')

            }
        }
    });
    })
  </script>
@endsection
<div class="col-12  mt-5 show_service" id="{{$add->id}}">
  <div class="col-12 mt-3">
      <input type="hidden" name="id_servicios[]" value="{{$add->id}}">
      <a href="javascript:void(0)" class="eliminar_tema rounded" data-id=""  onclick="delete_service(this)"  id="delete_service" >X</a>
      <a href="javascript:void(0)" class="eliminar_tema bg-success text-white rounded mr-5" data-id=""  data-toggle="modal"  data-target="#addservicio" onclick="edit_service({{$add->id}})" id="edit_service" ><i class="far fa-edit"></i></a>
      <div class="col-md-12">
          <label >Servicio : <span class="font-weight-bold text-primary"> {{$data->title}}</span> </label>
          <input type="hidden" name="data_get_tema_id" value="">
      </div>
      <div class="col-md-12">
          <label>Descripción del servicio :</label> <br>

          <div class="mb-3">
              {!!$data->description!!}
          </div>
      </div>
          <div class="form-group row">
              <label for="" class="col-2 col-form-label">Precio :</label>
             <div class="col-2">
                 <input type="text" class="form-control"  readonly value="{{$data->price}}">
             </div>
              <label for="" class="col-2 col-form-label">Descuento % :</label>

              <div class="col-2">
                  <input type="text" class="form-control"  readonly value="{{$data->discount}}">
              </div>
              <label for="" class="col-2 col-form-label">Total: S/:</label>
              <div class="col-2">
                  <input type="text" class="form-control" readonly value="{{$data->total}}">
              </div>
          </div>

  </div>
</div>
<div class="col-6 offset-md-3 mt-5">
    @if(count($list) > 0) 
        <table class="table table-striped text-center">
                <thead>
                    <tr>
                    <th scope="col">Cantidad de cambios</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Tribu</th>
                    
                    </tr>
                </thead>
                <tbody>
                    @foreach ($list as $item)
                        <tr>
                            <td># {{$item->cantidad}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->tribu}}</td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
    @else   
    <h1 class="text-danger text-center is-invalid">No se encontraron  datos</h1>
    @endif
</div>
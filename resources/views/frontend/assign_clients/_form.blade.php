<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Asignar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeModal()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" id="myForm">
              {{csrf_field()}}
            <div class="form-row">
                <div class="col-12 mt-2">
                    <label for="" >Seleccione ejecutiv@: </label>
                        <select name="user" id="user" class="form-control">
                          @foreach ($users as $user)
                          <option value="{{$user->id}}">{{$user->name}}</option>                              
                          @endforeach

                        </select>
                        <input type="hidden" id="id_cliente_user" name="id_cliente"  value="" >
                </div>
                <div class="col-12 mt-2 mb-3">
                  <label for="" >Seleccione cliente: </label>
                  <select name="cliente" id="cliente" class="form-control">
                    <optgroup label="Fee">
                        @foreach ($clients as $client)
                            @if($client->type == 1)
                            <option value="{{$client->id}}">{{$client->nombre}}</option>        
                            @endif
                         @endforeach
                    </optgroup>
                    <optgroup label="Spot A" class="form-control">
                        @foreach ($clients as $client)
                            @if($client->type == 2)
                            <option value="{{$client->id}}">{{$client->nombre}}</option>        
                            @endif
                         @endforeach
                    </optgroup>
                    <optgroup label="Spot B" class="form-control">
                        @foreach ($clients as $client)
                            @if($client->type == 3)
                            <option value="{{$client->id}}">{{$client->nombre}}</option>        
                            @endif
                         @endforeach
                    </optgroup>
                  

                  </select>
                </div>
                
                <div class="col-md-12 text-center">     
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="closeModal()">Cerrar</button>
                    <button type="submit"  class="btn btn-primary btn-custom" id="btn_submit">Guardar </button>
                </div>
    
        </div>
          </form>
        </div>
        <div class="modal-footer">
         
        </div>
      </div>
    </div>
  </div>

<div class="row items_temas">
    <a href="javascript:void(0)" class="eliminar_tema" data-id="{{ $tema->id }}" >X</a>
    <a href="javascript:void(0)" class="editar-tema rounded" data-toggle="modal" data-target="#modal_edit"  onclick="showInputTema({{ $tema->id }})" ><i class="fas fa-edit"></i></a>
    <div class="col-md-12">
        <h4 id="tema-title-{{$tema->id}}">Tema : {{ $tema->title }}</h4>
        <input type="hidden" name="data_get_tema_id" value="{{ $tema->id }}">
    </div>
    <div class="col-md-12">
        <h5>Acuerdos</h5>
    </div>
    <div class="col-md-6 mt-1">
        <h6 class="button">Media Impact: </h6>
        @if($tipo == 'U')
            <label class="title_acuerdos">Progreso : <span id="porcentaje_mi_{{$tema->id}}">0</span> % de acuerdos</label>
            <div class="progress">
                <div id="porcent_mi_{{$tema->id}}" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        @endif
        <div class="mt-4" id="acuerdos_mi_{{ $tema->id }}"></div>
        <form class="form_add">
            <hr>
            <div class="form-group">
                <textarea class="form-control" name="data_description" cols="2" required></textarea>
            </div>
            <input type="hidden" value="2" name="data_category">
            <input type="hidden" value="1" name="data_type">
            <input type="hidden" value="{{ $tema->id }}" class="data_tema_id" name="data_tema_id">
            <div class="form-group row">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-custom">Agregar</button>
                </div>
                <div class="col-md-6 text-right"><div class="alerta_acuerdo_mi_{{ $tema->id }}"></div></div>
            </div>
        </form>
    </div>

    <div class="col-md-6 mt-1 border-l">
        <h6 class="button">Cliente: </h6>
        @if($tipo == 'U')
            <label class="title_acuerdos">Progreso : <span id="porcentaje_cl_{{$tema->id}}">0</span> % de acuerdos</label>
            <div class="progress">
                <div id="porcent_cl_{{$tema->id}}" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        @endif
        <div class="mt-4" id="acuerdos_cl_{{ $tema->id }}"></div>
        <form class="form_add">
            <hr>
            <div class="form-group">
                <textarea class="form-control" name="data_description" cols="2" required></textarea>
            </div>
            <input type="hidden" value="2" name="data_category">
            <input type="hidden" value="2" name="data_type">
            <input type="hidden" value="{{ $tema->id }}" class="data_tema_id" name="data_tema_id">
            <div class="form-group row">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-custom">Agregar</button>
                </div>
                <div class="col-md-6 text-right"><div class="alerta_acuerdo_cl_{{ $tema->id }}"></div></div>
            </div>
        </form>
    </div>
</div>
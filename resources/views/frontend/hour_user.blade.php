<div class="col-12  mt-5">
    @if ($flag ) 
            <form action="" class="form" id="update_asistencia" onsubmit="preventDefault()">
                    <div class="form-row" >
                        <div class="col-12 text-center">
                        <label for="" class="color-input">Usuario : {{$user->name}}</label> <br>
                        @php
                            $fecha = explode('-',$asistencia[0]->fecha);
                            $fecha = $fecha[2].'-'.$fecha[1].'-'.$fecha[0]
                        @endphp
                        <label for="" class="color-input">Fecha : {{$fecha}}</label>
                        <input type="hidden" name="id_asistencia" value="{{$asistencia[0]->id}}">
                        </div>
                        
        
                    </div>
                    <div class="form-row">
                        <div class="col-6">
                            <label for="" class="color-input">Hora de entrada :</label>
                            @if($asistencia[0]->hora_inicio !== null ) 
                            <input type="text"  class="form-control" id="hora_inicio" value={{$asistencia[0]->hora_inicio}} name="hora_inicio">
                            @else 
                            <input type="text"  class="form-control is-invalid" placeholder="sin datos" name="hora_inicio"  id="hora_inicio">
                            @endif
                        </div>
                        <div class="col-6">
                            <label for="" class="color-input">
                                    Hora de salida :
                            </label>
                                @if($asistencia[0]->hora_fin !== null ) 
                                <input type="text"  class="form-control" id="hora_fin" name="hora_fin" value={{$asistencia[0]->hora_fin}}>
                                @else 
                                <input type="text"  class="form-control is-invalid" placeholder="sin datos" name="hora_fin" id="hora_fin">
                                @endif
                            </div>
                    </div> 
                    <div class="col-4 offset-lg-4 mt-4">
{{--                        @if($asistencia[0]->hora_inicio == null || $asistencia[0]->hora_fin == null ) --}}
                       <button class="btn btn-primary" type="submit" onclick="updateAsistencia({{$asistencia[0]->id}}),event.preventDefault()">Actualizar</button>
{{--                        @endif --}}
                    </div>
                </form>
     
    @else  
        <h1 class="text-danger text-center is-invalid">No se encontraron  datos</h1>
    @endif
</div>
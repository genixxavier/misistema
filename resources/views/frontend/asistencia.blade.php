<!doctype html>
<html lang="es">
  <head>
    <title>@yield('titulo')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/main.css') }}">

	  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favi/apple-icon-57x57.png')}}">
	  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favi/apple-icon-60x60.png')}}">
	  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favi/apple-icon-72x72.png')}}">
	  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favi/apple-icon-76x76.png')}}">
	  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favi/apple-icon-114x114.png')}}">
	  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favi/apple-icon-120x120.png')}}">
	  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favi/apple-icon-144x144.png')}}">
	  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favi/apple-icon-152x152.png')}}">
	  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favi/apple-icon-180x180.png')}}">
	  <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('favi/android-icon-192x192.png')}}">
	  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favi/favicon-32x32.png')}}">
	  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favi/favicon-96x96.png')}}">
	  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favi/favicon-16x16.png')}}">
	  <link rel="manifest" href="{{asset('favi/manifest.json')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	  <link rel="stylesheet" href="{{ asset('css/asistencia.css') }}">
	  <link href="https://fonts.googleapis.com/css?family=Catamaran:400,500,700,900" rel="stylesheet"> 
	  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
      <style>
          .top_menu {
              top: 47px !important;
          }
		  .main-title {
			  margin-top: 30px;
		  }
          .btnro{
			width: 200px;
			height: 200px;
			
			background-color: darkgray;
			
			color: #fff;
			text-align: center;
            font-size: 35px;
			border-radius: 50%;
			margin: 0px;
			border: solid 1px #FFFCF9;
			cursor: pointer;
		  }
		  .btnro:focus {
              outline: 0;
			  
		  }
      </style>
</head>

  @php
	  $data = array (7,9,10,35,36,61,50,71,5,78);
  @endphp
  {{-- <header>
	  <div class="container" >
		  <div class="container-fluid">
			  <div class="row">
				  <div class="col-md-2 col-sm-2 col-4 responsi">
					  <a href="{{route('historial')}}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
				  </div>


				  <div class="col-6 col-sm-6 col-md-7 col-lg-7   text-right align-middle  time">
	    				<span class="fecha">{{ date('D')." ".date('d')." de ".date('F').", " }}
							<form name="form_reloj" style="display: inline;">
								<input type="text" name="reloj" size="10" onfocus="window.document.form_reloj.reloj.blur()" style="background-color: transparent;border: none;color: white">
							</form>
						</span>
					  <a class="btn" href="{{ route('logout') }}"
						 onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
						  Salir
					  </a>

					  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						  {{ csrf_field() }}
					  </form>
				  </div>
			  </div>
		  </div>
	  </div>
  </header> --}}
  
  	<header class="d-flex align-items-center">		
	    <div class="container"  >
	    	<div class="container-fluid">
	    		<div class="row align-items-center">
	    			<div class="col-md-2 offset-sm-2 col-sm-2 col-4 offset-lg-1 col-lg-2 offset-xl-1 col-xl-2 responsi">
						<a href="{{route('historial')}}"><img src="{{ asset('img/logo.png') }}" alt=""></a>
					</div>
					<div class="col-6 col-sm-6 col-md-7 offset-lg-2 col-lg-5 offset-xl-2 col-xl-5 text-right align-middle  time">
						<div>
						<span class="fecha">{{ date('D')." ".date('d')." de ".date('F').", " }}
	    					<form name="form_reloj" style="display: inline;">
									<input type="text" name="reloj" size="10" onfocus="window.document.form_reloj.reloj.blur()" style="background-color: transparent;border: none;color: white" id="reloj_header">
								</form>
							</span>
	    				<a class="btn" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
						</form>
					</div>
					</div>
					
	    		</div>
	    	</div>
		</div>
	</header>

	<div class="container_img" style="position:absolute;top:0;right:0">
		<div class="card">
		@if(\Auth::user()->img != '')
			<img class="rounded" src="/img_users/{{\Auth::user()->img}}" width="140px" height="170px" alt="{{Auth::user()->name}}">
		@else
			<img class="rounded" src="{{asset("img/user-img.png")}}" width="140px" height="170px" alt="{{Auth::user()->name}}" style="width: 45px !important;margin-top:3px !important;">
		@endif
			<div class="card-body">
				<h5 class="card-title">{{\Auth::user()->name}}</h5>
			</div>
		</div>
		@php
			$nombreUsuario = \Auth::user()->name;
			$nomUs = explode(" ",$nombreUsuario);
		@endphp
		<h5 class="card-title">Hola, {{$nomUs[0]}}</h5>
	</div>
	
	<section id="main" style="min-height:135vh !important">

		<div class="container" >
			<div class="row opacity">
				
                <div class="col-12 col-md-12  text-center ">	
					<h1 class="main-title " id="main_title">Check In / Out</h1>
					
				
                    {{-- <p class="title-description">No olvides de marcar tu entrada y tu salida.
                    </p> --}}
                </div>

			</div>
			<div class="row">
					<div class=" menu box top_menu" >
						<ul class="nav flex-column" style="cursor: pointer;">
							<li class="nav-item text-center">
								<a class="nav-link respo-boton" id="m1" style="">
									<i class="fa fa-bars" aria-hidden="true"></i></a>
							</li>
						</ul>
						
						<ul class="nav flex-column contenido" id="myDIV">

							@if(\Auth::user()->tipo != 'colaborador')
								<li class="nav-item text-center">
									<a class="nav-link" id="m9" href="/ticketblank" data-toggle="tooltip" data-placement="right" title="completa tus tickets para ser el ganador del mes y darle puntos a tu tribu" ><i class="fa fa-clipboard" aria-hidden="true"></i><span>MI Tickets</span></a>
								</li>
								<li class="nav-item text-center">
									<a class="nav-link" id="m9" href="/calendario" data-toggle="tooltip" data-placement="right" title="Completa tus tickets para ser el ganador del mes y darle puntos a tu tribu"><img src="{{asset('img/tickets.png')}}" ><span>Tickets 2.0</span></a>
								</li>
								<li class="nav-item text-center">
									<a class="nav-link" id="m4" href="/crear_reunion" data-toggle="tooltip" data-placement="right" title=" agenda reuniones con tus clientes o miembros de tribu"><i class="fa fa-users" aria-hidden="true"></i><span>Reunión</span></a>
								</li>

								@if (in_array(\Auth::user()->id,$data))

									<li class="nav-item text-center">
										<a class="nav-link" id="m3" href="/funnel/contact_report" data-toggle="tooltip" data-placement="right" title="Ingresa los acuerdos realizados con tus clientes"><i class="fas fa-funnel-dollar"></i><span>Contact | Cotización</span></a>
									</li>
								@endif

							@endif
							<li class="nav-item text-center">
								<a class="nav-link" id="m21" href="/asistencia" data-toggle="tooltip" data-placement="right" title="No olvides de marcar tu entrada y tu salida"><i class="fa fa-clock-o" aria-hidden="true"></i><span>Check In / Out</span></a>
							</li>



							<li class="nav-item text-center">
								<a class="nav-link" id="m7" href="/informes" data-toggle="tooltip" data-placement="right" title="Reportes de tu asistencia, tickets completados y desempeño del mes"><i class="fa fa-file" aria-hidden="true"></i><span>MI Historial</span></a>
							</li>
							<li class="nav-item text-center">

								<a class="nav-link" id="m8" href="/podio/8" target="_blank" data-toggle="tooltip" data-placement="right" title="Llega temprano, completa tus tickets y podrás ayudar a tu tribu a ganar premios increíbles">
									<img src="{{asset('img/icono-mb.png')}}" width="35%" height="35%" alt="">
									<span>Media Bros</span></a>
							</li>
							<li class="nav-item text-center">
								<a class="nav-link" id="m8" href="https://rockstar.equipowik.com" target="_blank" data-toggle="tooltip" data-placement="right" title="Cada día tenemos muchas responsabilidades, pero son esas acciones adicionales las que nos permiten ser mejores y todos debemos reconocerlas."><img
											src="{{asset('img/rockstar.png')}}" alt="rockstar"><span>MI Rockstar</span></a>
							</li>
							<li class="nav-item text-center">

								<a class="nav-link" id="m8" href="https://docs.google.com/spreadsheets/d/1TCNMbjylTNd4ojuR1oAvvyK1KDQfxcQJiHWzCSSoL7U/edit?usp=asogm&ouid=116965352010057924204" target="_blank" data-toggle="tooltip" data-placement="right" title="Cada Rockstar tiene su premio. Completa con tu regalo deseado para que podamos hacerlo realidad."><i class="fas fa-list-alt"></i><span>Wishlist</span></a>
							</li>
							<li class="nav-item text-center">

								<a class="nav-link" id="m8" href="https://docs.google.com/spreadsheets/d/1SmRAQ8UgMLr1eXKCKcOSXNLh85BotdlSwnVT8u722-4/edit?usp=asogm&ouid=116965352010057924204" target="_blank" data-toggle="tooltip" data-placement="right" title="Cumpleaños, aniversarios y datos importantes de todos nosotros a nuestra disposición" ><i class="fas fa-database"></i><span>Base de Datos</span></a>
							</li>
							<li class="nav-item text-center">

								<a class="nav-link" id="m8" href="https://docs.google.com/document/d/1N49WXta1ziJ4vywJyUkFHLALF0sYlvfKGxRW4AxC6aU/edit" target="_blank" data-toggle="tooltip" data-placement="right" title="Las reglas para que tengamos un buen ambiente laboral"><i class="fas fa-align-justify"></i><span>Reglamento</span></a>
							</li>
								<li class="nav-item text-center">

									<a class="nav-link" id="m8" href="{{route('reporte_horas')}}"  data-toggle="tooltip" data-placement="right" title="Reporte de las horas que ha trabajado el usuario con los clientes " ><i class="fas fa-clipboard-list"></i><span>Reporte</span></a>
								</li>
								{{-- <li class="nav-item text-center">

									<a class="nav-link" id="m8" href="{{route('horas_extras')}}"  data-toggle="tooltip" data-placement="right" title="Contador horas extras"  ><i class="fas fa-hourglass-half"></i><span>Horas extras</span></a>
								</li> --}}
								<li class="nav-item text-center">

									<a class="nav-link" id="m8" href="{{route('horas_recompensadas')}}"  data-toggle="tooltip" data-placement="right" title="Contador horas recompensadas"  ><i class="fas fa-user-check"></i><span>Horas recompensadas</span></a>
								</li>
								<li class="nav-item text-center">

										<a class="nav-link" id="m8" href="{{route('clientes')}}"  data-toggle="tooltip" data-placement="right" title="registro de clientes"   ><i class="fas fa-user-tie"></i><span>Clientes</span></a>
									</li>
									<li class="nav-item text-center">

											<a class="nav-link" id="m8" href="{{route('usuarios')}}"  data-toggle="tooltip" data-placement="right" title="actualizar datos"  ><i class="fas fa-user"></i><span>Actualizar datos</span></a>
										</li>
						</ul>
					</div>
				

<div class="col-12 offset-md-4 col-md-4 " id="select_asistencia" style="@if(isset($mostrar_combo)) {{'display:none'}} @else {{'display:block'}} @endif " style="display: none !important;">
	<div class="form-group text-center">
		<label for="tipo_asistencia">Asistencia</label>
		<select name="tipo_asistencia" id="tipo_asistencia" class="form-control">
			<option value="0">-- Opciones --</option>
			<option value="1">MI</option>
			<option value="2">Teletrabajo</option>
		</select>
	</div>
</div>
<div class="col main-content opacity "  style="@if(isset($mostrar_combo)) {{'display:block'}} @else {{'display:none'}} @endif " id="content_checkin_out">
	<div class="row">
		<div class=" col-12 col-md-9 linear_separate" >
			<div class="detalle_tiket">
				<form  action="{{route('create_asistencia')}}" method="POST" id="frm-ticket">
					{{csrf_field()}}
					<div class="form-row">
						<div class="form-group col-md-12">
					  		<h4 style="padding-left: 50px">Bienvenido:</h4>
						  </div>
						  <input type="hidden" id="tipo_a" name="tipo_a">
					</div>
					<div class="col-md-12">
						<div class="form-row">
							<div class="col-md-2"></div>
							<div class="col-md-4">
								{{-- <div class="row frm-mascara">
									<div class="col mk1"></div>
									<div class="col mk2"></div>
									<div class="col mk3"></div>
									<div class="col mk4"></div>
								</div> --}}
								{{-- <input type="hidden" id="codigo" name="codigo" value=""> --}}
								
								@if(isset($asistencia))
									@if($asistencia->hora_inicio != "")
										<input type="hidden" name="salida" value="{{ $asistencia->id }}">
									  @endif
					  			@endif
								<div class="form-group box-teclado">
								  	<div class="row">
								    	{{-- <div class="col clickn" data-id="1">1</div>
								    	<div class="col clickn" data-id="2">2</div>
								    	<div class="col clickn" data-id="3">3</div> --}}
								  	</div>
								  	<div class="row">
								    	{{-- <div class="col clickn" data-id="4">4</div>
								    	<div class="col clickn" data-id="5">5</div>
								    	<div class="col clickn" data-id="6">6</div> --}}
								  	</div>
								  	<div class="row">
								    	{{-- <div class="col clickn" data-id="7">7</div>
								    	<div class="col clickn" data-id="8">8</div>
								    	<div class="col clickn" data-id="9">9</div> --}}
								  	</div>
								  	<div class="row">
								  		{{-- <div class="col"></div>
								    	<div class="col clickn" data-id="0">0</div>
								    	<div class="col clickn" data-id="x"><i class="fa fa-caret-left"></i></div> --}}
								  	</div>
								</div>
								<div class="form-group text-center">
									<div class="form-row text-center justify-content-center">
										@if(!isset($asistencia))
										<button type="submit"   id="btn_submit"  class="btnro">Inicio</button>
										@endif
							  			{{-- @if(isset($asistencia->id))
							  				@if($asistencia->hora_fin == "")
								  			<button type="submit"  id="btn_submit"  class="btn btn-block btn-primary btn-custom">Marcar
								  				@if($asistencia->hora_inicio == "" )
								  					Entrada
								  				@elseif($asistencia->hora_fin == "")
								  					Salida
								  				@else
								  					Entrada
								  				@endif
								  			</button>
							  				@endif
							  			@else
							  				<button type="submit"  id="btn_submit"  class="btn btn-block btn-primary btn-custom">Marcar Entrada</button>
							  			@endif --}}
									</div>
									<div class="form-row">
								  		<p class="vn-alerta"></p>
									</div>
								</div>
							</div>
							{{--  --}}
							<div class="col-md-1"></div>
							<div class="col-md-5">
									
								@if (session('status'))
								@if($asistencia)
									@if(session('status') == "G")
										<div class="text-center">
									        <h2 class="text-success">Bienvenido a MI</h2><br><br>
											<p><img src="{{asset('img/bien.png')}}" alt=""></p>
									        {{-- <h5 class="text-danger">VUELVA A INTENTAR</h5> --}}
										</div>
										@endif
									@endif	
									@if(session('status') == "fail")
									<div class="text-center">
										<h5 class="text-danger">Error al marcar salida<br>Intentas marcar salida con diferentes tipos de asistencia (Teletrabajo | MI), debes marcar salida con el mismo tipo de asistencia que marcaste entrada</h5>
										<p><img src="{{asset('img/eip.png')}}" alt=""></p>
										<h5 class="text-danger">VUELVA A INTENTAR</h5>
									</div>
								@endif
									@if(session('status') == "E")
										<div class="text-center">
											<p><img src="{{asset('img/error.png')}}" alt=""></p>
									        <h4 class="text-danger">SU CÓDIGO ES ERRÓNEO, VUELVA A INTENTAR.</h4>
									    </div>
									@endif
									@if(session('status') == "M")
										<div class="text-center">
											<h3>{{session('horaf')}}</h3>
												<p><img src="{{asset('img/muytarde.png')}}" alt=""></p>
									        <h5 class="text-danger">Oh <br>Súper tarde.</h5>
									    </div>
									@endif
									@if(session('status') == "T")
										<div class="text-center">
											<h3>{{session('horaf')}}</h3>
												<p><img src="{{asset('img/tarde.png')}}" alt=""></p>
									        <h5 class="text-warning">Upss<br>Estás en tardanza.</h5>
									    </div>
									@endif
									
										{{-- <div class="text-center"> --}}
											{{-- <h3>{{session('horaf')}}</h3> --}}
												{{-- <p><img src="{{asset('img/bien.png')}}" alt=""></p>
									        <h5 class="text-success">¡Genial {{ \Auth::user()->name }}!, Llegaste puntual.</h5>
										</div> --}}
										
									

									@if(session('status') == "EX")
										<div class="text-center">
											<h3>{{session('horaf')}}</h3>
												<p><img src="{{asset('img/muytarde.png')}}" alt=""></p>
									        <h5 class="text-danger">Oh <br>Súper tarde.</h5>
									    </div>
									@endif
									@if(session('status') == "D")
										<div class="text-center">
											<h3>{{session('horaf')}}</h3>
												<p><img src="{{asset('img/bien.png')}}" alt=""></p>
									        <h5 class="text-success">Su horario de salida es a las 7:00pm.</h5>
									    </div>
									@endif
									@if(session('status') == "C")
										<div class="text-center">
											<h3>{{session('horaf')}}</h3>
												<p><img src="{{asset('img/bien.png')}}" alt=""></p>
									        <h5 class="text-success">Su horario de salida es a las 6:30pm.</h5>
									    </div>
									@endif
									@if(session('status') == "B")
										<div class="text-center">
											<h3>{{session('horaf')}}</h3>
												<p><img src="{{asset('img/bien.png')}}" alt=""></p>
									        <h5 class="text-success">¡Genial {{ \Auth::user()->name }}!, Llegaste puntual, hora de salida 6:00pm.</h5>
									    </div>
									@endif
									@if(session('status') == "A")
										<div class="text-center">
											<h3>{{session('horaf')}}</h3>
												<p><img src="{{asset('img/bien.png')}}" alt=""></p>
									        <h5 class="text-success">¡Genial {{ \Auth::user()->name }}!, Llegaste puntual, hora de salida 5:30pm.</h5>
									    </div>
									@endif
									@if(session('status') == "S")
										<div class="text-center">
											<h3>Hora de Salida<br>{{session('horaf')}}</h3>
											<p><img src="{{asset('img/salida.png')}}" alt=""></p>
									    </div>
									@endif
									{{-- @if($asistencia->estado = 'G')
										<div class="text-center"> --}}
											{{-- <h3>Su hora de salida es a las: <br>{{session('horaf')}}<br>por favor trabaje.</h3> --}}
											{{-- <p><img src="{{asset('img/muytarde.png')}}" alt=""></p>
									    </div>
									@endif --}}
								@else
									@if(isset($asistencia->id))
										<!-- @if($asistencia->hora_fin != '')
											<div class="text-center">
												<h3>Hora de Salida<br>{{$asistencia->hora_fin}}</h3>
												<p><img src="{{asset('img/salida.png')}}" alt=""></p>
										    </div>
										@else
											{{-- @if($asistencia->estado = 'G') --}}
												<div class="text-center">
													<h3>Hora de Ingreso<br>{{$asistencia->hora_inicio}}</h3>
														{{-- <p><img src="{{asset('img/muytarde.png')}}" alt=""></p> --}}
											        <h5 class="text-danger">Oh <br>Súper Tarde</h5>
											    </div>
											{{-- @endif --}}

											@if($asistencia->estado == "T")
												<div class="text-center">
													<h3>Hora de Ingreso<br>{{$asistencia->hora_inicio}}</h3>
														<p><img src="{{asset('img/tarde.png')}}" alt=""></p>
											        <h5 class="text-warning">Upss<br>Estas en  Tardanza</h5>
											    </div>
											@endif

											{{-- @if($asistencia->estado == "G")
												<div class="text-center">
													<h3>Hora de Ingreso<br>{{$asistencia->hora_inicio}}</h3>
														<p><img src="{{asset('img/bien.png')}}" alt=""></p>
											        <h5 class="text-success">¡Genial {{ \Auth::user()->name }}!<br>Estas Invicto</h5>
											    </div>
											@endif --}}
										@endif -->
										<div class="" style="display:none">
											<div class="text-center" >
												<?php $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");?>
												<h4>Mes {{$meses[date('n')-1]}}</h4>
												<table class="table table-bordered table-responsive-lg mitabla asis-table">
													<thead>
														<tr>
															<th scope="col">Estado</th>
															<th scope="col">Cantidad</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Puntual</td>
															<td>@if(isset($gg))
																{{ $gg }}
																@else
																	0
																@endif
															</td>
														</tr>
														<tr>
															<td>Tardanza</td>
															<td>@if(isset($gt))
																{{ $gt }}
																@else
																	0
																@endif
															</td>
														</tr>
														<tr>
															<td>Súper Tarde</td>
															<td>@if(isset($gst))
																{{ $gst }}
																@else
																	0
																@endif
															</td>
														</tr>
													</tbody>
												</table>
												
											</div>
										</div>
									@endif
								@endif
							</div>
						</div>
					</div>
					
				</form>
			</div>
		</div>
        <div class="col-12 col-md-3 text-center">
			@if(\Auth::user()->tipo == 'ejecutiva')
            <div class="col-12 text-center linear_item">
                <h4  class="main-title col-12">Check In / Out x Tribu</h4>
                <a class="btn btn-custom col-12"  style="color:white;margin-top: 20px" href="{{asset('asistencia_diaria')}}"><span>Entrada por tribu</span></a>

            </div>
			@endif
			@if(\Auth::user()->tipo == 'gerente')
					<div class="col-12 text-center ">
						<h4 class="main-title col-12">Entrada | Salida</h4>
						<a class="btn btn-custom col-12"  style="color:white" href="{{ route('correct_hours') }}"><span>Corregir</span></a>
					</div>

			@endif
				<div class="col-12 text-center ">
					<h4 class="main-title col-12">Justificación</h4>
					<a class="btn btn-custom col-12"  style="color:white" href="/crear_documento"><span>Crear</span></a>
				</div>
				
	</div>
</div>
			</div>
		</div>
	</section>
  <section id="footer" >
	  <div class="container-fluid">
		  <div class="container">
			  <div class="row">
				  <div class="col-md-12 text-center">
					  Copyright 2019 © Powered by <a href="https://www.mediaimpact.pe/" target="_blank">Media Impact</a>
				  </div>
			  </div>
		  </div>
	  </div>
  </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<script language="JavaScript">
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		});
	function mueveReloj(){ 
	   	momentoActual = new Date() 
	   	hora = momentoActual.getHours() 
	   	minuto = momentoActual.getMinutes() 
	   	segundo = momentoActual.getSeconds() 

	   	str_segundo = new String (segundo) 
	   	if (str_segundo.length == 1) 
	      	segundo = "0" + segundo 

	   	str_minuto = new String (minuto) 
	   	if (str_minuto.length == 1) 
	      	minuto = "0" + minuto 

	   	str_hora = new String (hora) 
	   	if (str_hora.length == 1) 
	      	hora = "0" + hora 

	   	horaImprimible = hora + " : " + minuto + " : " + segundo 

	   	document.form_reloj.reloj.value = horaImprimible 

	   	setTimeout("mueveReloj()",1000) 
	} 
	</script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>
<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript">
	$("#m1").click(function(){
		$("#myDIV").toggle();
	});
	$(".clickn").click(function(){
		var valor = $(this).data("id");
		if(valor == "x"){
			//eliminar
			var mks = ['mk4', 'mk3', 'mk2','mk1'];
			$.each( mks, function( i, val ) {
			  	var vv = $( "."+val).html();
			  	if(vv !=""){
			  		$( "."+val).html("");
			  		return false;
			  	}
			});
		}
		else{
			//imprime
			$(".frm-mascara div").each(function(){
				if($(this).html() == ""){
					$(this).html(valor);
					return false;
				}
        	});
			//console.log(valor);
		}
		var v1 = $(".mk1").html();
		var v2 = $(".mk2").html();
		var v3 = $(".mk3").html();
		var v4 = $(".mk4").html();
		$("#codigo").val(v1+v2+v3+v4);
	});




	$("#frm-ticket").submit(function(e){
        //e.preventDefault();
        var cero = 0;
        $(".frm-mascara div").each(function(){
			if($(this).html() == ""){
				cero = 1;
				return false;
			}
    	});
          
        // if( cero == 0){
		// 	document.getElementById('btn_submit').disabled = true
        //   console.log("desabilitando :)");
        //   return true;
        // }
        // else{
        //   console.log("llenar campos :(");
        //   //$(".vn-alerta").fade();
        //   $(".vn-alerta").html("Complete su código");
        //   $(".vn-alerta").addClass("text-danger");
        //   return false;
        // }

    });
    $('#m21').addClass('active');
    function mueveReloj(){
        momentoActual = new Date()
        hora = momentoActual.getHours()
        minuto = momentoActual.getMinutes()
        segundo = momentoActual.getSeconds()

        str_segundo = new String (segundo)
        if (str_segundo.length == 1)
            segundo = "0" + segundo

        str_minuto = new String (minuto)
        if (str_minuto.length == 1)
            minuto = "0" + minuto

        str_hora = new String (hora)
        if (str_hora.length == 1)
            hora = "0" + hora

        horaImprimible = hora + " : " + minuto + " : " + segundo

        document.form_reloj.reloj.value = horaImprimible

        setTimeout("mueveReloj()",1000)
    }
    mueveReloj()
	
	let tipo_asistencia = document.getElementById('tipo_asistencia'),
		tipo_a = document.getElementById('tipo_a'),
		content_checkin_out = document.getElementById('content_checkin_out'),
		select_asistencia = document.getElementById('select_asistencia'), 
		main_title = document.getElementById('main_title')

	// tipo_asistencia.addEventListener('change', e => {
	// 	main_title.textContent = 'Check In / Out | MI '
	// 	if (e.target.value == 0 ) return ''
	// 	if (e.target.value == 2 ) {
	// 		main_title.textContent = 'Check In / Out | Teletrabajo '
	// 	}
	
 	// 	content_checkin_out.style.display = 'block'
	// 	tipo_a.value = e.target.value
	// 	select_asistencia.style.display = 'none'
	// 	console.log(e.target.value)
	// })

	const btn_submit = document.getElementById('btn_submit');

	addEventListener("DOMContentLoaded", e =>{
		main_title.textContent = 'Check In / Out | Teletrabajo '

		content_checkin_out.style.display = 'block'
		tipo_a.value = e.target.value
		select_asistencia.style.display = 'none'
		console.log("di o no di")		
	});

</script>
  </body>
</html>
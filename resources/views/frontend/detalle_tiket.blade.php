@extends('layout.app')
@section('titulo','Detalle Ticket')

@section('css')
	<link rel="stylesheet" href="{{ asset('css/detalle_tiket.css') }}">
@endsection
@section('content')
 
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				<p class="h5">Estado: 
					@if($ticket->estado == 0) Sin asignar <span class="bg-light"></span> 
					@elseif($ticket->estado == 1) En curso <span class="bg-info"></span>
					@elseif($ticket->estado == 2) Culminado <span class="bg-warning"></span>
					@elseif($ticket->estado == 3) Atrasado <span class="bg-danger"></span>
					@elseif($ticket->estado == 4) Aprobado <span class="bg-success"></span>  
					@elseif($ticket->estado == 5) Asignado <span class="bg-primary"></span>
					@endif

				</p> 
				<p><b>Nombre del pedido:</b> {{$ticket->nombre}}</p>
				<p><b>Cliente:</b> @if($ticket->cliente) {{$ticket->cliente->nombre}} @endif</p>
				<p><b>Descripción de pedido:</b> {!! $ticket->descripcion !!}</p>
				<p><b>Fecha de entrega tentativa:</b> {{$ticket->fecha_limite}}</p>
				<p><b>Colaborador:</b> @if($ticket->user){{$ticket->user->name}} @endif</p>
				<p><b>Fecha de inicio:</b> {{$ticket->fecha_inicio}}</p>
				<p><b>Fecha de fin:</b> {{$ticket->fecha_fin}}</p>
				@if($ticket->fecha_limite && $ticket->estado != 0)
					@if($ticket->fecha_fin <= $ticket->fecha_limite)
						<p><div class="bg-success" style="color: white;padding: 15px;text-align: center;">El pedido se entregará a tiempo</div></p>
					@else
						<p><div class="bg-danger" style="color: white;padding: 15px;text-align: center;">El pedido no se entregará a tiempo</div></p>
					@endif
				@endif
				<p class="text-center">
					<a href="/historial" class="btn btn-primary">Volver</a>
					@if($ticket->estado == 0 && \Auth::user()->tipo == 'jefe')
					<a href="/asignar/{{$ticket->id}}" class="btn btn-success">Asignar</a>
					@endif
				</p>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content_extras')
@endsection

@section('js')
	<script>
	    $('#m1').addClass('active');
	</script>
@endsection
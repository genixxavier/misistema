@extends('layout.app')
@section('titulo','Asistencia')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="{{ asset('css/asistencia.css') }}">
@endsection
@section('content')
 
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				<form  action="{{route('create_asistencia')}}" method="POST" id="frm-ticket">
					{{csrf_field()}}
					<div class="form-row">
						<div class="col-12 col-md-12  text-center ">
							<h1 class="main-title ">MI Historial</h1>
							<p class="title-description">Reportes de tu asistencia, tickets completados y desempeño del mes.
							</p>
						</div>
					</div>
					<div style="margin-left: 30px" class="col-md-12">
						<iframe width="100%" height="600" src="https://app.powerbi.com/view?r=eyJrIjoiN2IxZDVhOWQtOTFkZi00ODRkLTlkZmMtN2FkYzI5NjExZDA5IiwidCI6ImEyYjA2NGZmLTJmMzEtNGVjNy1iYWQxLTMyM2M4NzliNGQ0NCIsImMiOjR9" frameborder="0" allowFullScreen="true"></iframe>
{{-- 						<iframe width="100%" height="600" src="https://app.powerbi.com/view?r=eyJrIjoiMGNiMGVlMzEtYzRhNC00ZTYxLWIyZDItZDM0Nzk0NjYzMDQ5IiwidCI6ImEyYjA2NGZmLTJmMzEtNGVjNy1iYWQxLTMyM2M4NzliNGQ0NCIsImMiOjR9" frameborder="0" allowFullScreen="true"></iframe> --}}
					</div>

				</form>
			</div>
		</div>
	</div>
</div>

@endsection

@section('content_extras')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>
<script type="text/javascript">
    $('#m7').addClass('active');
</script>
@endsection
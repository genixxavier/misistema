@extends('layout.app')
@section('titulo','Corregir horas')
@section('css')

{{-- Icon Fontastic  --}}
<link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>

<style>
 .btn-button {
    background: #263238;
    color:white;
 }
</style>
@endsection
@section('content')


<div class="container ">
   <div class="row">
       <div class="col-12 text-center">
           <h3>Historial de cambios en la asistencia</h3>
       </div>
       <div class="col-4 offset-md-4 text-center">
           <form action="" id="formMonth">
               <div class="form-group">
                   <label for="">Seleccione el mes :</label>
                   <select name="month" id="" class="form-control">
                       <option value="01">Enero</option>
                       <option value="02">Febrero</option>
                       <option value="03">Marzo</option>
                       <option value="04">Abril</option>
                       <option value="05">Mayo</option>
                       <option value="06">Junio</option>
                       <option value="07">Julio</option>
                       <option value="08">Agosto</option>
                       <option value="09">Septiembre</option>
                       <option value="10">Octubre</option>
                       <option value="11">Noviembre</option>
                       <option value="12">Diciembre</option>
                   </select>
               </div>
               <button type="submit" class="btn btn-button btn-custom" >Buscar</button>
            <a href="{{route('correct_hours')}}" class="btn btn-primary">Volver</a>
           </form>
          
       </div>
   </div>
   <div class="row" id="content">

   </div>
</div>
@endsection
@section('content_extras')
  

    
  

   
    
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    document.getElementById('formMonth').addEventListener('submit' , e => {

        e.preventDefault()
        let content = document.getElementById('content')
        let month = e.target.month.value 
        console.log(month)
        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
        
                $.ajax({
                    method: 'GET',
                    url: 'show_history_assistance',
                    data : {
                        month
                    },
                    success: function(data){
                       
                    content.innerHTML = data


                    }
        });
    })
</script>
@endsection
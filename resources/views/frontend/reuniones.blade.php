@extends('layout.app')
@section('titulo','Historial')

@section('css')
<style>

    .nav_bravo {
        height: 25px;
    }

</style>
@section('content')
    <div class="row ">
        {{ Breadcrumbs::render('reuniones') }}
    </div>

<div class="col main-content">

    @if (session('status'))
        <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <p class="text-success">{{ session('status') }}</p>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary btn-ok btn-custom" >OK</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
        <div class="row ">
            <div class="col-12 col-md-12  text-center ">
                <h1 class="main-title ">Reuniones</h1>
                {{-- <p class="title-description">Completa tus tickets para ser el ganador del mes y darle puntos a tu tribu.
                </p> --}}
            </div>
        </div>
    <div class="row align_div">
        <div class="col-12 col-md-6 text-md-left text-center " style="padding-top:20px">
            <a href="{{ route('crear_reunion')}}" class="btn btn-primary btn-custom">Crear reunión </a>

            <a class="btn btn-custom"  href="{{route('reunion_marcacion')}}"><span>Crear corrección  por reu</span></a>

          </div>
        <div class="col-12 col-md-6 text-md-right text-center">
        <a href="/historial" class="refres">Tickets <i class="fa fa-users"></i></a>
        <div class="refres " style="cursor:pointer">Actualizar <i class="fa fa-refresh"></i></div>
      </div>

      
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-responsive-lg mitabla ">
                <thead class="">
                    <tr>
                      <th scope="col">F. de inicio</th>
                      <th scope="col">Horas</th>
                      <th scope="col">Nombre</th>
                      <!-- <th scope="col">Colaboradores</th> -->
                      <th scope="col">Cliente</th>
                      <th scope="col">Responsable</th>
                      <th scope="col">Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($reuniones as $reunion)
                      <tr class="">
                            <td scope="row">
                              <?php
                                  $var = $reunion->fecha_inicio;
                                  if($var){
                                    $fechari = date("d/m H:i", strtotime($var) );
                                    echo $fechari;
                                  }
                                ?>
                            </td>
                            <td>
                              {{$reunion->horas}}
                            </td>
                            <td>{{$reunion->nombre}}</td>
                            <!-- <td>
                              @if($reunion->user)
                                  {{$reunion->user->name}}
                              @endif
                            </td> -->
                            <td>{{$reunion->cliente->nombre}}</td>
                            <td>
                              @foreach($users as $uu)
                                @if($uu->id == $reunion->user_create)
                                  {{$uu->name}}                            
                                @endif
                              @endforeach
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        {{-- <a class="dropdown-item"  target="_BLANK" href="{{route('reporte_cotizacion', $item->id) }}"><i class="fas fa-file-pdf"></i> Ver</a> 
                                        @if ($item->c_type_file == 1) --}}
                                        <a href="{{ route('detalle_reunion', $reunion->id) }}" class="dropdown-item"  data-toggle="tooltip" data-placement="top" title="Ver"><i class="fa fa-eye"></i> Ver </a>
                                        
                                        <a href="{{ route('editar_reunion', $reunion->id) }}" class="dropdown-item"  data-toggle="tooltip" data-placement="top" title="Ver"><i class="fas fa-edit"></i> Editar </a>


                                            <a href="" class="dropdown-item" onclick="event.preventDefault();
                                                     document.getElementById('myForm').submit();" data-toggle="tooltip" data-placement="top" title="Ver"><i class="fas fa-minus-circle"></i> Eliminar </a>

                                        <form id="myForm"    style="display: none;" action="{{ route('eliminar_reunion', $reunion->id) }}" method="POST">
                                            {{ csrf_field() }}
                                        </form>
                                        
                                      
                                      
                                    
                                    </div>
                                  </div>
                               
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
            </table>
            <div class="row">
                <div class="col-lg-12">
                   <center>{{$reuniones}}</center>
                </div>   
            </div>
        </div>
    </div>
</div>
@endsection

@section('content_extras')
    <!-- Modal -->
@endsection

@section('js')
  <script>
      $("#alerta_popup").modal("show");
      $(".btn-ok").click(function(){
          location.href ="/reuniones";
      });
    $('.g-tooltip').mouseover(function(){
      //$(this).toggle();
    });
    $(".refres").click(function(){
      location.href ="/reuniones";
    });
    $('.validar').click(function(){
      var id = $(this).attr("data-id");

      $(".yes").unbind("click")

      $('.yes').click(function(){
           location.href='validar/'+id+'/'+4;
      })

    });
    $(".validarc").click(function(){ 
      var id = $(this).attr('data-id');
      $("#ticketuser").val(id);
      $("#observacion_tiket form").attr('action', 'culminarticket');
    });
    $('#m1').addClass('active');

  </script>
@endsection
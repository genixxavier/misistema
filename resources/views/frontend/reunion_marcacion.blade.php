@extends('layout.app')
@section('titulo','Crear Reunión | Marcacion')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('css/reunion.css') }}">
@endsection
@section('content')
<div class="col main-content">
	<div class="row ">
		<div class="col-12">
			{{ Breadcrumbs::render('correcion_por_reu') }}
		</div>
		<div class="col-12 text-center">
			<h1 class="main-title ">  Correción por reunión </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-11">
			<div class="detalle_tiket">
				@if (session('status'))
				    <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-body text-center">
					        <p class="text-success">{{ session('status') }}</p>
					        <div class="col-md-12">
					        	<button type="button" class="btn btn-primary btn-ok btn-custom" >OK</button>
					        </div>
					      </div>
					    </div>
					  </div>
					</div>
				@endif
				@if(isset($tribu[0]->nombre))
				<h3 class="text-center">Tribu: {{$tribu[0]->nombre}}</h3>
			

				@endif
				<form action="{{route('create_reunion_marcacion')}}" method="POST" id="frm-ticket">
					{{ csrf_field() }}
					<input type="hidden" name="estado" value="0">
					<input type="hidden" name="user_create" value="{{ \Auth::user()->id }}">
					<div class="form-row">
					  <div class="form-group col-md-6">
					    <label for="nombre">Nombre de reunión:</label>
					    <input type="text" class="form-control" name="nombre" id="nombre" required>
					  </div>
					  <div class="form-group col-md-6">
					    <label for="cliente">Cliente:</label>
					    <select class="form-control" name="cliente_id" id="cliente">
							<optgroup label="Fees">
								@foreach($clientes as $cliente)
									@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
										@if($cliente->type == 1)
											<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										@endif
									@else
										@if ($cliente->type == 1)
											<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
										@endif
									@endif
								@endforeach
							</optgroup>
							<optgroup label="Spots">
								@foreach($clientes as $cliente)
									@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
										@if($cliente->type == 0)
											<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										@endif
									@else
										@if ($cliente->type == 0)
											<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
										@endif
									@endif
								@endforeach
							</optgroup>
					    </select>
					  </div>
					</div>

					<div class="form-row invitados">
						<div class="form-group col-md-6">
						    <label for="colaborador">Invitados:</label>
						    <select class="form-control" name="colaborador" id="colaborador">
						    	@foreach($colaboradores as $colaborador)
						    		<option value="{{$colaborador->id}}" data-name="{{ $colaborador->name }}">{{ $colaborador->name }}</option>
						    	@endforeach
						    </select>
						</div>
						<div class="form-group col-md-1">
						    <a href="javascript:void(0)" class="btn btn-block btn-primary addcolab btn-custom" data-toggle="tooltip" data-placement="top" title="Agregar"><i class="fa fa-plus"></i></a>
						</div>

						<div class="form-group col-md-12">
						    <ul id="lista_colaborador"></ul>
						    <input type="hidden" id="datajson" name="datajson" >
						    <div for="referencias" class="error referencias" id="referencias-error"></div>
						</div>
					</div>
					
					<div class="form-row">
					  <div class="form-group col-12 col-md-6 box-calendario">
					    <label for="dia">Día:</label>
					    <input type="text" class="form-control" name="fecha_inicio" id="fecha">
					    <i class="fa fa-calendar"></i>
					  </div>
					  <div class="form-group col-12 col-md-6">
					    <label for="hora">Tiempo / Horas(Incluir tiempo de transporte):</label>
					    <input type="number" class="form-control" name="horas" id="hora" value="1">
					  </div>
					</div>
					

					<div class="form-row">
						<div class="form-group col-md-12">
						    <label for="fecha">Descripción de la reunión:</label>
						    <textarea  id="descripcion" name="descripcion" ckeditor="editorOptions" required></textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 text-center">
					  		<button type="submit" class="btn btn-primary btn-custom">Crear</button>
{{-- 					  		<button type="submit" class="btn btn-dark">Volver</button> --}}
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content_extras')
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>

<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.js"></script>
<script type="text/javascript">
	$("#alerta_popup").modal("show");
	$(".btn-ok").click(function(){
		location.href ="/reuniones";
	});
	$(".addcolab").click(function(){
		var name = $("#colaborador").find(':selected').data('name')
		var id = $("#colaborador").val();
		var iii = 0;
		$("#lista_colaborador li").each(function(){
            var nn = $(this).children(".ditem").text();
            var ii = $(this).data("id");
            //console.log(ii);
            if(ii == id && name == nn){
                iii = 1;
            }
        });
        //console.log(iii);
        if(iii == 0){
            if(id == ""){
                $("#referencias-error").text("Elija Indumentaria");
            }
            else{
                $("#referencias-error").text("");
                $("#cantidad").val("");
                $("#lista_colaborador").append('<li data-id="'+id+'"><span class="ditem">'+name+'</span><span class="delete_item">x</span></li>');
            }
        }
        else{
            $("#referencias-error").text("Item ya ingresado");
        }
        var DATA  = [];
        $("#lista_colaborador li").each(function(){
          var nn = $(this).children(".ditem").text();
          var ii = $(this).data("id");
          var item = {};
          item ["nn"] = nn;
          item ["ii"] = ii;
          DATA.push(item);
        });
        var INFO  = new FormData();
        var aInfo   = JSON.stringify(DATA);
        $("#datajson").val(aInfo);
        //console.log(DATA);
	});

	$(document).on("click",".delete_item",function(){
        $(this).parent().remove();
        var DATA  = [];
        $("#lista_colaborador li").each(function(){
          var nn = $(this).children(".ditem").text();
          var ii = $(this).data("id");
          var item = {};
          item ["nn"] = nn;
          item ["ii"] = ii;
          DATA.push(item);
        });
        var INFO  = new FormData();
        var aInfo   = JSON.stringify(DATA);
        $("#datajson").val(aInfo);
        //console.log(DATA);
    });


	$('#descripcion').summernote({
        placeholder: 'Cuéntanos más sobre esta reunión.',
        tabsize: 2,
        height: 200
      });
	$("#frm-ticket").validate({
		rules: {
			nombre: "required",
			descripcion: "required",
			cliente_id: "required",
			user_id: "required",
			fecha_inicio: "required",
			fecha_limite: "required",
			horas_pedido: {
				required: true,
				number: true,
				minlength: 1,
				min: 1
			},
			horas_supervision: {
				required: true,
				number: true,
				minlength: 1,
				min: 0
			},
		},
		messages: {
			nombre: "Ingresar nombre",
			descripcion: {
				required: "Ingresar Descripción del cambio"
			},
			cliente_id: "Escoga cliente",
			user_id: "Escoga Colaborador",
			fecha_inicio: "Escoga fecha",
			fecha_limite: "Escoga fecha",
			horas_pedido: {
				required: "Ingrese horas",
				number: "Ingrese numeros",
				min: "Las horas deber ser mayor a 0"
			},
			horas_supervision: {
				required: "Ingrese horas",
				number: "Ingrese numeros",
				min: "Las horas deber ser mayor o igual 0"
			}
		},
		ignore: ":hidden:not(#descripcion),.note-editable.panel-body"
	});

    $(function () {
    	var dateToday = new Date(); 
        $('#fecha').datetimepicker({
      		format: 'YYYY-MM-DD HH:00:00',
        	sideBySide: true,
        	daysOfWeekDisabled:[0,6],
        	minDate: dateToday
        });
    });
    $('#m20').addClass('active');
</script>
@endsection
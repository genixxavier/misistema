@extends('layout.app')
@section('titulo','Reporte horas')
@section('css')

    <link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=374b9rm3q420nrmdtfwnqf3miwqwvrto3p2dgt51spdlozup"></script>



    <style>


    </style>
@endsection
@section('content')

    <div class="row ">
        <div class="col-12 text-center">
            <h1 class="main-title ">  Contador de vacaciones</h1>
        </div>
       

        <div class="col-12 text-center">
            <form action="" id="myForm" class="col-12  col-md-12 d-flex justify-content-center">
              <div class="form-group col-12 col-md-6 ">

                  <label for="">Seleccioné el mes</label>
                  <select name="mes" id="mes" class="form-control">
                      <option value="0">-- mes --</option>
                      <option value="01">Enero</option>
                      <option value="02">Febrero</option>
                      <option value="03">Marzo</option>
                      <option value="04">Abril</option>
                      <option value="05">Mayo</option>
                      <option value="06">Junio</option>
                      <option value="07">Julio</option>
                      <option value="08">Agosto</option>
                      <option value="09">Septiembre</option>
                      <option value="10">Octubre</option>
                      <option value="11">Noviembre</option>
                      <option value="12">Diciembre</option>
                  </select>

                 <div class="row">
                     <div class="col-12  mt-4 text-center">
                         <button id="calcular" type="submit" value="1" name="btn" class="btn btn-primary btn-custom">Calcular</button>
                     </div>
                   
                 </div>
              </div>

            </form>
        </div>
    </div>
    <div class="row d-flex justify-content-center mt-4 " id="content_info">

    </div>

@endsection
@section('content_extras')

@endsection

@section('js')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
        document.getElementById('myForm').addEventListener('submit', e => {
            e.preventDefault()
           
            let mes = e.target.mes.value,
                content = document.getElementById('content_info')
            if (mes != 0 ) {
                $.ajax({
                    method: 'GET',
                    url: `/content_horas_extras/${mes}/2019`,
                    success: function(data){
                        content.innerHTML = data
                    }
                });
            }

        })

       const registrarHorasExtras = (month,year) => {
           
            let data = {
                year ,
                month
            }
        
            $.ajax({
                    method: 'POST',
                    data: data,
                    url: '/save_horas_extras',
                   
                    success: function(response){
                       if (response.status !== 200) {
                           return alertify.error(response.message);
                       }
                       return alertify.success(response.message);
                    }
                });
       }
    </script>
@endsection
@extends('layout.app')
@section('titulo','Historial')

@section('css')

@section('content')
<div class="col main-content">
    <div class="row">
        <div class="col-12">
            {{ Breadcrumbs::render('documentos') }}
        </div>
        <div class="col-12 text-center">
            <h1>Documentos</h1>
        </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-right">
        <a href="/historial" class="refres">Tickets <i class="fa fa-users"></i></a>
        <div class="refres">Actualizar <i class="fa fa-refresh"></i></div>
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-responsive-lg mitabla">
                <thead>
                    <tr>
                      <th>Fecha</th>
                      <th>Nombre</th>
                      <th>Tipo</th>
                      <th width="150"></th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($documentos as $documento)
                      <tr class="">
                            <td>
                              <?php
                                  $var = $documento->fecha;
                                  if($var){
                                    $fechari = date("d/m/Y", strtotime($var) );
                                    echo $fechari;
                                  }
                                ?>
                            </td>
                            <td>
                               @foreach($users as $uu)
                                @if($uu->id == $documento->user_id)
                                  {{ $uu->name }}                            
                                @endif
                              @endforeach
                            </td>
                            <td>
                              @if($documento->estado == "J")
                                Justificación
                              @else
                                Vacaciones
                              @endif
                            </td>
                            <td scope="row" style="display: flex;">
                              <a href="{{ route('detalle_documento', $documento->id) }}" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="Ver"><i class="fa fa-eye"></i></a>
                              <a href="{{ route('edit_documento', $documento->id) }}" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fas fa-edit"></i></a>
                              <a href="{{ route('delete_documento', $documento->id) }}" class="btn btn-outline-dark" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                  </tbody>
            </table>
            <div class="row">
                <div class="col-lg-12">
                   <center>{{$documentos}}</center>
                </div>   
            </div>
        </div>
    </div>
</div>
@if (session('status'))
  @if(session('status') == 'exito')
    <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body text-center">
          <p class="text-success">Su registro fue creado con éxito</p>
          <div class="col-md-12">
            <button type="button" class="btn btn-primary btn-ok" >OK</button>
          </div>
        </div>
      </div>
    </div>
  </div>
  @else
  <div class="modal fade" id="alerta_popup" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body text-center">
          <p class="text-danger">Error: tiene marcado asistencia el día: {{ session('dia') }}, verificar el rango de dias</p>
          <div class="col-md-12">
            <a href="javascript:void(0)" class="btn btn-primary btn-close" >Cerrar</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif
@endif
@endsection

@section('content_extras')
    <!-- Modal -->
@endsection

@section('js')
  <script>
    $("#alerta_popup").modal("show");
    $(".btn-close, .btn-ok").click(function(){
      $("#alerta_popup").modal("hide");
    });

    $(".refres").click(function(){
      location.href ="/documentos";
    });
    $('#m6').addClass('active');

  </script>
@endsection
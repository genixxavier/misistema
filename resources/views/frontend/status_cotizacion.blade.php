@extends('layout.app')
@section('titulo','Estados de la cotización')
@section('css')
{{-- Icon Fontastic  --}}
<link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
<style>
    .btn-crear:hover {
  background-color: #f4511e;
}
.btn-crear{
  background: #263238;
  display: inline-block;
  padding: 7px 15px;
  cursor: pointer;
  font-size: 13px;
  line-height: 13px;
  text-decoration: none;
  border-radius: 5px;
  color: #fff;
  margin-left: 5px;
  margin-right: 5px;
  margin-bottom: 20px;
}
.btn {
    cursor: unset;
}
</style>
@endsection
@section('content')

<div class="container-fluid">
    <div class="row">
        {{ Breadcrumbs::render('ver_estado') }}
    </div>
    <div class="row">
        <div class="col-12 mt-3"><h1 class="text-center main-title"> Cotizaciones</h1></div>
    </div>
    <div class="row text-center">
        {{-- <div class="col-6 mt-3">
         
            <a href="/funnel/crear_cotizacion" class="btn-crear">Crear Cotización </a>
        </div> --}}
        <div class="col mt-6 ">
       
            <a href="/funnel/contact_report" class="btn-crear btn-custom">Volver </a>
        </div>
        <div class="col mt-6 ">

            <a href="{{route('project.index')}}" class="btn-crear btn-custom">Proyectos </a>
        </div>
      <div class="col-12 mt-3">
        <form class="form-inline my-2 my-lg-0" method="GET">
          <select name="cliente" id="clientes" class="form-control ">
            <option value="0">-- Todos --</option>
            @foreach($clientes as $key => $cliente)

              <option value="{{$cliente->nombre}}">{{$cliente->nombre}}</option>

            @endforeach

          </select>
          <input class="form-control mr-sm-2 ml-sm-3" type="search" name="nombre"   placeholder="Buscar" aria-label="Buscar">
          <button class="btn btn-outline-success my-2 my-sm-0 ml-3" type="submit">Buscar</button>
        </form>
      </div>

    </div>
    <div class="row mt-5">
    
        <div class="col-md-11">
            <table class="table table-bordered table-responsive-lg mitabla custom-table">
                <thead class="">
                    <tr>
                        <th>
                            Marca
                        </th>
                        <th>
                            Contacto 
                        </th>
                        <th>
                            Ejecutivo (a)
                        </th>
                        <th>
                            Inversión Total
                        </th>

                        <th> 

                            Número
                        </th>
                        <th>
                            Estado
                        </th>
                        <th>
                            Opciones
                        </th>
                    </tr>
                    
                </thead>
                <tbody>
                        @foreach ($cotizacion as $item)
                        <tr>
                            <td  data-toggle="tooltip" title="{{$item->title_servicio}}">
                                    {{$item->marca}}
                            </td>
                            <td> 
                                {{$item->c_contacto}}
                            </td>
                            <td >
                                {{$item->ejecutivo}}
                            </td>
                            <td>
                                @if ($item->c_tipo_moneda == 1)
                                    <b>S/</b> {{number_format($item->total,2,'.',',')}}
                                @else
                                   <i class="fas fa-dollar-sign"></i> {{number_format($item->total,2,'.',',')}}   
                                @endif
                            </td>
                            <td> 
                                #{{$item->c_num}}
                            </td>
                            <td  class="text-center">
                                @if ($item->status == 1)
                                    <span class="btn btn-success">Aprobado</span>
                                @elseif ($item->status == 2)
                                <span class="btn btn-danger">Desaprobado</span>
                                @elseif ($item->status == 3)
                                <span class="btn btn-info">Pendiente</span>
                                @elseif ($item->status == 4)
                                <span class="btn btn-warning">Con Observación</span>
                                @elseif ($item->status == 5)
                                <span class="btn btn-light">En proceso</span>
                                @elseif ($item->status == 6)
                                <span class="text-danger font-weight-bold"> - </span>
                             
                              
                                @endif
                            </td>

                            <td>
                                <div class="dropdown">
                                    <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item"  target="_BLANK" href="{{route('reporte_cotizacion', $item->id) }}"><i class="fas fa-file-pdf"></i> Ver</a> 
                                        @if ($item->c_type_file == 1)
                                        <a   class="dropdown-item"   href= {{route('download_file',$item->c_file)}}><i class="fas fa-file-download"></i> Adjunto</a>
                                        @elseif ($item->c_type_file == 2)
                                        <a   class="dropdown-item" target="_blank"  href= {{ $item->c_file }}><i class="fas fa-link"></i>Adjunto</a>
                                        @endif
                                        @if ($item->status != 5 )
                                            <a class="dropdown-item" data-toggle="modal" data-target="#exampleModal"   onclick="pushId({{$item->id_status}})"><i class="far fa-edit"></i> Actualizar</a> 
                                        @endif
                                        @php
                                        if ($item->status == 5) {
                                            echo '<a class="dropdown-item" href="/funnel/cotizacion/'.$item->id.'"><i class="fas fa-edit"></i> Editar</a> 
                                            <a class="dropdown-item" href="#" onclick="change_status('.$item->id_status.')"><i class="fas fa-redo"></i> Enviar a Supervisión </a>
                                            <a class="dropdown-item" href="/funnel/nueva_version/'.$item->id.'" ><i class="fas fa-plus"></i> Generar nueva versión</a>
                                            ';
                                        }
                                        else if ($item->status == 3 || $item->status == 4  ) {
                                            echo ' <a class="dropdown-item" href="/funnel/nueva_version/'.$item->id.'" ><i class="fas fa-plus"></i> Generar nueva versión</a>';
                                        }
                                        else if ($item->status == 1 ) {
                                            echo ' <a class="dropdown-item" href="/funnel/projects/create_project/'.$item->id.'" ><i class="fas fa-plus-square"></i> Crear Proyecto</a> <a class="dropdown-item" href="/funnel/nueva_version/'.$item->id.'" ><i class="fas fa-plus"></i> Generar nueva versión</a>';
                                        }
                                    @endphp
                                    </div>
                                  </div>
                        </tr>
                     @endforeach
                </tbody>
            </table>
            {!! $cotizacion->render() !!}

        </div>
   
    </div>

        
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Cambiar estado de la cotización</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                   <div class="container">
                        <form action="" id="formModal" class="">
                                <input type="hidden" name="id_cotizacion" id="id_cotizacion">
                                <div class="form-group row">
                                    <label for="" class="col-4">Seleccione: </label>
                                    <select name="status" id="status" class="form-control col-8"   >
                                        <option value="1">Aprobado</option>
                                        <option value="2">Desaprobado</option>
                                        {{-- <option value="3">Pendiente</option> --}}
                                        <option value="4">Observación</option>
                                    </select>
                                </div>
                                <div class="form-group" id="group-observacion" style="display:none">
                                    <label for="">Observación:</label>
                                    <textarea name="observacion" id="" cols="10" rows="8" class="form-control"></textarea>
                                </div>
                                <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                        <button type="submit" class="btn btn-primary">Cambiar Estado</button>
                                </div>
                            </form>
                   </div>
                </div>
               
            </div>
            </div>
        </div>
</div>
@endsection
@section('content_extras')
  

    
  

   
    
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>

    const delete_cotizacion = (id) => {

        alertify.confirm("Cotización","Desea eliminar la cotización ?",
            function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    method: 'POST',
                    url: 'delete_cotizacion',
                    data: {
                        id
                    },

                    success: function(data){
                        console.log(data)
                        if (data.status !== 200) {
                            alertify.error(data.message)
                        }
                        else {

                        alertify.success(data.message)
                            location.reload()
                        }


                    }
                });


            },
            function () {
            alertify.error('Cancelado !')
            }
           )
    }

    let status = document.getElementById('status')
    status.addEventListener('change', e => {
        // console.log(e.target.value)
        let group = document.getElementById('group-observacion')
        if (e.target.value == 4 ) {
            group.style.display = 'block'
        }
        else {
            group.style.display = 'none'
        }
    }) 
    const pushId = (id) => {
        document.getElementById('id_cotizacion').value = id 
    }
    const formModal = document.getElementById('formModal')
    formModal.addEventListener('submit', e => {
        e.preventDefault()
        let status = e.target.status.value, 
            obs = e.target.observacion.value, 
            id = e.target.id_cotizacion.value
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });
        $.ajax({
                method: 'POST',
                url: './update_status_cotizacion',
                data: {
                    status,
                    obs, 
                    id
                },

                success: function(data){
                    // console.log(data)
                    if (data.status !== 200) {
                        alertify.error('Ups ! No se pudo actualizar')
                    }
                    else {

                    alertify.success(data.message)
                        location.reload()
                    }


                }
            });
        
    })

   </script>
@endsection
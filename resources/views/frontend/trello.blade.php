@extends('layout.app')
@section('titulo','Bienvenid@ a Media Impact')
@section('css')
<style>.ro1 {
    background: url(../img/MI.jpg)center;
    background-size: cover;
    overflow: hidden;
    width: 100%;
    height: 100%;
}   
</style>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12 p-0 d-flex justify-content-center">
                   <img src="{{asset('img/imagenesmi/image.png')}}" alt="" width="100%" height="300px">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container d-flex justify-content-center align-items-center flex-column">
    <div class=" row p-4">
      <div class="col-md-4 col-xs-12">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('img/imagenesmi/chunga.jpg')}}" alt="Card image cap">
            <div class="card-body d-flex flex-column align-items-center">
              <h5 class="card-title">Luis Chunga</h5>
                
              <a href="https://trello.com/luis65748044/cards" target="_blank" target="_blank" class="btn btn-primary">MI TRELLO</a>
            </div>
          </div>
        </div>
          <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{asset('img/imagenesmi/carlos.jpg')}}" alt="Card image cap">
                <div class="card-body d-flex flex-column align-items-center">
                  <h5 class="card-title">Carlos Guizado</h5>
                    
                  <a href="https://trello.com/cgu31/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
                </div>
           </div>
      </div>
      <div class="col-md-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('img/imagenesmi/cesar.jpg')}}" alt="Card image cap">
            <div class="card-body d-flex flex-column align-items-center">
              <h5 class="card-title">César Mestanza</h5>
                
              <a href="https://trello.com/cesarmestanza1/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
            </div>
          </div>
        </div>
    </div>
    <div class=" row p-4">
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/isabel.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Isabel Lazarte</h5>
                  
                <a href="https://trello.com/isabel_lazarte/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="{{asset('img/imagenesmi/gian.jpg')}}" alt="Card image cap">
                  <div class="card-body d-flex flex-column align-items-center">
                    <h5 class="card-title">Gian Oliva</h5>
                      
                    <a href="https://trello.com/gian359/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
                  </div>
             </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/imagen4.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Alex Omar</h5>
                  
                <a href="https://trello.com/omar54134472/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
      </div>
      <div class=" row p-4">
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/erick.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Erick Saldaña</h5>
                  
                <a href="https://trello.com/ericksaldana/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="{{asset('img/imagenesmi/luis.jpg')}}" alt="Card image cap">
                  <div class="card-body d-flex flex-column align-items-center">
                    <h5 class="card-title">Luis Zárate</h5>
                      
                    <a href="https://trello.com/luiszarate20/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
                  </div>
             </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/renzo.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Renzo Torres</h5>
                  
                <a href="https://trello.com/renzotorresramirez/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
      </div>
      <div class=" row p-4">
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/eduardo.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Eduardo Mayhuasca</h5>
                  
                <a href="https://trello.com/mi_eduardomayhuasca/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="{{asset('img/imagenesmi/culqui.jpg')}}" alt="Card image cap">
                  <div class="card-body d-flex flex-column align-items-center">
                    <h5 class="card-title">Erick Culqui</h5>
                      
                    <a href="https://trello.com/erikculqui1/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
                  </div>
             </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/fahed.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Fahed Guevara</h5>
                  
                <a href="https://trello.com/fahedguevara1/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
      </div>
      <div class=" row p-4">
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/freddy.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Freddy Morales</h5>
                  
                <a href="https://trello.com/freddymorales3/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="{{asset('img/imagenesmi/greeys.jpg')}}" alt="Card image cap">
                  <div class="card-body d-flex flex-column align-items-center">
                    <h5 class="card-title">Greeys Velarde</h5>
                      
                    <a href="https://trello.com/greeysvelarde/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
                  </div>
             </div>
        </div>
        <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{asset('img/imagenesmi/dani.jpg')}}" alt="Card image cap">
                <div class="card-body d-flex flex-column align-items-center">
                  <h5 class="card-title">Daniel Garay</h5>
                    
                  <a href="https://trello.com/danielgaray8/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
                </div>
              </div>
            </div>
      </div>
      <div class=" row p-4">
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/joan.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Joan Cochachi</h5>
                  
                <a href="https://trello.com/joancochachichiuyari/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="{{asset('img/imagenesmi/malena.jpg')}}" alt="Card image cap">
                  <div class="card-body d-flex flex-column align-items-center">
                    <h5 class="card-title">Malena Huarcaya</h5>
                      
                    <a href="https://trello.com/malenahuarcaya1/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
                  </div>
             </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/marilu.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Marilú Aguilar</h5>
                  
                <a href="https://trello.com/mariluaguilar/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
      </div>
      <div class=" row p-4">
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/pablo.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Pablo Carrillo</h5>
                  
                <a href="https://trello.com/pablocarrillo14/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
            <div class="col-md-4">
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="{{asset('img/imagenesmi/leslie.jpg')}}" alt="Card image cap">
                  <div class="card-body d-flex flex-column align-items-center">
                    <h5 class="card-title">Leslie Gomez</h5>
                      
                    <a href="https://trello.com/lesliegr/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
                  </div>
             </div>
        </div>
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/rogger.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Rogger Palomino</h5>
                  
                <a href="https://trello.com/rogger32/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
     </div>
     <div class=" row p-4">
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('img/imagenesmi/imagen4.jpg')}}" alt="Card image cap">
              <div class="card-body d-flex flex-column align-items-center">
                <h5 class="card-title">Hector Cv</h5>
                  
                <a href="#" class="btn btn-primary">MI TRELLO</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="{{asset('img/imagenesmi/jpedro.jpg')}}" alt="Card image cap">
                <div class="card-body d-flex flex-column align-items-center">
                  <h5 class="card-title">Juan Pedro Olórtegui</h5>
                    
                  <a href="https://trello.com/juanpedro174/cards" target="_blank" class="btn btn-primary">MI TRELLO</a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                  </div>
                </div>
        </div>
     </div>
    
@endsection
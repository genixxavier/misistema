@extends('layout.app')
@section('titulo','Corregir horas')
@section('css')

{{-- Icon Fontastic  --}}
<link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>

<style>
 .btn-per  {
    background-color: #263238;
    color: white;
 }
 .color-input {
   color: #263238;
   font-weight: bold;
 }
</style>
@endsection
@section('content')


<div class="container text-center">
    
    <div class="row mt-3">
        <div class="col-6 offset-lg-3">
            <div class="form-group row">
                <div class="col-sm-4">
                    <p style="font-size: 20px;position: relative;top: 4px">{{$tribus->nombre}}</p>
                </div>
                <input type="date" id="date" value="{{$fecha2}}" class="form-control col-sm-5">
                <a href="javascript:void(0)" onclick="obtenerFecha()" style="color: black" class="btn btn-primary col-sm-3">GO</a>
            </div>
        </div>
        <div class="col-8 offset-lg-2">
            <table class="table table-bordered">
                <tr>
                    <th>Nombre</th>
                    <th>Entrada</th>
                    <th>Salida</th>
                </tr>
                @foreach($resultado as $result)
                <tr @if($result->entrada < '08:00:00')  style="background-color: #85fc8b" 
                    @elseif($result->entrada < '08:30:00') style="background-color: #61ce66" 
                    @elseif($result->entrada < '09:00:00') style="background-color: #39b540" 
                    @elseif($result->entrada < '09:30:00') style="background-color: #05b22d" 
                    @else   style="background-color: #f9a9a9" 
                    @endif>
                    <td>{{$result->nombre}}</td>
                    <td>{{$result->entrada}}</td>
                    <td>{{$result->salida}}</td>
                </tr>
                @endforeach
            </table>
            
        </div>
        <div class="col-12" id="form_to_user_change">

        </div>
    </div>
</div>
@endsection
@section('content_extras')
  

@endsection
@section('js')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
function obtenerFecha()
{
  var  fec = $('#date').val();
  var fecha = moment(fec);
  window.location = '/'+fecha.format("DD-MM-YYYY");
}
</script>
@endsection
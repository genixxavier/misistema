  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cliente</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closeModal()">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="" id="myForm">
              {{csrf_field()}}
            <div class="form-row">
                <div class="col-12 mt-1">
                    <label for="" >Nombre</label>
                        <input type="text"  id="cliente" name="cliente" class="form-control" value="">
                        <input type="hidden" id="id_cliente" name="id_cliente"  value="" >
                </div>
                <div class="col-12 mt-1">
                    <label for="">Razón Social: </label>
                    <input type="text" class="form-control" name="razon_social" id="razon_social">
                </div>
                <div class="col-12 mt-1">
                    <label for="">RUC:  </label>
                    <input type="text" id="ruc" name="ruc" class="form-control">
                </div>
                <div class="col-12 mt-3 mb-3">
                    <label for="">tipo :</label>
                    <select name="tipo" id="tipo" class="form-control">
                        <option value="0"> --Seleccioné --</option>
                        <option value="1">Fee</option>
                        <option value="2">Spot A</option>
                        <option value="3">Spot B</option>
                    </select>
                </div>
                <div class="col-md-12 text-center">     
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="closeModal()">Cerrar</button>
                    <button type="submit"  class="btn btn-primary btn-custom" id="btn_submit">Guardar </button>
                </div>
    
        </div>
          </form>
        </div>
        <div class="modal-footer">
         
        </div>
      </div>
    </div>
  </div>

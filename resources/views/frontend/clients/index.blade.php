@extends('layout.app')
@section('titulo','Clientes')
@section('css')

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    {{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <style>
        .btn-crear:hover {
            background-color: #f4511e;
        }
        .btn-crear{
            background: #263238;
            display: inline-block;
            padding: 7px 15px;
            cursor: pointer;
            font-size: 13px;
            line-height: 13px;
            text-decoration: none;
            border-radius: 5px;
            color: #fff;
            margin-left: 5px;
            margin-right: 5px;
            margin-bottom: 20px;
        }
        .btn {
            cursor: unset;
        }
        .mitabla tr th {
            text-align: center;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid">

      <div class="row">
            <div class="col-12 text-center">
                <h1>Clientes</h1>
            </div>
  
            <div class="col-12">
                <button class="btn btn-primary btn-custom" data-toggle="modal" data-target="#myModal">
                    Crear cliente
                </button>
                <a class="btn btn-primary btn-custom" href="{{route('asignar_clientes')}}">
                    Asignar clientes
                </a>
            </div>
      </div>
        <div class="row mt-5">

            <div class="col-md-12">
                <table class="table table-bordered table-responsive-lg mitabla custom-table">
                    <thead class="">
                    <tr>
                        <th>
                            Nombre
                        </th>

                        <th>
                            Razón Social
                        </th>

                        <th>

                            RUC
                        </th>

                        <th>
                            Tipo
                        </th>
                       
                        <th>
                            Opciones
                        </th>
                    </tr>

                    </thead>
                    <tbody>
                        @foreach($clients as $client)
                            <tr>
                                <td> {{$client->nombre}}</td>
                                <td> {{ $client->razon_social }}  </td>
                                <td> {{$client->ruc}} </td>
                                <td> 

                                @if ($client->type == 1)
                                    Fee
                                @elseif($client->type == 2)
                                    Spot A
                                @else 
                                    Spot B
                                @endif

                                </td>
                                
                                <td>
                                    <div class="dropdown">
                                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item"  onclick="showModal('{{$client->id}}')" href="javascript:void(0)"><i class="fas fa-edit"></i> Editar</a>
                                        <a class="dropdown-item"  onclick="deleteClient('{{$client->id}}')" href="javascript:void(0)"><i class="fas fa-trash"></i> Eliminar</a>
                                        </div>
                                    </div>



                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $clients->render() !!}

            </div>

        </div>
    </div>
@endsection
@section('content_extras')


  
 @include('frontend.clients._form')




@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="{{asset('js/client/app.js')}}"></script>



@endsection
@extends('layout.app')
@section('titulo','crear cliente')
@section('css')

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    {{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    <style>
        .btn-crear:hover {
            background-color: #f4511e;
        }
        .btn-crear{
            background: #263238;
            display: inline-block;
            padding: 7px 15px;
            cursor: pointer;
            font-size: 13px;
            line-height: 13px;
            text-decoration: none;
            border-radius: 5px;
            color: #fff;
            margin-left: 5px;
            margin-right: 5px;
            margin-bottom: 20px;
        }
        .btn {
            cursor: unset;
        }
        .mitabla tr th {
            text-align: center;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid">
        <h1 class="main-title">Crear Cliente</h1>
        @include('frontend.clients._form')
    </div>
    </div>
@endsection
@section('content_extras')

    @include('frontend.clients.modals')

@endsection
    
@section('js')
<script src="{{asset('js/client/app.js')}}"></script>
@endsection
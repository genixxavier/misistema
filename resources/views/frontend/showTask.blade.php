@extends('layout.app')
@section('titulo','Pendientes de  Tribu')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
    <style>





        .tablahoras table tr th {
            border-bottom: 1px solid white;
        }
        .style_depends_screen {
            display: none;
        }
        .flag {

            display: flex !important;
            align-items: baseline !important;
            flex-wrap: nowrap !important;
            overflow-x: auto;
            /* margin-top:5px ; */
        }
        a.btn.btn-outline-dark.popup_editar {
            position: absolute;
            top: 0;
            left: 5px;
            padding: 1px 7px !important;
            display: inline-block;
            border: 0px;
        }

        @media  (max-width: 992px) {
            .tablahoras table tr .h-active h2 {
                font-size: 12px;
            }
            .flag_2 , .flag_2 tbody tr th, .flag_2 tbody tr td  {
                width: 350px;
            }
            .first_child {
                padding: 0;
            }
            .leyenda {
                text-align: center;
            }

            .center_div {
                text-align: center !important;
                margin-top: 10px;
            }
            .align_button {
                text-align: center;
            }
            .flag_table {
                width: 120px !important;
            }
            .flag_table tbody tr th  {
                width: 120px;
            }
        }


        .mitabla tbody tr:nth-child(n + 1 ) {
            height: 70px !important;
        }
       .content-table {
           display: flex;
           overflow-x: auto;
           flex-wrap: nowrap;


       }
        .content-table table {
            width: 280px ;

        }

        .custom-table thead {
            color: white;
            text-align: center;
        }
        .custom-table  table tbody tr  {
            border-left-color: #0A0025;
            border-right-color: #0A0025;
        }
        .child-content  table tbody tr td  {
            font-size: 0.9em ;
            word-break: break-word;
        }

        
    </style>
@endsection
@section('content')
@php
    function returnHour ( $date) {
        $new_date = explode(' ',$date);
        $new_date = explode(':' , $new_date[1]);
        $hour = ''.$new_date[0].':'.$new_date[1].'';
        return $hour;
    }



    $previusDay = date("Y-m-d",strtotime($fecha."- 1 days"));
    $nextDay = date("Y-m-d",strtotime($fecha."+ 1 days"));

@endphp
    <div class="col main-content">
        <div class="row">


                <div class="col-4 col-md-4 mt-2 previus">
                  <div class="row">
                      <div class="col-6">
                          <form action="{{ route('showTasks' , [
                    $nameTribu->Id,
                    $previusDay
                    ])  }}"  method="GET">
                              <button type="submit" class="btn btn-primary btn-custom">Día ant.</button>
                          </form>
                      </div>
                      <div class="col-6">
                          <form action="{{ route('showTasks' , [
                    $nameTribu->Id,
                    $nextDay
                    ])  }}"  method="GET">
                              <button type="submit" class="btn btn-primary btn-custom">Día sig.</button>
                          </form>
                      </div>
                  </div>
                </div>
                <div class="col-4 col-md-4 mt-2 tribu text-center ">
                    <h1 class="main-title">  {{ $nameTribu->nombre  }}</h1>

                </div>
                <div class="col-4 col-md-4 mt-2 next text-right">
                    <a href="{{ route('ticketblank') }}" class="btn btn-primary btn-custom">Atrás</a>
                </div>



                <div class="col-12 text-center mb-2 ">
                    <p class="main-content">  {{ $nameOfDay  }} </p>
                </div>

            <div class="col-12 content-table">
                @foreach($data as $a)
                    <div class="child-content" >
                        <table  class="table  table-bordered custom-table"  >
                            <thead>
                            @php
                                $name = explode(' ',$a['name'])
                            @endphp
                            <th> {{  $name[0]  }} </th>
                            </thead>
                            <tbody>
                            @if (!empty($a['tickets']))
                                @foreach( $a['tickets'] as $task )
                                    <tr class="">

                                        @php
                                            $inicio  = returnHour($task['fecha_inicio'] );
                                            $fin  = returnHour($task['fecha_fin'] );
                                        @endphp

                                        <td class="">
                                            <p class="main-title font-weight-bold">Pendiente: {{ $task['nombre_ticket']  }}</p>
                                            <div class="d-flex justify-content-between">
                                                <p> <span class="font-weight-bold">Horas:</span> {{ $task['horas_pedido']  }}</p>
                                                <p> <span class="font-weight-bold">Inicio:</span> {{ $inicio}}</p>
                                                <p> <span class="font-weight-bold">Fin:</span> {{ $fin }}</p>
                                            </div>

                                        </td>
                                    </tr>

                                @endforeach

                            @endif
                            </tbody>
                        </table>
                    </div>

                @endforeach
            </div>
        </div>
    </div>

@endsection






@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>

    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

    <script type="text/javascript">

    </script>
@endsection

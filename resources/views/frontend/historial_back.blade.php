@extends('layout.app')
@section('titulo','Historial')
@section('css')
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('content')
<div class="col main-content">
	<div class="row">
    <div class="col-md-12 text-right fech" id="filtro">
      <label>Filtrar por:</label>
      @if(!isset($activeco))
        <?php $activeco = 'colaborador';?>
      @endif
      <a href="javascript:void(0)" class="btn-filtro fil-first @if($activeco == 'colaborador') act @endif" data-id="form_filter"><i class="fa fa-user"></i> Colaborador</a>
      <a href="javascript:void(0)" class="btn-filtro fil-med @if($activeco == 'cliente') act @endif" data-id="form_filter2"><i class="fa fa-users"></i> Cliente</a>
      <a href="javascript:void(0)" class="clie btn-filtro fil-last @if($activeco == 'fecha') act @endif" data-id="form_filter3"><i class="fa fa-calendare"></i> Fecha</a>
      <form id="form_filter" class="@if($activeco == 'colaborador') active @endif ">
          <input type="hidden" value="{{Request::root()}}" id="data_url_host">
          {{ csrf_field() }}
          <input type="hidden" id="data_tipo" name="data_tipo" value="colaborador"/>
          <select class="form-control" id="data_colaborador" name="data_colaborador">
            <option value=""></option>
            @foreach($getcolaboradores as $colaborador)
              <option value="{{$colaborador->id}}">{{$colaborador->name}}</option>
            @endforeach
          </select>
          <button type="submit" class="btn-buscar"><i class="fa fa-search"></i></button>
      </form>
      <form id="form_filter2" class="@if($activeco == 'cliente') active @endif">
          <input type="hidden" value="{{Request::root()}}" id="data_url_host">
          {{ csrf_field() }}
          <input type="hidden" id="data_tipo" name="data_tipo" value="cliente"/>
          <select class="form-control" id="data_colaborador" name="data_colaborador">
            <option value=""></option>
            @foreach($getclientes as $cliente1)
              <option value="{{$cliente1->id}}">{{$cliente1->nombre}}</option>
            @endforeach
          </select>
          <button type="submit" class="btn-buscar"><i class="fa fa-search"></i></button>
      </form>
      <form id="form_filter3" class="@if($activeco == 'fecha') active @endif">
          <input type="hidden" value="{{Request::root()}}" id="data_url_host">
          {{ csrf_field() }}
          <input type="hidden" id="data_tipo" name="data_tipo" value="fecha"/>
          <input type="text"  id="data_colaborador" name="data_colaborador"/>
          <button type="submit" class="btn-buscar"><i class="fa fa-search"></i></button>
      </form>
    </div>
  </div>
	<div class="row">
    <div class="col-md-1 col-*"></div>
      <div class="col-md-5 col-8 text-left">
        <a href="/crear_ticket" class="btn-crear">CREAR TICKET</a>
        <a href="/crear_reunion" class="btn-crear">CREAR REUNIÓN</a>
      </div>
	  <div class="col-md-6 col-4 text-right">
        @if(isset($mensaje_filtro) && $mensaje_filtro != "")
          <p class="filtrado">Search: <span>{{$mensaje_filtro}}</span></p>
        @endif
        <a href="{{Request::root()}}/historial" class="btn-crear">Actualizar <i class="fa fa-refresh"></i></a>
    </div>
  </div>
	<div class="row">
    <div class="col-md-1"></div>
		<div class="col-md-11 text-center" id="btn-estados">
      <?php 
        $urll = Request::url();
        $urll = explode('/', $urll );
        if(count($urll) == 7){
          $urlf =  $urll[0].'/'.$urll[1].'/'.$urll[2].'/'.$urll[3].'/'.$urll[4].'/'.$urll[5];
        }
        else{
            if(count($urll) == 5){
              $urlf =  $urll[0].'/'.$urll[1].'/'.$urll[2].'/'.$urll[3];
            }
            else{
              $urlf = Request::url();
            }
        }
      ?>
      <a href="{{ $urlf }}/0" class="cuadro cuadro-sin-asignar">SIN ASIGNAR</a>
			{{-- <a href="{{ route('ordenar',0) }}" class="cuadro cuadro-sin-asignar">SIN ASIGNAR</a> --}}
			<a href="{{ $urlf }}/5" class="cuadro cuadro-asignados">PENDIENTES</a>
			<a href="{{ $urlf }}/1" class="cuadro cuadro-en-proceso">EN PROCESO</a>
			<a href="{{ $urlf }}/2" class="cuadro cuadro-culminados">CULMINADOS</a>
		</div>
	</div>
	<div class="row">
    <div class="col-md-1"></div>
		<div class="col-md-11 text-center" id="btn-tags">
			<a href="{{ $urlf }}/3"><span class="tag-atrazados">{{$reg_campo[0]['natrazadp']}}</span> Atrazados</a>
			<a href="{{ $urlf }}/4"><span class="tag-aprobados">{{$reg_campo[0]['naprobado']}}</span> Aprobados</a>
		</div>
	</div>
    <div class="row">

        <div class="col-md-12">
            @if (session('status'))
              @if(session('status') == 'pausa')
                <div class="alert alert-success">Éxito ticket Pausado</div>
              @elseif(session('status') == 'play')
                <div class="alert alert-success">Éxito ticket En Curso</div>
              @elseif(session('status')== 'eliminado')
                <div class="alert alert-success">Éxito ticket Eliminado</div>
              @elseif(session('status')== 'asignado')
                <div class="alert alert-success">Éxito ticket Asignado</div>
              @elseif(session('status')== 'culminado')
                <div class="alert alert-success">Éxito ticket culminado</div>
              @endif
            @endif
            <table class="table table-bordered table-responsive-lg mitabla">
                <thead>
                    <tr>
                      <th scope="col">F. Inicio</th>
                      <th scope="col">F. Fin</th>
                      <th scope="col">Cliente</th>
                      <th scope="col">Tarea</th>
                      <th scope="col">Responsable</th>
                      <th scope="col">Colaborador</th>
                      <th scope="col">Área</th>
					            <th scope="col">Estado</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    @if(isset($ticketwhite))
                      @foreach($ticketwhite as $ticket)
                        <tr id="{{$ticket->id}}">
                            <td scope="row">
                              <?php
                                  $var = $ticket->fecha_inicio;
                                  if($var){
                                    $fechari = date("d/m H:i", strtotime($var) );
                                    echo $fechari;
                                  }
                                ?>
                              {{-- $ticket->fecha_inicio --}}
                            </td>
                            <td>
                              <?php
                                  $var = $ticket->fecha_fin;
                                  if($var){
                                    $fechari = date("d/m H:i", strtotime($var) );
                                    echo $fechari;
                                  }
                              ?>
                              {{-- $ticket->fecha_fin --}}
                            </td>
							@if($ticket->cliente)
                               <td>{{$ticket->cliente->nombre}}</td>
                            @else
                                <td>--</td>
                            @endif
                            <td>{{$ticket->nombre}}</td>
							<td>
                              @foreach($users as $uu)
                                @if($uu->id == $ticket->user_create)
                                  {{$uu->name}}                            
                                @endif
                              @endforeach
                            </td>
                            <td>
                              @if($ticket->user)
                                  {{$ticket->user->name}}
                              @endif
                            </td>
                            <td>{{ $ticket->area->name }}</td>
							<td>
								@if($ticket->estado == 0 )
									<span class="tck-estado tck-sin-asignar">Sin asignar</span>
								@elseif($ticket->estado == 1 )
									<span class="tck-estado tck-proceso">En proceso</span>
								@elseif($ticket->estado == 2 )
									<span class="tck-estado tck-culminado">Culminado</span>
								@elseif($ticket->estado == 3 )
									<span class="tck-estado tck-atrazado">Atrazado</span>
								@elseif($ticket->estado == 4 )
									<span class="tck-estado tck-aprobado">Aprobado</span>
								@elseif($ticket->estado == 5 )
									<span class="tck-estado tck-asignado">Pendiente</span>
								@endif
							  </td>
							<td>
								<div class="dropdown">
								  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="{{route('detalle_ticket',$ticket->id)}}"><i class="fa fa-eye"></i> Ver</a>
									
									<a href="{{route('detalle_ticket',$ticket->id)}}" class="dropdown-item"><i class="fa fa-eye"></i> Ver</a>
									@if($ticket->estado == 2 && \Auth::user()->tipo == 'ejecutiva')
										<a href="#" class="dropdown-item validar" data-toggle="modal" data-target="#validar_tiket" data-id="{{$ticket->id}}"><i class="fa fa-smile-o"></i> Validar</a>
									@endif
									@if(\Auth::user()->tipo == 'jefe' && $ticket->estado == 0)
										<a href="{{route('asignar',$ticket->id)}}" class="dropdown-item validar"><i class="fa fa-user-plus"></i> Asignar</a>
										<a href="{{route('eliminar',$ticket->id)}}" class="dropdown-item validar"><i class="fa fa-trash-o"></i> Eliminar</a>
									@endif
									@if($ticket->estado == 1)
										<a href="{{route('culminar',$ticket->id)}}" class="dropdown-item validar"><i class="fa fa-check"></i> Culminar</a>
										<a href="javascript:void(0)" class="dropdown-item asignar_u" data-toggle="modal" data-target="#asignar_u" data-id="{{$ticket->id}}"><i class="fa fa-user-plus"></i> Asignar</a>
										<a href="{{route('editar_ticket',$ticket->id)}}" class="dropdown-item editar" data-id="{{$ticket->id}}"><i class="fa fa-pencil"></i> Editar</a>
										<!-- <a href="{{route('pausar_tk',$ticket->id)}}" class="btn btn-outline-dark pausar_tk" data-toggle="tooltip" data-target="#pausar_tk" data-id="{{$ticket->id}}" title="Pausar"><i class="fa fa-pause"></i></a> -->
									@endif
									@if($ticket->estado == 10)
										<a href="{{route('play_tk',$ticket->id)}}" class="dropdown-item play_tk" data-target="#play_tk" data-id="{{$ticket->id}}"><i class="fa fa-play"></i> Continuar</a>
									@endif
									@if( $ticket->estado==3)
										<a href="#" class="dropdown-item validarc" data-toggle="modal" data-target="#observacion_tiket" data-id="{{$ticket->id}}"><i class="fa fa-check"></i> Culminar</a>
									@endif
									@if( $ticket->estado == 5)
										<a href="{{route('editar_ticket',$ticket->id)}}" class="dropdown-item editar" data-id="{{$ticket->id}}"><i class="fa fa-pencil"></i> Editar</a>
										<a href="{{route('eliminar',$ticket->id)}}" class="dropdown-item validar"><i class="fa fa-trash-o"></i> Eliminar</a>
									@endif
								  </div>
								</div>
                            </td>
                        </tr>
                      @endforeach
                    @endif

                    @foreach($tickets as $ticket)
                      @if(\Auth::user()->tipo == 'jefe')
                        <tr id="{{$ticket->id}}">
                            <td scope="row">
                              <?php
                                  $var = $ticket->fecha_inicio;
                                  if($var){
                                    $fechari = date("d/m H:i", strtotime($var) );
                                    echo $fechari;
                                  }
                                ?>
                              {{-- $ticket->fecha_inicio --}}
                            </td>
                            <td>
                              <?php
                                  $var = $ticket->fecha_fin;
                                  if($var){
                                    $fechari = date("d/m H:i", strtotime($var) );
                                    echo $fechari;
                                  }
                              ?>
                              {{-- $ticket->fecha_fin --}}
                            </td>
							@if($ticket->cliente)
                               <td>{{$ticket->cliente->nombre}}</td>
                            @else
                                <td>--</td>
                            @endif
                            <td>{{$ticket->nombre}}</td>
							<td>
                              @foreach($users as $uu)
                                @if($uu->id == $ticket->user_create)
                                  {{$uu->name}}                            
                                @endif
                              @endforeach
                            </td>
                            <td>
                              @if($ticket->user)
                                  {{$ticket->user->name}}
                              @endif
                            </td>
                            
                            <td>{{ $ticket->area->name }}</td>
							
							<td>
								@if($ticket->estado == 0 )
									<span class="tck-estado tck-sin-asignar">Sin asignar</span>
								@elseif($ticket->estado == 1 )
									<span class="tck-estado tck-proceso">En proceso</span>
								@elseif($ticket->estado == 2 )
									<span class="tck-estado tck-culminado">Culminado</span>
								@elseif($ticket->estado == 3 )
									<span class="tck-estado tck-atrazado">Atrazado</span>
								@elseif($ticket->estado == 4 )
									<span class="tck-estado tck-aprobado">Aprobado</span>
								@elseif($ticket->estado == 5 )
									<span class="tck-estado tck-asignado">Pendiente</span>
								@endif
							  </td>
                            <td>
								<div class="dropdown">
								  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a href="{{route('detalle_ticket',$ticket->id)}}" class="dropdown-item"><i class="fa fa-eye"></i> Ver</a>
									@if($ticket->estado == 2 && \Auth::user()->tipo == 'ejecutiva')
										<a href="#" class="dropdown-item validar" data-toggle="modal" data-target="#validar_tiket" data-id="{{$ticket->id}}"><i class="fa fa-smile-o"></i> Validar</a>
									@endif
									@if(\Auth::user()->tipo == 'jefe' && $ticket->estado == 0)
										<a href="{{route('asignar',$ticket->id)}}" class="dropdown-item validar"><i class="fa fa-user-plus"></i> Asignar</a>
										<a href="{{route('eliminar',$ticket->id)}}" class="dropdown-item validar"><i class="fa fa-trash-o"></i> Eliminar</a>
									@endif
									<?php 
										if($ticket->fecha_inicio){
										  $fa = date('Y-m-d H:i');
										  $fi = date("Y-m-d H:i", strtotime($ticket->fecha_inicio) );
										  $ff = date("Y-m-d H:i", strtotime($ticket->fecha_fin) );
										}
									?>
									@if($ticket->estado == 1)
									  <a href="{{route('culminar',$ticket->id)}}" class="dropdown-item validar"><i class="fa fa-check"></i> culminar</a>
									  <a href="javascript:void(0)" class="dropdown-item asignar_u" data-toggle="modal" data-target="#asignar_u" data-id="{{$ticket->id}}"><i class="fa fa-user-plus"></i> Asignar</a>
									  <a href="{{route('editar_ticket',$ticket->id)}}" class="dropdown-item editar" data-id="{{$ticket->id}}"><i class="fa fa-pencil"></i> Editar</a>
									  <!-- <a href="{{route('pausar_tk',$ticket->id)}}" class="btn btn-outline-dark pausar_tk" data-toggle="tooltip" data-target="#pausar_tk" data-id="{{$ticket->id}}" title="Pausar"><i class="fa fa-pause"></i></a> -->
									@endif
									@if($ticket->estado == 10)
										<a href="{{route('play_tk',$ticket->id)}}" class="dropdown-item play_tk" data-target="#play_tk" data-id="{{$ticket->id}}"><i class="fa fa-play"></i> Continuar</a>
									@endif
									@if( $ticket->estado==3)
										<a href="#" class="dropdown-item validarc" data-toggle="modal" data-target="#observacion_tiket" data-id="{{$ticket->id}}"><i class="fa fa-check"></i> Culminar</a>
									@endif
									@if( $ticket->estado == 5)
										<a href="{{route('editar_ticket',$ticket->id)}}" class="dropdown-item editar" data-id="{{$ticket->id}}"><i class="fa fa-pencil"></i> Editar</a>
										<a href="{{route('eliminar',$ticket->id)}}" class="dropdown-item validar"><i class="fa fa-trash-o"></i> Eliminar</a>
									@endif
								  </div>
								</div>
                            </td>
                        </tr>
                      @else
                        <tr id="{{$ticket->id}}">
                              <td scope="row">
                                <?php
                                  $var = $ticket->fecha_inicio;
                                  if($var){
                                    $fechari = date("d/m H:i", strtotime($var) );
                                    echo $fechari;
                                  }
                                  else{

                                  }
                                ?>
                                {{--$ticket->fecha_inicio--}}
                              </td>
                              <td>
                                <?php
                                  $var = $ticket->fecha_fin;
                                  if($var){
                                    $fechari = date("d/m H:i", strtotime($var) );
                                    echo $fechari;
                                  }
                                ?>
                                {{--$ticket->fecha_fin--}}
                              </td>
							  @if($ticket->cliente)
                                @if(\Auth::user()->tipo == 'gerente')
                                 <td>{{$ticket->cliente->nombre}}</td>
                                @else
                                 <td>{{$ticket->cliente}}</td>
                                @endif
                              @else
                                  <td>--</td>
                              @endif
                              <td>{{$ticket->nombre}}</td>
							  <td>
                              @foreach($users as $uu)
                                @if($uu->id == $ticket->user_create)
                                  {{$uu->name}}                            
                                @endif
                              @endforeach
                              </td>
                               <td>
                              {{--@foreach($colaboradores as $cc)
                                @if($cc->id == $ticket->user_id)
                                {{ $cc->name }}                              
                                @endif
                              @endforeach --}}
                              @foreach($users as $uu)
                                @if($uu->id == $ticket->user_id)
                                  {{$uu->name}}                            
                                @endif
                              @endforeach
                              </td>
                              
                              @if(\Auth::user()->tipo == 'ejecutiva')
                                <td>
                                    @foreach($areas as $area)
                                      @if($area->id == $ticket->area_id)
                                        {{ $area->name }}
                                      @endif
                                    @endforeach
                                </td>
                              @else
                              <?php $ddi = $ticket->narea($ticket->area_id)?>
                              <td>{{ $ddi["name"]}}</td>
                              @endif
                              <td>
								@if($ticket->estado == 0 )
									<span class="tck-estado tck-sin-asignar">Sin asignar</span>
								@elseif($ticket->estado == 1 )
									<span class="tck-estado tck-proceso">En proceso</span>
								@elseif($ticket->estado == 2 )
									<span class="tck-estado tck-culminado">Culminado</span>
								@elseif($ticket->estado == 3 )
									<span class="tck-estado tck-atrazado">Atrazado</span>
								@elseif($ticket->estado == 4 )
									<span class="tck-estado tck-aprobado">Aprobado</span>
								@elseif($ticket->estado == 5 )
									<span class="tck-estado tck-asignado">Pendiente</span>
								@endif
							  </td>
                              <td>
								<div class="dropdown">
								  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
									<a class="dropdown-item" href="{{route('detalle_ticket',$ticket->id)}}"><i class="fa fa-eye"></i> Ver</a>
									@if((\Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente') == 'ejecutiva' && ($ticket->estado == 1 || $ticket->estado == 3))
									<a href="#" class="dropdown-item validarc" data-toggle="modal" data-target="#observacion_tiket" data-id="{{$ticket->id}}"><i class="fa fa-check"></i> Culminar</a>
									@endif
									@if($ticket->estado == 2 && (\Auth::user()->tipo == 'ejecutiva' || \Auth::user()->tipo == 'gerente'))
									  <a href="#" class="dropdown-item validar" data-toggle="modal" data-target="#validar_tiket" data-id="{{$ticket->id}}"><i class="fa fa-smile-o"></i> Validar</a>
									@endif
									@if($ticket->estado == 0 || $ticket->estado == 5)
									  <a href="{{route('editar_ticket',$ticket->id)}}" class="dropdown-item editar" data-id="{{$ticket->id}}"><i class="fa fa-pencil"></i> Editar</a>
									  <a href="{{route('eliminar',$ticket->id)}}" class="dropdown-item validar"><i class="fa fa-trash-o"></i> Eliminar</a>
									@endif
								  </div>
								</div>
                              </td>
                        </tr>
                      @endif
                    @endforeach
                  </tbody>
            </table>
            <div class="row">
                <div class="col-lg-12">
                    <div class="container">
                      <center>{{$tickets}}</center>
                    </div>
                </div>   
            </div>
        </div>
    </div>
</div>
@endsection

@section('content_extras')
    <!-- Modal -->
    <div class="modal fade" id="validar_tiket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <h4>¿Se cumplió con los requisitos?</h4>
            <form class="form-horizontal" method="POST" action="{{ route('validar',2) }}">
              {{ csrf_field() }}
              <input type="hidden" value="" name="idvalidar" id="idvalidar">
              <button class="btn btn-primary yes" >Si</button>
              <a href="javascript:void(0)" class="btn btn-secondary" data-dismiss="modal">No estoy seguro</a>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="observacion_tiket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Motivos del retraso</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <form class="form-horizontal" method="POST" action="{{ route('culminarticket') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="exampleInputEmail1">Observación</label>
                <input type="hidden" value="" name="ticketuser" id="ticketuser">
                <textarea class="form-control" name="observacion" rows="4" required></textarea>
              </div>
              <button class="btn btn-primary">Culminar</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="asignar_u" tabindex="-1" role="dialog"  aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Cambiar Asignado</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <form class="form-horizontal" method="POST" action="{{ route('postcolaborador') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="exampleInputEmail1">Colaborador:</label>
                <div class="getcolaborador">
                  <select name="iduserc" class="form-control" id="">
                    <option value=""></option>
                    @foreach($getcolaboradores as $getcolaborador)
                      <option value="{{$getcolaborador->id}}">{{$getcolaborador->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
              <input type="hidden" id="get_asignar" name="get_asignar" value="">
              <button class="btn btn-primary">Asignar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <script>
    $(function() {
      $('input[name="data_colaborador"]').daterangepicker({
        opens: 'left',
        linkedCalendars: false,
        locale: {
          format: 'YYYY-MM-DD'
        }
      }, function(start, end, label) {
        console.log("A new date selection was made: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
      });
    });

    $('.g-tooltip').mouseover(function(){
      //$(this).toggle();
    });
    $(".refres").click(function(){
      location.href ="/historial";
    });
    $('.validar').click(function(){
      var id = $(this).attr("data-id");
      $("#idvalidar").val(id);
    });
    $(".validarc").click(function(){  
      var id = $(this).attr('data-id');
      $("#ticketuser").val(id);
      // $("#observacion_tiket form").attr('action', 'culminarticket');
      $("textarea[name=observacion]").val("");
    });
    $(".asignar_u").click(function(){  
      var id = $(this).attr('data-id');
      $("#get_asignar").val(id);
      // $("#observacion_tiket form").attr('action', 'culminarticket');
      $("textarea[name=observacion]").val("");
    });
    $("#filtro .btn-filtro").click(function(){
      $("#filtro form").removeClass('active');
      $("#filtro a").removeClass('act');
      $(this).addClass('act');
      var valor = $(this).data('id');
      $("#"+valor).addClass('active');
    });

    $("#form_filter").submit(function(e){
      e.preventDefault();
      var url = $("#form_filter #data_url_host").val();
      var tipo = $("#form_filter #data_tipo").val();
      var id = $("#form_filter #data_colaborador").val();
      window.location.href = url+'/historial/'+tipo+'/'+id;
    });

    $("#form_filter2").submit(function(e){
      e.preventDefault();
      var url = $("#form_filter2 #data_url_host").val();
      var tipo = $("#form_filter2 #data_tipo").val();
      var id = $("#form_filter2 #data_colaborador").val();
      window.location.href = url+'/historial/'+tipo+'/'+id;
    });

    $("#form_filter3").submit(function(e){
      e.preventDefault();
      var url = $("#form_filter3 #data_url_host").val();
      var tipo = $("#form_filter3 #data_tipo").val();
      var id = $("#form_filter3 #data_colaborador").val();
      window.location.href = url+'/historial/'+tipo+'/'+id;
    });
    
    $('#m1').addClass('active');

  </script>
@endsection
@extends('layout.app')
@section('titulo','Completar Tickets')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" href="{{ asset('css/asistencia.css') }}">

	<style>
		@media  (max-width: 772px) {
			.responsive_row {
				margin-top: 50px !important;
				text-align: center !important;
			}
		}
	</style>
@endsection
@section('content')
 
<div class="col main-content">
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="detalle_tiket">
				<div class="row responsive_row" >

					<div class="col-12 col-md-12 text-center ">
						<h1 class="main-title ">MI Tickets</h1>
						<p class="title-description">Completa tus tickets para ser el ganador del mes y darle puntos a tu tribu.
						</p>
					</div>
					<div class="col-md-6 ">
						Equipo:
						<form>
							{{  csrf_field() }}
							<select  class="form-control col-md-10" name="iduser" id="trabajador">
									<option></option>
								@foreach($colaboradores as $colaborador)
									<option value="{{ $colaborador->id }}">{{ $colaborador->name }}</option>
								@endforeach
							</select>
							<br>
							<a href="javascript:void(0)" id="urlh" class="btn btn-primary btn-custom">Cargar historial</a>
						</form>
					</div>
					<div class="col-md-6 " style="@php
								if (\Auth::user()->tipo == 'colaborador' || \Auth::user()->tipo == 'jefe') {
								echo "display:none" ;
								}
							@endphp">
						Tribu:
						<form id="myFormTribu">



									<select name=""  class="form-control " id="tribu">
										@foreach($allTribus as $tribu)
											<option value="{{$tribu->Id}}">  {{$tribu->nombre}} </option>

										@endforeach
									</select>
							<input type="hidden" name="date" value="@php
									echo date('Y-m-d');
									@endphp">


							<br>
									<button  type="submit"  class="btn btn-primary btn-custom ">Ver tickets</button>


						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('content_extras')
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function(){
		$("#trabajador").change(function(e){
			var data= $("#trabajador").val();
			var urlg = 'ticket_historial/'+data;
			$("#urlh").attr('href',urlg);
		});

	});
	

    $('#m9').addClass('active');

    document.getElementById('myFormTribu').addEventListener('submit', e => {
        e.preventDefault()
		let tribu = e.target.tribu.value,
			date = e.target.date.value

		location.href = `/showTasks/${tribu}/${date}`
	})
</script>
@endsection
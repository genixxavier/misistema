@extends('layout.app')
@section('titulo','Completar Tickets')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">
<style>





.tablahoras table tr th {
	border-bottom: 1px solid white;
}
		.style_depends_screen {
			display: none;
		}
		.flag {

		display: flex !important;
		/*align-items: baseline !important;*/
		flex-wrap: nowrap !important;
		overflow-x: auto;
	 /* margin-top:5px ; */
		}
		a.btn.btn-outline-dark.popup_editar {
		    position: absolute;
		    top: 0;
		    left: 5px;
		    padding: 1px 7px !important;
		    display: inline-block;
		    border: 0px;
		}

		@media  (max-width: 992px) {
			.tablahoras table tr .h-active h2 {
				font-size: 12px;
			}
			.flag_2 , .flag_2 tbody tr th, .flag_2 tbody tr td  {
				width: 350px;
			}
			.first_child {
				padding: 0;
			}
			.leyenda {
				text-align: center;
			}

		.center_div {
			text-align: center !important;
			margin-top: 10px;
		}
		.align_button {
			text-align: center;
		}
		.flag_table {
			width: 120px !important;
		}
		.flag_table tbody tr th  {
			width: 120px;
		}
		}


		.mitabla tbody tr:nth-child(n + 1 ) {
			height: 70px !important;
		}

.horas-2 {
	/*height: 92px;*/
	height: 140px;
}

.horas-3 {
	/* height: 138px;*/
	height: 210px;
}

.horas-4 {
	/*height: 184px;*/
	height: 280px;
}

.horas-5 {

	/*height: 230px;*/
	height: 350px;
}

.horas-1 {
	/*height: 46px;*/
	height: 70px;
	padding: 1px !important;
}
.main-title {
	font-size: 1.25rem;
}
</style>
@endsection
@section('content')

<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
			<div class="center_div">
					@if (session('status'))
					@if(session('status') == 'Creado exitosamente')
					<div class="alert alert-success alert-dismissible fade show" role="alert">
					  <strong>Éxito,</strong> Ticket creado.
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
					@endif
					@if(session('status') == 'existe')
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
					  <strong>Error,</strong> las horas puestas en el ticket estan ocupadas por otro ticket, verificar horas del pedido.
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
					@endif
					@if(session('status') == 'excedio')
					<div class="alert alert-warning alert-dismissible fade show" role="alert">
					  <strong>Error,</strong> excedió las horas del día en el ticket.
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					    <span aria-hidden="true">&times;</span>
					  </button>
					</div>
					@endif
					@if(session('status') == 'excedio_extra')
						<div class="alert alert-warning alert-dismissible fade show" role="alert">
							<strong>Error,</strong> excedió las horas extras del día en el ticket.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
					@endif
					@if(session('status') == 'existe_extra')
						<div class="alert alert-danger alert-dismissible fade show" role="alert">
							<strong>Error,</strong> las horas puestas en el ticket extra estan ocupadas por otro ticket, verificar horas del pedido.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
					@endif
					@if(session('status') == 'add_extra')
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							<strong>Éxito,</strong> Ticket extra creado.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
					@endif
					@if(session('status') == 'eliminado')
						<div class="alert alert-success alert-dismissible fade show" role="alert">
							<strong>Éxito,</strong> Ticket eliminado.
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						</div>
					@endif
				@endif
			</div>
				<div class="row">
					<div class="col-md-6 align_button">
						<p><a href="{{ route('ticketblank') }}" class="btn btn-primary btn-custom">Atrás</a></p>
					</div>
					<div class="col-md-6 text-right">
						<div class="leyenda">
							<p><span class="alert_falto"></span> Faltó / Feriado</p>
							<p><span class="alert_vaciones"></span> Vacaciones</p>
							<p><span class="alert_justificacion"></span> Permiso</p>
						</div>
					</div>
					<div class="col-md-12 b-box">
						<?php
							$dia = date("d-m-Y",strtotime($semanaactual));
			        $dia = date("w",strtotime($dia));//5
			        $diax = $diaxx;//224
			        if($dia > 1){
								$restar = ($dia - 1);
								// preguntamos si es 31
								$gddd = strtotime ( '-'.$restar.' day' , strtotime ( $semanaactual ) ) ;
								$mesantt = date ( 'd-m-Y' , $gddd );
								$diax = date ( 'd' , $gddd );

								// if($diax == 1){
								// 	// preguntamos si es 31
								// 	$gddd = strtotime ( '-1 day' , strtotime ( $semanaactual ) ) ;
								// 	$mesantt = date ( 'd-m-Y' , $gddd );
								// 	$gddd = date ( 'd' , $gddd );

								// 	$diax = $gddd - ($dia-2);
								// }
								// else{
								// 	$diax = $diax - ($dia - 1);
								// }
				        }
				        else{
				            $diax = $diax;
				        }
				        if($diax < 10){
				        	$diax = $diax;
						}

						if(isset($mesantt)){
							$fi = date('Y-m',strtotime($mesantt));
						}
						else{
							$fi = date('Y-m',strtotime($semanaactual));
						}
						$flunes = $fi.'-'.$diax;

				        $fmartes = strtotime ( '+1 day' , strtotime ( $flunes ) ) ;
        				$fmartes = date ( 'Y-m-d' , $fmartes );

				        $fmiercoles = strtotime ( '+2 day' , strtotime ( $flunes ) ) ;
        				$fmiercoles = date ( 'Y-m-d' , $fmiercoles );

				        $fjueves = strtotime ( '+3 day' , strtotime ( $flunes ) ) ;
        				$fjueves = date ( 'Y-m-d' , $fjueves );

				        $fviernes = strtotime ( '+4 day' , strtotime ( $flunes ) ) ;
        				$fviernes = date ( 'Y-m-d' , $fviernes );

        				$semana_ant = strtotime ( '-7 day' , strtotime ( $flunes ) ) ;
        				$semana_ant = date ( 'Y-m-d' , $semana_ant );

        				$semana_sig = strtotime ( '+3 day' , strtotime ( $fviernes ) ) ;
        				$semana_sig = date ( 'Y-m-d' , $semana_sig );


        				$dia7 = date("d-m-Y");
				        $dia7 = date("w",strtotime($dia7));//5
				        $diax7 = date('d');//24
				        if($dia7 > 1){
				           $diax7 = $diax7 - ($dia7 - 1);
				        }
				        else{
				            $diax7 = $diax7;
				        }
				        if($diax7 < 10){
				        	$diax7 = '0'.$diax7;
				        }
				        $fi7 = date('Y-m-'.$diax7);
						$fecha_actual = date("Y-m-d");
						$fmin = $fi7;
						$fmax = $fecha_actual;
						//echo $fmin;
						$loquefalta = 0;
						?>


						<p class="text-center"><span class="main-title">Tickets: {{ $user->name }}   </span> <span style="opacity: 0;">       asdsa</span><small>   {{date("d/m/Y",strtotime($flunes))}} - {{date ( 'd/m/Y' , strtotime($fviernes) )}}</small></p>
						<div class="row">
							<div class="col-12 col-md-6  center_div"><a href="/ticket_historial/{{$user->id}}/{{ $semana_ant }}" class="btn btn-tabs btn-custom"><i class="fa fa-caret-left"></i> Semana ant.</a></div>
							@if($semana_sig <= $fecha_actual)
							<div class="col-12 col-md-6  text-right center_div "><a href="/ticket_historial/{{$user->id}}/{{ $semana_sig }}" class="btn btn-tabs btn-custom">Semana sig. <i class="fa fa-caret-right"></i></a></div>
							@endif
						</div>
						<div class="row tablahoras flag ">
								<div class="col-md-2 first_child" style="padding:0">
									<table class="table table-bordered  flag_table   table-responsive-lg mitabla bgprim text-center">
										<tr>
											<th>
												DÍA
												<p><small>Horas</small></p>
											</th>
										</tr>
										<tr>
											<td>09:00 - 10:00</td>
										</tr>
										<tr>
											<td>10:00 - 11:00</td>
										</tr>
										<tr>
											<td>11:00 - 12:00</td>
										</tr>
										<tr>
											<td>12:00 - 1:00</td>
										</tr>
										<tr>
											<td>1:00 - 2:00</td>
										</tr>
										<tr>
											<td>2:00 - 3:00</td>
										</tr>
										<tr>
											<td>3:00 - 4:00</td>
										</tr>
										<tr>
											<td>4:00 - 5:00</td>
										</tr>
										<tr>
											<td>5:00 - 6:00</td>
										</tr>
										<tr>
											<td>6:00 - 7:00</td>
										</tr>
									</table>
								</div>

								<div class="col-md-2 " style="padding: 0px">
									<table class="table table-bordered flag_2 table-responsive-lg mitabla  {{$asistencia->verificar_asistencia($flunes, $user->id)}}">
										<tr>
											<th>
												Lunes
												<p><small>{{ date("d/m/Y", strtotime($flunes)) }}</small></p>
											</th>
										</tr>
										<?php $ii=9; ?>
										@if (count($lunesbefore) > 0)
											<?php
												$horaf = date("H", strtotime($lunesbefore[0]->fecha_fin));
												$dd = $horaf-9;
												$ii = $horaf;
												$dant = 0;
												$dnext = 0;
											?>
											@if($horaf > 13)
												@for($i=9;$i< $horaf;$i++)
													@if($i == 13)
														<?php
															$dnext = $dd - ($dant+1);
															break;
														?>
													@else
														<?php
															$dant = $dant + 1;
														?>
													@endif
												@endfor
												<tr>
													<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
														<h2>{{$lunesbefore[0]->nombre}}</h2>
														<p>{{$lunesbefore[0]->cliente}}</p>
													</td>
												</tr>
												<tr>
													<td class="text-center bg-gris">
															ALMUERZO
													</td>
												</tr>
												<tr>
													<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
														<h2>{{$lunesbefore[0]->nombre}}</h2>
														<p>{{$lunesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@else
												<tr>
													<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
														<h2>{{$lunesbefore[0]->nombre}}</h2>
														<p>{{$lunesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@endif
										@endif
										@if(count($lunes) > 0)
											@foreach($lunes as $tik)
												@for($i=$ii;$i<=19;$i++)
													<?php
														$gfecha = date("Y-m-d", strtotime($tik->fecha_inicio));
														$gfechafin = date("Y-m-d", strtotime($tik->fecha_fin));
														$hora = date("H", strtotime($tik->fecha_inicio));
														$horaf = date("H", strtotime($tik->fecha_fin));
														$dd = $horaf-$hora;
														$dant = 0;
														$dnext = 0;
													?>
													@if($i == $hora)
														{{-- comparamos si la fecha fin es de otro dia --}}
														@if($gfechafin > $flunes)
															<?php
																$horaf = 19;
																$dd = $horaf-$hora;
															?>
															@if($hora < 13 and $horaf > 13)
																@for($i=$hora;$i< $horaf;$i++)
																	@if($i == 13)
																		<?php
																			$dnext = $dd - ($dant+1);
																			break;
																		?>
																	@else
																		<?php
																			$dant = $dant + 1;
																		?>
																	@endif
																@endfor
																<tr>
																	<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
																<tr>
																	<td class="text-center bg-gris">
																			ALMUERZO
																	</td>
																</tr>
																<tr>
																	<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
															@else
																<tr>
																	<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
															@endif
														@else
															@if($hora < 13 and $horaf > 13)
																@for($i=$hora;$i< $horaf;$i++)
																	@if($i == 13)
																		<?php
																			$dnext = $dd - ($dant+1);
																			break;
																		?>
																	@else
																		<?php
																			$dant = $dant + 1;
																		?>
																	@endif
																@endfor
																<tr>
																	<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
																<tr>
																	<td class="text-center bg-gris">
																			ALMUERZO
																	</td>
																</tr>
																<tr>
																	<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
															@else
																<tr>
																	<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
															@endif
														@endif
														{{-- end comparacion --}}
														<?php
															$ii = $ii + $dd;
															break;
														?>
													@elseif($i == 13)
														<tr>
															<td class="text-center bg-gris">
																ALMUERZO
															</td>
														</tr>
													@else
														<?php
														$gfecha = $gfecha." ".$i.":00:00";
														?>
														<tr>
															<?php $flunes = date("Y-m-d", strtotime($flunes));?>
															{{-- @if( ($flunes >= $fmin) and ($flunes<=$fmax) ) --}}
															@if( $flunes<=$fmax )
															<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
															@else
															<td class="text-center"> - </td>
															@endif
														</tr>
													@endif
													<?php $ii++;?>
												@endfor
											@endforeach
											@if($ii <= 18)
												@for($i=$ii;$i<=18;$i++)
													@if($i == 13)
													<tr>
														<td class="text-center bg-gris">
															ALMUERZO
														</td>
													</tr>
													@else
													<tr>
														<?php $gfecha = $flunes." ".$i.":00:00";?>
														<?php $flunes = date("Y-m-d", strtotime($flunes));?>
														{{-- @if( ($flunes >= $fmin) and ($flunes<=$fmax) ) --}}
														@if( $flunes<=$fmax )
														<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
														@else
														<td class="text-center"> - </td>
														@endif
													</tr>
													@endif
												@endfor
											@endif
										@else
											@for($i=$ii;$i<=18;$i++)
												@if($i == 13)
												<tr>
													<td class="text-center bg-gris">
														ALMUERZO
													</td>
												</tr>
												@else
												<tr>
													<?php $gfecha = $flunes." ".$i.":00:00";?>
													{{-- @if( ($flunes >= $fmin) and ($flunes<=$fmax) ) --}}
													@if( $flunes<=$fmax )
													<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
													@else
													<td class="text-center"> - </td>
													@endif
												</tr>
												@endif
											@endfor
										@endif
									</table>
								</div>

								<div class="col-md-2 " style="padding: 0px">
									<table class="table table-bordered flag_2  table-responsive-lg mitabla {{$asistencia->verificar_asistencia($fmartes,$user->id)}}">
										<tr>
											<th>
												Martes
												<p><small>{{ date("d/m/Y", strtotime($fmartes)) }}</small></p>
											</th>
										</tr>
										<?php $ii=9; ?>
										@if (count($martesbefore) > 0)
											<?php
												$horaf = date("H", strtotime($martesbefore[0]->fecha_fin));
												$dd = $horaf-9;	
												$ii = $horaf;
												$dant = 0;
												$dnext = 0;
											?>
											@if($horaf > 13)
												@for($i=9;$i< $horaf;$i++)
													@if($i == 13)
														<?php
															$dnext = $dd - ($dant+1);
															break;
														?>
													@else
														<?php
															$dant = $dant + 1;
														?>
													@endif
												@endfor
												<tr>
													<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
														<h2>{{$martesbefore[0]->nombre}}</h2>
														<p>{{$martesbefore[0]->cliente}}</p>
													</td>
												</tr>
												<tr>
													<td class="text-center bg-gris">
															ALMUERZO
													</td>
												</tr>
												<tr>
													<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
														<h2>{{$martesbefore[0]->nombre}}</h2>
														<p>{{$martesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@else
												<tr>
													<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
														<h2>{{$martesbefore[0]->nombre}}</h2>
														<p>{{$martesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@endif
										@endif
										@if(count($martes) > 0)
											@foreach($martes as $tik)
												@for($i=$ii;$i<=19;$i++)
													<?php
														$gfecha = date("Y-m-d", strtotime($tik->fecha_inicio));
														$gfechafin = date("Y-m-d", strtotime($tik->fecha_fin));
														$hora = date("H", strtotime($tik->fecha_inicio));
														$horaf = date("H", strtotime($tik->fecha_fin));
														$dd = $horaf-$hora;
														$dant = 0;
														$dnext = 0;
													?>
													@if($i == $hora)
														{{-- comparamos si la fecha fin es de otro dia --}}
															@if($gfechafin > $fmartes)
																<?php
																	$horaf = 19;
																	$dd = $horaf-$hora;
																?>
																@if($hora < 13 and $horaf > 13)
																	@for($i=$hora;$i< $horaf;$i++)
																		@if($i == 13)
																			<?php
																				$dnext = $dd - ($dant+1);
																				break;
																			?>
																		@else
																			<?php
																				$dant = $dant + 1;
																			?>
																		@endif
																	@endfor
																	<tr>
																		<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																			<h2>{{$tik->nombre}}</h2>
																			<p>{{$tik->cliente}}</p>
																			<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		</td>
																	</tr>
																	<tr>
																		<td class="text-center bg-gris">
																				ALMUERZO
																		</td>
																	</tr>
																	<tr>
																		<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																			<h2>{{$tik->nombre}}</h2>
																			<p>{{$tik->cliente}}</p>
																			<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		</td>
																	</tr>
																@else
																	<tr>
																		<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																			<h2>{{$tik->nombre}}</h2>
																			<p>{{$tik->cliente}}</p>
																			<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		</td>
																	</tr>
																@endif
															@else
																@if($hora < 13 and $horaf > 13)
																	@for($i=$hora;$i< $horaf;$i++)
																		@if($i == 13)
																			<?php
																				$dnext = $dd - ($dant+1);
																				break;
																			?>
																		@else
																			<?php
																				$dant = $dant + 1;
																			?>
																		@endif
																	@endfor
																	<tr>
																		<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																			<h2>{{$tik->nombre}}</h2>
																			<p>{{$tik->cliente}}</p>
																			<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																			<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																		</td>
																	</tr>
																	<tr>
																		<td class="text-center bg-gris">
																				ALMUERZO
																		</td>
																	</tr>
																	<tr>
																		<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																			<h2>{{$tik->nombre}}</h2>
																			<p>{{$tik->cliente}}</p>
																			<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																			<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																		</td>
																	</tr>
																@else
																	<tr>
																		<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																			<h2>{{$tik->nombre}}</h2>
																			<p>{{$tik->cliente}}</p>
																			<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																			<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																		</td>
																	</tr>
																@endif
															@endif
														{{-- end comparacion --}}
													<?php
														$ii = $ii + $dd;
														break;
													?>
													@elseif($i == 13)
													<tr>
														<td class="text-center bg-gris">
															ALMUERZO
														</td>
													</tr>
													@else
													<?php
													$gfecha = $gfecha." ".$i.":00:00";
													?>
													<tr>
														{{-- @if( ($fmartes >= $fmin) and ($fmartes<=$fmax) ) --}}
														@if($fmartes<=$fmax)
														<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
														@else
														<td class="text-center"> - </td>
														@endif
													</tr>
													@endif
													<?php $ii++;?>
												@endfor
											@endforeach
											@if($ii <= 18)
												@for($i=$ii;$i<=18;$i++)
													@if($i == 13)
													<tr>
														<td class="text-center bg-gris">
															ALMUERZO
														</td>
													</tr>
													@else
													<tr>
														<?php $gfecha = $fmartes." ".$i.":00:00";?>
														{{-- @if( ($fmartes >= $fmin) and ($fmartes<=$fmax) ) --}}
														@if($fmartes<=$fmax)
														<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
														@else
														<td class="text-center"> - </td>
														@endif
													</tr>
													@endif
												@endfor
											@endif
										@else
											@for($i=$ii;$i<=18;$i++)
												@if($i == 13)
												<tr>
													<td class="text-center bg-gris">
														ALMUERZO
													</td>
												</tr>
												@else
												<tr>
													<?php $gfecha = $fmartes." ".$i.":00:00";?>
													{{-- @if( ($fmartes >= $fmin) and ($fmartes<=$fmax) ) --}}
													@if($fmartes<=$fmax)
													<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
													@else
													<td class="text-center"> - </td>
													@endif
												</tr>
												@endif
											@endfor
										@endif
									</table>
								</div>

								<div class="col-md-2 " style="padding: 0px">
									<table class="table table-bordered flag_2  table-responsive-lg mitabla {{$asistencia->verificar_asistencia($fmiercoles,$user->id)}}">
										<tr>
											<th>
											Miércoles
											<p><small>{{ date("d/m/Y", strtotime($fmiercoles)) }}</small></p>
											</th>
										</tr>
										<?php $ii=9; ?>
										@if (count($miercolesbefore) > 0)
											<?php
												$horaf = date("H", strtotime($miercolesbefore[0]->fecha_fin));
												$dd = $horaf-9;
												$ii = $horaf;
												$dant = 0;
												$dnext = 0;
											?>
											@if($horaf > 13)
												@for($i=9;$i< $horaf;$i++)
													@if($i == 13)
														<?php
															$dnext = $dd - ($dant+1);
															break;
														?>
													@else
														<?php
															$dant = $dant + 1;
														?>
													@endif
												@endfor
												<tr>
													<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
														<h2>{{$miercolesbefore[0]->nombre}}</h2>
														<p>{{$miercolesbefore[0]->cliente}}</p>
													</td>
												</tr>
												<tr>
													<td class="text-center bg-gris">
															ALMUERZO
													</td>
												</tr>
												<tr>
													<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
														<h2>{{$miercolesbefore[0]->nombre}}</h2>
														<p>{{$miercolesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@else
												<tr>
													<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
														<h2>{{$miercolesbefore[0]->nombre}}</h2>
														<p>{{$miercolesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@endif
										@endif
										@if(count($miercoles) > 0)
											@foreach($miercoles as $tik)
												@for($i=$ii;$i<=19;$i++)
													<?php
														$gfecha = date("Y-m-d", strtotime($tik->fecha_inicio));
														$gfechafin = date("Y-m-d", strtotime($tik->fecha_fin));
														$hora = date("H", strtotime($tik->fecha_inicio));
														$horaf = date("H", strtotime($tik->fecha_fin));
														$dd = $horaf-$hora;
														$dant = 0;
														$dnext = 0;
													?>
													@if($i == $hora)
														{{-- comparamos si la fecha fin es de otro dia --}}
														@if($gfechafin > $fmiercoles)
															<?php
																$horaf = 19;
																$dd = $horaf-$hora;
															?>
															@if($hora < 13 and $horaf > 13)
																@for($i=$hora;$i< $horaf;$i++)
																	@if($i == 13)
																		<?php
																			$dnext = $dd - ($dant+1);
																			break;
																		?>
																	@else
																		<?php
																			$dant = $dant + 1;
																		?>
																	@endif
																@endfor
																<tr>
																	<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
																<tr>
																	<td class="text-center bg-gris">
																			ALMUERZO
																	</td>
																</tr>
																<tr>
																	<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
															@else
																<tr>
																	<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
															@endif
														@else
															@if($hora < 13 and $horaf > 13)
																@for($i=$hora;$i< $horaf;$i++)
																	@if($i == 13)
																		<?php
																			$dnext = $dd - ($dant+1);
																			break;
																		?>
																	@else
																		<?php
																			$dant = $dant + 1;
																		?>
																	@endif
																@endfor
																<tr>
																	<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
																<tr>
																	<td class="text-center bg-gris">
																			ALMUERZO
																	</td>
																</tr>
																<tr>
																	<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
															@else
																<tr>
																	<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
															@endif
														@endif
														{{-- end comparacion --}}
														<?php
															$ii = $ii + $dd;
															break;
														?>
													@elseif($i == 13)
													<tr>
														<td class="text-center bg-gris">
															ALMUERZO
														</td>
													</tr>
													@else
													<?php
													$gfecha = $gfecha." ".$i.":00:00";
													?>
													<tr>
														{{-- @if( ($fmiercoles >= $fmin) and ($fmiercoles<=$fmax) ) --}}
														@if($fmiercoles<=$fmax)
														<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
														@else
														<td class="text-center"> - </td>
														@endif
													</tr>
													@endif
													<?php $ii++;?>
												@endfor
											@endforeach
											@if($ii <= 18)
												@for($i=$ii;$i<=18;$i++)
													@if($i == 13)
													<tr>
														<td class="text-center bg-gris">
															ALMUERZO
														</td>
													</tr>
													@else
													<tr>
														<?php $gfecha = $fmiercoles." ".$i.":00:00";?>
														{{-- @if( ($fmiercoles >= $fmin) and ($fmiercoles<=$fmax) ) --}}
														@if($fmiercoles<=$fmax)
														<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
														@else
														<td class="text-center"> - </td>
														@endif
													</tr>
													@endif
												@endfor
											@endif
										@else
											@for($i=$ii;$i<=18;$i++)
												@if($i == 13)
												<tr>
													<td class="text-center bg-gris">
														ALMUERZO
													</td>
												</tr>
												@else
												<tr>
													<?php $gfecha = $fmiercoles." ".$i.":00:00";?>
													{{-- @if( ($fmiercoles >= $fmin) and ($fmiercoles<=$fmax) ) --}}
													@if($fmiercoles<=$fmax)
													<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
													@else
													<td class="text-center"> - </td>
													@endif
												</tr>
												@endif
											@endfor
										@endif
									</table>
								</div>

								<div class="col-md-2  " style="padding: 0px">
									<table class="table table-bordered flag_2 table-responsive-lg mitabla {{$asistencia->verificar_asistencia($fjueves,$user->id)}}">
										<tr>
											<th>
												Jueves
												<p><small>{{ date("d/m/Y", strtotime($fjueves)) }}</small></p>
											</th>
										</tr>
										<?php $ii=9; ?>
										@if (count($juevesbefore) > 0)
											<?php
												$horaf = date("H", strtotime($juevesbefore[0]->fecha_fin));
												$dd = $horaf-9;
												$ii = $horaf;
												$dant = 0;
												$dnext = 0;
											?>
											@if($horaf > 13)
												@for($i=9;$i< $horaf;$i++)
													@if($i == 13)
														<?php
															$dnext = $dd - ($dant+1);
															break;
														?>
													@else
														<?php
															$dant = $dant + 1;
														?>
													@endif
												@endfor
												<tr>
													<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
														<h2>{{$juevesbefore[0]->nombre}}</h2>
														<p>{{$juevesbefore[0]->cliente}}</p>
													</td>
												</tr>
												<tr>
													<td class="text-center bg-gris">
															ALMUERZO
													</td>
												</tr>
												<tr>
													<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
														<h2>{{$juevesbefore[0]->nombre}}</h2>
														<p>{{$juevesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@else
												<tr>
													<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
														<h2>{{$juevesbefore[0]->nombre}}</h2>
														<p>{{$juevesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@endif
										@endif
										@if(count($jueves) > 0)
											@foreach($jueves as $tik)
												@for($i=$ii;$i<=19;$i++)
													<?php
														$gfecha = date("Y-m-d", strtotime($tik->fecha_inicio));
														$gfechafin = date("Y-m-d", strtotime($tik->fecha_fin));
														$hora = date("H", strtotime($tik->fecha_inicio));
														$horaf = date("H", strtotime($tik->fecha_fin));
														$dd = $horaf-$hora;
														$dant = 0;
														$dnext = 0;
													?>
													@if($i == $hora)
														{{-- comparamos si la fecha fin es de otro dia --}}
														@if($gfechafin > $fjueves)
															<?php
																$horaf = 19;
																$dd = $horaf-$hora;
															?>
															@if($hora < 13 and $horaf > 13)
																@for($i=$hora;$i< $horaf;$i++)
																	@if($i == 13)
																		<?php
																			$dnext = $dd - ($dant+1);
																			break;
																		?>
																	@else
																		<?php
																			$dant = $dant + 1;
																		?>
																	@endif
																@endfor
																<tr>
																	<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
																<tr>
																	<td class="text-center bg-gris">
																			ALMUERZO
																	</td>
																</tr>
																<tr>
																	<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
															@else
																<tr>
																	<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
															@endif
														@else
															@if($hora < 13 and $horaf > 13)
																@for($i=$hora;$i< $horaf;$i++)
																	@if($i == 13)
																		<?php
																			$dnext = $dd - ($dant+1);
																			break;
																		?>
																	@else
																		<?php
																			$dant = $dant + 1;
																		?>
																	@endif
																@endfor
																<tr>
																	<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
																<tr>
																	<td class="text-center bg-gris">
																			ALMUERZO
																	</td>
																</tr>
																<tr>
																	<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
															@else
																<tr>
																	<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
															@endif
														@endif
														{{-- end comparacion --}}
														<?php
															$ii = $ii + $dd;
															break;
														?>
													@elseif($i == 13)
													<tr>
														<td class="text-center bg-gris">
															ALMUERZO
														</td>
													</tr>
													@else
													<?php
													$gfecha = $gfecha." ".$i.":00:00";
													?>
													<tr>
														{{-- @if( ($fjueves >= $fmin) and ($fjueves<=$fmax) ) --}}
														@if($fjueves<=$fmax)
														<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
														@else
														<td class="text-center"> - </td>
														@endif
													</tr>
													@endif
													<?php $ii++;?>
												@endfor
											@endforeach
											@if($ii <= 18)
												@for($i=$ii;$i<=18;$i++)
													@if($i == 13)
													<tr>
														<td class="text-center bg-gris">
															ALMUERZO
														</td>
													</tr>
													@else
													<tr>
														<?php $gfecha = $fjueves." ".$i.":00:00";?>
														{{-- @if( ($fjueves >= $fmin) and ($fjueves<=$fmax) ) --}}
														@if($fjueves<=$fmax)
														<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
														@else
														<td class="text-center"> - </td>
														@endif
													</tr>
													@endif
												@endfor
											@endif
										@else
											@for($i=$ii;$i<=18;$i++)
												@if($i == 13)
												<tr>
													<td class="text-center bg-gris">
														ALMUERZO
													</td>
												</tr>
												@else
												<tr>
													<?php $gfecha = $fjueves." ".$i.":00:00";?>
													{{-- @if( ($fjueves >= $fmin) and ($fjueves<=$fmax) ) --}}
													@if($fjueves<=$fmax)
													<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
													@else
													<td class="text-center"> - </td>
													@endif
												</tr>
												@endif
											@endfor
										@endif
									</table>
								</div>

								<div class="col-md-2 " style="padding: 0px">
									<table class="table table-bordered flag_2 table-responsive-lg mitabla {{$asistencia->verificar_asistencia($fviernes,$user->id)}}">
										<tr>
											<th>
												Viernes
												<p><small>{{ date("d/m/Y", strtotime($fviernes)) }}</small></p>
											</th>
										</tr>
										<?php $ii=9; ?>
										@if (count($viernesbefore) > 0)
											<?php
												$horaf = date("H", strtotime($viernesbefore[0]->fecha_fin));
												$dd = $horaf-9;
												$ii = $horaf;
												$dant = 0;
												$dnext = 0;
											?>
											@if($horaf > 13)
												@for($i=9;$i< $horaf;$i++)
													@if($i == 13)
														<?php
															$dnext = $dd - ($dant+1);
															break;
														?>
													@else
														<?php
															$dant = $dant + 1;
														?>
													@endif
												@endfor
												<tr>
													<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
														<h2>{{$viernesbefore[0]->nombre}}</h2>
														<p>{{$viernesbefore[0]->cliente}}</p>
													</td>
												</tr>
												<tr>
													<td class="text-center bg-gris">
															ALMUERZO
													</td>
												</tr>
												<tr>
													<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
														<h2>{{$viernesbefore[0]->nombre}}</h2>
														<p>{{$viernesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@else
												<tr>
													<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
														<h2>{{$viernesbefore[0]->nombre}}</h2>
														<p>{{$viernesbefore[0]->cliente}}</p>
													</td>
												</tr>
											@endif
										@endif
										@if(count($viernes) > 0)
											@foreach($viernes as $tik)
												@for($i=$ii;$i<=19;$i++)
													<?php
														$gfecha = date("Y-m-d", strtotime($tik->fecha_inicio));
														$gfechafin = date("Y-m-d", strtotime($tik->fecha_fin));
														$hora = date("H", strtotime($tik->fecha_inicio));
														$horaf = date("H", strtotime($tik->fecha_fin));
														$dd = $horaf-$hora;
														$dant = 0;
														$dnext = 0;
													?>
													@if($i == $hora)
														{{-- comparamos si la fecha fin es de otro dia --}}
														@if($gfechafin > $fviernes)
															<?php
																$horaf = 19;
																$dd = $horaf-$hora;
															?>
															@if($hora < 13 and $horaf > 13)
																@for($i=$hora;$i< $horaf;$i++)
																	@if($i == 13)
																		<?php
																			$dnext = $dd - ($dant+1);
																			break;
																		?>
																	@else
																		<?php
																			$dant = $dant + 1;
																		?>
																	@endif
																@endfor
																<tr>
																	<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<!-- <a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a> -->
																	</td>
																</tr>
																<tr>
																	<td class="text-center bg-gris">
																			ALMUERZO
																	</td>
																</tr>
																<tr>
																	<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
															@else
																<tr>
																	<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																	</td>
																</tr>
															@endif
														@else
															@if($hora < 13 and $horaf > 13)
																@for($i=$hora;$i< $horaf;$i++)
																	@if($i == 13)
																		<?php
																			$dnext = $dd - ($dant+1);
																			break;
																		?>
																	@else
																		<?php
																			$dant = $dant + 1;
																		?>
																	@endif
																@endfor
																<tr>
																	<td colspan="{{ $dant }}" class="horas-{{ $dant }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
																<tr>
																	<td class="text-center bg-gris">
																			ALMUERZO
																	</td>
																</tr>
																<tr>
																	<td colspan="{{ $dnext }}" class="horas-{{ $dnext }} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
															@else
																<tr>
																	<td colspan="{{ $dd }}" class="horas-{{$dd}} h-active">
																		<h2>{{$tik->nombre}}</h2>
																		<p>{{$tik->cliente}}</p>
																		<a href="{{route('eliminar',$tik->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
																		<a href="javascript:void(0)" data-id="{{$tik->id}}" class="btn btn-outline-dark popup_editar" data-toggle="modal" data-target="#popup_editar"  title="Editar"><i class="fas fa-edit"></i></a>
																	</td>
																</tr>
															@endif
														@endif
														{{-- end comparacion --}}
														<?php
															$ii = $ii + $dd;
															break;
														?>
													@elseif($i == 13)
													<tr>
														<td class="text-center bg-gris">
															ALMUERZO
														</td>
													</tr>
													@else
													<?php
													$gfecha = $gfecha." ".$i.":00:00";
													?>
													<tr>
														{{-- @if( ($fviernes >= $fmin) and ($fviernes<=$fmax) ) --}}
														@if($fviernes<=$fmax)
														<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
														@else
														<td class="text-center"> - </td>
														@endif
													</tr>
													@endif
													<?php $ii++;?>
												@endfor
											@endforeach
											@if($ii <= 18)
												@for($i=$ii;$i<=18;$i++)
													@if($i == 13)
													<tr>
														<td class="text-center bg-gris">
															ALMUERZO
														</td>
													</tr>
													@else
													<tr>
														<?php $gfecha = $fviernes." ".$i.":00:00";?>
														{{-- @if( ($fviernes >= $fmin) and ($fviernes<=$fmax) ) --}}
														@if($fviernes<=$fmax)
														<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
														@else
														<td class="text-center"> - </td>
														@endif
													</tr>
													@endif
												@endfor
											@endif
										@else
											@for($i=$ii;$i<=18;$i++)
												@if($i == 13)
												<tr>
													<td class="text-center bg-gris">
														ALMUERZO
													</td>
												</tr>
												@else
												<tr>
													<?php $gfecha = $fviernes." ".$i.":00:00";?>
													{{-- @if( ($fviernes >= $fmin) and ($fviernes<=$fmax) ) --}}
													@if($fviernes<=$fmax)
													<td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
													@else
													<td class="text-center"> - </td>
													@endif
												</tr>
												@endif
											@endfor
										@endif
									</table>
								</div>
							</div>


                        <div class="col-lg-12 text-center">
                                <h4 style="margin-bottom:0px;display:none">Horas extra</h4>
                        </div>

                                <div class="row tablahoras flag">

                                    <div class="col-md-2 first_child" style="padding: 0px">
                                        <table class="table table-bordered flag_table table-responsive-lg mitabla bgprim text-center">
                                                <tr class="style_depends_screen" id="style_depends_screen">
                                                        <th>
                                                            DÍA
                                                            <p><small>Horas</small></p>
                                                        </th>
                                                    </tr>
                                            <tr>
                                                <td>7:00 - 8:00</td>
                                            </tr>
                                            <tr>
                                                <td>8:00 - 9:00</td>
                                            </tr>
                                            <tr>
                                                <td>9:00 - 10:00</td>
                                            </tr>
                                            <tr>
                                                <td>10:00 - 11:00</td>
                                            </tr>
                                            <tr>
                                                <td>12:00 - 12:00</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-2" style="padding: 0px">
                                        <table class="table table-bordered flag_2 table-responsive-lg mitabla {{$asistencia->verificar_asistencia($flunes,$user->id)}}">
                                                <tr class="style_depends_screen" id="style_depends_screen">
                                                        <th>
                                                            Lunes
                                                            <p><small>{{ date("d/m/Y", strtotime($flunes)) }}</small></p>
                                                        </th>
                                                    </tr>
                                        <?php
                                                $ii = 19;
                                            ?>
                                            @if( count($lunesextras) > 0 )
                                                @foreach($lunesextras as $extra)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        @if(date('H',strtotime($extra->fecha_inicio) ) == $i)
                                                            <tr>
                                                                <td colspan="{{ $extra->horas_pedido }}" class="horas-{{$extra->horas_pedido}} h-active">
                                                                    <h2>{{$extra->nombre}}</h2>
                                                                    <p>{{$extra->cliente}}</p>
                                                                    <a href="{{route('eliminar',$extra->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                                $ii= $i + ($extra->horas_pedido);
                                                                break;
                                                            ?>
                                                        @else
                                                            <tr>
                                                                <?php
                                                                    $gfecha = $flunes." ".$i.":00:00";
                                                                ?>

                                                                @if($flunes<=$fmax)
                                                                    <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                                @else
                                                                    <td class="text-center"> - </td>
                                                                @endif
                                                            </tr>
                                                        @endif
                                                    @endfor
                                                @endforeach
                                                @if($ii < 24)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        <tr>
                                                            <?php
                                                                $gfecha = $flunes." ".$i.":00:00";
                                                            ?>

                                                            @if($flunes<=$fmax)
                                                                <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                            @else
                                                                <td class="text-center"> - </td>
                                                            @endif
                                                        </tr>
                                                    @endfor
                                                @endif
                                            @else
                                                @for($i=$ii;$i<=23;$i++)
                                                    <tr>
                                                        <?php
                                                            $gfecha = $flunes." ".$i.":00:00";
                                                        ?>

                                                        @if($flunes<=$fmax)
                                                            <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                        @else
                                                            <td class="text-center"> - </td>
                                                        @endif
                                                    </tr>
                                                @endfor
                                            @endif
                                        </table>
                                    </div>
                                    <div class="col-md-2" style="padding: 0px">
                                        <table class="table table-bordered flag_2 table-responsive-lg mitabla {{$asistencia->verificar_asistencia($fmartes,$user->id)}}">
                                                <tr class="style_depends_screen" id="style_depends_screen">
                                                        <th>
                                                            Martes
                                                            <p><small>{{ date("d/m/Y", strtotime($fmartes)) }}</small></p>
                                                        </th>
                                                    </tr>
                                        <?php
                                                $ii = 19;
                                            ?>
                                            @if( count($martesextras) > 0 )
                                                @foreach($martesextras as $extra)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        @if(date('H',strtotime($extra->fecha_inicio) ) == $i)
                                                            <tr>
                                                                <td colspan="{{ $extra->horas_pedido }}" class="horas-{{$extra->horas_pedido}} h-active">
                                                                    <h2>{{$extra->nombre}}</h2>
                                                                    <p>{{$extra->cliente}}</p>
                                                                    <a href="{{route('eliminar',$extra->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                                $ii= $i + ($extra->horas_pedido);
                                                                break;
                                                            ?>
                                                        @else
                                                            <tr>
                                                                <?php
                                                                    $gfecha = $fmartes." ".$i.":00:00";
                                                                ?>

                                                                @if($fmartes<=$fmax)
                                                                    <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                                @else
                                                                    <td class="text-center"> - </td>
                                                                @endif
                                                            </tr>
                                                        @endif
                                                    @endfor
                                                @endforeach
                                                @if($ii < 24)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        <tr>
                                                            <?php
                                                                $gfecha = $fmartes." ".$i.":00:00";
                                                            ?>

                                                            @if($fmartes<=$fmax)
                                                                <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                            @else
                                                                <td class="text-center"> - </td>
                                                            @endif
                                                        </tr>
                                                    @endfor
                                                @endif
                                            @else
                                                @for($i=$ii;$i<=23;$i++)
                                                    <tr>
                                                        <?php
                                                            $gfecha = $fmartes." ".$i.":00:00";
                                                        ?>

                                                        @if($fmartes<=$fmax)
                                                            <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                        @else
                                                            <td class="text-center"> - </td>
                                                        @endif
                                                    </tr>
                                                @endfor
                                            @endif
                                        </table>
                                    </div>
                                    <div class="col-md-2" style="padding: 0px">
                                        <table class="table table-bordered flag_2 table-responsive-lg mitabla {{$asistencia->verificar_asistencia($fmiercoles,$user->id)}}">
                                                <tr class="style_depends_screen" id="style_depends_screen">
                                                        <th>
                                                            Miercoles
                                                            <p><small>{{ date("d/m/Y", strtotime($fmiercoles)) }}</small></p>
                                                        </th>
                                                    </tr>
                                        <?php
                                                $ii = 19;
                                            ?>
                                            @if( count($miercolesextras) > 0 )
                                                @foreach($miercolesextras as $extra)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        @if(date('H',strtotime($extra->fecha_inicio) ) == $i)
                                                            <tr>
                                                                <td colspan="{{ $extra->horas_pedido }}" class="horas-{{$extra->horas_pedido}} h-active">
                                                                    <h2>{{$extra->nombre}}</h2>
                                                                    <p>{{$extra->cliente}}</p>
                                                                    <a href="{{route('eliminar',$extra->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                                $ii= $i + ($extra->horas_pedido);
                                                                break;
                                                            ?>
                                                        @else
                                                            <tr>
                                                                <?php
                                                                    $gfecha = $fmiercoles." ".$i.":00:00";
                                                                ?>

                                                                @if($fmiercoles<=$fmax)
                                                                    <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                                @else
                                                                    <td class="text-center"> - </td>
                                                                @endif
                                                            </tr>
                                                        @endif
                                                    @endfor
                                                @endforeach
                                                @if($ii < 24)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        <tr>
                                                            <?php
                                                                $gfecha = $fmiercoles." ".$i.":00:00";
                                                            ?>

                                                            @if($fmiercoles<=$fmax)
                                                                <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                            @else
                                                                <td class="text-center"> - </td>
                                                            @endif
                                                        </tr>
                                                    @endfor
                                                @endif
                                            @else
                                                @for($i=$ii;$i<=23;$i++)
                                                    <tr>
                                                        <?php
                                                            $gfecha = $fmiercoles." ".$i.":00:00";
                                                        ?>

                                                        @if($fmiercoles<=$fmax)
                                                            <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                        @else
                                                            <td class="text-center"> - </td>
                                                        @endif
                                                    </tr>
                                                @endfor
                                            @endif
                                        </table>
                                    </div>
                                    <div class="col-md-2" style="padding: 0px">
                                        <table class="table table-bordered flag_2 table-responsive-lg mitabla {{$asistencia->verificar_asistencia($fjueves,$user->id)}}">
                                                <tr class="style_depends_screen" id="style_depends_screen">
                                                        <th>
                                                            Jueves
                                                            <p><small>{{ date("d/m/Y", strtotime($fjueves)) }}</small></p>
                                                        </th>
                                                    </tr>
                                        <?php
                                                $ii = 19;
                                            ?>
                                            @if( count($juevesextras) > 0 )
                                                @foreach($juevesextras as $extra)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        @if(date('H',strtotime($extra->fecha_inicio) ) == $i)
                                                            <tr>
                                                                <td colspan="{{ $extra->horas_pedido }}" class="horas-{{$extra->horas_pedido}} h-active">
                                                                    <h2>{{$extra->nombre}}</h2>
                                                                    <p>{{$extra->cliente}}</p>
                                                                    <a href="{{route('eliminar',$extra->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>

                                                                </td>
                                                            </tr>
                                                            <?php
                                                                $ii= $i + ($extra->horas_pedido);
                                                                break;
                                                            ?>
                                                        @else
                                                            <tr>
                                                                <?php
                                                                    $gfecha = $fjueves." ".$i.":00:00";
                                                                ?>

                                                                @if($fjueves<=$fmax)
                                                                    <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                                @else
                                                                    <td class="text-center"> - </td>
                                                                @endif
                                                            </tr>
                                                        @endif
                                                    @endfor
                                                @endforeach
                                                @if($ii < 24)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        <tr>
                                                            <?php
                                                                $gfecha = $fjueves." ".$i.":00:00";
                                                            ?>

                                                            @if($fjueves<=$fmax)
                                                                <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                            @else
                                                                <td class="text-center"> - </td>
                                                            @endif
                                                        </tr>
                                                    @endfor
                                                @endif
                                            @else
                                                @for($i=$ii;$i<=23;$i++)
                                                    <tr>
                                                        <?php
                                                            $gfecha = $fjueves." ".$i.":00:00";
                                                        ?>

                                                        @if($fjueves<=$fmax)
                                                            <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                        @else
                                                            <td class="text-center"> - </td>
                                                        @endif
                                                    </tr>
                                                @endfor
                                            @endif
                                        </table>
                                    </div>
                                    <div class="col-md-2" style="padding: 0px">
                                        <table class="table table-bordered flag_2 table-responsive-lg mitabla {{$asistencia->verificar_asistencia($fviernes,$user->id)}}">
                                                <tr class="style_depends_screen" id="style_depends_screen">
                                                        <th>
                                                            Viernes
                                                            <p><small>{{ date("d/m/Y", strtotime($fviernes)) }}</small></p>
                                                        </th>
                                                </tr>
                                        <?php
                                                $ii = 19;
                                            ?>
                                            @if( count($viernesextras) > 0 )
                                                @foreach($viernesextras as $extra)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        @if(date('H',strtotime($extra->fecha_inicio) ) == $i)
                                                            <tr>
                                                                <td colspan="{{ $extra->horas_pedido }}" class="horas-{{$extra->horas_pedido}} h-active">
                                                                    <h2>{{$extra->nombre}}</h2>
                                                                    <p>{{$extra->cliente}}</p>
                                                                    <a href="{{route('eliminar',$extra->id)}}" class="btn btn-outline-dark validar" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                                $ii= $i + ($extra->horas_pedido);
                                                                break;
                                                            ?>
                                                        @else
                                                            <tr>
                                                                <?php
                                                                    $gfecha = $fviernes." ".$i.":00:00";
                                                                ?>

                                                                @if($fviernes<=$fmax)
                                                                    <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                                @else
                                                                    <td class="text-center"> - </td>
                                                                @endif
                                                            </tr>
                                                        @endif
                                                    @endfor
                                                @endforeach
                                                @if($ii < 24)
                                                    @for($i=$ii;$i<=23;$i++)
                                                        <tr>
                                                            <?php
                                                                $gfecha = $fviernes." ".$i.":00:00";
                                                            ?>

                                                            @if($fviernes<=$fmax)
                                                                <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                            @else
                                                                <td class="text-center"> - </td>
                                                            @endif
                                                        </tr>
                                                    @endfor
                                                @endif
                                            @else
                                                @for($i=$ii;$i<=23;$i++)
                                                    <tr>
                                                        <?php
                                                            $gfecha = $fviernes." ".$i.":00:00";
                                                        ?>

                                                        @if($fviernes<=$fmax)
                                                            <td class="text-center addtk" data-inicio="{{$gfecha}}"> <i class="fa fa-plus"></i></td>
                                                        @else
                                                            <td class="text-center"> - </td>
                                                        @endif
                                                    </tr>
                                                @endfor
                                            @endif
                                        </table>
                                    </div>
                                </div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('content_extras')
   <!-- Modal -->
	<div class="modal fade" id="agregartiket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Agregar ticket</h5>

	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <!-- crear ticket -->
	        <form action="{{route('create_ticket2')}}" method="POST" id="frm-ticket">
				{{ csrf_field() }}
				<input type="hidden" name="estado" value="0">
				<input type="hidden" name="user_create" value="{{ \Auth::user()->id }}">
				<div class="form-row mb-0">
						<div class="form-group col-12  leyenda  offset-md-8 col-md-3">
								<h6 class=""  id="getDate"></h6>
						</div>
				</div>
				<div class="form-row mt-0" style="margin-top: -15px !important">
						<div class="form-group offset-md-1 col-md-9 text-center">
								{{-- <label for="cliente">Cliente:</label> --}}
							<select class="form-control" name="cliente_id" id="edit_cliente">
								<optgroup label="Fees">
									@foreach($clientes as $cliente)
										@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
											@if($cliente->type == 1)
												<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
											@endif
										@else
											@if ($cliente->type == 1)
												<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
											@endif
										@endif
									@endforeach
								</optgroup>
								<optgroup label="Spot A">
									@foreach($clientes as $cliente)
										@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
											@if($cliente->type == 2)
												<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
											@endif
										@else
											@if ($cliente->type == 2)
												<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
											@endif
										@endif
									@endforeach
								</optgroup>
								<optgroup label="Spot B">
									@foreach($clientes as $cliente)
										@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
											@if($cliente->type == 3)
												<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
											@endif
										@else
											@if ($cliente->type == 3)
												<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
											@endif
										@endif
									@endforeach
								</optgroup>
								<optgroup label="Otros">
									@foreach($clientes as $cliente)
										@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
											@if($cliente->type == 0)
												<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
											@endif
										@else
											@if ($cliente->type == 0)
												<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
											@endif
										@endif
									@endforeach
								</optgroup>
							</select>
							</div>
				  <div class="form-group offset-md-1 col-md-7">
				    {{-- <label for="nombre">Nombre de pedido:</label> --}}
				    <input type="text" class="form-control" placeholder="Nombre del pedido" name="nombre" id="nombre" required>
					</div>
					<div class="form-group col-md-2">
							<input type="number" class="form-control" placeholder="Horas" name="horas_pedido" id="hora" required>
					</div>

				</div>

				<input type="hidden" name="user_id" value="{{$user->id}}">
				<div class="form-row form-row-2">
				  {{-- <div class="form-group col box-calendario"> --}}
				    {{-- <label for="dia">Día:</label> --}}
				    <input type="hidden" class="form-control form-control-2" name="fecha_inicio" id="fecha" readonly required>
				    <!-- <i class="fa fa-calendar"></i> -->
				  {{-- </div> --}}
				  {{-- <div class="form-group col">
				    <label for="hora">Horas del pedido:</label>
				    <input type="number" class="form-control" name="horas_pedido" id="hora" required>
				  </div> --}}
				  <input type="hidden" class="form-control" name="horas_supervision" id="hora_s" value="0">
				</div>

				<div class="form-row">
					<div class="form-group  offset-md-1 col-md-9">
					    {{-- <label for="fecha">Descripción del cambio:</label> --}}
					    <textarea  placeholder="Descripción del pedido"  id="descripcion" class="form-control" name="descripcion" rows="3" ckeditor="editorOptions" ></textarea>
					</div>
				</div>
				<div class="form-row">
					<div class="offset-md-1 col-md-10 offset-md-1 text-center">
							<button type="submit" class="btn btn-primary">Crear</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</form>
	        <!-- crear end ticket -->
	      </div>
	      {{-- <div class="modal-footer">

	      </div> --}}
	    </div>
	  </div>
	</div>

<!-- editarr -->
<div class="modal fade" id="popup_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar ticket</h5>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<!-- crear ticket -->
				<form action="{{ route('update_ticket2')}}" method="POST" id="frm-ticket_edit">
			{{ csrf_field() }}
			<input type="hidden" name="estado" value="1">
			<input type="hidden" name="user_create" value="{{ \Auth::user()->id }}">
			<div class="form-row mb-0">
					<div class="form-group col-12  leyenda  offset-md-8 col-md-3">
							<h6 class=""  id="edit_getDate"></h6>
					</div>
			</div>
			<div class="form-row mt-0" style="margin-top: -15px !important">
				<div class="form-group offset-md-1 col-md-5 text-center">
						<select class="form-control" name="cliente_id" id="edit_cliente">
							<optgroup label="Fees">
								@foreach($clientes as $cliente)
									@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
										@if($cliente->type == 1)
											<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										@endif
									@else
										@if ($cliente->type == 1)
											<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
										@endif
									@endif
								@endforeach
							</optgroup>
							<optgroup label="Spot A">
								@foreach($clientes as $cliente)
									@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
										@if($cliente->type == 2)
											<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										@endif
									@else
										@if ($cliente->type == 2)
											<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
										@endif
									@endif
								@endforeach
							</optgroup>
							<optgroup label="Spot B">
									@foreach($clientes as $cliente)
										@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
											@if($cliente->type == 3)
												<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
											@endif
										@else
											@if ($cliente->type == 3)
												<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
											@endif
										@endif
									@endforeach
							</optgroup>
							<optgroup label="Otros">
								@foreach($clientes as $cliente)
									@if(\Auth::user()->tipo == 'jefe' || \Auth::user()->tipo == 'gerente')
										@if($cliente->type == 0)
											<option value="{{$cliente->id}}">{{$cliente->nombre}}</option>
										@endif
									@else
										@if ($cliente->type == 0)
											<option value="{{$cliente->cliente_id}}">{{$cliente->nombre}}</option>
										@endif
									@endif
								@endforeach
							</optgroup>
						</select>
				</div>
				<div class="form-group col-md-4">
					<select class="form-control" name="hora_inicio" id="edit_fecha_inicio" required>
						<option value="">Hora de inicio</option>
						<option value="09:00:00">09:00</option>
						<option value="10:00:00">10:00</option>
						<option value="11:00:00">11:00</option>
						<option value="12:00:00">12:00</option>
						<option value="14:00:00">14:00</option>
						<option value="15:00:00">15:00</option>
						<option value="16:00:00">16:00</option>
						<option value="17:00:00">17:00</option>
						<option value="18:00:00">18:00</option>
					</select>
				</div>
				<div class="form-group offset-md-1 col-md-7">
					<input type="text" class="form-control" placeholder="Nombre del pedido" name="nombre" id="edit_nombre" required>
				</div>
				<div class="form-group col-md-2">
						<input type="number" class="form-control" placeholder="Horas" name="horas_pedido" id="edit_hora" required>
				</div>

			</div>

			<input type="hidden" name="user_id" value="{{$user->id}}">
			<div class="form-row form-row-2">
				<input type="hidden" class="form-control" name="fecha_inicio" id="edit_fecha" readonly required>
				<input type="hidden" class="form-control" name="id_tiket" id="id_edit_tik" readonly required>
				<input type="hidden" class="form-control" name="horas_supervision" id="edit_hora_s" value="0">
			</div>

			<div class="form-row">
				<div class="form-group  offset-md-1 col-md-9">
						<textarea  placeholder="Descripción del pedido"  id="edit_descripcion" class="form-control" name="descripcion" rows="3" ckeditor="editorOptions" ></textarea>
				</div>
			</div>
			<div class="form-row">
				<div class="offset-md-1 col-md-10 offset-md-1 text-center">
						<button type="submit" class="btn btn-primary">Actualizar</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</form>
			</div>
		</div>
	</div>
</div>
<input type="hidden" value="{{Request::root()}}" id="data_url_host">
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/locale/es.js"></script>

<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script type="text/javascript">
	$("#alerta_popup").modal("show");
	$(document).ready(function(){
		$.ajaxSetup({
	        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
	    });
		$(".popup_editar").click(function(){
			console.log("popup_editar")
			let id = $(this).data("id");
			var URLactual = $("#data_url_host").val();
			console.log(window.location.hostname);
			$.ajax({
				url: URLactual+'/getdatosaeditar/'+id,
				type: 'GET',
				beforeSend: function(){
				},
				success: function(data){
					console.log(data);
					let ff = data.fecha_inicio.split(' ')
					let fi = ff[0]
					let hi = ff[1]

					$("#edit_fecha").val(fi)
					$("#id_edit_tik").val(data.id)
					$("#edit_cliente option").removeAttr("selected")
					$("#edit_fecha_inicio option").removeAttr("selected")
					$("#edit_nombre").val(data.nombre)
					$("#edit_descripcion").val(data.descripcion)
					$("#edit_hora").val(data.horas_pedido)

					$("#edit_cliente option").each(function(){
		        if ($(this).val() != "" ){
							if( $(this).val() == data.cliente_id){
								$(this).attr("selected", "selected")
							}
		        }
		     });

				 $("#edit_fecha_inicio option").each(function(){
					 if ($(this).val() != "" ){
						 if( $(this).val() == hi){
							 $(this).attr("selected", "selected")
						 }
					 }
				});

				 $("#edit_getDate").html(data.fecha_inicio)
				}
			});

		});

		$(".addtk").click(function(){
			var fn = $(this).data('inicio');
			$("#fecha").val(fn);
			var hextra = new Date(fn).getHours();
			if(hextra >= 19){
				$("#descripcion").val('#extra : ');
			}
			else{
				$("#descripcion").val('');
			}
			$("#agregartiket").modal('show');
			console.log(fn+'-'+hextra);
			let days = {
				1 : 'Lun',
				2 : 'Mar',
				3 : 'Mie',
				4 : 'Jue',
				5 : 'Vie'
			}

			let  fn_new  = fn.split(' ')
			let hour = fn_new[1].split(':')
			let fn_date = fn_new[0].split('-')
			let today = new Date(`${fn_date[0]},${fn_date[1] },${fn_date[2]}`)
			console.log(today.toDateString())
			let today_new = today.toDateString().split(' ')
			console.log(today_new[0])
			let txt_day = ''
			let txt_month = today_new[1]
			switch (today_new[0]) {
				case "Mon":
										txt_day = 'Lun'
				break;
				case "Tue":
										txt_day = 'Mar'
				break;
				case "Wed":
										txt_day = 'Mie'
				break;
				case "Thu":
										txt_day = 'Jue'
				break;
				case "Fri":
										txt_day = 'Vie'
				break;

				default:
					break;
			}
			switch (today_new[1]) {
				case 'Jan':
									txt_month = 'Ene'
				break;
				case 'Apr':
									txt_month = 'Abr'
				break;
				case 'Aug':
									txt_month = 'Ago'
				break;
				case 'Dec':
									txt_month = 'Dic'
				break;

				default:
					break;
			}
			  let total_day =  `${txt_day}, ${Number(fn_date[2])} ${txt_month} ${hour[0]}:${hour[1]}`
				document.getElementById('getDate').textContent = total_day

		});

	});

	$(function () {
    	var dateToday = new Date();
        $('#fecha').datetimepicker({
      		format: 'YYYY-MM-DD HH:00:00',
        	sideBySide: true,
        	daysOfWeekDisabled:[0,6],
        	//minDate: dateToday
        });
    });

    $('#m9').addClass('active');
		console.log(window.innerWidth + 'inner width')
		if (window.innerWidth <= 772) {
			let elements =  Array.from(document.querySelectorAll('#style_depends_screen'))
				elements.forEach(element => {
					element.style.display = 'block'
				})
		}
</script>
@endsection

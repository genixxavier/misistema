<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>

	<meta  http-equiv="refresh" content="32"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favi-ticket/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favi-ticket//apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favi-ticket//apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favi-ticket//apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favi-ticket//apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favi-ticket//apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favi-ticket//apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favi-ticket//apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favi-ticket//apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('favi-ticket//android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favi-ticket//favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favi-ticket//favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favi-ticket//favicon-16x16.png')}}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
  <link rel="manifest" href="{{asset('favi-ticket//manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/alertas.css')}}">
</head>
<body  onload="mueveReloj()">
		<header>
	    <div class="container">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-md-2 col-sm-2 col-6 responsi">
	    				<img src="{{ asset('img/logo.png') }}" alt="">
	    			</div>
	    			<div class="col-6 col-sm-9 text-right align-middle time">
	    				<span class="fecha">{{ date('D')." ".date('d')." de ".date('F').", " }}
	    					<form name="form_reloj" style="display: inline;"> 
								<input type="text" name="reloj" size="10" onfocus="window.document.form_reloj.reloj.blur()" style="background-color: transparent;border: none;color: white"> 
							</form>  				
						</span>
	    				<a class="btn" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</header>
	<style>
  @media (max-width: 425px){
  .text-responsa{

  }
  }
  i{
    width: 10px;
    height: 5px;
    border-radius: 3px;
    margin-right: 5px;
  }
  .one{
    background: #5bc0de;
    color: #5bc0de;
  }
  .two{
    background: #d9534f;
    color: #d9534f;
  }
  .three{
    background: #5cb85c;
    color: #5cb85c;
  }
  .atrasado{
    width: 10px;
    height: 10px;
    background: red;
  }
	.pe{
    padding-left: 15px;
	}
	.cardx{
		border-radius: 5px;
	}
	.info{
		padding-left: 10px;
	}
	h3{
		color: #fff;
    
	}
  p{
    color: #fff;
  }
  .ticket, .crear{
    margin-top: 20px;
  }
	</style>
<div class="container crear">
  <div class="row">

    <div class="col-12 text-right">
      <h4 class="parpadea" style="padding: 10px">Avance: <span>49%</span></h4>
    </div>
  </div>
</div>
  <div class="container ticket">
  	<div class="row">
  		<div class="col-md-3 col-6 pe">
  			<div class="cardx bg-info">
  				<div class="info">
  			<h3 class="cuenta">ANDES</h3>
  			<p class="colaborador">Anny Mamani</p>
  			<p class="des">Mailing semanal</p>
  			<p class="fecha">Inicio: 09/07 12:00<br>Fin: 10/07 14:00</p>
			</div>
			</div>
  		</div>
  		<div class="col-md-3 col-6 pe">
  			<div class="cardx bg-danger">
  				<div class="info">
  			<h3 class="cuenta">ANDES</h3>
  			<p class="colaborador">Anny Mamani</p>
  			<p class="des">Mailing semanal</p>
  			<p class="fecha">Inicio: 09/07 12:00<br>Fin: 10/07 14:00</p>
			</div>
		</div>
  		</div>
  		<div class="col-md-3 col-6 pe">
  			<div class="cardx bg-success">
  				<div class="info">
  			<h3 class="cuenta">ANDES</h3>
  			<p class="colaborador">Anny Mamani</p>
  			<p class="des">Mailing semanal</p>
  			<p class="fecha">Inicio: 09/07 12:00<br>Fin: 10/07 14:00</p>
			</div>
			</div>
  		</div>
  		<div class="col-md-3 col-6 pe">
  			<div class="cardx bg-danger">
  				<div class="info">
  			<h3 class="cuenta">ANDES</h3>
  			<p class="colaborador">Anny Mamani</p>
  			<p class="des">Mailing semanal</p>
  			<p class="fecha">Inicio: 09/07 12:00<br>Fin :10/07 14:00</p>
			</div>
			</div>
  		</div>
  </div>
  <div class="row">
  		<div class="col-md-3 col-6 pe">
  			<div class="cardx bg-danger">
  				<div class="info">
  			<h3 class="cuenta">ANDES</h3>
  			<p class="colaborador">Anny Mamani</p>
  			<p class="des">Mailing semanal</p>
  			<p class="fecha">Inicio: 09/07 12:00<br>Fin:10/07 14:00</p>
			</div>
			</div>
			</div>
  		<div class="col-md-3 col-6 pe">
  			<div class="cardx bg-info">
  				<div class="info">
  			<h3 class="cuenta">ANDES</h3>
  			<p class="colaborador">Anny Mamani</p>
  			<p class="des">Mailing semanal</p>
  			<p class="fecha">Inicio: 09/07 12:00<br>Fin :10/07 14:00</p>
			</div>
			</div>
  		</div>
  		<div class="col-md-3 col-6 pe">
  			<div class="cardx bg-danger">
  				<div class="info">
  			<h3 class="cuenta">ANDES</h3>
  			<p class="colaborador">Anny Mamani</p>
  			<p class="des">Mailing semanal</p>
  			<p class="fecha">Inicio: 09/07 12:00<br>Fin :10/07 14:00</p>
			</div>
			</div>
  		</div>
  		<div class="col-md-3 col-6 pe">
  			<div class="cardx bg-success">
  				<div class="info">
  			<h3 class="cuenta">ANDES</h3>
  			<p class="colaborador">Anny Mamani</p>
  			<p class="des">Mailing semanal</p>
  			<p class="fecha">Inicio: 09/07 12:00<br>Fin :10/07 14:00</p>
			</div>
			</div>
  		</div>
  </div>
 </div>
  <div class="container">
 	<div class="row">
      <div class="col-lg-8 col-md-6 col-6 text-lg-right text-right">
        <p style="margin-top: 0.5rem;margin-bottom: 0.5rem; color:#333;" class="text-respon"><i class="one">aaaaaa</i>En Proceso</p>
      </div>
      <div class="col-lg-2 col-md-3 col-6  text-lg-right text-center">  
        <p style="margin-top: 0.5rem;margin-bottom: 0.5rem; color: #333" class="text-respon"><i class="two">aaaaaa</i>Atrasado</p>
      </div>
      <div class="col-lg-2 col-md-3 col-12 text-lg-right text-center">
        <p style="margin-top: 0.5rem;margin-bottom: 0.5rem; color: #333" class="text-respon"><i class="three">aaaaaa</i>Terminado</p>
      </div>
</div>
</div>
  <section id="footer">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <p>Copyright 2018 - All rights reserved || Versión 1.3</p>
          </div>
          <div class="col-md-4 text-right">
             powered by <a href="https://www.mediaimpact.pe/" target="_blank">Media Impact</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script language="JavaScript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
  function mueveReloj(){ 
      momentoActual = new Date() 
      hora = momentoActual.getHours() 
      minuto = momentoActual.getMinutes() 
      segundo = momentoActual.getSeconds() 

      str_segundo = new String (segundo) 
      if (str_segundo.length == 1) 
          segundo = "0" + segundo 

      str_minuto = new String (minuto) 
      if (str_minuto.length == 1) 
          minuto = "0" + minuto 

      str_hora = new String (hora) 
      if (str_hora.length == 1) 
          hora = "0" + hora 

      horaImprimible = hora + " : " + minuto + " : " + segundo 

      document.form_reloj.reloj.value = horaImprimible 

      setTimeout("mueveReloj()",1000) 
  } 
  </script> 
</body>
</html>
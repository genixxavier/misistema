@extends('layout.app')
@section('titulo','Reporte horas')
@section('css')

    <link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=374b9rm3q420nrmdtfwnqf3miwqwvrto3p2dgt51spdlozup"></script>



    <style>


    </style>
@endsection
@section('content')

    <div class="row ">
        <div class="col-12 text-center">
            <h1 class="main-title ">  Reporte de H. de trabajo  x Cliente</h1>
        </div>
    </div>
    <div class="row d-flex justify-content-center mt-4">
        <div class="col-6 text-center ">

            <form action="" id="myForm" >
                <div class="form-group">
                    <label for="">Seleccione la fecha</label>
                    <input type="text" name="daterange" id="date" value="01/01/2019 - 31/01/2019" class="form-control">
                    <a href="" id="link" name="link" style="display: none;" >ir </a>
                </div>

                <button type="submit" class="btn btn-primary btn-custom" > Generar Excel </button>
            </form>
        </div>
    </div>

@endsection
@section('content_extras')

@endsection

@section('js')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>
        $(function() {
            $('input[name="daterange"]').daterangepicker({
                opens: 'left',
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {


            });
        });

        document.getElementById('myForm').addEventListener('submit', e => {
            e.preventDefault()
            let fecha = document.getElementById('date').value.split('-')
            let enlace = document.getElementById('link')
            fecha[0] = fecha[0].trim()
            fecha[1] = fecha[1].trim()
            let  inicio = fecha[0].split('/')
            inicio = inicio[2]+'-'+inicio[1]+'-'+inicio[0]
            let fin = fecha[1].split('/')
            fin = fin[2]+'-'+fin[1]+'-'+fin[0]
            enlace.href = `/download_excel?inicio=${inicio}&fin=${fin}`
            enlace.click()
            


        })
    </script>
@endsection
@extends('layout.app')
@section('titulo','Asistencia')

@section('css')
@endsection
@section('content')
 
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				<div class="row">
					<div class="col-md-4">
						<p><a href="{{ route('horarios') }}">Atras</a></p>
						Selecionar día:
						<form id="agregardia">
							{{  csrf_field() }}
							<input type="hidden" name="id_user" value="{{ $user->id }}">
							<select  class="form-control" name="dia">
								@foreach($dias as $dia)
									<option value="{{ $dia->id }}">{{ $dia->descripcion }}</option>
								@endforeach
							</select>
							<label for="">Hora de entrada</label>
							<input type="text" class="form-control" name="entrada" placeholder="07:00">
							<label for="">Hora de salida</label>
							<input type="text" class="form-control" name="salida" placeholder="19:00">
							<br>
							<button type="submit" class="btn btn-primary">Agregar Horario</button>
						</form>
					</div>
					<div class="col-md-8 b-box">
						<h5 class="text-center">Horaio: {{ $user->name }}</h5>
						@if (session('status'))
							@if(session('status') == 'exito')
							    <p class="text-success">Item de horario actualizado</p>
							@endif
						@endif
						<div id="gethorario"></div>
						<table class="table table-bordered table-responsive-lg mitabla">
							<tr>
								<th>DIA</th>
								<th>ENTRADA</th>
								<th>SALIDA</th>
								<th></th>
							</tr>
							@foreach($horarios as $horario)
								<tr>
									<td>{{ $horario->dia->descripcion }}</td>
									<td>{{ $horario->entrada }}</td>
									<td>{{ $horario->salida }}</td>
									<td> <a href="#" class="btn btn-outline-dark g-tooltip validar gdata" data-toggle="modal" data-target="#validar_tiket" data-id="{{$horario->id}}" data-entrada="{{$horario->entrada}}" data-salida="{{$horario->salida}}" data-dia="{{ $horario->dia->descripcion }}"><i class="fa fa-edit"></i>
                                   	 <span class="hover-validar">Editar</span>
                                  	</a>
                              		</td>
								</tr>
							@endforeach
							
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('content_extras')
    <!-- Modal -->
    <div class="modal fade" id="validar_tiket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <h4>Actualizar horario</h4>
            Dia : <span id="gdia"></span>
            <form class="form-horizontal" method="POST" action="{{ route('update_horario') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="">Hora de entrada</label>
				<input type="text" class="form-control" name="gentrada" id="gentrada" placeholder="07:00">
              </div>
              <div class="form-group">
                <label for="">Hora de salida</label>
				<input type="text" class="form-control" name="gsalida" id="gsalida" placeholder="19:00">
              </div>

              <input type="hidden" value="" name="horario_id" id="horario_id">
              <button class="btn btn-primary yes" >Actualizar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection
@section('js')
<!-- <script src="https://code.jquery.com/jquery-3.2.1.min.js" ></script> -->
<script type="text/javascript">
	$(document).ready(function(){
		$.ajaxSetup({
	        headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
	    });
		$("#agregardia").submit(function(e){
			e.preventDefault();
			var data= $("#agregardia").serialize();
			$.ajax({
	            url: '/horario/create',
	            type: 'POST',
	            data: data,
	            beforeSend: function(){
	            	$(".alerta").show();
	            	$(".alerta").html('<div class="text-alert">Enviando mensaje....</div>');
	            },
	            success: function(response)
	            {
	            	if(response == "1"){
	            		$("#gethorario").html("Exito, horario registrado");
	            		setTimeout('location.reload()', 1000);
	            	}
	            	else{
	            		$("#gethorario").html(response);
	            	}
	            }
	        });

		});

		$(".gdata").click(function(){
			$("#horario_id").val($(this).data('id'));
			$("#gentrada").val($(this).data('entrada'));
			$("#gsalida").val($(this).data('salida'));
			$("#gdia").html($(this).data('dia'));
		});

	});
	

    $('#m3').addClass('active');
</script>
@endsection
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>SUPER MEDIA BROSS</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<style>
		@font-face {
		  font-family: 'Mario';
		  src: url(/fonts/SuperMario256.ttf) format("truetype");
		}
		html, body{
			height: 100%;
		}
		body{
			background-image: url('{{asset('img/mario.jpg')}}');
			background-position: 0px 0px;
			background-repeat: repeat-x;
			animation: animatedBackground 50s linear infinite;
		}
		h2{
			font-family: Mario;
			text-align: center;
			font-size: 60px;
			padding-top: 60px;
			text-shadow: 5px 5px 5px black;
			letter-spacing: -10px;
		}
		h2 span{
			padding: 0px;
			margin: 0px;
		}
		h2 span:nth-of-type(1),h2 span:nth-of-type(5),h2 span:nth-of-type(9),h2 span:nth-of-type(13),h2 span:nth-of-type(17){
			color: #01A6DA;
		}
		h2 span:nth-of-type(2),h2 span:nth-of-type(6),h2 span:nth-of-type(10),h2 span:nth-of-type(14),h2 span:nth-of-type(18){
			color: #F7CC0C;
		}
		h2 span:nth-of-type(3),h2 span:nth-of-type(7),h2 span:nth-of-type(11),h2 span:nth-of-type(15){
			color: #F13125;
		}
		h2 span:nth-of-type(4),h2 span:nth-of-type(8),h2 span:nth-of-type(12),h2 span:nth-of-type(16){
			color: #4DBA4D;
		}
		@keyframes animatedBackground {
		    from { background-position: 0 0; }
		    to { background-position: 100% 0; }
		}
		.medallas{
			padding-top: 150px;
		}
		.medallas img{
			width: 40%;
			margin-bottom: -125px;
			z-index: 100;
			display: block;
			position: relative;
			margin-right: auto;
			margin-left: auto;
		}
		.medallas p{
			width: 100%;
			text-align: center;
			padding: 10px 0px;
			margin-bottom: 0px;
			margin-top: -43px;
			font-weight: 700;
		}
		.medallas p:nth-of-type(1){
			background-color: yellow
		}
		.medallas p:nth-of-type(2){
			background-color: grey
		}
		.medallas p:nth-of-type(2){
			background-color: brown
		}
		.ultimos p{
			text-align: center;
			background-color: white;
			padding: 10px 5px;
			font-weight: 700;
		}
		.puntaje button{
			background-color: white;
			width: 70%;
			margin: 0;
			padding: 7px 10px;
			border:1px solid black;
		}
		.puntaje span{
			float: right;
		}
		.puntaje h5{
			font-family: Mario;
			margin-top: 30px;
		}
/* Style the buttons that are used to open and close the accordion panel */
.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  text-align: left;
  border: none;
  outline: none;
  transition: 0.4s;

}

/* Add a background color to the button if it is clicked on (add the .active class with JS), and when you move the mouse over it (hover) */
.active, .accordion:hover {
  background-color: #ccc;
}

/* Style the accordion panel. Note: hidden by default */
.panel {
  padding: 0 18px;
  display: none;
  overflow: hidden;
  width: 70%;
  background-color: #efe;
}
.panel p{
	font-size: 10px;
	margin-bottom: 3px;
	margin-top: 3px;
}
/*.accordion:after {
  content: '\02795'; 
  font-size: 13px;
  color: #777;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2796";
}*/
	</style>
</head>
<body >



	<div class="container" >
		<div class="row">
			<div class="col-md-12">
				<img src="{{asset('img/super-media-bros.png')}}" alt="" style="width: 80%;padding: 30px 50px 20px 50px;margin: auto auto;display: block;">
{{-- 				<h2>
					<span>S</span>
					<span>U</span>
					<span>P</span>
					<span>E</span>
					<span>R</span> &nbsp;

					<span>M</span>
					<span>E</span>
					<span>D</span>
					<span>I</span>
					<span>A</span> &nbsp;
  
					<span>B</span>
					<span>R</span>
					<span>O</span>
					<span>S</span> &nbsp;

					<span>2</span>
					<span>0</span>
					<span>1</span>
					<span>8</span>
				</h2>
 --}}			</div>

			<div class="col-md-4">
				<div class="puntaje">
					<h5>
					@if($n_mes-1 > 7)

					<a href="{{asset('/podio/'.($n_mes-1))}}" style="color: black"> < </a>
					@endif

					{{$mes}} 
					@if($n_mes+1 < 13)

					<a href="{{asset('/podio/'.($n_mes+1))}}" style="color: black"> > </a>
					
					@endif
					</h5>
	
					@foreach($areas2 as $area)
						<button class="accordion">{{$area->nombre}} <span>{{$area->puntaje_total}}</span></button>
						<div class="panel">
							@foreach($area->datos($area->id,$n_mes) as $datos)
								<p>{{$datos->motivo}} <span>{{$datos->puntaje}}</span></p>
							@endforeach
						</div>
					@endforeach
				</div>
			</div>


			<div class="col-lg-6 medallas">
				<div class="row">
					<div class="col-md-4 offset-md-4" style="background: grey;height: 70px">
						<p>{{$ganadores[0]['name'].' - '. $ganadores[0]['puntaje']}}</p>
						<img src="{{asset('img/oro.png')}}" alt="">
					</div>
				</div>
				<div class="row">
					<div class="col-md-4" style="background: grey;height: 70px">
						<p>{{$ganadores[1]['name'].' - '. $ganadores[1]['puntaje']}}</p>
						<img src="{{asset('img/plata.png')}}" alt="">
					</div>
					<div class="col-md-4" style="background: grey;height: 70px">
					</div>
				</div>
				<div class="row">
					<div class="col-md-8" style="background: grey;height: 70px">						
					</div>
					<div class="col-md-4" style="background: grey;height: 70px">
						<p>{{$ganadores[2]['name'] .' - '. $ganadores[2]['puntaje']}}</p>
						<img src="{{asset('img/bronce.png')}}" alt="">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12" style="background: grey;height: 70px">						
					</div>
				</div>
			</div>
		</div>

		<div class="row ultimos" style="margin-top: 20px">
			<div class="col-lg-10 offset-md-2">
				<div class="row">
					@for($a = 3; $a< 4;$a++)
					<div class="col-md-3">
						<p>{{$a+1}}) {{$ganadores[$a]['name'].' - '. $ganadores[$a]['puntaje']}}</p>
					</div>
					@endfor
				</div>
			</div>
		</div>
	</div>
	<script>
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		  acc[i].addEventListener("click", function() {
		    /* Toggle between adding and removing the "active" class,
		    to highlight the button that controls the panel */
		    this.classList.toggle("active");

		    /* Toggle between hiding and showing the active panel */
		    var panel = this.nextElementSibling;
		    if (panel.style.display === "block") {
		      panel.style.display = "none";
		    } else {
		      panel.style.display = "block";
		    }
		  });
		}
	</script>
</body>
</html>
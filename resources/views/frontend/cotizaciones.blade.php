@extends('layout.app')
@section('titulo','Cotizaciones')
@section('css')

  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
<style>

.btn-crear{

  display: inline-block;
  padding: 7px 15px;
  cursor: pointer;
  font-size: 13px;
  line-height: 13px;
  text-decoration: none;
  border-radius: 5px;
  color: #fff;
  margin-left: 5px;
  margin-right: 5px;
  margin-bottom: 20px;
}
.btn {
    cursor: unset;
}
.mitabla tr th {
    text-align: center;
}
@media only screen and (max-width: 768px) {
    .margin_div {
        margin-top: 30px;
    }
}
.tooltip.show {

    opacity: 1;
}


</style>
@endsection
@section('content')

<div class="container-fluid">
    <div class="row">
        {{ Breadcrumbs::render('cotizaciones') }}
    </div>
    <div class="row">
        <div class="col-12 mt-3"><h1 class="text-center"> Cotizaciones</h1></div>
    </div>
    <div class="row text-center margin_div">
        <div class="col-3 mt-3">
         
            <a href="/funnel/crear_cotizacion" class="btn-crear btn-custom">Crear Cotización </a>
        </div>
        <div class="col-3 mt-3">
         
        <a href="{{route('cotizaciones_create_2')}}" class="btn-crear btn-custom">Crear Cotización sin CR </a>
        </div>
        <div class="col-3 mt-3">

            <a href="{{route('project.index')}}" class="btn-crear btn-custom">Proyectos </a>
        </div>
        <div class="col-3 mt-3 ">
       
            <a href="{{ route('contact_report_index')}}" class="btn-crear btn-custom">Volver </a>
        </div>
      <div class="col-12 mt-3">
        <form class="form-inline my-2 my-lg-0" method="GET">
          <select name="cliente" id="clientes" class="form-control ">
            <option value="0">-- Todos --</option>
            @foreach($clientes as $key => $cliente)

              <option value="{{$cliente->nombre}}">{{$cliente->nombre}}</option>

            @endforeach

          </select>
          <input class="form-control mr-sm-2 ml-sm-3" type="search" name="nombre"   placeholder="Buscar" aria-label="Buscar">
          <button class="btn btn-outline-success my-2 my-sm-0 ml-3" type="submit">Buscar</button>
        </form>
      </div>

    </div>
    <div class="row mt-3">
    
        <div class="col-md-11">
            <table class="table table-bordered table-responsive-lg mitabla custom-table">
                <thead class="">
                    <tr>
                        <th>
                            Marca
                        </th>
                        <th>
                            Contacto 
                        </th>
                        <th>
                            Ejecutivo (a)
                        </th>
                        <th>
                            Inversión Total
                        </th>

                        <th> 

                            Número
                        </th>
                        <th>
                            Estado
                        </th>
                        <th>
                            Opciones
                        </th>
                    </tr>
                    
                </thead>
                <tbody>
                        @foreach ($cotizacion as $item)
                        <tr >
                            <td  data-toggle="tooltip" title="{{$item->title_servicio}}">
                                {{$item->marca}}
                            </td>
                            <td >
                                {{$item->c_contacto}}
                            </td>
                            <td >
                                {{$item->ejecutivo}}
                            </td>
                            <td >
                                @if ($item->c_tipo_moneda == 1)
                                 <b>S/</b> {{number_format($item->total,2,'.',',')}}
                                @else
                                <i class="fas fa-dollar-sign"></i> {{number_format($item->total,2,'.',',')}}   
                                @endif
                            </td>
                            <td> 
                                #{{$item->c_num}}
                            </td>
                            <td class="text-center">
                              <div class="options">
                                @if ($item->status == 1)
                                <span class="btn btn-success">Aprobado</span>
                           
                            @elseif ($item->status == 2)
                            <span class="btn btn-danger">Desaprobado</span>
                           
                            @elseif ($item->status == 3)
                            <span class="btn btn-info">Pendiente</span>
                           
                            @elseif ($item->status == 4)
                            <span class="btn btn-warning">Con Observación</span>
                            @elseif ($item->status == 5)
                            <span class="btn btn-light">En proceso</span>
                           
                            @elseif ($item->status == 6)
                              <span class="text-danger font-weight-bold"> -  </span>
                           
                            @endif
                              </div>
                            </td>

                            <td>
                                <div class="dropdown">
                                    <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item"  target="_BLANK" href="{{route('reporte_cotizacion', $item->id) }}"><i class="fas fa-file-pdf"></i> Ver</a> 
                                        @if ($item->c_type_file == 1)
                                        <a   class="dropdown-item"   href= {{route('download_file',$item->c_file)}}><i class="fas fa-file-download"></i> Adjunto</a>
                                        @elseif ($item->c_type_file == 2)
                                        <a   class="dropdown-item" target="_blank"  href= {{ $item->c_file }}><i class="fas fa-link"></i> Adjunto</a>
                                        @endif
                                        @if ($item->status != 4)
                                        {{-- <a class="dropdown-item" href=" {{route('edit_cotizaciones' , $item->id)}} "><i class="fas fa-edit"></i> Editar</a>  --}}
                                        @else
                                        <a class="dropdown-item"  href="#" data-toggle="modal" data-target="#exampleModal" onclick="pushIdStatus({{$item->id_status}})" ><i class="fa fa-eye"></i> Ver Observación</a>
                                        <a class="dropdown-item" href="{{route('edit_cotizaciones' , $item->id)}} " ><i class="far fa-edit"></i> Corregir observación</a>
                                        
                                        <a class="dropdown-item" href="#" onclick="change_status({{$item->id_status}})"><i class="fas fa-redo"></i> Enviar a Supervisión </a>
                                        @endif
                                        @php
                                            if ($item->status == 5) {
                                                echo '<a class="dropdown-item" href="/funnel/cotizacion/'.$item->id.'"><i class="fas fa-edit"></i> Editar</a> 
                                                <a class="dropdown-item" href="#" onclick="change_status('.$item->id_status.')"><i class="fas fa-redo"></i> Enviar a Supervisión </a>
                                                <a class="dropdown-item" href="/funnel/nueva_version/'.$item->id.'" ><i class="fas fa-plus"></i> Generar nueva versión</a>
                                                ';
                                            }
                                            else if ($item->status == 3 || $item->status == 4  ) {
                                                echo ' <a class="dropdown-item" href="/funnel/nueva_version/'.$item->id.'" ><i class="fas fa-plus"></i> Generar nueva versión</a>';
                                            }
                                            else if ($item->status == 1 ) {
                                                echo ' <a class="dropdown-item" href="/funnel/projects/create_project/'.$item->id.'" ><i class="fas fa-plus-square"></i> Crear Proyecto</a> <a class="dropdown-item" href="/funnel/nueva_version/'.$item->id.'" ><i class="fas fa-plus"></i> Generar nueva versión</a>';
                                            }
                                        @endphp

                                        <a class="dropdown-item" style="cursor: pointer"  onclick="delete_cotizacion('{{$item->id}}')" ><i class="fas fa-trash"></i> Eliminar</a>

                                    </div>
                                  </div>
                            </td>
                        </tr>
                     @endforeach
                </tbody>
            </table>
            {!! $cotizacion->render() !!}

        </div>
   
    </div>
    <input type="hidden" name="push_id_status" id="push_id_status">
     <!-- Modal -->
     <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Observación</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
               <div class="container" id="main_content">
                   
               </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                {{-- <button type="submit" class="btn btn-primary">Cambiar Estado</button> --}}
        </div>
           
        </div>
        </div>
    </div>
</div>
@endsection
@section('content_extras')
  

    
  

   
    
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>

    const delete_cotizacion = (id) => {

        alertify.confirm("Cotización","Desea eliminar la cotización ?",
            function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    method: 'POST',
                    url: '/funnel/delete_cotizacion',
                    data: {
                        id
                    },

                    success: function(data){
                        console.log(data)
                        if (data.status !== 200) {
                            alertify.error(data.message)
                        }
                        else {

                        alertify.success(data.message)
                            location.reload()
                        }


                    }
                });


            },
            function () {
            alertify.error('Cancelado !')
            }
           )


    }

    const pushIdStatus = id =>  {

        // document.getElementById('push_id_status').value = id  
        let main_content = document.getElementById('main_content')
        $.ajax({
                    method: 'GET',
                    url: './observacion',
                    data: {
                        id
                    },

                    success: function(data){
                        
                      main_content.innerHTML = data 


                    }
                });
        
    }
    const change_status = id => { 

        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }); 
        id_cot = id
        $.ajax({
                method: 'POST',
                url: '/funnel/update_status_cotizacion',
                data: {
                    status : 3,
                    id : id_cot,
                    obs : ''
                },

                success: function(data){
                    // console.log(data)
                    if (data.status !== 200) {
                        alertify.error('Ups ! No se pudo actualizar')
                    }
                    else {

                    alertify.success(data.message)
                    window.location.href = '/funnel/cotizaciones'
                    }


                
            }});
       
        }
    
       
   </script>
@endsection
@extends('layout.app')
@section('titulo','Historial')
@section('css')
{{-- Icon Fontastic  --}}
<link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
  <style> 
    a {
        color: white; 
        text-decoration: none;
    }
    a:hover {
        color: white; 
        text-decoration: none;
    }
    .mitabla {
            margin-top: 20px;

        }
        .mitabla thead tr {
            background: #263238;
            color: #fff;

        }
        .mitabla thead tr th {
            font-weight: 400;
            color: #fff;
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: 14px;
        }
        .mitabla tbody tr td {
            font-size: 14px;
        }
  </style>
@endsection
@section('content')

      <div class="col-12">
                <table class="table table-bordered table-responsive-lg mitabla">
                    <thead class="thead-dark">
                        <tr>
                            <th>
                                Proyecto 
                            </th>
                            <th>
                                Estado
                            </th>
                           
                            <th>
                                Opciones
                            </th>
                        </tr>
                        
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                ToyoBeta
                            </td>
                            <td>
                                Proceso
                            </td>
                         
                         
                          
                            <td>
                              <button class="btn btn-primary">  <i class="icon-eye"></i></button>
                               <button class="btn btn-secondary" >
                                    <i class="icon-delete"></i>
                               </button> 
                               <button class="btn btn-info">
                                    <i class="icon-plus"> Factura</i>       
                            </button>
                            </td>
                        </tr>
                        <tr>
                                <td>
                                    ToyoBeta
                                </td>
                                <td>
                                    Proceso
                                </td>
                             
                             
                              
                                <td>
                                  <button class="btn btn-primary">  <i class="icon-eye"></i></button>
                                   <button class="btn btn-secondary" >
                                        <i class="icon-delete"></i>
                                   </button> 
                                   <button class="btn btn-info">
                                        <i class="icon-plus"> Factura</i>       
                                </button>
                                </td>
                        </tr>
                        <tr>
                                    <td>
                                        ToyoBeta
                                    </td>
                                    <td>
                                        Proceso
                                    </td>
                                 
                                 
                                  
                                    <td>
                                      <button class="btn btn-primary">  <i class="icon-eye"></i></button>
                                       <button class="btn btn-secondary" >
                                            <i class="icon-delete"></i>
                                       </button> 
                                       <button class="btn btn-info">
                                            <i class="icon-plus"> Factura</i>       
                                    </button>
                                    </td>
                        </tr>
                              
                        <tr>
                                        <td>
                                            ToyoBeta
                                        </td>
                                        <td>
                                            Proceso
                                        </td>
                                     
                                     
                                      
                                        <td>
                                          <button class="btn btn-primary">  <i class="icon-eye"></i></button>
                                           <button class="btn btn-secondary" ">
                                                <i class="icon-delete"></i>
                                           </button> 
                                           <button class="btn btn-info">
                                                <i class="icon-plus"> Factura</i>       
                                        </button>
                                        </td>
                        </tr>
                                  
                    </tbody>
                </table>
            </div>
@endsection
@section('content_extras')
  

    
  

   
    
@endsection

@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
        function  mostrarId (item)  {
            console.log(item)
        }
   </script>
@endsection
@extends('layout.app')
@section('titulo','Usuarios')
@section('css')

    <link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=374b9rm3q420nrmdtfwnqf3miwqwvrto3p2dgt51spdlozup"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>



    <style>
        
        .paginar {
        display: -ms-flexbox;
        display: flex;
        padding-left: 0;
        list-style: none;
        border-radius: 0.25rem;
        justify-content: center;

        }
   

        .paginar .paginar-link {
        padding: 0.25rem 0.5rem;
        font-size: 0.875rem;
        line-height: 1.5;
        }
        
        .paginar-item:first-child .paginar-link {
        margin-left: 0;
        border-top-left-radius: 0.25rem;
        border-bottom-left-radius: 0.25rem;
        }

        .paginar-item:last-child .paginar-link {
        border-top-right-radius: 0.25rem;
        border-bottom-right-radius: 0.25rem;
        }

        .paginar-item.active .paginar-link {
        z-index: 1;
        color: white;
        background-color: #F21D4D;
        border-color: #007bff;
        }

        .paginar-link {
        color: white !important;
        cursor: pointer;
        background-color: #0A0025 ;
        border-color: #dee2e6;
        }
        .paginar-link:hover {
        z-index: 2;
        color: #0056b3;
        text-decoration: none;
        background-color: #F21D4D;
        border-color: #dee2e6;
        }

        .paginar-link:focus {
        z-index: 2;
        outline: 0;
        box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.25);
        }

     
    </style>
@endsection
@section('content')
  
   <div class="row" id="app">
    <div class="col-6 col-md-4 col-lg-3 text-center">
        <a class="nav-link base_de_datos" id="m8" target="_blank" href="https://docs.google.com/spreadsheets/d/1SmRAQ8UgMLr1eXKCKcOSXNLh85BotdlSwnVT8u722-4/edit#gid=1891176735" style="
        background-color:#0a0025;
        padding: 20px;
        margin-bottom: 15px;
        transition: .2s;
        color: #fff;
        text-decoration: none;
        border-radius: 15px;
            ">
            <i class="fas fa-database" aria-hidden="true" style="
                font-size: 40px;
                display: block;"></i><span>Base de Datos</span></a>
    </div>
   <user_component></user_component>
    
   </div>
@endsection
@section('content_extras')
   
@endsection

@section('js')
    <style>
        .base_de_datos:hover{
            background-color: #f21d4d !important;
        }
    </style>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="{{asset('js/app.js')}}">

    </script>
@endsection
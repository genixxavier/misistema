<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>

	<meta  http-equiv="refresh" content="32"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favi-ticket/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favi-ticket//apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favi-ticket//apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favi-ticket//apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favi-ticket//apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favi-ticket//apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favi-ticket//apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favi-ticket//apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favi-ticket//apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('favi-ticket//android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favi-ticket//favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favi-ticket//favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favi-ticket//favicon-16x16.png')}}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
  <link rel="manifest" href="{{asset('favi-ticket//manifest.json')}}">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/alertas.css')}}">
</head>
<style>
@media(max-width: 425px) {
	.estados{
		font-size: 10px;
	}
	.btn-proceso{
		font-size: 8px;
	}
	.btn-pendiente{
		font-size: 8px;
	}
	.btn-culminado{
		font-size: 8px;
	}
	.btn-reunion{
		font-size: 8px;
	}
	.btn-cr{
	font-size: 10px;
		}
	.btn-ct{
	font-size: 	10px;
}
	.estado-a{
		text-decoration: none;

	}
}
.estados{
	margin-top: 20px;
}
.num-pend{
	margin-right: 7px;
	background-color: #ffc107;
	color: #fff;
	padding: 1px 5px;
	border-radius: 4px;
}
.num-apro{
	margin-right: 7px;
	background-color: #ea80fc;
	color: #fff;
	padding: 1px 5px;
	border-radius: 4px;
}
.num-term{
	margin-right: 7px;
	background-color: #5cb85c;
	color: #fff;
	padding: 1px 5px;
	border-radius: 4px;
}
.num-atra{
	margin-right: 7px;
	background-color: #d9534f;
	color: #fff;
	padding: 1px 5px;
	border-radius: 4px;
}
.btn-cr{
	padding: 5px 0px;
	background-color: #d9534f;
	color: #fff;
}
.btn-ct{
	background-color:  #333;
	color: #fff;
}
.btn-crear{
	margin-top: 20px;
}
.btn-proceso{
	background-color: #5bc0de;
	color: #fff;
}
.btn-reunion{
	background-color: #d9534f;
	color: #fff;
}
.botones{
	margin-top: 20px;
}
.mitabla {
	margin-top: 20px;

}
	.mitabla thead tr {
	background: #263238;
	color: #fff;
}
.mitabla thead tr th {
	font-weight: 400;
	color: #fff;
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 14px;
}
.mitabla tbody tr td {
	font-size: 14px;
}
.mitabla tbody tr td .btn {
	padding: 1px 0.75rem;
	font-size: 14px;
}
.mitabla tbody tr td .tck-estado {
	padding: 2px 8px;
	font-size: 11px;
	border-radius: 3px;
}	
.mitabla tbody tr td .tck-proceso {
	background-color: #29b6f6;
	color: #fff;
}
.mitabla tbody tr td .tck-terminado {
	background-color: #5cb85c;
	color: #fff;
}
.mitabla tbody tr td .tck-atrazado {
	background-color: #ef5350;
	color: #fff;
}
.mitabla tbody tr td .tck-aprobado {
	background-color: #ea80fc;
	color: #fff;
}
.mitabla tbody tr td .tck-sin-asignar {
	background-color: #fff;
	color: #505050;
	border: 1px solid #505050;
}
.mitabla tbody tr td .tck-asignado {
	background-color: #007bff;
	color: #fff;
}
.mitabla tbody tr td .tck-parado {
	background-color: #f4511e;
	color: #fff;
}

.table-bordered th {
	border: 1px solid #263238;
}

.table-bordered tbody tr:last-child td {
	border-bottom: 1px solid #ececec;
}

.table-bordered td {
	border-left: 1px solid #ececec;
	border-right: 1px solid #ececec;
	border-top: 1px solid #fff;
	border-bottom: 1px solid #fff;
}
</style>
<body onload="mueveReloj()">
		<header>
	    <div class="container">
	    	<div class="container-fluid">
	    		<div class="row">
	    			<div class="col-md-2 col-sm-2 col-6 responsi">
	    				<img src="{{ asset('img/logo.png') }}" alt="">
	    			</div>
	    			<div class="col-6 col-sm-9 text-right align-middle time">
	    				<span class="fecha">{{ date('D')." ".date('d')." de ".date('F').", " }}
	    					<form name="form_reloj" style="display: inline;"> 
								<input type="text" name="reloj" size="10" onfocus="window.document.form_reloj.reloj.blur()" style="background-color: transparent;border: none;color: white"> 
							</form>  				
						</span>
	    				<a class="btn" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</header>
	<div class="container">
		<div class="row btn-crear">
			<div class="col-lg-2 col-md-3 col-4 btn-c">
				<button class="btn container-fluid btn-ct">CREAR TICKET</button>
			</div>
			<div class="col-lg-2 col-md-3 col-4">
				<button class="btn container-fluid btn-cr">CREAR REUNION</button>
			</div>
		</div>
		<div class="row botones">
				<div class="col-lg-3 col-3 col-md-3">
					<button class="btn container btn-pendiente">Pendientes</button>
				</div>
				<div class="col-lg-3 col-3 col-md-3">
					<button class="btn container btn-proceso">
						En Proceso
					</button>
				</div>
				<div class="col-lg-3 col-3 col-md-3">
					<button class="btn container btn-culminado">
						Culminados
					</button>
				</div>
				<div class="col-lg-3 col-3 col-md-3">
					<button class="btn container btn-reunion">
						Reuniones
					</button>
				</div>
				</div>
				<div class="row estados">
					<div class="col-lg-6 col-6 text-right">
							<a class="estado-a" href="#"><span class="num-atra">1</span>Atrasado</a>
							<a href="#" class="estado-a"><span class="num-term">32</span>Terminados</a>
						</div>
					<div class="col-lg-6 col-6">
							<a href="#" class="estado-a"><span class="num-apro">23</span>Aprobados</label>
							<a href="#" class="estado-a"><span class="num-pend">15</span>Pendientes</a>
					</div>
				</div>
			<div class="row">
			<div class="col-lg-12">
				<table class="table table-bordered table-responsive-lg mitabla">
					<thead>
						<tr>
							<th scope="col">F.Inicio</th>
							<th scope="col">F.Final</th>
							<th scope="col">Cliente</th>
							<th scope="col">Tarea</th>
							<th scope="col">Responsable</th>
							<th scope="col">Colaborador</th>
							<th scope="col">Estado</th>
							<th scope="col">Acciones</th>	
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope="row">09/07 12:00</td>
							<td scope="row">10/07 14:00</td>
							<td scope="row">Andes</td>
							<td scope="row">Mailing semanal</td>
							<td scope="row">Fiorella</td>
							<td scope="row">Any</td>
							<td>
							<span class="tck-estado tck-proceso">En proceso</span>
							</td>
							                              <td>
								<div class="dropdown">
								  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								  	 <a href="#" class="dropdown-item editar" data-toggle="modal"><i class="fa fa-pencil"></i>  Editar</a>
									<a href="#" class="dropdown-item pausar"><i class="fa fa-pause"></i>  Pausar</a>	
									<a href="#" class="dropdown-item validarc" data-toggle="modal" data-target="#observacion_tiket"><i class="fa fa-check"></i>  Culminar</a>
									 
								  </div>
								</div>
                              </td>
						</tr>
						<tr>
							<td scope="row">09/07 12:00</td>
							<td scope="row">10/07 14:00</td>
							<td scope="row">Andes</td>
							<td scope="row">Mailing semanal</td>
							<td scope="row">Fiorella</td>
							<td scope="row">Any</td>
							<td>
							<span class="tck-estado tck-atrazado">Atrasado</span>
							</td>
							                              <td>
								<div class="dropdown">
								  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								  	 <a href="#" class="dropdown-item editar" data-toggle="modal"><i class="fa fa-pencil"></i>  Editar</a>
									<a href="#" class="dropdown-item pausar" data_target="#observacion_ticket"><i class="fa fa-pause"></i>  Pausar</a>	
									<a href="#" class="dropdown-item validarc" data-toggle="modal" data-target="#observacion_tiket"><i class="fa fa-check"></i>  Culminar</a>
									 
								  </div>
								</div>
                              </td>
						</tr>
						<tr>
							<td scope="row">09/07 12:00</td>
							<td scope="row">10/07 14:00</td>
							<td scope="row">Andes</td>
							<td scope="row">Mailing semanal</td>
							<td scope="row">Fiorella</td>
							<td scope="row">Any</td>
							<td>
							<span class="tck-estado tck-terminado">Terminado</span>
							</td>
							                              <td>
								<div class="dropdown">
								  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								  	 <a href="#" class="dropdown-item editar" data-toggle="modal"><i class="fa fa-pencil"></i>  Editar</a>
									<a href="#" class="dropdown-item pausar"><i class="fa fa-pause"></i>  Pausar</a>	
									<a href="#" class="dropdown-item validarc" data-toggle="modal" data-target="#observacion_tiket"><i class="fa fa-check"></i>  Culminar</a>
									 
								  </div>
								</div>
                              </td>
						</tr>
						<tr>
							<td scope="row">09/07 12:00</td>
							<td scope="row">10/07 14:00</td>
							<td scope="row">Andes</td>
							<td scope="row">Mailing semanal</td>
							<td scope="row">Fiorella</td>
							<td scope="row">Any</td>
							<td>
							<span class="tck-estado tck-aprobado">Aprobado</span>
							</td>
							                              <td>
								<div class="dropdown">
								  <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-cog"></i>
								  </button>
								  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								  	 <a href="#" class="dropdown-item editar" data-toggle="modal"><i class="fa fa-pencil"></i>  Editar</a>
									<a href="#" class="dropdown-item pausar"><i class="fa fa-pause"></i>  Pausar</a>	
									<a href="#" class="dropdown-item validarc" data-toggle="modal" data-target="#observacion_tiket"><i class="fa fa-check"></i>  Culminar</a>
									 
								  </div>
								</div>
                              </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>		
	</div>
  <section id="footer">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <p>Copyright 2018 - All rights reserved || Versión 1.3</p>
          </div>
          <div class="col-md-4 text-right">
             powered by <a href="https://www.mediaimpact.pe/" target="_blank">Media Impact</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  <script language="JavaScript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });
  function mueveReloj(){ 
      momentoActual = new Date() 
      hora = momentoActual.getHours() 
      minuto = momentoActual.getMinutes() 
      segundo = momentoActual.getSeconds() 

      str_segundo = new String (segundo) 
      if (str_segundo.length == 1) 
          segundo = "0" + segundo 

      str_minuto = new String (minuto) 
      if (str_minuto.length == 1) 
          minuto = "0" + minuto 

      str_hora = new String (hora) 
      if (str_hora.length == 1) 
          hora = "0" + hora 

      horaImprimible = hora + " : " + minuto + " : " + segundo 

      document.form_reloj.reloj.value = horaImprimible 

      setTimeout("mueveReloj()",1000) 
  } 
  </script> 
</body>
</html>
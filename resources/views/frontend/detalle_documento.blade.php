@extends('layout.app')
@section('titulo','Detalle Documento')

@section('css')
	<link rel="stylesheet" href="{{ asset('css/detalle_tiket.css') }}">
@endsection
@section('content')
 
<div class="col main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="detalle_tiket">
				<p><b>Nombre de Reunión:</b> 
					@if($documento->estado == "V")
						Vacaciones
					@else
						Justicicación
					@endif
				</p>
				<p><b>Fecha:</b> {{$documento->fecha}}</p>
				<p><b>Nombre:</b>
					@foreach($users as $uu)
                    @if($uu->id == $documento->user_id)
                      {{ $uu->name }}                            
                    @endif
                  @endforeach
				</p>
				<p><b>Descripción de documento:</b> {!! $documento->descripcion !!}</p>
	
				<p class="text-center">
					<a href="javascript:history.back(1)" class="btn btn-primary">Volver</a>
				</p>
			</div>
		</div>
	</div>
</div>
@endsection

@section('content_extras')
@endsection

@section('js')
	<script>
	    $('#m6').addClass('active');
	</script>
@endsection
@extends('layout.app')
@section('titulo','Detalle')
@section('css')

    <link href="https://file.myfontastic.com/QuWMctCNAye4e7wpQ3gpKU/icons.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote-bs4.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/tiket.css') }}">
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
    <script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=374b9rm3q420nrmdtfwnqf3miwqwvrto3p2dgt51spdlozup"></script>



    <style>


    </style>
@endsection
@section('content')

    <div class="row ">
        <div class="col-10 text-center">
            <h1 class="main-title "> Minutos Extras  por día  </h1>
            <h3>{{$user->name}}  -  {{$month}}/{{$year}}</h3>
        </div>
        <div class="col-md-2 col-12">
            <a class="btn btn-primary btn-custom"  href="{{route('horas_extras')}}">Volver</a>
        </div>
    </div>

    <div class="row  mt-4">
       <div class="col-12">
           <table class="table table-bordered table-responsive-lg mitabla custom-table">
               <thead class="">
               <tr>
                   <th>
                       Fecha
                   </th>

                   <th>
                       hora de inicio
                   </th>
                   <th>
                       Hora fin
                   </th>
                   <th>
                       Minutos extras
                   </th>
                   <th>
                       Minutos deuda
                   </th>
               </tr>

               </thead>
               <tbody>
               @foreach($query as $item)

                   <tr>
                       <td>
                               {{ $item->fecha }}
                       <td>
                           @if ($item->hora_inicio != null)
                               {{ $item->hora_inicio }}
                           @else
                               No marcó entrada
                           @endif
                       </td>
                       <td>
                           @if ($item->hora_fin != null  )
                               {{ $item->hora_fin }}
                           @else
                               No marcó salida
                           @endif
                       </td>
                       <td>
                       @if ($item->extras != null  )
                               {{ $item->extras }}
                           @else
                              0
                           @endif
                       </td>
                       <td>
                           @if ($item->deuda != null  )
                               {{ $item->deuda }}
                           @else
                               0
                           @endif
                       </td>
                   </tr>

               @endforeach
               </tbody>

           </table>
       </div>
    </div>

@endsection
@section('content_extras')

@endsection

@section('js')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <script>

    </script>
@endsection
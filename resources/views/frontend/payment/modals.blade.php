{{-- Buscar Proyectos --}}
<div class="modal fade" id="modalProyecto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Proyectos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body center">
                <table class="table table-striped table-bordered mitabla custom-table" id="dataTable" >
                    <thead>
                    <tr>
                        <th>Opciones</th>
                        <th>Proyecto</th>
                        <th>Estado</th>
                        <th>Cliente</th>

                    </tr>
                    </thead>
                    <tbody>
                        @forelse($projects as $project)
                            <tr>
                                <td>
                                    <button class="btn btn-primary icon-btn"
                                            onclick="addProject('{{ $project->id}}','{{ $project->name}}','{{ $project->id_cotizacion}}')">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </td>
                                <td>
                                    {{$project->name}}
                                </td>
                                <td>
                                    {{$project->estado}}
                                </td>
                                <td>
                                    {{$project->cliente}}
                                </td>
                            </tr>
                        @empty
                            No hay datos
                        @endforelse


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-12">
        <form action="" id="myForm" class="col-12" enctype="multipart/form-data">
            {{ csrf_field()}}
<div class="form-row">

    <div class="col-md-12 mb-1">

        <button type="button"  class="icon-btn btn btn-info btn-custom "
                data-toggle="modal"   data-target="#modalProyecto"
                id="btn-modal" >
            <i class="icon-eye" ></i>
            Buscar Proyecto
        </button>
    </div>

    <div class="col-4">
        <label for="" >Proyecto</label>
        @if (isset($addProject))
            <input type="text" readonly id="proyecto" class="form-control" value="{{$addProject->name}}">

            <input type="hidden" id="id_proyecto" name="id_proyecto"  value="{{$addProject->id}}" >
        @else
            <input type="text" readonly id="proyecto" class="form-control" value="@if(isset($id)) {{$project->name}} @endif">

            <input type="hidden" id="id_proyecto" name="id_proyecto"  value="@if(isset($id)) {{$project->id}} @endif" >
        @endif

        <input type="hidden" id="id_payment" name="id_payment"  value="@if(isset($id)) {{$payment->id}} @endif" >
    </div>
    <div class="col-4">
        <label for="">Tipo: </label>
        <select name="type" id="type" class="form-control">
            <option value="0">-- seleccione --</option>
            <option value="1"   @if(isset($id) && $payment->type == 1) {{"selected"}} @endif > Pago total del Proyecto </option>
            <option value="2" @if(isset($id) && $payment->type == 2) {{"selected"}} @endif > Servicios adicionales</option>
        </select>
    </div>
    <div class="col-4">
        <label for="">Tipo de comprobante: </label>
        <select name="voucher" id="voucher" class="form-control">
            <option value="0">-- seleccione --</option>
            <option value="1" @if(isset($id) && $payment->voucher == 1) {{"selected"}} @endif> Factura </option>
            <option value="2" @if(isset($id) && $payment->voucher == 2) {{"selected"}} @endif> Boleta</option>

            <option value="4" @if(isset($id) && $payment->voucher == 4) {{"selected"}} @endif> Otros</option>
        </select>
    </div>
    <div class="col-4 mt-3">
        <label for="">Monto :</label>
        <input type="text" class="form-control" id="monto" name="monto" value="@if(isset($id) ) {{$payment->total}} @endif">
    </div>
    <div class="col-4 mt-3" style="@if(isset($id) && $payment->type == 2){{"display: block;"}}@else {{"display: none;"}}@endif" id="content_gastos_extras">
        <label for="">Seleccione los gastos extras:  </label>

        <select name="type_payment[]" id="type_payment" multiple    class="form-control">
            <option value="0">-- seleccione --</option>
            <option value="1" @if(isset($id) && $payment->type == 2 && in_array(1,$array_options)) {{ "selected"    }} @endif > Taxi </option>
            <option value="2" @if(isset($id) && $payment->type == 2 && in_array(2,$array_options)) {{ "selected"    }} @endif> Compra de fotos</option>
            <option value="3" @if(isset($id) && $payment->type == 2 && in_array(3,$array_options)) {{ "selected"    }} @endif> Materiales</option>
            <option value="4" @if(isset($id) && $payment->type == 2 && in_array(4,$array_options)) {{ "selected"    }} @endif> Otros</option>
        </select>
    </div>
    <div class="col-4 mt-3" style="@if(isset($id) && $payment->type == 2){{"display: block;"}}@else {{"display: none;"}}@endif" id="content_description">
        <label for="">Descripción: </label>
        <textarea name="descripcion" id="" cols="30" class="form-control" rows="4">@if(isset($id)){{$payment->description}} @endif</textarea>
    </div>
    <div class="col-12 mt-4"></div>
    <div class="col-6 mt-4">
        <label for="" >Cotización:</label>

        <input type="file"   id="oc" name="oc" style="opacity: 0" class="form-control">
        <div class="row">
            <div class="col-12 mt-3 d-flex justify-content-center " id="">
                <a href="@if(isset($addProject))
                            {{"/funnel/reporte_cotizacion/".$addProject->id_cotizacion}}
                         @elseif(isset($id))
                            {{"/funnel/reporte_cotizacion/".$project->id_cotizacion}}
                         @else
                            {{"#"}}
                         @endif" style="font-size: 5em;color:black" id="uri_oc"  target="_blank">
                    <i class="fas fa-file-alt" ></i>
                </a>
            </div>

        </div>
    </div>
    <div class="col-6 mt-4">
        <label for="" id="title_oc">Seleccione OC : </label>
        <input type="file"   id="oc" name="oc"  onchange="readFile(event,'show_oc')" class="form-control">
        <div class="row ">
            <div class="col-12 mt-3 d-flex justify-content-center " id="show_oc">

              @if(isset($id))

                    @if($payment->oc != '' || $payment->oc != null)

                        @if ($oc == 1)
                            <div class="text-center">
                                <img src="{{asset('uploads/'.$payment->oc)}}" alt="" class="img-thumbnail" style="height: 300px;width: 300px">
                            </div>
                        @elseif($oc == 0)

                            <a href="{{asset('uploads/'.$payment->oc)}}"  target="_blank" style="font-size: 5em;color:black" >
                                <i class="fas fa-file-alt" ></i>
                            </a>

                        @endif
                    @endif
              @endif
            </div>
        </div>
    </div>
    <div class="col-6 mt-4">
        <label for="" id="title_factura">Seleccione Factura: </label>
        <input type="file"   id="factura" name="factura"  class="form-control" onchange="readFile(event,'show_factura')">
        <div class="row ">
            <div class="col-12 mt-3 d-flex justify-content-center " id="show_factura">
            @if(isset($id))
                    @if($payment->factura != '' || $payment->factura != null)

                        @if ($f == 1)
                            <div class="text-center">
                                <img src="{{asset('uploads/'.$payment->factura)}}" alt="" class="img-thumbnail" style="height: 300px;width: 300px">
                            </div>
                        @elseif($f == 0)

                            <a href="{{asset('uploads/'.$payment->factura)}}" target="_blank" style="font-size: 5em;color:black" >
                                <i class="fas fa-file-alt" ></i>
                            </a>

                        @endif
                    @endif
            @endif
            </div>
        </div>
    </div>
    <div class="col-6 mt-4">
        <label for="" id="title_codigo_entrada"  data-toggle="tooltip" data-placement="top" title="MIGO HES" >Seleccione Codigo de entrada: </label>
        <input type="file"   id="codigo_entrada" name="codigo_entrada"  onchange="readFile(event,'show_codigo_entrada')"  class="form-control">
        <div class="row ">
            <div class="col-12 mt-3 d-flex justify-content-center " id="show_codigo_entrada">
               @if(isset($id))
                    @if($payment->codigo_entrada != '' || $payment->codigo_entrada != null)

                        @if ($ce == 1)
                            <div class="text-center">
                                <img src="{{asset('uploads/'.$payment->codigo_entrada)}}" alt="" class="img-thumbnail" style="height: 300px;width: 300px">
                            </div>
                        @elseif($ce == 0)

                            <a href="{{asset('uploads/'.$payment->codigo_entrada)}}" target="_blank" style="font-size: 5em;color:black" >
                                <i class="fas fa-file-alt" ></i>
                            </a>

                        @endif
                    @endif
                @endif
            </div>
        </div>
    </div>
    <div class="row mt-4 text-center"  style="display: flex;" id="content_images" >

    </div>

    <div class="col-md-12 text-center">
        <hr>
            @if(isset($id))
                <button  type="submit" class="btn btn-primary btn-custom">Actualizar Pago</button>
            @else
                <button  type="submit" class="btn btn-primary btn-custom">Guardar Pago</button>
            @endif


        <a href="{{route('payment.index')}}" class="btn  btn-primary ml-5" >Cancelar</a>
    </div>

</div>
</form>

</div>
</div>

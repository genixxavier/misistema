@extends('layout.app')
@section('titulo','Facturación')
@section('css')

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    {{-- <link rel="stylesheet" href="{{ asset('css/funnel.css')}}"> --}}
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/default.min.css"/>
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/semantic.min.css"/>
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/themes/bootstrap.min.css"/>
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
    <style>
        .btn-crear:hover {
            background-color: #f4511e;
        }
        .btn-crear{
            background: #263238;
            display: inline-block;
            padding: 7px 15px;
            cursor: pointer;
            font-size: 13px;
            line-height: 13px;
            text-decoration: none;
            border-radius: 5px;
            color: #fff;
            margin-left: 5px;
            margin-right: 5px;
            margin-bottom: 20px;
        }
        .btn {
            cursor: unset;
        }
        .mitabla tr th {
            text-align: center;
        }
    </style>
@endsection
@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-12 mt-3"><h1 class="text-center main-title"> Facturación </h1></div>
        </div>
        <div class="row text-center">
            <div class="col-6 mt-3">

                <a href="{{route('payment.create')}}" class="btn-crear btn-custom">Crear Factura </a>
            </div>
            <div class="col-6 mt-3 ">

                <a href="{{ route('project.index')}}" class="btn-crear btn-custom">Volver a proyectos </a>
            </div>

        </div>
        <div class="row mt-2 text-center">
            
                <form class="col-12 col-md-6 form-inline " id="myForm">
                        <select class="form-control" id="tribu" name="tribu">
                                <option value="0"> Todos </option>
                                @foreach ($tribus as $tribu)
                                  <option value="{{$tribu->Id}}">{{$tribu->nombre}}</option>
                                @endforeach
                        </select>
                        <button class="btn btn-primary btn-custom ml-sm-2" type="submit">Filtrar</button>
                </form>
            
        </div>
        <div class="row mt-2">

            <div class="col-md-12">
                <table class="table table-bordered table-responsive-lg mitabla custom-table">
                    <thead class="">
                    <tr>
                        <th>
                            Proyecto
                        </th>
                        <th>
                            Tribu
                        </th>
                        <th>
                            Tipo
                        </th>

                        <th>

                            Comprobante
                        </th>

                          <th>
                              OC | <i class="fas fa-file"></i>
                          </th>
                        <th>
                            Factura | <i class="fas fa-file"></i>
                        </th>
                        <th>
                            Codigo Entrada | <i class="fas fa-file"></i>
                        </th>
                        <th>
                            Monto
                        </th>
                        <th>
                            Opciones
                        </th>
                    </tr>

                    </thead>
                    <tbody>
                        @foreach($payment as $pay)
                            @if($filterTribu != 0 && $pay->tribu_id != $filterTribu)
                                @continue
                            @endif
                            <tr>
                                <td> {{$pay->name}}</td>
                                <td> {{$pay->tribu}}</td>
                                <td> {{ $pay->pagos }}  </td>
                                <td> {{$pay->tipo_comprobante}} </td>
                                <td class="text-center">
                                   @if ( $pay->oc  == "Si")
                                        <i class="fas fa-check"></i>
                                   @else
                                        <i class="fas fa-times"></i>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if ( $pay->factura  == "Si")
                                        <i class="fas fa-check"></i>
                                    @else
                                        <i class="fas fa-times"></i>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @if ( $pay->codigo  == "Si")
                                        <i class="fas fa-check"></i>
                                    @else
                                        <i class="fas fa-times"></i>
                                    @endif
                                </td>
                                <td> S/ {{number_format($pay->precio,2,'.',',')}}</td>
                                <td>
                                    <div class="dropdown">
                                        <button class="btn  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog"></i>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item"   href="{{route('payment.edit', $pay->id) }}"><i class="fas fa-edit"></i> Editar</a>
                                        </div>
                                    </div>



                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $payment->render() !!}

            </div>

        </div>
    </div>
@endsection
@section('content_extras')







@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
    
    const myForm = document.getElementById('myForm')
    myForm.addEventListener('submit', e => {
        e.preventDefault();
        if (e.target.tribu.value == 0 ) {
            location.href = '/funnel/projects/payment'
        } 
        else {
            location.href = '/funnel/projects/payment?tribu=' + e.target.tribu.value
        }
    })
    
    </script>


@endsection
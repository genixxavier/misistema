<!doctype html>
<html lang="es">
  <head>
    <title>Login</title>
    <!-- Required meta tags -->

    {{-- <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favi-ticket/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favi-ticket//apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favi-ticket//apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favi-ticket//apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favi-ticket//apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favi-ticket//apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favi-ticket//apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favi-ticket//apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favi-ticket//apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('favi-ticket//android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favi-ticket//favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favi-ticket//favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favi-ticket//favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('favi-ticket//manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff"> --}}
      <link rel="apple-touch-icon" sizes="57x57" href="{{asset('favi/apple-icon-57x57.png')}}">
      <link rel="apple-touch-icon" sizes="60x60" href="{{asset('favi/apple-icon-60x60.png')}}">
      <link rel="apple-touch-icon" sizes="72x72" href="{{asset('favi/apple-icon-72x72.png')}}">
      <link rel="apple-touch-icon" sizes="76x76" href="{{asset('favi/apple-icon-76x76.png')}}">
      <link rel="apple-touch-icon" sizes="114x114" href="{{asset('favi/apple-icon-114x114.png')}}">
      <link rel="apple-touch-icon" sizes="120x120" href="{{asset('favi/apple-icon-120x120.png')}}">
      <link rel="apple-touch-icon" sizes="144x144" href="{{asset('favi/apple-icon-144x144.png')}}">
      <link rel="apple-touch-icon" sizes="152x152" href="{{asset('favi/apple-icon-152x152.png')}}">
      <link rel="apple-touch-icon" sizes="180x180" href="{{asset('favi/apple-icon-180x180.png')}}">
      <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('favi/android-icon-192x192.png')}}">
      <link rel="icon" type="image/png" sizes="32x32" href="{{asset('favi/favicon-32x32.png')}}">
      <link rel="icon" type="image/png" sizes="96x96" href="{{asset('favi/favicon-96x96.png')}}">
      <link rel="icon" type="image/png" sizes="16x16" href="{{asset('favi/favicon-16x16.png')}}">
      <link rel="manifest" href="{{asset('favi/manifest.json')}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        html,body{
            height: 100%;
        }
        .site-wrapper {
            display: table;
            width: 100%;
            height: 100%;
            min-height: 100%;
            box-shadow: inset 0 0 5rem rgba(0,0,0,.5);
        }
        .site-wrapper-inner {
            display: table-cell;
            vertical-align: middle;
        }
        .body {
            background-color: #120F23 !important;
        }
    </style>
  </head>
  <body class="body">

    <div class="site-wrapper">

        <div class="site-wrapper-inner">

            <div class="container-fluid">
                <div class="row">
                    <div class="col"></div>
                    <div class="col-md-6 text-center">
                        <h1><img src="{{asset('img/GIF-MI-WEB_.gif')}}" alt="" style=""></h1>
                        <p style="color:white">Iniciar sesión con google +</p>
                        <p><a href="{{asset('login/google')}}" class="btn btn-danger btn-google"><i class="fa fa-google-plus"></i> Google +</a></p>
                    </div>
                    <div class="col">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @if (session('status'))
                            <div class="alert alert-danger" style="width: 270px;display: block;margin: 0 auto;text-align: center;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>
{{-- @extends('layout.app')
@section('titulo','Historial')

@section('css')

@section('content')
<div class="col main-content">
    <div class="row menu-top">
        <div class="col align-middle"><span class="bg-success"></span>Pedidos culminados</div>
        <div class="col align-middle"><span class="bg-info"></span>Pedidos en curso</div>
        <div class="col align-middle"><span class="bg-danger"></span>Pedidos atrazados</div>
        <div class="col align-middle"><span class="bg-light"></span>Pedidos sin asignar</div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-responsive-lg mitabla">
                <thead>
                    <tr>
                      <th scope="col">Fecha inicios</th>
                      <th scope="col">Fecha fin</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Colaborador</th>
                      <th scope="col">Cliente</th>
                      <th scope="col"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td scope="row">-</td>
                      <td>-</td>
                      <td>Subir reclamos</td>
                      <td>-</td>
                      <td>Norvial</td>
                      <td><a href="/detalle_tiket" class="btn btn-outline-dark">Ver</a> <a href="#" class="btn btn-primary oculto">asignar</a></td>
                    </tr>
                    <tr class="bg-info">
                      <td scope="row">15/11/17 11:00</td>
                      <td>15/11/17 14:00</td>
                      <td>Nueva función</td>
                      <td>Russel</td>
                      <td>Toyota</td>
                      <td><a href="/detalle_tiket" class="btn btn-light">Ver</a> <a href="#" class="btn btn-primary oculto">asignar</a></td>
                    </tr>
                    <tr class="bg-success">
                      <td scope="row">15/11/17 11:00</td>
                      <td>15/11/17 11:00</td>
                      <td>Modulo 2</td>
                      <td>Genix</td>
                      <td>Packapp</td>
                      <td><a href="/detalle_tiket" class="btn btn-light">Ver</a> <a href="#" class="btn btn-light" data-toggle="modal" data-target="#validar_tiket">Validar</a> <a href="#" class="btn btn-primary oculto">asignar</a></td>
                    </tr>
                    <tr class="bg-danger">
                      <td scope="row">15/11/17 11:00</td>
                      <td>15/11/17 11:00</td>
                      <td>Footer</td>
                      <td>Joan</td>
                      <td>VDO</td>
                      <td><a href="/detalle_tiket" class="btn btn-light">Ver</a> <a href="#" class="btn btn-primary oculto">asignar</a></td>
                    </tr>
                  </tbody>
            </table>
            <div>
                <nav>
                  <ul class="pagination pagination-sm justify-content-center">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1"><<</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">>></a>
                    </li>
                  </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content_extras')
    <!-- Modal -->
    <div class="modal fade" id="validar_tiket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            <h4>¿Este si cumplio con los requisitos?</h4>
            <button type="button" class="btn btn-primary">Si</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No estoy seguro</button>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('js')
@endsection --}}
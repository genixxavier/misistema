<html>
<head></head>
<body>
<h2>Ticket - MediaImpact</h2>
<p><b>Ticket Culminado: </b> {{ $ticket}}</p>
<p><b>Cliente: </b> {{ $cliente}}</p>
<p><b>Área:</b> {{$area}}</p>
<p><b>Colaborador:</b> {{$colaborador}}</p>
<p><b>Responsable:</b> {{$jefe}} </p>
<p><b>Fecha:</b> {{$fecha}} </p>
<p><b>Url:</b> <a href="{{$url}}" target="_blank">{{$url}}</a> </p>
</body>
</html>